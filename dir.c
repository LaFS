
/*
 * fs/lafs/dir.c
 * Copyright (C) 2005-2009
 * Neil Brown <neilb@suse.de>
 * Released under the GPL, version 2
 *
 * Directory operations
 */

#include	"lafs.h"
#include	<linux/slab.h>

#define S_SHIFT 12

static unsigned char file_type[1 + (S_IFMT >> S_SHIFT)] = {
	[S_IFREG >> S_SHIFT]	= DT_REG,
	[S_IFDIR >> S_SHIFT]	= DT_DIR,
	[S_IFCHR >> S_SHIFT]	= DT_CHR,
	[S_IFBLK >> S_SHIFT]	= DT_BLK,
	[S_IFIFO >> S_SHIFT]	= DT_FIFO,
	[S_IFSOCK >> S_SHIFT]	= DT_SOCK,
	[S_IFLNK >> S_SHIFT]	= DT_LNK,
};

static inline int mode_to_dt(int mode)
{
	return file_type[(mode & S_IFMT) >> S_SHIFT];
}

/*****************************************************************************
 * Directory lookup
 * Most of the work is done in dir_lookup_blk which returns the block
 * that should hold the entry.
 * dir_lookup takes that result and finds an inode number if possible.
 */
static struct datablock *
dir_lookup_blk(struct inode *dir, const char *name, int nlen,
	       u8 *pp, int forcreate, u32 *hashp, REFARG)
{
	struct lafs_inode *lai = LAFSI(dir);
	u32 seed = lai->md.file.seed;
	struct datablock *b, *cb = NULL;
	loff_t bn;
	u32 hash;
	int err;
	int bits = dir->i_blkbits-8;
	if (nlen == 0)
		nlen = strlen(name);

	hash = lafs_hash_name(seed, nlen, name);
	dprintk("name= %s hash=%lu\n", name, (unsigned long)bn);

	while (1) {
		char *buf;
		bn = hash+1;
		dprintk("bn=%lu\n", (unsigned long)bn);
		if (lafs_find_next(dir, &bn) == 0)
			bn = 0;
		dprintk("now=%lu\n", (unsigned long)bn);

		err = -ENOMEM;
		b = lafs_get_block(dir, bn, NULL, GFP_KERNEL, REF);
		if (!b)
			break;
		err = lafs_read_block(b);
		if (err)
			break;
		buf = map_dblock(b);
		if (lafs_trace) {
			void lafs_dir_print(char *buf, int psz);
			lafs_dir_print(buf, bits);
		}
		for ( ; hash < (bn ? bn : MaxDirHash) ; hash++) {
			u8 piece;
			struct dir_ent de;
			int rv = lafs_dir_find(buf, bits, seed, hash, &piece);

			if (rv == 0) {
				unmap_dblock(b, buf);
				if (forcreate) {
					if (cb) {
						putdref(b, REF);
						b = cb;
					} else
						*hashp = hash;
					return b;
				}
				putdref(b, REF);
				return ERR_PTR(-ENOENT);
			}
			lafs_dir_extract(buf, bits, &de, piece, NULL);
			if (!de.target) {
				if (forcreate && !cb) {
					cb = getdref(b, REF);
					*hashp = hash;
				}
				continue;
			}
			if (de.nlen != nlen)
				continue;
			if (strncmp(de.name, name, de.nlen) != 0)
				continue;
			unmap_dblock(b, buf);
			if (forcreate) {
				putdref(b, REF);
				if (cb)
					putdref(cb, REF);
				return ERR_PTR(-EEXIST);
			}
			*pp = piece;
			if (hashp)
				*hashp = hash;
			return b;
		}
		unmap_dblock(b, buf);
		putdref(b, REF);
	}

	putdref(b, REF);
	putdref(cb, REF);
	return ERR_PTR(err);
}

static int
dir_lookup(struct inode *dir, const char *name, int nlen,
	   u32 *inum)
{
	/*
	 * return
	 *    -ve on error
	 *     0 on not found
	 *  type on found
	 */
	struct datablock *b;
	char *buf;
	struct dir_ent de;
	int bits = dir->i_blkbits-8;
	u8 piece;

	b = dir_lookup_blk(dir, name, nlen, &piece, 0, NULL, MKREF(dir_lookup));
	if (IS_ERR(b))
		return PTR_ERR(b);
	buf = map_dblock(b);
	*inum = lafs_dir_extract(buf, bits, &de, piece, NULL)->target;
	unmap_dblock(b, buf);
	putdref(b, MKREF(dir_lookup));
	return 1;
}

/*****************************************************************************
 *
 * Directory modification routines.
 * We have sets of prepare, pin, commit, abort.
 * 'prepare' happens outside of a phase-lock and can allocate
 * and reserve blocks, and is free to fail.  Everything it does
 * should be revertable - or atomically committed.
 * 'pin' happens inside a phase-lock and should pin any blocks
 * that might need to be dirtied.  It only fails if the pin fails.
 * 'commit' is called inside the phase-lock if prepare and pin succeed.
 * is must clean up anything that was set up by 'prepare'.
 * 'abort' is called if anything fails.  It too should clean up anything
 * that prepare sets up.
 * If 'prepare' is called, then either 'commit' or 'abort' will be called
 * so failure in prepare can leave cleanup to 'abort'.
 *
 * The whole processes uses a dirop_handle to store various aspects
 * of state that might need to be unwound or committed.
 * A compound operation such as rename may included several
 * simple operations such as delete + create.  In that case there
 * will be a separate dirop_handle for each simple operation.
 * There will only be one create
 */
struct dirop_handle {
	struct datablock *dirent_block;
	struct datablock *new;
	char	*temp;
	u32	hash;
	u8	index;
	int	chainoffset;
};

/*............................................................................
 * Creating an entry in a directory.
 * This is split into pre_create and commit_create
 * We already know that the name doesn't exist so a lookup will fail,
 * but will find the right place in the tree.
 * pre_create allocates blocks as needed and stores info in the dirop_handle.
 * commit_create finalises the create and cannot fail.
 */

static int dir_create_prepare(struct fs *fs, struct inode *dir,
			      const char *name, int nlen,
			      u32 inum, int type,
			      struct dirop_handle *doh)
{
	/*
	 * We need one or 2 blocks
	 * - a dirent block with space to receive the new entry.
	 * - A free block into which the block index may split
	 * These will be pinned and allocated credits before we commit.
	 *
	 * For a single-block which still has room,
	 * we only need the first of those.
	 *
	 */
	int blocksize = fs->blocksize;
	struct datablock *dirblk;
	char *buf, *n1, *n2;
	int bits = dir->i_blkbits-8;
	u8 piece;
	struct lafs_inode *lai = LAFSI(dir);
	u32 seed = lai->md.file.seed;
	struct dirheader *dh;
	int chainoffset;
	u32 hash, newhash;
	int rv;

	doh->temp = NULL;
	doh->new = NULL;

	doh->dirent_block =
		dirblk = dir_lookup_blk(dir, name, nlen, &piece, 1, &hash,
					MKREF(dir_blk));

	if (IS_ERR(dirblk))
		return PTR_ERR(dirblk);

	lafs_iolock_written(&dirblk->b);
	set_bit(B_PinPending, &dirblk->b.flags);
	/* i_mutex protect us now, so don't need to maintain the lock */
	lafs_iounlock_block(&dirblk->b);

	chainoffset = hash - lafs_hash_name(seed, nlen, name);
	buf = map_dblock(dirblk);
	rv = lafs_dir_add_ent(buf, bits, name, nlen, 0, DT_TEST,
			      seed, hash, chainoffset);
	unmap_dblock(dirblk, buf);
	if (rv < 0)
		return -EEXIST;
	if (rv == 1) {
		doh->hash = hash;
		doh->chainoffset = chainoffset;
		return 0;
	}
	/* Doesn't fit, try repacking */

	buf = map_dblock(dirblk);
	dh = (struct dirheader *)buf;
	if (dh->freepieces >= space_needed(nlen, chainoffset, bits)) {
		char *tmp;
		unmap_dblock(dirblk, buf);
		tmp = kmalloc(256<<bits, GFP_KERNEL);
		if (tmp) {
			buf = map_dblock(dirblk);
			lafs_dir_repack(buf, bits, tmp, seed, 0);
			if (lafs_dir_add_ent(tmp, bits, name, nlen, 0, DT_TEST,
					     seed, hash, chainoffset)) {
				memcpy(buf, tmp, blocksize);
				unmap_dblock(dirblk, buf);
				kfree(tmp);
				doh->hash = hash;
				doh->chainoffset = chainoffset;
				return 0;
			}
			kfree(tmp);
		} else {
			unmap_dblock(dirblk, buf);
			return -ENOMEM;
		}
	}
	unmap_dblock(dirblk, buf);

	/* Really doesn't fit, need to split.
	 * We have to perform the split now so that we can choose a new
	 * index and pin that block.
	 */
	n1 = kmalloc(blocksize, GFP_KERNEL);
	n2 = kmalloc(blocksize, GFP_KERNEL);
	if (!n1 || !n2) {
		kfree(n1); kfree(n2);
		return -ENOMEM;
	}
	doh->new = lafs_get_block(dir, newhash+1, NULL, GFP_KERNEL,
				  MKREF(dir_new));
	if (doh->new == NULL) {
		kfree(n1); kfree(n2);
		return -ENOMEM;
	}
	buf = map_dblock(dirblk);
	lafs_dir_split(buf, bits, n1, n2, name, inum, type,
		       &newhash, seed, hash, chainoffset);
	unmap_dblock(dirblk, buf);
	buf = map_dblock(doh->new);
	memcpy(buf, n1, blocksize);
	unmap_dblock(doh->new, buf);
	set_bit(B_Valid, &doh->new->b.flags);
	set_bit(B_PinPending, &doh->new->b.flags);
	kfree(n1);
	doh->temp = n2;
	return 0;
}

static void
dir_create_commit(struct dirop_handle *doh,
		  struct fs *fs, struct inode *dir,
		  const char *name, int nlen, u32 target, int type)
{
	/* We are committed to creating this entry.
	 * Everything has been allocated and pinned.
	 * All we do is:
	 * - add name to doh->dirent_block.
	 * - if doh->index == NULL, done.
	 * - possibly update indexslot to have address of doh->dirent_block.
	 * - if doh->new, split doh->index into doh->new adding doh->previndex
	 * - else add doh->previndex into doh->index
	 */
	char *buf;
	int bits = fs->blocksize_bits - 8;
	int blocksize = fs->blocksize;
	struct lafs_inode *lai = LAFSI(dir);
	u32 seed = lai->md.file.seed;

	buf = map_dblock(doh->dirent_block);

	if (doh->new) {
		/* We did a split, and have the block ready to go */
		memcpy(buf, doh->temp, blocksize);
		kfree(doh->temp);
		doh->temp = NULL;
		lafs_dirty_dblock(doh->new);
		if ((((loff_t)doh->new->b.fileaddr+1) << dir->i_blkbits)
		    > dir->i_size) {
			i_size_write(dir, (((loff_t)doh->new->b.fileaddr+1)
					   << dir->i_blkbits));
			lafs_dirty_inode(dir);
		}
		clear_bit(B_PinPending, &doh->new->b.flags);
		putdref(doh->new, MKREF(dir_new));
	} else
		lafs_dir_add_ent(buf, bits, name, nlen, target,
				 type, seed, doh->hash, doh->chainoffset);
	lafs_dirty_dblock(doh->dirent_block);
	if (dir->i_size <= blocksize) {
		/* Make dir fit in inode if possible */
		i_size_write(dir, lafs_dir_blk_size(buf, bits));
		lafs_dirty_inode(dir);
	}
	clear_bit(B_PinPending, &doh->dirent_block->b.flags);
	unmap_dblock(doh->dirent_block, buf);
	putdref(doh->dirent_block, MKREF(dir_blk));
}

static int
dir_create_pin(struct dirop_handle *doh)
{
	int err;
	err = lafs_pin_dblock(doh->dirent_block, NewSpace);
	if (err || doh->new == NULL)
		return err;
	err = lafs_pin_dblock(doh->new, NewSpace);
	return err;
}

static void
dir_create_abort(struct dirop_handle *doh)
{
	kfree(doh->temp);
	if (!IS_ERR(doh->dirent_block) && doh->dirent_block) {
		clear_bit(B_PinPending, &doh->dirent_block->b.flags);
		putdref(doh->dirent_block, MKREF(dir_blk));
	}
	if (!IS_ERR(doh->new) && doh->new) {
		clear_bit(B_PinPending, &doh->new->b.flags);
		putdref(doh->new, MKREF(dir_new));
	}
}

/*---------------------------------------------------------------
 * Delete directory entry.
 * Deleting involves invalidating the entry in the dirent block,
 * and then removing entry deleted entries that are not in a chain.
 * If we cannot be sure, we schedule orphan processing to do
 * the fine details of chain clearing.
 */
static int
dir_delete_prepare(struct fs *fs, struct inode *dir,
	       const char *name, int nlen, struct dirop_handle *doh)
{
	struct datablock *dirblk;
	int orphan = 0;

	doh->dirent_block =
		dirblk = dir_lookup_blk(dir, name, nlen, &doh->index,
					0, &doh->hash, MKREF(dir_blk));
	if (IS_ERR(dirblk) && PTR_ERR(dirblk) == -ENOENT) {
		lafs_trace = 1;
		dirblk = dir_lookup_blk(dir, name, nlen, &doh->index,
					0, &doh->hash, MKREF(dir_blk));
		if (!IS_ERR(dirblk))
			printk("Weird: %s\n", strblk(&dirblk->b));
		lafs_trace = 0;
	}

	if (IS_ERR(dirblk))
		return PTR_ERR(dirblk);
	lafs_iolock_written(&dirblk->b);
	set_bit(B_PinPending, &dirblk->b.flags);
	/* i_mutex protect us now, so don't need to maintain the lock */
	lafs_iounlock_block(&dirblk->b);

	/* Only make this block an orphan if there is a real
	 * possibilitiy.
	 * i.e. one of
	 *    We found the last possible entry
	 *    We found the first entry
	 *    We found the only entry (in which case we found the first)
	 *    First entry is deleted
	 */
	if (((doh->hash+1) & MaxDirHash) == doh->dirent_block->b.fileaddr)
		/* Last possible entry is being remove */
		orphan=1;
	if (!orphan) {
		u32 seed = LAFSI(dir)->md.file.seed;
		u8 firstpiece = 0;
		struct dir_ent de;
		char bits = dir->i_blkbits - 8;
		char *buf = map_dblock(dirblk);
		lafs_dir_find(buf, bits, seed, 0, &firstpiece);
		if (doh->index == firstpiece ||
		    lafs_dir_extract(buf, bits, &de,
				     firstpiece, NULL)->target == 0)
			orphan = 1;
		unmap_dblock(dirblk, buf);
	}
	if (orphan)
		return lafs_make_orphan(fs, doh->dirent_block);
	return 0;
}

static void
dir_delete_commit(struct dirop_handle *doh,
		  struct fs *fs, struct inode *dir,
		  const char *name, int nlen)
{
	char *buf = map_dblock(doh->dirent_block);
	char bits = dir->i_blkbits - 8;
	struct dir_ent de;
	u32 seed = LAFSI(dir)->md.file.seed;
	u8 ignore;

	/* First mark the entry as deleted, then consider removing it*/
	de.target = 0;
	de.type = 0;
	lafs_dir_set_target(buf, bits, &de, doh->index);

	/* If 'hash+1' is not in this block, make me an orphan
	 *   (as we cannot check the chain)
	 * If it is and exists, do nothing (could be in active chain).
	 * If it doesn't exist:
	 *    Remove this entry and any earlier deleted entries in a chain,
	 *    but don't remove the first entry in the block.
	 *    If we end up leaving that first entry, make me an orphan so
	 *    the we can check if the chain continues in a previous block.
	 */
	if (((doh->hash+1) & MaxDirHash) == doh->dirent_block->b.fileaddr)
		unmap_dblock(doh->dirent_block, buf);
	else if (lafs_dir_find(buf, bits, seed, doh->hash+1, &ignore) == 0) {
		/* This is the end of a chain, clean up */
		u8 firstpiece;
		u8 piece;
		u32 hash;

		lafs_dir_find(buf, bits, seed, 0, &firstpiece);
		hash = doh->hash; piece = doh->index;
		do {
			if (piece == firstpiece)
				break;
			lafs_dir_del_ent(buf, bits, seed, hash);
			BUG_ON(hash == 0 || doh->hash - hash > 256);
			hash--;
		} while (lafs_dir_find(buf, bits, seed, hash, &piece) &&
			 lafs_dir_extract(buf, bits, &de, piece,
					  NULL)->target == 0);

		unmap_dblock(doh->dirent_block, buf);
	} else
		unmap_dblock(doh->dirent_block, buf);

	lafs_dirty_dblock(doh->dirent_block);
	clear_bit(B_PinPending, &doh->dirent_block->b.flags);
	putdref(doh->dirent_block, MKREF(dir_blk));
}

static int
dir_delete_pin(struct dirop_handle *doh)
{
	int err;
	err = lafs_pin_dblock(doh->dirent_block, ReleaseSpace);
	if (err)
		return err;
	return 0;
}

static void
dir_delete_abort(struct dirop_handle *doh)
{
	if (doh->dirent_block &&
	    !IS_ERR(doh->dirent_block)) {
		clear_bit(B_PinPending, &doh->dirent_block->b.flags);
		putdref(doh->dirent_block, MKREF(dir_blk));
	}
}

/*--------------------------------------------------------------
 * Update directory entry
 * This is used for rename when the target already exists
 * Rather than delete+create it becomes delete+update
 * This is even similar to delete except that we don't bother
 * with orphans.
 */
static int
dir_update_prepare(struct fs *fs, struct inode *dir,
		   const char *name, int nlen, struct dirop_handle *doh)
{
	struct datablock *dirblk;

	doh->dirent_block =
		dirblk = dir_lookup_blk(dir, name, nlen, &doh->index,
					0, NULL, MKREF(dir_blk));
	if (IS_ERR(dirblk))
		return PTR_ERR(dirblk);
	lafs_iolock_written(&dirblk->b);
	set_bit(B_PinPending, &dirblk->b.flags);
	/* i_mutex protect us now, so don't need to maintain the lock */
	lafs_iounlock_block(&dirblk->b);
	return 0;
}

static void
dir_update_commit(struct fs *fs, u32 target, int type,
		  struct dirop_handle *doh)
{
	char *buf = map_dblock(doh->dirent_block);
	int bits = doh->dirent_block->b.inode->i_blkbits - 8;
	struct dir_ent de;

	de.target = target;
	de.type = type;
	lafs_dir_set_target(buf, bits, &de, doh->index);
	unmap_dblock(doh->dirent_block, buf);
	lafs_dirty_dblock(doh->dirent_block);
	clear_bit(B_PinPending, &doh->dirent_block->b.flags);
	putdref(doh->dirent_block, MKREF(dir_blk));
}

static int
dir_update_pin(struct dirop_handle *doh)
{
	return lafs_pin_dblock(doh->dirent_block, ReleaseSpace);
}

static void
dir_update_abort(struct dirop_handle *doh)
{
	if (doh->dirent_block &&
	    !IS_ERR(doh->dirent_block)) {
		clear_bit(B_PinPending, &doh->dirent_block->b.flags);
		putdref(doh->dirent_block, MKREF(dir_blk));
	}
}

/*------------------------------------------------------------
 * Directory operations needs to be logged as transactions.
 * The transaction is formed from the name preceded by 4 bytes
 * for the inode number.
 * A look to allow matching of related update is stored in the block number.
 * The 'type' of transaction is recorded in the offset
 */

static int dir_log_prepare(struct update_handle *uh,
			   struct fs *fs,
			   struct qstr *name)
{
	return lafs_cluster_update_prepare(uh, fs, name->len+4);
}

static void dir_log_commit(struct update_handle *uh,
			   struct fs *fs, struct inode *dir,
			   struct qstr *name, u32 target,
			   int operation, u32 *handle)
{
	char mb[4];
	static u32 hancnt;
	u32 han = 0;

	switch (operation) {
	case DIROP_LINK:
	case DIROP_UNLINK:
		han = 0;
		break;
	case DIROP_REN_SOURCE:
		while (++hancnt == 0)
			;
		han = hancnt;
		*handle = han;
		break;
	case DIROP_REN_TARGET:
		han = *handle;
		break;
	default:
		BUG();
	}

	*(u32 *)mb = cpu_to_le32(target);
	lafs_cluster_update_commit_buf(uh, fs, dir, han, operation,
				       4+name->len, mb,
				       name->len, name->name);
}

int
lafs_dir_roll_mini(struct inode *dir, int handle, int dirop,
		   u32 inum, char *name, int len)
{
	int err = 0;
	struct dirop_handle doh, old_doh;
	struct datablock *inodb = NULL, *olddb = NULL;
	struct inode *inode = NULL;
	struct rename_roll *rr = NULL, **rrp;
	struct fs *fs = fs_from_inode(dir);
	int last;

	if (inum)
		inode = lafs_iget(LAFSI(dir)->filesys, inum, SYNC);
	if (IS_ERR(inode))
		return PTR_ERR(inode);
	if (!inode && dirop != DIROP_REN_TARGET)
		return -EINVAL;

	switch (dirop) {
	default:
		err = -EINVAL;
		break;
	case DIROP_LINK:
		/* name doesn't exist - we create it. */
		err = dir_create_prepare(fs, dir, name, len,
					 inum, mode_to_dt(inode->i_mode), &doh);
		inodb = lafs_inode_dblock(dir, SYNC, MKREF(roll_dir));
		if (IS_ERR(inodb))
			err = PTR_ERR(inodb);

		err = err ?: dir_create_pin(&doh);
		err = err ?: lafs_pin_dblock(inodb, ReleaseSpace);
		if (err < 0) {
			dir_create_abort(&doh);
			break;
		}
		inode_inc_link_count(inode);
		lafs_inode_checkpin(inode);
		lafs_dirty_dblock(inodb);
		clear_bit(B_PinPending, &inodb->b.flags);
		dir_create_commit(&doh, fs, dir, name, len,
				  inum, mode_to_dt(inode->i_mode));
		err = 0;
		break;

	case DIROP_UNLINK:
		/* Name exists, we need to remove it */
		last = (inode->i_nlink == 1);
		err = dir_delete_prepare(fs, dir, name, len, &doh);
		inodb = lafs_inode_dblock(inode, SYNC, MKREF(roll_dir));
		if (IS_ERR(inode))
			err = PTR_ERR(inodb);
		if (last && !err)
			err = lafs_make_orphan(fs, inodb);
		if (err) {
			dir_delete_abort(&doh);
			break;
		}
		lafs_iolock_block(&inodb->b);
		set_bit(B_PinPending, &inodb->b.flags);
		lafs_iounlock_block(&inodb->b);
		err = dir_delete_pin(&doh);
		err = err ?: lafs_pin_dblock(inodb, ReleaseSpace);
		if (err < 0) {
			dir_delete_abort(&doh);
			break;
		}
		inode_dec_link_count(inode);
		dir_delete_commit(&doh, fs, dir, name, len);
		lafs_inode_checkpin(inode);
		lafs_dirty_dblock(inodb);
		clear_bit(B_PinPending, &inodb->b.flags);
		err = 0;
		break;

	case DIROP_REN_SOURCE:
		rr = kmalloc(sizeof(*rr) + len, GFP_KERNEL);
		if (!rr) {
			err = -ENOMEM;
			break;
		}
		rr->next = fs->pending_renames;
		rr->key = handle;
		rr->dir = dir; igrab(dir);
		rr->inode = inode; igrab(inode);
		rr->nlen = len;
		strncpy(rr->name, name, len);
		rr->name[len] = 0;
		fs->pending_renames = rr;
		rr = NULL;
		break;

	case DIROP_REN_TARGET:
		rrp = &fs->pending_renames;
		while (*rrp) {
			rr = *rrp;
			if (rr->key == handle)
				break;
			rrp = &rr->next;
		}
		if (!*rrp) {
			rr = NULL;
			err = -EINVAL;
			break;
		}
		*rrp = rr->next;
		rr->next = NULL;

		last = (inode && inode->i_nlink == 1);

		/* FIXME check both are dirs or non-dirs, and that a
		 * target directory is empty */
		err = dir_delete_prepare(fs, rr->dir,
					 rr->name, rr->nlen,
					 &old_doh);
		olddb = lafs_inode_dblock(rr->inode, SYNC, MKREF(roll_dir));
		if (IS_ERR(olddb))
			err = PTR_ERR(olddb);
		if (inode) {
			/*unlink inode, update name */
			err = dir_update_prepare(fs, dir, name, len, &doh)
				?: err;
			inodb = lafs_inode_dblock(inode, SYNC, MKREF(roll_dir));
			if (IS_ERR(inodb))
				err = PTR_ERR(inodb);
			if (last && !err)
				err = lafs_make_orphan(fs, inodb);
			lafs_iolock_block(&inodb->b);
			set_bit(B_PinPending, &inodb->b.flags);
			lafs_iounlock_block(&inodb->b);
		} else
			/* create new link */
			err = dir_create_prepare(fs, dir, name, len,
						 rr->inode->i_ino,
						 mode_to_dt(rr->inode->i_mode),
						 &doh) ?: err;

		if (!err) {
			lafs_iolock_block(&olddb->b);
			set_bit(B_PinPending, &olddb->b.flags);
			lafs_iounlock_block(&olddb->b);
		}

		err = err ?: dir_delete_pin(&old_doh);
		err = err ?: lafs_pin_dblock(olddb, ReleaseSpace);
		if (inode) {
			err = err ?: lafs_pin_dblock(inodb, ReleaseSpace);
			err = err ?: dir_update_pin(&doh);
		} else
			err = err ?: dir_create_pin(&doh);
		if (err < 0) {
			dir_delete_abort(&old_doh);
			if (inode)
				dir_update_abort(&doh);
			else
				dir_create_abort(&doh);
			break;
		}
		dir_delete_commit(&old_doh, fs, rr->dir, rr->name, rr->nlen);
		if (S_ISDIR(rr->inode->i_mode)) {
			inode_dec_link_count(rr->dir);
			if (!inode)
				inode_inc_link_count(dir);
		}
		if (inode)
			dir_update_commit(fs, rr->inode->i_ino,
					  mode_to_dt(rr->inode->i_mode),
					  &doh);
		else
			dir_create_commit(&doh, fs, dir, name, len,
					  rr->inode->i_ino,
					  mode_to_dt(rr->inode->i_mode));
		switch (LAFSI(rr->inode)->type) {
		case TypeFile:
			LAFSI(rr->inode)->md.file.parent = dir->i_ino;
			break;
		case TypeInodeFile:
			LAFSI(rr->inode)->md.fs.parent = dir->i_ino;
			break;
		}
		if (inode) {
			if (S_ISDIR(inode->i_mode))
				inode_dec_link_count(inode);
			inode_dec_link_count(inode);
			lafs_inode_checkpin(inode);
		}
		lafs_dirty_inode(rr->inode);
		lafs_inode_checkpin(rr->dir);
		lafs_inode_checkpin(dir);
		clear_bit(B_PinPending, &olddb->b.flags);
		if (inode) {
			clear_bit(B_PinPending, &inodb->b.flags);
			putdref(inodb, MKREF(dir_roll));
		}
		err = 0;
		break;
	}
	if (inode && !IS_ERR(inode))
		iput(inode);
	if (inodb && !IS_ERR(inodb))
		putdref(inodb, MKREF(roll_dir));
	if (olddb && !IS_ERR(olddb))
		putdref(olddb, MKREF(roll_dir));
	if (rr) {
		iput(rr->dir);
		iput(rr->inode);
		kfree(rr);
	}
	return err;
}
/*------------------------------------------------------------
 * Now we have the lowlevel operations in place, we
 * can implement the VFS interface.
 */
static int
lafs_create(struct inode *dir, struct dentry *de, int mode,
     struct nameidata *nd)
{
/* Need to allocate an inode and space in the directory */
	struct fs *fs = fs_from_inode(dir);
	struct datablock *db;
	struct inode *ino = lafs_new_inode(fs, LAFSI(dir)->filesys,
					   dir, TypeFile, 0, mode, &db);
	struct dirop_handle doh;
	struct update_handle uh;
	int err;

	if (IS_ERR(ino))
		return PTR_ERR(ino);

	err = dir_create_prepare(fs, dir, de->d_name.name, de->d_name.len,
				 ino->i_ino, DT_REG, &doh);

	dprintk("ERR = %d\n", err);
	err = dir_log_prepare(&uh, fs, &de->d_name) ?: err;
	dprintk("ERR2 = %d\n", err);
	if (err)
		goto abort;

retry:
	dprintk("lc: dirblk = %p\n", doh.dirent_block);
	lafs_checkpoint_lock(fs);

	err = dir_create_pin(&doh);
	err = err ?: lafs_cluster_update_pin(&uh);
	err = err ?: lafs_pin_dblock(db, NewSpace);
	if (err == -EAGAIN) {
		lafs_checkpoint_unlock_wait(fs);
		goto retry;
	}
	if (err < 0)
		goto abort_unlock;

	ino->i_nlink = 1;
	lafs_add_orphan(fs, db);
	LAFSI(ino)->md.file.parent = dir->i_ino;
	lafs_dirty_inode(ino);
	lafs_inode_checkpin(ino);
	dir_log_commit(&uh, fs, dir, &de->d_name, ino->i_ino, DIROP_LINK, NULL);
	dir_create_commit(&doh, fs, dir, de->d_name.name, de->d_name.len,
			  ino->i_ino, DT_REG);
	lafs_checkpoint_unlock(fs);
	d_instantiate(de, ino);
	clear_bit(B_PinPending, &db->b.flags);
	putdref(db, MKREF(inode_new));
	return 0;

abort_unlock:
	lafs_checkpoint_unlock(fs);
abort:
	lafs_cluster_update_abort(&uh);
	dir_create_abort(&doh);
	iput(ino);
	clear_bit(B_PinPending, &db->b.flags);
	putdref(db, MKREF(inode_new));
	return err;
}

static int
lafs_link(struct dentry *from, struct inode *dir, struct dentry *to)
{
	/* Create the new name and increase the link count on the target */
	struct fs *fs = fs_from_inode(dir);
	struct dirop_handle doh;
	struct update_handle uh;
	struct inode *inode = from->d_inode;
	struct datablock *inodb;
	int err;

	if (inode->i_nlink >= LAFS_MAX_LINKS)
		return -EMLINK;
	err = dir_create_prepare(fs, dir, to->d_name.name, to->d_name.len,
				 inode->i_ino, mode_to_dt(inode->i_mode),
				 &doh);
	err = dir_log_prepare(&uh, fs, &to->d_name) ?: err;

	inodb = lafs_inode_dblock(dir, SYNC, MKREF(link));
	if (IS_ERR(inodb))
		err = PTR_ERR(inodb);
	if (err)
		goto abort;
retry:
	lafs_checkpoint_lock(fs);

	err = dir_create_pin(&doh);
	err = err ?: lafs_cluster_update_pin(&uh);
	err = err ?: lafs_pin_dblock(inodb, NewSpace);
	if (err == -EAGAIN) {
		lafs_checkpoint_unlock_wait(fs);
		goto retry;
	}
	if (err < 0)
		goto abort_unlock;

	inode_inc_link_count(inode);
	lafs_inode_checkpin(inode);
	lafs_dirty_dblock(inodb);
	clear_bit(B_PinPending, &inodb->b.flags);
	putdref(inodb, MKREF(link));

	dir_log_commit(&uh, fs, dir, &to->d_name, inode->i_ino,
		       DIROP_LINK, NULL);
	dir_create_commit(&doh, fs, dir, to->d_name.name, to->d_name.len,
			  inode->i_ino, mode_to_dt(inode->i_mode));
	/* Don't log the nlink change - that is implied in the name creation */
	d_instantiate(to, inode);

	lafs_checkpoint_unlock(fs);
	return 0;
abort_unlock:
	lafs_checkpoint_unlock(fs);
	clear_bit(B_PinPending, &inodb->b.flags);
abort:
	if (!IS_ERR(inodb))
		putdref(inodb, MKREF(link));
	dir_create_abort(&doh);
	lafs_cluster_update_abort(&uh);
	return err;
}

static int
lafs_unlink(struct inode *dir, struct dentry *de)
{
	struct fs *fs = fs_from_inode(dir);
	struct inode *inode = de->d_inode;
	int last = (inode->i_nlink == 1);
	struct dirop_handle doh;
	struct update_handle uh;
	struct datablock *inodb;
	int err;

	dprintk("unlink %s\n", de->d_name.name);

	err = dir_delete_prepare(fs, dir, de->d_name.name, de->d_name.len,
				 &doh);
	BUG_ON(err == -ENOENT);
	err = dir_log_prepare(&uh, fs, &de->d_name) ?: err;
	inodb = lafs_inode_dblock(inode, SYNC, MKREF(inode_update));
	if (IS_ERR(inodb))
		err = PTR_ERR(inodb);
	if (last && !err)
		err = lafs_make_orphan(fs, inodb);
	if (err)
		goto abort;
	lafs_iolock_block(&inodb->b);
	set_bit(B_PinPending, &inodb->b.flags);
	lafs_iounlock_block(&inodb->b);
retry:
	lafs_checkpoint_lock(fs);

	err = dir_delete_pin(&doh);
	err = err ?: lafs_cluster_update_pin(&uh);
	err = err ?: lafs_pin_dblock(inodb, ReleaseSpace);
	if (err == -EAGAIN) {
		lafs_checkpoint_unlock_wait(fs);
		goto retry;
	}
	if (err < 0)
		goto abort_unlock;

	inode_dec_link_count(inode);
	dir_log_commit(&uh, fs, dir, &de->d_name, inode->i_ino,
		       DIROP_UNLINK, NULL);
	dir_delete_commit(&doh, fs, dir, de->d_name.name, de->d_name.len);
	lafs_checkpoint_unlock(fs);
	lafs_inode_checkpin(inode);
	lafs_dirty_dblock(inodb);
	clear_bit(B_PinPending, &inodb->b.flags);
	putdref(inodb, MKREF(inode_update));
	return 0;
abort_unlock:
	clear_bit(B_PinPending, &inodb->b.flags);
	lafs_checkpoint_unlock(fs);
abort:
	if (!IS_ERR(inodb))
		putdref(inodb, MKREF(inode_update));
	lafs_cluster_update_abort(&uh);
	dir_delete_abort(&doh);
	return err;
}

static void dir_flush_orphans(struct fs *fs, struct inode *inode)
{
	/*
	 * Orphans cannot clear while we hold i_mutex, so
	 * we have to run them ourselves.
	 */
	struct datablock *db;
	DEFINE_WAIT(wq);
	while ((db = lafs_find_orphan(inode))) {
		int still_orphan;
		prepare_to_wait(&fs->async_complete, &wq,
				TASK_UNINTERRUPTIBLE);
		getdref(db, MKREF(rmdir_orphan));
		lafs_dir_handle_orphan(db);
		still_orphan = test_bit(B_Orphan, &db->b.flags);
		putdref(db, MKREF(rmdir_orphan));
		if (still_orphan)
			/* still an orphan, need to wait */
			schedule();
	}
	finish_wait(&fs->async_complete, &wq);
}

static int
lafs_rmdir(struct inode *dir, struct dentry *de)
{
	struct fs *fs = fs_from_inode(dir);
	struct inode *inode = de->d_inode;
	struct dirop_handle doh;
	struct update_handle uh;
	struct datablock *inodb;
	int err;

	if (inode->i_nlink > 2)
		return -ENOTEMPTY;
	if (inode->i_size) {
		/* Probably not empty, but it could be that we
		 * just need to wait for orphans the clear.
		 */
		dir_flush_orphans(fs, inode);
		if (inode->i_size)
			return -ENOTEMPTY;
	}

	dprintk("rmdir %s\n", de->d_name.name);

	err = dir_delete_prepare(fs, dir, de->d_name.name, de->d_name.len,
				 &doh);
	err = dir_log_prepare(&uh, fs, &de->d_name) ?: err;
	inodb = lafs_inode_dblock(inode, SYNC, MKREF(inode_update));
	if (IS_ERR(inodb))
		err = PTR_ERR(inodb);
	if (!err)
		err = lafs_make_orphan(fs, inodb);
	if (err)
		goto abort;
	lafs_iolock_block(&inodb->b);
	set_bit(B_PinPending, &inodb->b.flags);
	lafs_iounlock_block(&inodb->b);
retry:
	lafs_checkpoint_lock(fs);

	err = dir_delete_pin(&doh);
	err = err ?: lafs_cluster_update_pin(&uh);
	err = err ?: lafs_pin_dblock(inodb, ReleaseSpace);
	if (err == -EAGAIN) {
		lafs_checkpoint_unlock_wait(fs);
		goto retry;
	}
	if (err < 0)
		goto abort_unlock;

	inode_dec_link_count(dir);
	inode_dec_link_count(inode); /* . */
	inode_dec_link_count(inode); /* .. */
	dir_log_commit(&uh, fs, dir, &de->d_name, inode->i_ino,
		       DIROP_UNLINK, NULL);
	dir_delete_commit(&doh, fs, dir, de->d_name.name, de->d_name.len);
	lafs_dirty_dblock(inodb);
	clear_bit(B_PinPending, &inodb->b.flags);
	putdref(inodb, MKREF(inode_update));
	lafs_inode_checkpin(inode);
	lafs_inode_checkpin(dir);
	lafs_checkpoint_unlock(fs);
	return 0;
abort_unlock:
	lafs_checkpoint_unlock(fs);
	clear_bit(B_PinPending, &inodb->b.flags);
abort:
	if (!IS_ERR(inodb))
		putdref(inodb, MKREF(inode_update));
	lafs_cluster_update_abort(&uh);
	dir_delete_abort(&doh);
	return err;
}

static int
lafs_symlink(struct inode *dir, struct dentry *de,
	     const char *symlink)
{
	int l;
	struct inode *ino;
	struct fs *fs = fs_from_inode(dir);
	struct datablock *b, *inodb;
	struct dirop_handle doh;
	struct update_handle uh;
	char *buf;
	int err;

	l = strlen(symlink);
	if (l > fs->blocksize-1)
		return -ENAMETOOLONG;

	ino = lafs_new_inode(fs, LAFSI(dir)->filesys, dir,
			     TypeSymlink, 0, 0666, &inodb);
	if (IS_ERR(ino))
		return PTR_ERR(ino);
	b = lafs_get_block(ino, 0, NULL, GFP_KERNEL, MKREF(symlink));
	if (!b) {
		putdref(inodb, MKREF(inode_new));
		iput(ino);
		return -ENOMEM;
	}
	set_bit(B_PinPending, &b->b.flags);

	err = dir_create_prepare(fs, dir, de->d_name.name, de->d_name.len,
				 ino->i_ino, DT_LNK, &doh);
	err = dir_log_prepare(&uh, fs, &de->d_name) ?: err;
	if (err)
		goto abort;
retry:
	lafs_checkpoint_lock(fs);

	err = dir_create_pin(&doh);
	err = err ?: lafs_pin_dblock(b, NewSpace);
	err = err ?: lafs_cluster_update_pin(&uh);
	if (err == -EAGAIN) {
		lafs_checkpoint_unlock_wait(fs);
		goto retry;
	}
	if (err < 0)
		goto abort_unlock;

	ino->i_nlink = 1;
	LAFSI(ino)->md.file.parent = dir->i_ino;
	lafs_add_orphan(fs, inodb);

	lafs_iolock_block(&b->b);
	buf = map_dblock(b);
	memcpy(buf, symlink, l);
	buf[l] = 0;
	unmap_dblock(b, buf);
	set_bit(B_Valid, &b->b.flags);
	lafs_dirty_dblock(b);
	clear_bit(B_PinPending, &b->b.flags);
	lafs_cluster_allocate(&b->b, 0); /* Content will go in the next cluster - almost like
					  * an update */
	putdref(b, MKREF(symlink));
	i_size_write(ino, l);
	lafs_dirty_inode(ino);
	lafs_inode_checkpin(ino);

	dir_log_commit(&uh, fs, dir, &de->d_name, ino->i_ino, DIROP_LINK, NULL);
	dir_create_commit(&doh, fs, dir, de->d_name.name, de->d_name.len,
			  ino->i_ino, DT_LNK);
	lafs_checkpoint_unlock(fs);
	d_instantiate(de, ino);
	putdref(inodb, MKREF(inode_new));
	return 0;
abort_unlock:
	lafs_checkpoint_unlock(fs);
abort:
	putdref(inodb, MKREF(inode_new));
	clear_bit(B_PinPending, &b->b.flags);
	putdref(b, MKREF(symlink));
	dir_create_abort(&doh);
	lafs_cluster_update_abort(&uh);
	iput(ino);
	return err;
}

static int
lafs_mkdir(struct inode *dir, struct dentry *de, int mode)
{
	struct inode *ino;
	struct lafs_inode *lai;
	struct fs *fs = fs_from_inode(dir);
	int err;
	struct dirop_handle doh;
	struct update_handle uh;
	struct datablock *inodb;

	if (dir->i_nlink >= LAFS_MAX_LINKS)
		return -EMLINK;

	ino = lafs_new_inode(fs, LAFSI(dir)->filesys, dir,
			     TypeDir, 0, mode, &inodb);
	if (IS_ERR(ino))
		return PTR_ERR(ino);

	err = dir_create_prepare(fs, dir, de->d_name.name, de->d_name.len,
				 ino->i_ino, DT_DIR, &doh);
	err = dir_log_prepare(&uh, fs, &de->d_name) ?: err;
	if (err)
		goto abort;
retry:
	lafs_checkpoint_lock(fs);

	err = dir_create_pin(&doh);
	err = err ?: lafs_pin_dblock(inodb, NewSpace);
	err = err ?: lafs_cluster_update_pin(&uh);
	if (err == -EAGAIN) {
		lafs_checkpoint_unlock_wait(fs);
		goto retry;
	}
	if (err < 0)
		goto abort_unlock;

	lai = LAFSI(ino);
	lai->md.file.parent = dir->i_ino;
	inode_inc_link_count(dir);
	ino->i_nlink = 2; /* From parent, and from '.' */
	lafs_dirty_inode(ino);
	lafs_inode_checkpin(dir);
	lafs_inode_checkpin(ino);
	lafs_add_orphan(fs, inodb);
	dir_create_commit(&doh, fs, dir, de->d_name.name, de->d_name.len,
			  ino->i_ino, DT_DIR);
	d_instantiate(de, ino);
	clear_bit(B_PinPending, &inodb->b.flags);
	putdref(inodb, MKREF(inode_new));
	lafs_checkpoint_unlock(fs);
	return 0;
abort_unlock:
	lafs_checkpoint_unlock(fs);
abort:
	dir_create_abort(&doh);
	lafs_cluster_update_abort(&uh);
	iput(ino);
	clear_bit(B_PinPending, &inodb->b.flags);
	putdref(inodb, MKREF(inode_new));
	return err;
}

static int
lafs_mknod(struct inode *dir, struct dentry *de, int mode,
	   dev_t rdev)
{
	struct inode *ino;
	struct fs *fs = fs_from_inode(dir);
	int err;
	struct dirop_handle doh;
	struct update_handle uh;
	struct datablock *inodb;
	int type;

	if (!new_valid_dev(rdev))
		return -EINVAL;

	type = TypeSpecial;
	switch (mode & S_IFMT) {
	case S_IFREG:
		type = TypeFile;
		break;
	case S_IFCHR:
	case S_IFBLK:
	case S_IFIFO:
	case S_IFSOCK:
		break;
	default:
		return -EINVAL;
	}
	ino = lafs_new_inode(fs, LAFSI(dir)->filesys, dir,
			     type, 0, mode, &inodb);
	if (IS_ERR(ino))
		return PTR_ERR(ino);
	init_special_inode(ino, ino->i_mode, rdev);

	err = dir_create_prepare(fs, dir, de->d_name.name, de->d_name.len,
				 ino->i_ino, type, &doh);
	err = dir_log_prepare(&uh, fs, &de->d_name) ?: err;
	if (err)
		goto abort;
retry:
	lafs_checkpoint_lock(fs);

	err = dir_create_pin(&doh);
	err = err ?: lafs_pin_dblock(inodb, NewSpace);
	err = err ?: lafs_cluster_update_pin(&uh);
	if (err == -EAGAIN) {
		lafs_checkpoint_unlock_wait(fs);
		goto retry;
	}
	if (err < 0)
		goto abort_unlock;

	LAFSI(ino)->md.file.parent = dir->i_ino;
	ino->i_nlink = 1;
	lafs_add_orphan(fs, inodb);
	lafs_dirty_inode(ino);
	lafs_inode_checkpin(ino);

	dir_log_commit(&uh, fs, dir, &de->d_name, ino->i_ino, DIROP_LINK, NULL);
	dir_create_commit(&doh, fs, dir, de->d_name.name, de->d_name.len,
			  ino->i_ino, type);
	lafs_checkpoint_unlock(fs);
	d_instantiate(de, ino);
	clear_bit(B_PinPending, &inodb->b.flags);
	putdref(inodb, MKREF(inode_new));
	return 0;
abort_unlock:
	lafs_checkpoint_unlock(fs);
abort:
	dir_create_abort(&doh);
	lafs_cluster_update_abort(&uh);
	iput(ino);
	clear_bit(B_PinPending, &inodb->b.flags);
	putdref(inodb, MKREF(inode_new));
	return err;
}

static int
lafs_rename(struct inode *old_dir, struct dentry *old_dentry,
	    struct inode *new_dir, struct dentry *new_dentry)
{
	/* VFS has checked that this is file->file or dir->dir or
	 * something->nothing.
	 * We just need to check that if the target is a directory,
	 * it is empty, then perform the rename
	 */
	struct fs *fs = fs_from_inode(old_dir);

	struct inode *old_inode = old_dentry->d_inode;
	struct inode *new_inode = new_dentry->d_inode;

	struct datablock *olddb, *newdb = newdb;

	struct dirop_handle old_doh, new_doh;
	struct update_handle old_uh, new_uh;
	int last = (new_inode && new_inode->i_nlink == 1);
	u32 renhandle;
	int err;

	if (S_ISDIR(old_inode->i_mode)) {
		if (new_inode) {
			if (new_inode->i_size) {
				dir_flush_orphans(fs, new_inode);
				if (new_inode->i_size)
					return -ENOTEMPTY;
			}
		} else if (new_dir != old_dir) {
			/* New dir is getting a new link */
			if (new_dir->i_nlink >= LAFS_MAX_LINKS)
				return -EMLINK;
		}
	}
	dprintk("rename %s %s\n", old_dentry->d_name.name,
		new_dentry->d_name.name);

	/* old entry gets deleted, new entry gets created or updated. */
	err = dir_delete_prepare(fs, old_dir,
				 old_dentry->d_name.name,
				 old_dentry->d_name.len,
				 &old_doh);
	err = dir_log_prepare(&old_uh, fs, &old_dentry->d_name) ?: err;
	err = dir_log_prepare(&new_uh, fs, &new_dentry->d_name) ?: err;
	olddb = lafs_inode_dblock(old_inode, SYNC, MKREF(inode_update));
	if (IS_ERR(olddb))
		err = PTR_ERR(olddb);

	if (new_inode) {
		/* unlink object, update name */
		err = dir_update_prepare(fs, new_dir,
					 new_dentry->d_name.name,
					 new_dentry->d_name.len,
					 &new_doh) ?: err;
		newdb = lafs_inode_dblock(new_inode, SYNC, MKREF(inode_update));
		if (IS_ERR(newdb))
			err = PTR_ERR(newdb);
		if (last && !err)
			err = lafs_make_orphan(fs, newdb);
		lafs_iolock_block(&newdb->b);
		set_bit(B_PinPending, &newdb->b.flags);
		lafs_iounlock_block(&newdb->b);
	} else
		/* create new link */
		err = dir_create_prepare(fs, new_dir,
					 new_dentry->d_name.name,
					 new_dentry->d_name.len,
					 old_inode->i_ino,
					 mode_to_dt(old_inode->i_mode),
					 &new_doh) ?: err;

	if (err)
		goto abort;

	lafs_iolock_block(&olddb->b);
	set_bit(B_PinPending, &olddb->b.flags);
	lafs_iounlock_block(&olddb->b);
retry:
	lafs_checkpoint_lock(fs);

	err = dir_delete_pin(&old_doh);
	err = err ?: lafs_cluster_update_pin(&old_uh);
	err = err ?: lafs_cluster_update_pin(&new_uh);
	err = err ?: lafs_pin_dblock(olddb, ReleaseSpace);
	if (new_inode) {
		err = err ?: lafs_pin_dblock(newdb, NewSpace);
		err = err ?: dir_update_pin(&new_doh);
	} else
		err = err ?: dir_create_pin(&new_doh);

	if (err == -EAGAIN) {
		lafs_checkpoint_unlock_wait(fs);
		goto retry;
	}
	if (err < 0)
		goto abort_unlock;

	/* OK, let's do the deed */
	dir_delete_commit(&old_doh, fs, old_dir, old_dentry->d_name.name,
			  old_dentry->d_name.len);
	dir_log_commit(&old_uh, fs, old_dir, &old_dentry->d_name,
		       old_inode->i_ino, DIROP_REN_SOURCE, &renhandle);
	dir_log_commit(&new_uh, fs, new_dir, &new_dentry->d_name,
		       new_inode ? new_inode->i_ino : 0,
		       DIROP_REN_TARGET,
		       &renhandle);
	if (S_ISDIR(old_inode->i_mode)) {
		inode_dec_link_count(old_dir);
		if (!new_inode)
			inode_inc_link_count(new_dir);
	}
	if (new_inode)
		dir_update_commit(fs, old_inode->i_ino,
				  mode_to_dt(old_inode->i_mode),
				  &new_doh);
	else
		dir_create_commit(&new_doh, fs, new_dir,
				  new_dentry->d_name.name,
				  new_dentry->d_name.len,
				  old_inode->i_ino,
				  mode_to_dt(old_inode->i_mode));
	switch (LAFSI(old_inode)->type) {
	case TypeFile:
		LAFSI(old_inode)->md.file.parent = new_dir->i_ino;
		break;
	case TypeInodeFile:
		LAFSI(old_inode)->md.fs.parent = new_dir->i_ino;
		break;
	}
	if (new_inode) {
		if (S_ISDIR(new_inode->i_mode))
			inode_dec_link_count(new_inode);
		inode_dec_link_count(new_inode);
		lafs_inode_checkpin(new_inode);
	}
	lafs_dirty_inode(old_inode);
	lafs_inode_checkpin(new_dir);
	lafs_inode_checkpin(old_dir);
	clear_bit(B_PinPending, &olddb->b.flags);
	putdref(olddb, MKREF(inode_update));
	if (new_inode) {
		clear_bit(B_PinPending, &newdb->b.flags);
		putdref(newdb, MKREF(inode_new));
	}

	lafs_checkpoint_unlock(fs);
	return 0;

abort_unlock:
	lafs_checkpoint_unlock(fs);
	clear_bit(B_PinPending, &olddb->b.flags);
	if (new_inode)
		clear_bit(B_PinPending, &newdb->b.flags);
abort:
	dir_delete_abort(&old_doh);
	lafs_cluster_update_abort(&old_uh);
	lafs_cluster_update_abort(&new_uh);
	if (!IS_ERR(olddb))
		putdref(olddb, MKREF(inode_update));
	if (new_inode) {
		dir_update_abort(&new_doh);
		if (!IS_ERR(newdb))
			putdref(newdb, MKREF(inode_new));
	} else
		dir_create_abort(&new_doh);
	return err;
}

/*--------------------------------------------------------------------
 * Directory Orphan handling.
 *
 * blocks in a directory file that are 'orphans' have recently had a deletion
 * and may need:
 *   - to be punched as a hole if empty
 *   - to have 'deleted' entries purged in they are freeable
 *   - to schedule next block for orphan handling if that might be appropriate.
 *
 *
 * Specifically:
 *  Lock the directory.
 *   If last possible entry (addr-1) is deleted,
 *    look for next entry.
 *    If it doesn't exist, remove last entry an preceding deleted
 *     entries, just like with delete.
 *   If first block is 'deleted' and next is removed,
 *     remove that deleted entry.
 *     look for previous entry.
 *     if it is deleted, schedule orphan handling.
 *   If ->root is 0, punch a hole
 */

int lafs_dir_handle_orphan(struct datablock *b)
{
	struct inode *dir = b->b.inode;
	struct fs *fs = fs_from_inode(dir);
	int bits = dir->i_blkbits-8;
	u32 seed = LAFSI(dir)->md.file.seed;
	u32 hash;
	char *buf, *buf2;
	struct datablock *b2 = NULL;
	u8 piece, firstpiece;
	struct dir_ent de;
	int err = 0;

	dprintk("HANDLE ORPHAN h=%x %s\n", (unsigned)hash, strblk(&b->b));

	if (!lafs_iolock_written_async(&b->b))
		return -EAGAIN;
	set_bit(B_PinPending, &b->b.flags);
	lafs_iounlock_block(&b->b);

	lafs_checkpoint_lock(fs);

	if (!test_bit(B_Valid, &b->b.flags)) {
		/* probably have already erased this block,
		 * but the orphan_release failed due to
		 * space being tight.
		 * just try again
		 */
		lafs_orphan_release(fs, b);
		err = 0;
		goto abort;
	}

	/* First test:  Does a chain of deleted entries extend beyond
	 * the end of this block.  i.e. is the last entry deleted.
	 * If so, look at the next block and see if the chain is still
	 * anchored, or if it can all be released.
	 */
	buf = map_dblock(b);
	hash = (b->b.fileaddr-1) & MaxDirHash;
	if (lafs_dir_find(buf, bits, seed, hash, &piece) &&
	    lafs_dir_extract(buf, bits, &de, piece, NULL)->target == 0) {
		loff_t bnum;
		unmap_dblock(b, buf);
		bnum = b->b.fileaddr + 1;
		if (lafs_find_next(dir, &bnum) == 0)
			/* FIXME what if it returns an error */
			bnum = 0;

		b2 = lafs_get_block(dir, bnum, NULL, GFP_KERNEL,
				    MKREF(dir_orphan));
		err = -ENOMEM;
		if (!b2)
			goto abort;
		err = lafs_read_block_async(b2);
		if (err)
			goto abort;

		buf2 = map_dblock(b2);
		if (lafs_dir_find(buf2, bits, seed, hash+1, &piece) == 0) {
			u8 firstpiece;
			/* We can remove that last entry, and maybe others */
			unmap_dblock(b2, buf2);

			err = lafs_pin_dblock(b, ReleaseSpace);
			if (err)
				goto abort;
			buf = map_dblock(b);
			lafs_dir_find(buf, bits, seed, 0, &firstpiece);
			do {
				if (piece == firstpiece)
					break;
				lafs_dir_del_ent(buf, bits, seed, hash);
				hash--;
			} while (lafs_dir_find(buf, bits, seed, hash, &piece) &&
				 lafs_dir_extract(buf, bits, &de, piece,
						  NULL)->target == 0);
			unmap_dblock(b, buf);
			lafs_dirty_dblock(b);
		} else
			unmap_dblock(b2, buf2);
		buf = map_dblock(b);
		putdref(b2, MKREF(dir_orphan));
		b2 = NULL;
	}

	/* Second test:  if we have an unanchored chain at the start
	 * of the block, then schedule orphan handling for previous block,
	 * and remove the unanchor.
	 */
	lafs_dir_find(buf, bits, seed, 0, &firstpiece);
	hash = seed;
	if (firstpiece &&
	    lafs_dir_extract(buf, bits, &de, firstpiece, &hash)->target == 0 &&
	    lafs_dir_find(buf, bits, seed, hash+1, &piece) == 0) {
		unmap_dblock(b, buf);
		b2 = lafs_get_block(dir, hash, NULL, GFP_KERNEL,
				    MKREF(dir_orphan));
		err = -ENOMEM;
		if (!b2)
			goto abort;
		err = lafs_read_block_async(b2);
		if (err)
			goto abort;

		buf2 = map_dblock(b2);
		if (lafs_dir_find(buf2, bits, seed, (hash-1) & MaxDirHash,
				  &piece) &&
		    lafs_dir_extract(buf2, bits, &de, piece, NULL)->target == 0)
			err = lafs_make_orphan_nb(fs, b2);
		unmap_dblock(b2, buf2);
		putdref(b2, MKREF(dir_orphan));
		b2 = NULL;
		if (err)
			goto abort;
		err = lafs_pin_dblock(b, ReleaseSpace);
		if (err)
			goto abort;
		buf = map_dblock(b);
		lafs_dir_del_ent(buf, bits, seed, hash);
		lafs_dirty_dblock(b);
	}

	if (lafs_dir_empty(buf)) {
		loff_t bnum;
		unmap_dblock(b, buf);

		err = lafs_pin_dblock(b, ReleaseSpace);
		if (err)
			goto abort;

		bnum = 1;
		err = lafs_find_next(dir, &bnum);
		if (err < 0)
			goto abort;
		if (err == 0) {
			if (b->b.fileaddr == 0)
				i_size_write(dir, 0);
			else {
				b2 = lafs_get_block(dir, 0, NULL, GFP_KERNEL,
						    MKREF(dir_orphan));
				err = -ENOMEM;
				if (!b2)
					goto abort;
				err = lafs_read_block_async(b2);
				if (err)
					goto abort;
				buf2 = map_dblock(b2);
				i_size_write(dir,
					     lafs_dir_blk_size(buf2, bits));
				unmap_dblock(b2, buf2);
				putdref(b2, MKREF(dir_orphan));
				b2 = NULL;
			}
			lafs_dirty_inode(dir);
		}
		lafs_erase_dblock(b);
	} else
		unmap_dblock(b, buf);

	lafs_orphan_release(fs, b);
	err = 0;

abort:
	clear_bit(B_PinPending, &b->b.flags);
	putdref(b2, MKREF(dir_orphan));
	lafs_checkpoint_unlock(fs);
	return err;
}

/*--------------------------------------------------------------------
 * Finally the read-only operations
 */
static int
lafs_readdir(struct file *filp, void *dirent, filldir_t filldir)
{
	struct dentry *dentry = filp->f_dentry;
	struct lafs_inode *lai = LAFSI(dentry->d_inode);
	ino_t ino;
	loff_t i = filp->f_pos;
	loff_t bnum;
	u32 hash;
	int err = 0;
	int over;
	int bits = dentry->d_inode->i_blkbits - 8;
	u32 seed = lai->md.file.seed;

	switch (i) {
	case 0:
		ino = dentry->d_inode->i_ino;
		if (filldir(dirent, ".", 1, i, ino, DT_DIR) < 0)
			break;
		filp->f_pos++;
		i++;
		/* fallthrough */
	case 1:
		ino = lai->md.file.parent;
		if (filldir(dirent, "..", 2, i, ino, DT_DIR) < 0)
			break;
		filp->f_pos++;
		i++;
		/* fallthrough */
	default:
		hash = i - 2;
		err = 0;
		over = 0;
		do {
			struct datablock *b;
			char *buf;

			bnum = hash+1;
			switch (lafs_find_next(dentry->d_inode, &bnum)) {
			case 1:
				break;
			case 0:
				bnum = 0;
				break;
			default:
				return -EIO;
			}
			b = lafs_get_block(dentry->d_inode, bnum, NULL,
					   GFP_KERNEL, MKREF(readdir));
			if (!b) {
				err = -ENOMEM;
				break;
			}
			err = lafs_read_block(b);
			if (err)
				break;
			/* buf = map_dblock(b); */
			buf = kmap(b->page);
			buf += dblock_offset(b);
			while (1) {
				u8 piece;
				struct dir_ent de;

				lafs_dir_find(buf, bits, seed, hash, &piece);
				if (!piece)
					break;
				hash = seed;
				lafs_dir_extract(buf, bits, &de,
						 piece, &hash);

				if (de.target == 0) {
					hash++;
					filp->f_pos = hash+2;
					continue;
				}
				/* This is a good name to return */
				over = filldir(dirent, de.name, de.nlen,
					       hash+2, de.target, de.type);
				hash++;
				if (!over)
					filp->f_pos = hash+2;
				else
					break;
			}
			/* unmap_dblock(b, buf); */
			kunmap(b->page);
			putdref(b, MKREF(readdir));
			hash = bnum;
		} while (bnum && !over);
		break;
	}
	return err;
}

static struct dentry *
lafs_lookup(struct inode *dir, struct dentry *dentry, struct nameidata *nd)
{
	/* Simple lookup that maps inode number to that inode. */
	u32 inum = 0;
	struct inode *ino;
	int err = dir_lookup(dir, dentry->d_name.name, dentry->d_name.len,
			     &inum);

	if (err == -ENOENT) {
		d_add(dentry, NULL);
		return NULL;
	}
	/* FIXME range check inum */

	if (err < 0)
		return ERR_PTR(err);
	ino = lafs_iget(LAFSI(dir)->filesys, inum, SYNC);

	if (IS_ERR(ino))
		return ERR_PTR(PTR_ERR(ino));
	return d_splice_alias(ino, dentry);
}

static int lafs_getattr_dir(struct vfsmount *mnt, struct dentry *dentry,
			    struct kstat *stat)
{
	lafs_fillattr(dentry->d_inode, stat);
	/* hide 'holes' in directories by making the size match
	 * the number of allocated blocks.
	 */
	if (stat->size > dentry->d_inode->i_sb->s_blocksize)
		stat->size = (dentry->d_inode->i_sb->s_blocksize *
			     (LAFSI(dentry->d_inode)->cblocks +
			      LAFSI(dentry->d_inode)->pblocks +
			      LAFSI(dentry->d_inode)->ablocks));
	return 0;
}

const struct file_operations lafs_dir_file_operations = {
	.llseek		= generic_file_llseek,	/* Just set 'pos' */
	.read		= generic_read_dir,	/* return error */
	.readdir	= lafs_readdir,
#if 0
	.fsync		= lafs_sync_cluster,
#endif
};

const struct inode_operations lafs_dir_ino_operations = {
	.lookup		= lafs_lookup,
	.create		= lafs_create,
	.link		= lafs_link,
	.unlink		= lafs_unlink,
	.symlink	= lafs_symlink,
	.mkdir		= lafs_mkdir,
	.rmdir		= lafs_rmdir,
	.rename		= lafs_rename,
	.mknod		= lafs_mknod,
	.setattr	= lafs_setattr,
	.getattr	= lafs_getattr_dir,
};
