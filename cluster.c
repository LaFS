
/*
 * write-cluster management routines for LaFS
 * fs/lafs/cluster.c
 * Copyright (C) 2006-2009
 * NeilBrown <neilb@suse.de>
 * Released under the GPL, version 2
 */

/*
 * Blocks to be written to a cluster need to be sorted so as to minimise
 * fragmentation and to keep the cluster head small.
 * So that we can track the amount of space needed in the cluster head,
 * we really need to keep the list sorted rather than just sort it
 * before writeout.
 * We do this using a linked list of blocks and an insertion sort.
 * To improve the speed of insertion we keep a modify skiplist structure
 * over the list.
 * This is 'modified' in that it explicitly understands sequential
 * runs within the list and doesn't keep extra skip-points within a run.
 *
 * The skip-points require memory allocation, which cannot be
 * guaranteed here in the writeout path.  To handle this we
 *  - preallocate some entries
 *  - accept a less-efficient list if memory isn't available
 *  - flush early if things get really bad.
 *
 * Skip point placement and runs.
 * When an entry is added to the list, we place a skip-point over it
 * based on the value of a random number, with a 1-in-4 chance of placement.
 * The height of the skip point is also random - we keep rolling the dice,
 * increasing height each time that we get 1-in-4.
 * If the new entry is placed immediately after a skip point, and is
 * consecutive with that skip point, the skip point is moved forward
 * so that it will always be at the end of a run.
 * When a new entry finds itself at the start or end of a run, a skip
 * point is not added.
 */

/* A height of 8 with a fanout of 1-in-4 allows reasonable
 * addressing for 2^16 entries, which is probably more than we need
 */

#include	"lafs.h"
#include	<linux/crc32.h>
#include	<linux/random.h>
#include	<linux/slab.h>

static void cluster_flush(struct fs *fs, int cnum);

static void skip_discard(struct skippoint *sp)
{
	while (sp) {
		struct skippoint *next = sp->next[0];
		kfree(sp);
		sp = next;
	}
}

static int cmp_blk(struct block *a, struct block *b)
{
	/* compare the filesys/inode/index/addr info
	 * of two block and return:
	 *  0 if equals
	 * +ve if a > b
	 * -1 if consecutive in order
	 * -2 if in same inode and in order
	 * <=-3 if different inodes and correct order
	 */

	if (a->inode != b->inode) {
		struct inode *fsinoa = LAFSI(a->inode)->filesys;
		struct inode *fsinob = LAFSI(b->inode)->filesys;
		if (fsinoa->i_ino <
		    fsinob->i_ino)
			return -3;
		if (fsinoa->i_ino >
		    fsinob->i_ino)
			return 3;
		/* same filesystem */
		if (a->inode->i_ino <
		    b->inode->i_ino)
			return -3;
		return 3;
	}
	/* Same inode.  If both data blocks they might be
	 * consecutive
	 */
	if (!test_bit(B_Index, &a->flags) &&
	    !test_bit(B_Index, &b->flags)) {
		if (a->fileaddr == b->fileaddr)
			return 0;
		if (a->fileaddr + 1 == b->fileaddr)
			return -1;
		if (a->fileaddr < b->fileaddr)
			return -2;
		return 2;
	}
	/* at least one is an index block. Sort them before data */
	if (!test_bit(B_Index, &a->flags))
		/* a is data, b is index, b first */
		return 2;

	if (!test_bit(B_Index, &b->flags))
		return -2;

	/* both are index blocks, use fileaddr or depth */
	if (a->fileaddr < b->fileaddr)
		return -2;
	if (a->fileaddr > b->fileaddr)
		return 2;
	if (iblk(a)->depth < iblk(b)->depth)
		return -2;
	/* equality really shouldn't be possible */
	return 2;
}

static struct block *skip_find(struct skippoint *head,
			       struct list_head *list,
			       struct block *target,
			       struct skippoint *where)
{
	/* Find the last block that precedes 'target'
	 * and return it.  If target will be the start,
	 * return NULL.
	 * 'where' gets filled in with the last skip point
	 * at each level which is before-or-at the found
	 * block.  This can be used for easy insertion.
	 */

	int level;
	struct block *b, *next;
	for (level = SKIP_MAX_HEIGHT-1; level >= 0 ; level--) {
		while (head->next[level] &&
		       cmp_blk(head->next[level]->b, target) < 0)
			head = head->next[level];
		where->next[level] = head;
	}
	if (head->b == NULL) {
		if (list_empty(list) ||
		    cmp_blk(list_entry(list->next, struct block, lru),
			    target) > 0)
			/* This goes at the start */
			return NULL;
		b = list_entry(list->next, struct block, lru);
	} else
		b = head->b;
	/* b is before target */
	next = b;
	list_for_each_entry_continue(next, list, lru) {
		if (cmp_blk(next, target) > 0)
			break;
		b = next;
	}
	/* b is the last element before target */
	return b;
}

static int cluster_insert(struct skippoint *head,
			  struct list_head *list,
			  struct block *target,
			  int avail)
{
	/* Insert 'target' at an appropriate point in the
	 * list, creating a skippoint if appropriate.
	 * Return:
	 *  0 if part of a run
	 *  1 if not in a run, but adjacent to something in same file
	 *  2 otherwise
	 * However if the value we would return would be more than
	 * 'avail' abort the insert and return -1;
	 */
	int rv;
	struct skippoint pos, *newpoint;
	int level;
	struct block *b;
	int cmpbefore, cmpafter;
	unsigned long rand[DIV_ROUND_UP(SKIP_MAX_HEIGHT * 2 / 8 + 1,
					sizeof(unsigned long))];
	int height;

	BUG_ON(!list_empty(&target->lru));
	if (unlikely(list_empty(list))) {
		int height;
		/* handle this trivial case separately */
		if (avail < 2)
			return -1;
		list_add(&target->lru, list);
		head->b = NULL;
		for (height = 0; height < SKIP_MAX_HEIGHT; height++)
			head->next[height] = NULL;
		return 2;
	}
	b = skip_find(head, list, target, &pos);
	if (b == NULL) {
		list_add(&target->lru, list);
		cmpbefore = 3;
	} else {
		list_add(&target->lru, &b->lru);
		cmpbefore = cmp_blk(b, target);
	}
	if (target->lru.next  == list) /* new last */
		cmpafter = -3;
	else
		cmpafter = cmp_blk(target, list_entry(target->lru.next,
						      struct block,
						      lru));
	if (cmpbefore == -1) {
		/* adjacent with previous.  Possibly move skippoint */
		if (pos.next[0]->b == b) {
			pos.next[0]->b = target;
			return 0;
		}
		rv = 0;
	} else if (cmpafter == -1)
		/* adjacent with next. No need to add a skippoint */
		return 0;
	else if (cmpbefore == -2 || cmpafter == -2)
		/* same inodes as next or previous */
		rv = 1;
	else
		rv = 2;

	if (rv > avail) {
		/* Doesn't fit */
		list_del_init(&target->lru);
		return -1;
	}
	/* Now consider adding a skippoint here.  We want 2 bits
	 * per level of random data
	 */
	get_random_bytes(rand, sizeof(rand));
	height = 0;
	while (height < SKIP_MAX_HEIGHT &&
	       test_bit(height*2, rand) &&
	       test_bit(height*2+1, rand))
		height++;
	if (height == 0)
		return rv;

	newpoint = kmalloc(sizeof(struct skippoint), GFP_NOFS);
	if (!newpoint)
		return rv;
	newpoint->b = target;
	for (level = 0; level < height-1; level++) {
		newpoint->next[level] = pos.next[level];
		pos.next[level] = newpoint;
	}
	return rv;
}

/*-----------------------------------------------------------------------
 * A segment is divided up in a slightly complicated way into
 * tables, rows and columns.  This allows us to align writes with
 * stripes in a raid4 array or similar.
 * So we have some support routines to help track our way around
 * the segment
 */

static int seg_remainder(struct fs *fs, struct segpos *seg)
{
	/* return the number of blocks from the start of segpos to the
	 * end of the segment.
	 * i.e. remaining rows in this table, plus remaining tables in
	 * this segment
	 */
	struct fs_dev *dv = &fs->devs[seg->dev];
	int rows = dv->rows_per_table - seg->st_row;

	BUG_ON(seg->dev < 0);
	rows += dv->rows_per_table * (dv->tables_per_seg - seg->st_table - 1);
	return rows * dv->width;
}

static void seg_step(struct fs *fs, struct segpos *seg)
{
	/* reposition this segpos to be immediately after it's current end
	 * and make the 'current' point be the start.
	 * Size will be empty
	 */
	seg->st_table = seg->nxt_table;
	seg->st_row = seg->nxt_row;
	seg->table = seg->st_table;
	seg->row = seg->st_row;
	seg->col = 0;
}

u32 lafs_seg_setsize(struct fs *fs, struct segpos *seg, u32 size)
{
	/* move the 'nxt' table/row to be 'size' blocks beyond
	 * current start. size will be rounded up to a multiple
	 * of width.
	 */
	struct fs_dev *dv = &fs->devs[seg->dev];
	u32 rv;
	BUG_ON(seg->dev < 0);
	size = (size + dv->width - 1) / dv->width;
	rv = size * dv->width;
	size += seg->st_row;
	seg->nxt_table = seg->st_table + size / dv->rows_per_table;
	seg->nxt_row = size % dv->rows_per_table;
	return rv;
}

void lafs_seg_setpos(struct fs *fs, struct segpos *seg, u64 addr)
{
	/* set seg to start at the given virtual address, and be
	 * of size 0
	 * 'addr' should always be at the start of a row, though
	 * as it might be read off storage, we need to be
	 * a little bit careful.
	 */
	u32 offset;
	u32 row, col, table;
	struct fs_dev *dv;
	virttoseg(fs, addr, &seg->dev, &seg->num, &offset);
	dv = &fs->devs[seg->dev];
	col = offset / dv->stride;
	row = offset % dv->stride;
	table = col / dv->width;
	col = col % dv->width;
	BUG_ON(col);

	seg->st_table = table;
	seg->st_row = row;

	seg->table = seg->nxt_table = seg->st_table;
	seg->row = seg->nxt_row = seg->st_row;
	seg->col = 0;
}

static u64 seg_addr(struct fs *fs, struct segpos *seg)
{
	/* Return the virtual address of the blocks pointed
	 * to by 'seg'.
	 */
	struct fs_dev *dv = &fs->devs[seg->dev];
	u64 addr;

	if (seg->dev < 0)
		/* Setting 'next' address for last cluster in
		 * a cleaner segment */
		return (u64)-1;
	if (seg->table > seg->nxt_table ||
	    (seg->table == seg->nxt_table &&
	     seg->row > seg->nxt_row))
		/* We have gone beyond the end of the cluster,
		 * must be bad data during roll-forward
		 */
		return (u64)-1;
	addr = seg->col * dv->stride;
	addr += seg->row;
	addr += seg->table * dv->rows_per_table;
	addr += seg->num * dv->segment_stride;
	addr += dv->start;
	return addr;
}

u64 lafs_seg_next(struct fs *fs, struct segpos *seg)
{
	/* step forward one block, returning the address of
	 * the block stepped over
	 * The write cluster can have three sections:
	 * - the tail end of one table
	 * - a number of complete tables
	 * - the head of one table
	 *
	 * For each table or partial table we want to talk a full
	 * column at a time, then go to the next column.
	 * So we step to the next row.  If it is beyond the range of
	 * the current table, go to 'start' of next column, or to
	 * next table.
	 */
	struct fs_dev *dv = &fs->devs[seg->dev];
	u64 addr = seg_addr(fs, seg);

	if (!(addr+1))
		/* Beyond end of cluster */
		return addr;

	/* now step forward in column or table or seg */
	seg->row++;
	if (seg->row >= dv->rows_per_table ||
	    (seg->table == seg->nxt_table
	     && seg->row >= seg->nxt_row)) {
		seg->col++;
		if (seg->col >= dv->width) {
			seg->col = 0;
			seg->table++;
		}
		if (seg->table == seg->st_table)
			seg->row = seg->st_row;
		else
			seg->row = 0;
	}
	return addr;
}

static void cluster_reset(struct fs *fs, struct wc *wc)
{
	if (wc->seg.dev < 0) {
		wc->remaining = 0;
		wc->chead_size = 0;
		return;
	}
	wc->remaining = seg_remainder(fs, &wc->seg);
	wc->chead_blocks = 1;
	wc->remaining--;
	wc->cluster_space = fs->blocksize - sizeof(struct cluster_head);
	wc->chead = page_address(wc->page[wc->pending_next]);
	wc->chead_size = sizeof(struct cluster_head);
}

static void close_segment(struct fs *fs, int cnum)
{
	/* Release old segment */
	/* FIXME I need lafs_seg_{de,}ref for every snapshot,
	 * don't I ???
	 */
	struct wc *wc = fs->wc + cnum;
	if (wc->seg.dev >= 0) {
		if (cnum) {
			spin_lock(&fs->lock);
			fs->clean_reserved -= seg_remainder(fs, &wc->seg);
			spin_unlock(&fs->lock);
		}
		lafs_seg_forget(fs, wc->seg.dev, wc->seg.num);
		wc->seg.dev = -1;
	}
}

void lafs_close_all_segments(struct fs *fs)
{
	int cnum;
	for (cnum = 0; cnum < WC_NUM; cnum++)
		close_segment(fs, cnum);
}

static int new_segment(struct fs *fs, int cnum)
{
	/* new_segment can fail if cnum > 0 and there is no
	 * clean_reserved
	 */
	struct wc *wc = fs->wc + cnum;
	unsigned int dev;
	u32 seg;

	close_segment(fs, cnum);

	if (cnum && fs->clean_reserved < fs->max_segment) {
		/* we have reached the end of the last cleaner
		 * segment.  The remainder of clean_reserved
		 * is of no value (if there even is any)
		 */
		if (fs->clean_reserved) {
			spin_lock(&fs->alloc_lock);
			fs->free_blocks += fs->clean_reserved;
			fs->clean_reserved = 0;
			spin_unlock(&fs->alloc_lock);
		}
		return -ENOSPC;
	}
	/* This gets a reference on the 'segsum' */
	lafs_free_get(fs, &dev, &seg, 0);
	lafs_seg_setpos(fs, &wc->seg, segtovirt(fs, dev, seg));
	BUG_ON(wc->seg.dev != dev);
	BUG_ON(wc->seg.num != seg);

	if (cnum == 0)
		/* wc mutex protects us here */
		fs->newsegments++;

	return 0;
}

/*-------------------------------------------------------------------------
 * lafs_cluster_allocate
 * This adds a block to the writecluster list and possibly triggers a
 * flush.
 * The flush will assign a physical address to each block and schedule
 * the actual write out.
 *
 * There can be multiple active write clusters. e.g one for new data,
 * one for cleaned data, one for defrag writes.
 *
 * It is here that the in-core inode is copied to the data block, if
 * appropriate, and here that small files get copied into the inode
 * thus avoiding writing a separate block for the file content.
 *
 * Roll-forward sees any data block as automatically extending the
 * size of the file.  This requires that we write blocks in block-order
 * as much as possible (which we would anyway) and that we inform
 * roll-forward when a file ends mid-block.
 *
 * A block should be iolocked when being passed to lafs_cluster_allocate.
 * When the IO is complete, it will be unlocked.
 */
static int flush_data_to_inode(struct block *b)
{
	/* This is the first and only datablock in a file
	 * and it will fit in the inode. So put it there.
	 */

	char *ibuf, *dbuf;
	loff_t size = i_size_read(b->inode);
	struct lafs_inode *lai = LAFSI(b->inode);
	struct datablock *db;
	struct fs *fs = fs_from_inode(b->inode);

	lafs_iolock_block(&lai->iblock->b);
	dprintk("FLUSHING single block into inode - s=%d %s\n",
		(int)size, strblk(b));
	if (lai->iblock->depth > 1)
		/* Too much indexing still - truncate must be happening */
		goto give_up;
	if (lai->iblock->depth == 1) {
		/* If there is nothing in this block we can still use it */
		if (lai->iblock->children.next != &b->siblings ||
		    b->siblings.next != &lai->iblock->children ||
		    lafs_leaf_next(lai->iblock, 1) != 0xffffffff)
			goto give_up;
	}
	/* Need checkpoint lock to pin the dblock */
	lafs_checkpoint_lock(fs);
	if (test_and_clear_bit(B_Dirty, &b->flags)) {
		int credits = 1;
		/* Check size again, just in case it changed */
		size = i_size_read(b->inode);
		if (size + lai->metadata_size > fs->blocksize) {
			/* Lost a race, better give up restoring Dirty bit */
			if (test_and_set_bit(B_Dirty, &b->flags))
				if (test_and_set_bit(B_Credit, &b->flags))
					lafs_space_return(fs, 1);
			lafs_checkpoint_unlock(fs);
			goto give_up;
		}
		if (!test_and_set_bit(B_Credit, &lai->dblock->b.flags))
			credits--;
		if (test_and_clear_bit(B_UnincCredit, &b->flags))
			credits++;
		if (!test_and_set_bit(B_ICredit, &lai->dblock->b.flags))
			credits--;
		lafs_space_return(fs, credits);
		LAFS_BUG(!test_bit(B_SegRef, &lai->dblock->b.flags), &lai->dblock->b);
		set_bit(B_PinPending, &lai->dblock->b.flags);
		if (test_bit(B_Pinned, &b->flags))
			lafs_pin_block_ph(&lai->dblock->b, !!test_bit(B_Phase1, &b->flags));
		else
			lafs_pin_block(&lai->dblock->b);
		lafs_dirty_dblock(lai->dblock);
	} else if (test_and_clear_bit(B_Realloc, &b->flags)) {
		int credits = 1;
		LAFS_BUG(!test_bit(B_Valid, &lai->iblock->b.flags),
			 &lai->iblock->b);
		if (test_and_clear_bit(B_UnincCredit, &b->flags))
			credits++;
		if (!test_and_set_bit(B_Realloc, &lai->iblock->b.flags))
			credits--;
		if (!test_and_set_bit(B_UnincCredit, &lai->iblock->b.flags))
			credits--;
		LAFS_BUG(credits < 0, b);
		lafs_space_return(fs, credits);
	} else {
		printk("Wasn't dirty or realloc: %s\n", strblk(b));
		LAFS_BUG(1, b);
	}
	lafs_checkpoint_unlock(fs);
	lai->depth = 0;
	lai->iblock->depth = 0;
	clear_bit(B_Valid, &lai->iblock->b.flags);

	/* safe to reference ->dblock as b is a dirty child */
	db = getdref(lai->dblock, MKREF(flush2db));
	ibuf = map_dblock(db);
	dbuf = map_dblock_2(dblk(b));

	if (test_and_clear_bit(B_UnincCredit, &b->flags))
		lafs_space_return(fs, 1);

	/* It is an awkward time to call lafs_inode_fillblock,
	 * so do this one little change manually
	 */
	((struct la_inode *)ibuf)->depth = 0;
	memcpy(ibuf + lai->metadata_size,
	       dbuf, size);
	memset(ibuf + lai->metadata_size + size,
	       0,
	       fs->blocksize - lai->metadata_size - size);
	unmap_dblock_2(db, ibuf);
	unmap_dblock(dblk(b), dbuf);
	lafs_iounlock_block(&lai->iblock->b);

	putdref(db, MKREF(flush2db));
	lafs_refile(b, 0);
	return 1;

give_up:
	lafs_iounlock_block(&lai->iblock->b);
	return 0;
}

int lafs_cluster_allocate(struct block *b, int cnum)
{
	struct fs *fs = fs_from_inode(b->inode);
	struct wc *wc = &fs->wc[cnum];
	struct lafs_inode *lai;
	loff_t size;
	int used;
	int err = 0;

	LAFS_BUG(test_bit(B_Index, &b->flags) &&
		 iblk(b)->uninc_table.pending_cnt > 0, b);

	LAFS_BUG(!test_bit(B_IOLock, &b->flags), b);
	lai = LAFSI(b->inode);

	LAFS_BUG(!test_bit(B_Valid, &b->flags), b);
	LAFS_BUG(!fs->rolled, b);
	LAFS_BUG(lai->flags & File_nonlogged &&
		 !test_bit(B_Index, &b->flags), b);

	/* We cannot change the physaddr of an uniblock in a
	 * different phase to the parent, else the parent will
	 * get the wrong address.
	 */
	LAFS_BUG(test_bit(B_Index, &b->flags) &&
		 test_bit(B_Uninc, &b->flags) &&
		 !!test_bit(B_Phase1, &b->flags) !=
		 !!test_bit(B_Phase1, &b->parent->b.flags), b);

	size = i_size_read(b->inode);
	if (!test_bit(B_Index, &b->flags) &&	      /* it is a datablock */
	    b->fileaddr == 0 &&
	    b->parent == lai->iblock &&		      /* No indexing */
	    lai->type >= TypeBase &&		      /* 'size' is meaningful */
	    size + lai->metadata_size <= fs->blocksize) {
		int success = flush_data_to_inode(b);
		if (success) {
			lafs_summary_update(fs, b->inode, b->physaddr, 0,
					    0, !!test_bit(B_Phase1, &b->flags),
					    test_bit(B_SegRef, &b->flags));
			b->physaddr = 0;
			lafs_iounlock_block(b);
			return 0;
		}
		/* There are conflicting blocks in the index tree */
		if (test_and_clear_bit(B_Realloc, &b->flags))
			lafs_space_return(fs, 1);
		if (!test_bit(B_Dirty, &b->flags) ||
		    !test_bit(B_Pinned, &b->flags)) {
			/* It is safe to just leave this for later */
			lafs_iounlock_block(b);
			return 0;
		}
		/* We really need to write this, so use a full block. */
		printk("WARNING %s\n", strblk(b));
		WARN_ON(1);
	}

	/* The symbiosis between the datablock and the indexblock for an
	 * inode needs to be dealt with here.
	 * The data block will not normally be written until the InoIdx
	 * block is unpinned or phase-flipped.  However if it is, as the
	 * result of some 'sync' style operation, then we write it even
	 * though the index info might not be uptodate.  The rest of the
	 * content will be safe for roll-forward to pick up, and the rest
	 * will be written when the InoIdx block is ready.
	 *
	 * When the InoIdx block is ready, we don't want to write it as
	 * there is nowhere for it to be incorporated in to.  Rather we
	 * move the pinning and credits across to the Data block
	 * which will subsequently be written.
	 */

	if (test_bit(B_InoIdx, &b->flags)) {
		struct block *b2 = &lai->dblock->b;
		int credits = 0;

		LAFS_BUG(!test_bit(B_Pinned, &b->flags), b);
		if (!test_bit(B_Pinned, &b2->flags)) {
			/* index block pinned, data block not,
			 * carry the pinning across
			 */
			if (b2->parent == NULL &&
			    b->parent)
				b2->parent =
					getiref(b->parent, MKREF(child));
			LAFS_BUG(b->parent != b2->parent, b);
			set_bit(B_PinPending, &b2->flags);  // FIXME is this right?
			set_phase(b2, test_bit(B_Phase1,
					       &b->flags));
			lafs_refile(b2, 0);
		}

		if (cnum) {
			if (test_and_clear_bit(B_Realloc, &b->flags))
				credits++;
			LAFS_BUG(test_bit(B_Dirty, &b->flags), b);
		} else {
			if (test_and_clear_bit(B_Dirty, &b->flags))
				credits++;
			if (test_and_clear_bit(B_Realloc, &b->flags))
				credits++;
		}
		if (test_and_clear_bit(B_UnincCredit, &b->flags))
			credits++;

		/* We always erase the InoIdx block before the
		 * inode data block, so this cannot happen
		 */
		LAFS_BUG(!test_bit(B_Valid, &b2->flags), b2);

		if (cnum == 0) {
			if (!test_and_set_bit(B_UnincCredit, &b2->flags))
				if (!test_and_clear_bit(B_ICredit, &b2->flags))
					credits--;

			if (!test_bit(B_Dirty, &b2->flags))
				/* need some credits... */
				if (!test_and_set_bit(B_Credit, &b2->flags))
					credits--;
			lafs_dirty_dblock(dblk(b2));
		} else {
			if (!test_and_set_bit(B_UnincCredit, &b2->flags))
				credits--;

			if (!test_and_set_bit(B_Realloc, &b2->flags))
				credits--;
		}
		LAFS_BUG(credits < 0, b2);
		lafs_space_return(fs, credits);
		/* make sure 'dirty' status is registered */
		lafs_refile(b2, 0);
		lafs_iounlock_block(b);
		return 0;
	}

	if (!test_bit(B_Dirty, &b->flags) &&
	    !test_bit(B_Realloc, &b->flags)) {
		/* block just got truncated, so don't write it. */
		lafs_iounlock_block(b);
		return 0;
	}

	if (test_bit(B_EmptyIndex, &b->flags)) {
		int credits = 0;
		if (test_and_set_bit(B_Writeback, &b->flags))
			LAFS_BUG(1, b);
		lafs_iounlock_block(b);
		lafs_allocated_block(fs, b, 0);
		if (cnum) {
			if (test_and_clear_bit(B_Realloc, &b->flags))
				credits++;
			LAFS_BUG(test_bit(B_Dirty, &b->flags), b);
		} else {
			if (test_and_clear_bit(B_Dirty, &b->flags))
				credits++;
			if (test_and_clear_bit(B_Realloc, &b->flags))
				credits++;
		}
		lafs_writeback_done(b);
		lafs_space_return(fs, credits);
		return 0;
	}

	if (!test_bit(B_Index, &b->flags)) {
		struct inode *myi = rcu_my_inode(dblk(b));
		if (myi && test_and_clear_bit(I_Dirty, &LAFSI(myi)->iflags))
			lafs_inode_fillblock(myi);
		rcu_iput(myi);
	}

	mutex_lock(&wc->lock);

	/* The following check must be inside the mutex_lock
	 * to ensure exclusion between checkpoint and writepage.
	 * checkpoint must never see the block not on the
	 * leaf lru unless it is already in the cluster and so can
	 * be waited for.
	 */
	if (!list_empty_careful(&b->lru)) {
		/* Someone is flushing this block before
		 * checkpoint got to it - probably writepage.
		 * Must remove it from the leaf lru so it can go
		 * on the cluster list.
		 */
		LAFS_BUG(test_bit(B_OnFree, &b->flags), b);
		spin_lock(&fs->lock);
		if (!list_empty(&b->lru))
			list_del_init(&b->lru);
		spin_unlock(&fs->lock);
	}

	if (test_and_set_bit(B_Writeback, &b->flags))
		LAFS_BUG(1, b);
	lafs_iounlock_block(b);

	getref(b, MKREF(cluster)); /* we soon own a reference in the list */

	for (;;) {
		int avail;
		/* see how much room is in cluster.
		 * The interesting values are:
		 * none, one descriptor, one group_head and one descriptor
		 * These are assigned values 0, 1, 2.
		 */
		if (wc->cluster_space >= (sizeof(struct group_head) +
					  sizeof(struct descriptor)) ||
		    (wc->remaining > 2 &&
		     wc->chead_blocks < MAX_CHEAD_BLOCKS &&
		     (wc->chead_blocks+1) * fs->blocksize <= PAGE_SIZE)
			)
			avail = 2;
		else if (wc->cluster_space >= sizeof(struct descriptor))
			avail = 1;
		else
			avail = 0;
		if (wc->seg.dev < 0 || wc->remaining < 1)
			used = -1;
		else
			used = cluster_insert(&wc->slhead, &wc->clhead, b, avail);

		if (used >= 0)
			break;
		else if (wc->slhead.b)
			cluster_flush(fs, cnum);
		else {
			err = new_segment(fs, cnum);
			if (err) {
				/* No cleaner segments - this will have to go
				 * out with a checkpoint.
				 * Block is still 'dirty' - just clear
				 * writeback flag.
				 */
				lafs_writeback_done(b);
				putref(b, MKREF(cluster));
				mutex_unlock(&wc->lock);
				return -ENOSPC;
			}
			cluster_reset(fs, wc);
		}
	}

	if (used > 0)
		wc->cluster_space -= sizeof(struct descriptor);
	if (used > 1)
		wc->cluster_space -= sizeof(struct group_head);
	if (wc->cluster_space < 0) {
		/* need a new page */
		wc->chead_blocks++;
		wc->remaining--;
		wc->cluster_space += fs->blocksize;
	}
	wc->remaining--;
	BUG_ON(wc->remaining < 0);
	if (wc->remaining == 0)
		cluster_flush(fs, cnum);
	mutex_unlock(&wc->lock);
	return 0;
}

/*-------------------------------------------------------------------------
 * Cluster head building.
 * We build the cluster head bit by bit as we find blocks
 * in the list.  These routines help.
 */

static void cluster_addhead(struct wc *wc, struct inode *ino,
			    struct group_head **headstart)
{
	struct group_head *gh = (struct group_head *)((char *)wc->chead +
						      wc->chead_size);
	u16 tnf;
	u64 tstamp;
	dprintk("CLUSTER addhead %d\n", wc->chead_size);
	*headstart = gh;

	gh->inum = cpu_to_le32(ino->i_ino);
	gh->fsnum = cpu_to_le32(LAFSI(ino)->filesys->i_ino);
	if (LAFSI(ino)->type >= TypeBase) {
		tstamp = encode_time(&ino->i_mtime);
		if (tstamp != encode_time(&ino->i_ctime))
			tstamp = 0;
	} else
		tstamp = 0;
	gh->timestamp = cpu_to_le64(tstamp);

	tnf = ((ino->i_generation<<8) | (LAFSI(ino)->trunc_gen & 0xff))
		& 0x7fff;
	if (wc->cnum)
		tnf |= 0x8000;
	gh->truncatenum_and_flag = cpu_to_le16(tnf);
	wc->chead_size += sizeof(struct group_head);
}

static void cluster_closehead(struct wc *wc,
			      struct group_head *headstart)
{
	int size = wc->chead_size - (((char *)headstart) - (char *)wc->chead);

	dprintk("CLUSTER closehead %d %d\n", wc->chead_size, size);
	headstart->group_size_words = size / 4;
}

static void cluster_addmini(struct wc *wc, u32 addr, int offset,
			    int size, const char *data,
			    int size2, const char *data2)
{
	/* if size2 !=0, then only
	 * (size-size2) is at 'data' and the rest is at 'data2'
	 */
	struct miniblock *mb = ((struct miniblock *)
				((char *)wc->chead + wc->chead_size));

	dprintk("CLUSTER addmini %d %d\n", wc->chead_size, size);

	mb->block_num = cpu_to_le32(addr);
	mb->block_offset = cpu_to_le16(offset);
	mb->length = cpu_to_le16(size + DescMiniOffset);
	wc->chead_size += sizeof(struct miniblock);
	memcpy(mb->data, data, size-size2);
	if (size2)
		memcpy(mb->data + size-size2, data2, size2);
	memset(mb->data+size, 0, (-size)&3);
	wc->chead_size += ROUND_UP(size);
}

static void cluster_adddesc(struct wc *wc, struct block *blk,
			    struct descriptor **desc_start)
{
	struct descriptor *dh = (struct descriptor *)((char *)wc->chead +
						      wc->chead_size);
	*desc_start = dh;
	dprintk("CLUSTER add_desc %d\n", wc->chead_size);
	dh->block_num = cpu_to_le32(blk->fileaddr);
	dh->block_cnt = cpu_to_le32(0);
	dh->block_bytes = 0;
	if (test_bit(B_Index, &blk->flags))
		dh->block_bytes = DescIndex;
	wc->chead_size += sizeof(struct descriptor);
}

static void cluster_incdesc(struct wc *wc, struct descriptor *desc_start,
			    struct block *b, int blkbits)
{
	desc_start->block_cnt =
		cpu_to_le32(le32_to_cpu(desc_start->block_cnt)+1);
	if (!test_bit(B_Index, &b->flags)) {
		if (LAFSI(b->inode)->type >= TypeBase) {
			u64 size = b->inode->i_size;
			if (size > ((loff_t)b->fileaddr << blkbits) &&
			    size <= ((loff_t)(b->fileaddr + 1) << blkbits))
				desc_start->block_bytes =
					cpu_to_le32(size & ((1<<blkbits)-1));
			else
				desc_start->block_bytes =
					cpu_to_le32(1 << blkbits);
		} else
			desc_start->block_bytes = cpu_to_le32(1<<blkbits);
	}
}

/*------------------------------------------------------------------------
 * Miniblocks/updates
 * Updates are added to the cluster head immediately, causing a flush
 * if there is insufficient room.
 * This means that miniblocks always occur before block descriptors.
 * miniblocks are only ever written to cluster-series 0.
 */

int
lafs_cluster_update_prepare(struct update_handle *uh, struct fs *fs,
			    int len)
{
	/* make there there is room for a 'len' update in upcoming
	 * cluster.  Do nothing here.
	 */
	uh->fs = fs;
	uh->reserved = 0;
	return 0;
}

int
lafs_cluster_update_pin(struct update_handle *uh)
{
	/* last chance to bail out */
	if (lafs_space_alloc(uh->fs, 1, ReleaseSpace)) {
		uh->reserved = 1;
		uh->fs->cluster_updates++;
		return 0;
	}
	return -EAGAIN;
}

static unsigned long long
lafs_cluster_update_commit_both(struct update_handle *uh, struct fs *fs,
				struct inode *ino, u32 addr,
				int offset, int len, struct datablock *b,
				const char *str1,
				int len2, const char *str2)
{
	/* There is the data - alternate layout */
	/* flush 'size' bytes of data, as 'offset' of bnum in ino
	 * in the next write cluster
	 */
	struct wc *wc = &fs->wc[0];
	struct group_head *head_start;
	unsigned long long seq;
	char *mapping = NULL;

	BUG_ON(!fs->rolled);

	mutex_lock(&wc->lock);
	while (wc->cluster_space < (sizeof(struct group_head)+
				    sizeof(struct descriptor) +
				    ROUND_UP(len))) {
		if (wc->remaining > 0 && wc->chead_blocks < MAX_CHEAD_BLOCKS &&
		    (wc->chead_blocks+1) * fs->blocksize <= PAGE_SIZE) {
			wc->chead_blocks++;
			wc->remaining--;
			wc->cluster_space += fs->blocksize;
		} else
			cluster_flush(fs, 0);
	}

	fs->cluster_updates -= uh->reserved;
	lafs_space_return(fs, uh->reserved);
	uh->reserved = 0;

	cluster_addhead(wc, ino, &head_start);
	if (b) {
		mapping = map_dblock(b);
		str1 = mapping + offset;
	}
	cluster_addmini(wc, addr, offset, len, str1, len2, str2);
	if (b)
		unmap_dblock(b, mapping);
	cluster_closehead(wc, head_start);
	wc->cluster_space -= (sizeof(struct group_head)+
			      sizeof(struct descriptor) +
			      ROUND_UP(len));
	seq = wc->cluster_seq;
	mutex_unlock(&wc->lock);
	return seq;
}

unsigned long long
lafs_cluster_update_commit(struct update_handle *uh,
			   struct datablock *b,
			   int offset, int len)
{
	/* OK here is the data to put in the next cluster. */
	struct fs *fs = fs_from_inode(b->b.inode);
	unsigned long long seq;

	seq = lafs_cluster_update_commit_both(uh, fs, b->b.inode, b->b.fileaddr,
					      offset, len, b, NULL, 0, NULL);

	return seq;
}

unsigned long long
lafs_cluster_update_commit_buf(struct update_handle *uh, struct fs *fs,
			       struct inode *ino, u32 addr,
			       int offset, int len, const char *str1,
			       int len2, const char *str2)
{
	return lafs_cluster_update_commit_both(uh, fs,
					       ino, addr,
					       offset, len,
					       NULL,
					       str1, len2, str2);
}

void
lafs_cluster_update_abort(struct update_handle *uh)
{
	/* Didn't want that cluster_update after all */
	uh->fs->cluster_updates -= uh->reserved;
	lafs_space_return(uh->fs, uh->reserved);
}

int lafs_calc_cluster_csum(struct cluster_head *head)
{
	unsigned int oldcsum = head->checksum;
	unsigned long csum;

	head->checksum = 0;
	csum = crc32_le(0, (unsigned char *)head, le16_to_cpu(head->Hlength));
	head->checksum = oldcsum;
	return csum;
}

/*------------------------------------------------------------------------
 * Cluster flushing.
 * Once we have a suitable number of blocks and updates in a writecluster
 * we need to write them out.
 * We first iterate over the block list building the clusterhead.
 * Then we finalise and write the clusterhead and iterate again
 * writing out each block.
 * We keep a biassed blockcount in the wc, and decrement it on write-completion.
 * Once the blockcount hits the bias the cluster is written.  Then when
 * the next (or next-plus-one) cluster head is written we remove the bias
 * and the data is deemed to be safe.
 *
 * Update blocks will already have been included in the write cluster.
 *
 * We reinitialise the write cluster once we are finished.
 */

static void cluster_done(struct fs *fs, struct wc *wc)
{
	/* This is called when one of the pending_cnts hit 0, indicating
	 * that a write-cluster has been written and committed to storage.
	 * We find all the pending blocks on a completed cluster and
	 * acknowledge them.
	 */
	int i;
	struct block *b, *tmp;
	struct list_head tofree;

	INIT_LIST_HEAD(&tofree);

	spin_lock(&fs->lock);
	for (i = 0; i < 4 ; i++)
		if (atomic_read(&wc->pending_cnt[i]) == 0)
			list_splice_init(&wc->pending_blocks[i], &tofree);
	spin_unlock(&fs->lock);

	list_for_each_entry_safe(b, tmp, &tofree, lru) {
		list_del_init(&b->lru);
		lafs_writeback_done(b);
		putref(b, MKREF(cluster));
	}
}

void lafs_clusters_done(struct fs *fs)
{
	int i;
	for (i = 0 ; i < WC_NUM; i++)
		cluster_done(fs, &fs->wc[i]);
}

void lafs_done_work(struct work_struct *ws)
{
	struct fs *fs = container_of(ws, struct fs, done_work);
	lafs_clusters_done(fs);
}

static void cluster_flush(struct fs *fs, int cnum)
{
	struct wc *wc = &fs->wc[cnum];
	int i;
	struct block *b;
	struct inode *current_inode = NULL;
	u64 current_block = NoBlock;
	u64 head_addr[MAX_CHEAD_BLOCKS];
	struct descriptor *desc_start = NULL;
	struct group_head *head_start = NULL;
	struct segpos segend;
	int which;
	int wake;
	int vtype;
	int cluster_size;

	if (!test_bit(CheckpointOpen, &fs->fsstate)) {
		/* First cluster since a checkpoint, make sure
		 * we do another checkpoint within 30 seconds
		 */
		fs->next_checkpoint = jiffies + 30 * HZ;
		set_bit(CheckpointOpen, &fs->fsstate);
		lafs_wake_thread(fs);
	}
	/* set the writecluster size as this will guide layout in
	 * some cases
	 */

	if (test_and_clear_bit(FlushNeeded, &fs->fsstate))
		set_bit(SecondFlushNeeded, &fs->fsstate);
	else
		clear_bit(SecondFlushNeeded, &fs->fsstate);

	cluster_size = lafs_seg_setsize(fs, &wc->seg,
					seg_remainder(fs, &wc->seg)
					- wc->remaining);

	/* find, and step over, address header block(s) */
	for (i = 0; i < wc->chead_blocks ; i++)
		head_addr[i] = lafs_seg_next(fs, &wc->seg);

	list_for_each_entry(b, &wc->clhead, lru) {
		u64 addr;
		if (b->inode != current_inode) {
			/* need to create a new group_head */
			desc_start = NULL;
			if (head_start)
				cluster_closehead(wc, head_start);
			cluster_addhead(wc, b->inode, &head_start);
			current_inode = b->inode;
			current_block = NoBlock;
		}
		if (desc_start == NULL || b->fileaddr != current_block+1 ||
		    test_bit(B_Index, &b->flags)) {
			cluster_adddesc(wc, b, &desc_start);
			current_block = b->fileaddr;
		} else
			current_block++;
		cluster_incdesc(wc, desc_start, b, fs->blocksize_bits);
		addr = lafs_seg_next(fs, &wc->seg);
		BUG_ON(!addr || !(addr+1));
		if (cnum && test_bit(B_Dirty, &b->flags))
			/* We are cleaning but this block is now dirty.
			 * Don't waste the UnincCredit on recording the
			 * clean location as it will be written as
			 * dirty soon.  Just pin it to the current phase
			 * to ensure that it really does get written.
			 */
			lafs_pin_block(b);
		else
			lafs_allocated_block(fs, b, addr);
	}

	if (head_start)
		cluster_closehead(wc, head_start);
	segend = wc->seg; /* We may write zeros from here */
	seg_step(fs, &wc->seg);
	wc->remaining = seg_remainder(fs, &wc->seg);
	/* Need to make sure our ->next_addr gets set properly
	 * for non-cleaning segments
	 */
	if (wc->remaining < 2) {
		if (cnum == 0)
			new_segment(fs, cnum);
		else
			close_segment(fs, cnum);
	}

	/* Fill in the cluster header */
	strncpy(wc->chead->idtag, "LaFSHead", 8);
	wc->chead->flags = cpu_to_le32(0);

	/* checkpoints only happen in cnum==0 */
	if (cnum == 0 && fs->checkpointing) {
		spin_lock(&fs->lock);
		wc->chead->flags = cpu_to_le32(fs->checkpointing);
		if (fs->checkpointing & CH_CheckpointEnd) {
			fs->checkpointing = 0;
			clear_bit(CheckpointOpen, &fs->fsstate);
		} else if (fs->checkpointing & CH_CheckpointStart) {
			fs->checkpointcluster = head_addr[0];
			fs->checkpointing &= ~CH_CheckpointStart;
		}
		spin_unlock(&fs->lock);
		if (!fs->checkpointing)
			wake_up(&fs->phase_wait);
	}

	memcpy(wc->chead->uuid, fs->state->uuid, 16);
	wc->chead->seq = cpu_to_le64(wc->cluster_seq);
	wc->cluster_seq++;
	wc->chead->Hlength = cpu_to_le16(wc->chead_size);
	wc->chead->Clength = cpu_to_le16(cluster_size);
	spin_lock(&fs->lock);
	if (cnum)
		fs->clean_reserved -= cluster_size;
	else
		fs->free_blocks -= cluster_size;
	spin_unlock(&fs->lock);
	/* FIXME if this is just a header, no data blocks,
	 * then use VerifyNull.  Alternately if there is
	 * no-one waiting for a sync to complete (how do we
	 * track that?) use VerifyNext2
	 */
	vtype = VerifyNext;
	if (cnum)
		vtype = VerifyNull;
	wc->pending_vfy_type[wc->pending_next] = vtype;
	wc->chead->verify_type = cpu_to_le16(vtype);
	memset(wc->chead->verify_data, 0, 16);

	wc->chead->next_addr = cpu_to_le64(seg_addr(fs, &wc->seg));
	wc->chead->prev_addr = cpu_to_le64(wc->prev_addr);
	wc->prev_addr = head_addr[0];
	wc->chead->this_addr = cpu_to_le64(wc->prev_addr);

	wc->chead->checksum = lafs_calc_cluster_csum(wc->chead);

	/* We cannot write the header until any previous cluster
	 * which uses the header as a commit block has been
	 * fully written
	 */
	which = (wc->pending_next+3)%4;
	dprintk("AA which=%d vt=%d pc=%d\n", which, wc->pending_vfy_type[which],
		atomic_read(&wc->pending_cnt[which]));
	if (wc->pending_vfy_type[which] == VerifyNext)
		wait_event(wc->pending_wait,
			   atomic_read(&wc->pending_cnt[which]) == 1);
	which = (which+3) % 4;
	dprintk("AB which=%d vt=%d pc=%d\n", which, wc->pending_vfy_type[which],
		atomic_read(&wc->pending_cnt[which]));
	if (wc->pending_vfy_type[which] == VerifyNext2)
		wait_event(wc->pending_wait,
			   atomic_read(&wc->pending_cnt[which]) == 1);

	lafs_clusters_done(fs);
	dprintk("cluster_flush pre-bug pending_next=%d cnt=%d\n",
		wc->pending_next, atomic_read(&wc->pending_cnt
					      [wc->pending_next]));
	BUG_ON(atomic_read(&wc->pending_cnt[wc->pending_next]) != 0);
	BUG_ON(!list_empty(&wc->pending_blocks[wc->pending_next]));

	/* initialise pending count to 1.  This will be decremented
	 * once all related blocks have been submitted for write.
	 * This includes the blocks in the next 0, 1, or 2
	 * write clusters.
	 */
	atomic_inc(&wc->pending_cnt[wc->pending_next]);

	/* Now write it all out.
	 * Later we should possibly re-order the writes
	 * for raid4 stripe-at-a-time
	 */
	for (i = 0; i < wc->chead_blocks; i++)
		lafs_write_head(fs,
				page_address(wc->page[wc->pending_next])
				+ i * fs->blocksize,
				head_addr[i], wc);

	while (!list_empty(&wc->clhead)) {
		int credits = 0;
		b = list_entry(wc->clhead.next, struct block, lru);
		dprintk("flushing %s\n", strblk(b));

		if (test_and_clear_bit(B_Realloc, &b->flags))
			credits++;
		if (cnum == 0) {
			if (test_and_clear_bit(B_Dirty, &b->flags))
				credits++;
		}

		WARN_ON(credits == 0);
		lafs_space_return(fs, credits);
		spin_lock(&fs->lock);
		list_del(&b->lru);
		list_add(&b->lru, &wc->pending_blocks[(wc->pending_next+3)%4]);
		spin_unlock(&fs->lock);
		lafs_write_block(fs, b, wc);
		lafs_refile(b, 0);
	}
	skip_discard(wc->slhead.next[0]);
	/* FIXME write out zeros to pad raid4 if needed */

	/* Having submitted this request we need to remove the bias
	 * from pending_cnt of previous clusters
	 */
	which = wc->pending_next;
	wake = 0;
	dprintk("A which=%d vt=%d pc=%d\n", which, wc->pending_vfy_type[which],
		atomic_read(&wc->pending_cnt[which]));
	if (wc->pending_vfy_type[which] == VerifyNull)
		if (atomic_dec_and_test(&wc->pending_cnt[which]))
			wake = 1;
	which = (which+3) % 4;
	dprintk("B which=%d vt=%d pc=%d\n", which, wc->pending_vfy_type[which],
		atomic_read(&wc->pending_cnt[which]));
	if (wc->pending_vfy_type[which] == VerifyNext)
		if (atomic_dec_and_test(&wc->pending_cnt[which]))
			wake = 1;
	which = (which+3) % 4;
	dprintk("C which=%d vt=%d pc=%d\n", which, wc->pending_vfy_type[which],
		atomic_read(&wc->pending_cnt[which]));
	if (wc->pending_vfy_type[which] == VerifyNext2)
		if (atomic_dec_and_test(&wc->pending_cnt[which]))
			wake = 1;
	if (wake) {
		cluster_done(fs, wc);
		wake_up(&wc->pending_wait);
	}
	dprintk("D %d\n", wake);

	lafs_write_flush(fs, wc);

	wc->pending_next = (wc->pending_next+1) % 4;
	/* now re-initialise the cluster information */
	cluster_reset(fs, wc);

	wait_event(wc->pending_wait,
		   atomic_read(&wc->pending_cnt[wc->pending_next]) == 0);
	dprintk("cluster_flush end pending_next=%d cnt=%d\n",
		wc->pending_next, atomic_read(&wc->pending_cnt
					      [wc->pending_next]));
}

void lafs_cluster_flush(struct fs *fs, int cnum)
{
	struct wc *wc = &fs->wc[cnum];
	dprintk("LAFS_cluster_flush %d\n", cnum);
	mutex_lock(&wc->lock);
	if (!list_empty(&wc->clhead)
	    || wc->chead_size > sizeof(struct cluster_head)
	    || (cnum == 0 &&
		((fs->checkpointing & CH_CheckpointEnd)
		 || test_bit(FlushNeeded, &fs->fsstate)
		 || test_bit(SecondFlushNeeded, &fs->fsstate)
			)))
		cluster_flush(fs, cnum);
	mutex_unlock(&wc->lock);
}

void lafs_cluster_wait_all(struct fs *fs)
{
	int i;
	for (i = 0; i < WC_NUM; i++) {
		struct wc *wc = &fs->wc[i];
		int j;
		for (j = 0; j < 4; j++) {
			wait_event(wc->pending_wait,
				   atomic_read(&wc->pending_cnt[j]) <= 1);
		}
	}
}

/* The end_io function for writes to a cluster is one
 * of 4 depending on which in the circular list the
 * block is for.
 */
static void cluster_end_io(struct bio *bio, int err,
		   int which, int header)
{
	/* FIXME how to handle errors? */
	struct wc *wc = bio->bi_private;
	struct fs *fs = container_of(wc, struct fs, wc[wc->cnum]);
	int wake = 0;
	int done = 0;

	dprintk("end_io err=%d which=%d header=%d\n",
		err, which, header);

	if (atomic_dec_and_test(&wc->pending_cnt[which]))
		done++;
	if (atomic_read(&wc->pending_cnt[which]) == 1)
		wake = 1;
	which = (which+3) % 4;
	if (header &&
	    (wc->pending_vfy_type[which] == VerifyNext ||
	     wc->pending_vfy_type[which] == VerifyNext2) &&
	    atomic_dec_and_test(&wc->pending_cnt[which]))
		done++;
	if (atomic_read(&wc->pending_cnt[which]) == 1)
		wake = 1;

	which = (which+3) % 4;
	if (header &&
	    wc->pending_vfy_type[which] == VerifyNext2 &&
	    atomic_dec_and_test(&wc->pending_cnt[which]))
		done++;
	if (atomic_read(&wc->pending_cnt[which]) == 1)
		wake = 1;

	if (done)
		schedule_work(&fs->done_work);
	if (done || wake) {
		wake_up(&wc->pending_wait);
		if (test_bit(FlushNeeded, &fs->fsstate) ||
		    test_bit(SecondFlushNeeded, &fs->fsstate))
			lafs_wake_thread(fs);
	}
}

static void cluster_endio_data_0(struct bio *bio, int err)
{
	cluster_end_io(bio, err, 0, 0);
}

static void cluster_endio_data_1(struct bio *bio, int err)
{
	cluster_end_io(bio, err, 1, 0);
}

static void cluster_endio_data_2(struct bio *bio, int err)
{
	cluster_end_io(bio, err, 2, 0);
}

static void cluster_endio_data_3(struct bio *bio, int err)
{
	cluster_end_io(bio, err, 3, 0);
}

static void cluster_endio_header_0(struct bio *bio, int err)
{
	cluster_end_io(bio, err, 0, 1);
}

static void cluster_endio_header_1(struct bio *bio, int err)
{
	cluster_end_io(bio, err, 1, 1);
}

static void cluster_endio_header_2(struct bio *bio, int err)
{
	cluster_end_io(bio, err, 2, 1);
}

static void cluster_endio_header_3(struct bio *bio, int err)
{
	cluster_end_io(bio, err, 3, 1);
}

bio_end_io_t *lafs_cluster_endio_choose(int which, int header)
{
	if (header)
		if ((which&2) == 0)
			if (which == 0)
				return cluster_endio_header_0;
			else
				return cluster_endio_header_1;
		else
			if (which == 2)
				return cluster_endio_header_2;
			else
				return cluster_endio_header_3;
	else
		if ((which&2) == 0)
			if (which == 0)
				return cluster_endio_data_0;
			else
				return cluster_endio_data_1;
		else
			if (which == 2)
				return cluster_endio_data_2;
			else
				return cluster_endio_data_3;
}

int lafs_cluster_init(struct fs *fs, int cnum, u64 addr, u64 prev, u64 seq)
{
	struct wc *wc = &fs->wc[cnum];
	int i;

	wc->slhead.b = NULL;
	INIT_LIST_HEAD(&wc->clhead);

	for (i = 0; i < 4 ; i++) {
		wc->pending_vfy_type[i] = VerifyNull;
		atomic_set(&wc->pending_cnt[i], 0);
		wc->page[i] = alloc_page(GFP_KERNEL);
		if (!wc->page[i])
			break;
	}
	if (i < 4) {
		while (i > 0) {
			i--;
			put_page(wc->page[i]);
		}
		return -ENOMEM;
	}
	wc->pending_next = 0;
	wc->cluster_seq = seq;
	wc->prev_addr = prev;
	wc->cnum = cnum;

	if (addr) {
		lafs_seg_setpos(fs, &wc->seg, addr);
		cluster_reset(fs, wc);
		BUG_ON(cnum);
		spin_lock(&fs->lock);
		fs->free_blocks += wc->remaining+1;
		spin_unlock(&fs->lock);
	}

	return 0;
}

void lafs_flush(struct datablock *b)
{
	/* Need to write this block out and wait until
	 * it has been written, so that we can update it
	 * without risking corruption to previous snapshot.
	 *
	 */
}

int lafs_cluster_empty(struct fs *fs, int cnum)
{
	return list_empty(&fs->wc[cnum].clhead);
}

