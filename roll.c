
/*
 * fs/lafs/roll.c
 * Copyright (C) 2005-2010
 * Neil Brown <neilb@suse.de>
 * Released under the GPL, version 2
 *
 * 'rollforward'
 *
 * This file handles mounting of a filesystem once the superblocks
 * have been loaded.
 * It loads the root inode (the root of the filesystem, not of the
 * directory tree) and then handles roll-forward to pick up and changes
 * there are not in the filesystem yet, either due to a crash, or because
 * they cannot be consistently stored easily (final segusage/quota info).
 *
 * Roll-forward reads write-cluster header and handle things as appropriate.
 * Data blocks are only processed if they belong to:
 *   - regular files
 *   - segusage files
 *   - quota files.
 * A data block in a regular file implies an extension of the file size
 * to the end of the block, if it was previously at or before the start
 * of the block.  Datablocks that were just moved for cleaning are
 * ignored.
 *
 * Index blocks are always ignored - they need to be recalculated.
 *
 * 'miniblocks' or 'updates' are always processed - they represent an
 * atomic update that might affect multiple files - those files for which
 * data blocks are ignored.
 * Updates are understood:
 * - for inodes.  The update simply over-writes part of the inode metadata,
 *       which could affect the link count or size.  Such inodes become
 *       orphans in case truncation or deletion is needed.  This can create
 *       an inode which might affect the inode usage map.
 * - for directories.  The update identifies a name and an inode number.
 *       This can imply a change to the inode's link count and again could
 *       make it an orphan.  In some cases updates are paired, possibly across
 *       different directories.  This is needed for 'rename'.
 *
 * Each write-cluster has three levels of validation.
 *  Firstly, if the header is internally consistent, with correct tag,
 *   uuid, and sequence, then we know a write was attempted, and anything that
 *   must be written before that was successfully written.
 *  Secondly, if the header has a correct checksum, then it is all correct,
 *   and the miniblocks are valid.
 *  Thirdly, if the next or next-but-one header (depending on verify_type) is
 *   internally consistent, than we know that the data blocks in this cluster
 *   were all written successfully.
 */

#include	"lafs.h"
#include	<linux/slab.h>

static int
roll_valid(struct fs *fs, struct cluster_head *ch, unsigned long long addr)
{
	/* return 1 if the cluster_head looks locally valid.
	 * Don't check checksum as we may not have the whole head
	 */
	if (memcmp(ch->idtag, "LaFSHead", 8) != 0)
		return 0;
	if (memcmp(fs->state->uuid, ch->uuid, 16) != 0)
		return 0;
	if (le64_to_cpu(ch->this_addr) != addr)
		return 0;
	switch (le16_to_cpu(ch->verify_type)) {
	case VerifyNull:
	case VerifyNext:
	case VerifyNext2:
		break;
	default:
		return 0;
	}
	if (le16_to_cpu(ch->Clength) > fs->max_segment)
		return 0;
	return 1;
}

/*
 * roll_locate scopes out the full extent of the required roll-forward.
 * It starts at the start of the last checkpoint (recorded in the stateblock)
 * and checks that the end of the checkpoint exists, and continues following
 * the chain as far as valid cluster heads can be found.
 * roll_locate returns 0 if proper endpoints were found,
 * or -EIO if CheckpointStart and CheckpointEnd weren't found properly
 * "next" will contain the address of the next cluster to be written to,
 * "last" the cluster before that, and "seq" the seq number for next cluster
 * "maxp" will be used to report the maximum size of a cluster head.
 */
static int
roll_locate(struct fs *fs, u64 start,
	    u64 *nextp, u64 *lastp, u64 *seqp,
	    int *maxp, struct page *p)
{
	struct cluster_head *ch;
	u64 this, prev, prev2, last, next;
	u64 seq = 0;
	int max = 0;
	int prevtype, prev2type;

	ch = (struct cluster_head *)page_address(p);

	this = start; prev = start;

	/* First we walk through the checkpoint section, which should
	 * all be valid.
	 */
	do {
		if (lafs_load_page(fs, p, this, 1) != 0) {
			printk(KERN_ERR "LaFS: Could not read cluster %llu\n",
			       (unsigned long long) this);
			return -EIO;
		}
		if (!roll_valid(fs, ch, this)) {
			printk(KERN_ERR "LaFS: Bad cluster at %llu\n",
			       (unsigned long long) this);
			return -EIO;
		}
		if (this == start) {
			seq = le64_to_cpu(ch->seq);
			if (!(ch->flags & CH_CheckpointStart)) {
				printk(KERN_ERR "LaFS: Cluster at %llu not CheckpointStart!!\n",
				       (unsigned long long)this);
				return -EIO;
			}
		} else if (seq != le64_to_cpu(ch->seq)) {
			printk(KERN_ERR "LaFS: Cluster sequence bad at %llu: %llu->%llu\n",
			       (unsigned long long)this,
			       (unsigned long long)seq,
			       (unsigned long long)le64_to_cpu(ch->seq));
			return -EIO;
		}

		if (this != start && le64_to_cpu(ch->prev_addr) != prev) {
			printk(KERN_ERR "LaFS: Cluster Linkage error at %llu: %llu != %llu\n",
			       (unsigned long long)this,
			       (unsigned long long)le64_to_cpu(ch->prev_addr),
			       (unsigned long long)prev);
			return -EIO;
		}
		if (!ch->flags & CH_Checkpoint) {
			printk(KERN_ERR "LaFS: Cluster %llu not a Checkpoint cluster\n",
			       (unsigned long long)this);
			return -EIO;
		}
		dprintk("Found seq %llu at %llu\n",
			(unsigned long long)seq, (unsigned long long)this);
		if (le16_to_cpu(ch->Hlength) > max)
			max = le16_to_cpu(ch->Hlength);
		prev = this;
		this = le64_to_cpu(ch->next_addr);
		seq++;
	} while (!(ch->flags & CH_CheckpointEnd));

	/* 'seq' is sequence number of 'this' */
	dprintk("CheckpointEnd found at %llu, seq %llu\n", prev, seq-1);

	/* now we need to step forward a bit more carefully, as any
	 * cluster we find now could easily be bad.
	 * We keep:
	 *   this - address of cluster we are now considering
	 *   prev - address of previous cluster
	 *   prevtype - verify type of previous cluster
	 *   prev2 - address of cluster before prev
	 *   prev2type - verify type of that cluster.
	 *   start - "next_addr" entry from last known-good cluster
	 *
	 *
	 */

	last = prev;
	next = this;
	prev2 = prev;
	prevtype = prev2type = VerifyNull;

	while (1) {
		if (lafs_load_page(fs, p, this, 1) != 0)
			break;
		if (!roll_valid(fs, ch, this))
			break;
		if (le64_to_cpu(ch->prev_addr) != prev)
			break;
		if (le64_to_cpu(ch->seq) != seq)
			break;

		/* this head looks valid, so we can possibly verify previous
		 * clusters
		 */
		if (le16_to_cpu(ch->Hlength) > max)
			max = le16_to_cpu(ch->Hlength);

		if (prev2type == VerifyNext2) {
			next = prev;
			last = prev2;
		}
		if (prevtype == VerifyNext) {
			next = this;
			last = prev;
		}

		/* shift prev info back */
		prev2 = prev;
		prev2type = prevtype;
		prev = this;
		prevtype = le16_to_cpu(ch->verify_type);
		this = le64_to_cpu(ch->next_addr);
		if (prevtype == VerifyNull) {
			next = this;
			last = prev;
		}
		seq++;
	}

	dprintk("LaFS: Next address to write is %llu\n", next);
	*nextp = next;
	*lastp = last;
	if (next == this)
		*seqp = seq;
	else if (next == prev)
		*seqp = seq-1;
	else if (next == prev2)
		*seqp = seq-2;
	else
		BUG();
	*maxp = max;
	return 0;
}

static int __must_check
roll_mini(struct fs *fs, int fsnum, int inum, int trunc,
	  u32 bnum, int offset, int len, char *data)
{
	struct inode *inode;
	struct inode *fsinode;
	struct lafs_inode *li;
	struct datablock *db = NULL;
	int err = 0;
	void *buf;
	char *name;
	struct page *page;
	void *fsdata;

	dprintk("Roll Mini  %d/%d/%lu/%d,%d\n",
		fsnum, inum, (unsigned long) bnum,
		offset, len);

	/* The handling of miniblock updates is quite different for
	 * different objects.
	 *
	 * inode-files: meta-data updates, including size, are allowed.
	 *      index update and data update are not (data update must
	 *      go through the file).  Implied creation requires
	 *      orphan handling
	 * regular-files: We don't create miniblocks for regular files,
	 *      but we might write an inode with embedded data and want
	 *      that data to be safe.  When those inodes are found, at
	 *      miniblock is synthesised from the data so we need to
	 *      handle it.
	 * symlink,dev,pipe: as with reg-files
	 * directory: add/remove entries.  Each miniblock has an address and
	 *      identifies a name, an inode number, and one of:
	 *        LINK  - create a link with this name to the inode
	 *        UNLINK - remove the link
	 *        REN_SOURCE - record this info against the 'address' which must
	 *            be unique in this checkpoint across all directories
	 *        REN_TARGET - The source with matching 'address' is being
	 *            renamed to here.  So unlink the source and either create the
	 *            target (if inode is zero) or replace the target.  This
	 *            miniblock could be in a different directory to the matching
	 *            REN_SOURCE.
	 */

	inode = lafs_iget_fs(fs, fsnum, inum, SYNC);
	if (IS_ERR(inode))
		return PTR_ERR(inode);

	li = LAFSI(inode);

	switch (li->type) {
	default: /* Any unknown type is an error */
		printk(KERN_WARNING "LAFS impossibly file type for roll-forward: %d\n",
		       li->type);
		err = -EIO;
		break;

	case TypeInodeFile:

		if (fsnum) {
			printk(KERN_WARNING "LAFS: Ignoring impossible sub-subset\n");
			break;
		}

		fsinode = inode;
		inode = lafs_iget_fs(fs, inum, bnum, SYNC);
		if (IS_ERR(inode)) {
			err = PTR_ERR(inode);
			if (err != -ENOENT || offset != 0) {
				lafs_iput_fs(fsinode);
				return err;
			}

			db = lafs_get_block(fsinode, bnum, NULL, GFP_KERNEL,
					    MKREF(roll));
			lafs_inode_inuse(fs, fsinode, bnum);
			lafs_iput_fs(fsinode);
			if (!db)
				db = ERR_PTR(-ENOMEM);
		} else {
			lafs_iput_fs(fsinode);
			db = lafs_inode_dblock(inode, SYNC, MKREF(roll));
			if (!IS_ERR(db))
				/* Make sure block is in-sync with inode */
				lafs_inode_fillblock(inode);
		}
		if (IS_ERR(db)) {
			err = PTR_ERR(db);
			break;
		}
		/* Should normally iolock the block, but we don't
		 * need that during roll-forward */
		set_bit(B_PinPending, &db->b.flags);
		lafs_pin_dblock(db, CleanSpace);
		buf = map_dblock(db);
		memcpy(buf+offset, data, len);
		unmap_dblock(db, buf);
		if (inode)
			err = lafs_import_inode(inode, db);
		else {
			inode = lafs_iget_fs(fs, inum, bnum, SYNC);

		}
		lafs_dirty_dblock(db);
		break;

	case TypeDir:
		/* 'bnum' is the handle for match 'rename' parts.
		 * 'offset' is the DIROP type
		 * 'len' is 4 plus length of name.
		 * data contains 4-byte inode number, then name
		 */
		if (len <= 4) {	
			err = -EIO;
			break;
		}
		inum = le32_to_cpu(*(u32*)data);
		name = data + 4;
		err = lafs_dir_roll_mini(inode, bnum, offset, inum, name, len-4);
		break;

	case TypeFile:
	case TypeSymlink:
	case TypeSpecial:
		if (bnum != 0 || offset != 0) {
			/* We currently only expect update at the very start
			 * of a (small) file.
			 * So reject anything else.
			 */
			err = -EIO;
			break;
		}
		err = pagecache_write_begin(NULL, inode->i_mapping,
					    0, len, 0,
					    &page, &fsdata);
		if (!err) {
			char *b = kmap_atomic(page, KM_USER0);
			memcpy(b, data, len);
			kunmap_atomic(b, KM_USER0);
			pagecache_write_end(NULL, inode->i_mapping,
					    0, len, len, page, fsdata);
		}
		break;
	}
	/* We borrow the orphan list to keep a reference on
	 * this inode until all processing is finished
	 * to make sure inodes that are about to get linked
	 * don't get deleted early
	 */
	if (inode->i_nlink == 0) {
		if (!db)
			db = lafs_inode_get_dblock(inode, MKREF(roll));
		if (db &&
		    list_empty(&db->orphans)) {
			list_add(&db->orphans, &fs->pending_orphans);
			lafs_igrab_fs(inode);
			getdref(db, MKREF(roll_orphan));
		}
	}
	putdref(db, MKREF(roll));
	lafs_iput_fs(inode);
	return err;
}

static int __must_check
roll_block(struct fs *fs, int fsnum, int inum, int trunc,
	   u32 bnum, u64 baddr, int bytes, u64 tstamp, struct page *p)
{
	struct inode *inode;
	struct datablock *blk = NULL;
	struct lafs_inode *li;
	int err = 0;

	/* We found this block during roll-forward and need to
	 * include it in the filesystem.
	 * If 'bytes' is 0, the this is a 'hole' and we should
	 * ignore baddr
	 */
	if (bytes == DescHole)
		baddr = 0;

	dprintk("Roll Block %d/%d/%lu/%llu\n",
		fsnum, inum, (unsigned long) bnum,
		(unsigned long long)baddr);

	/* find/load the inode */
	inode = lafs_iget_fs(fs, fsnum, inum, SYNC);
	if (IS_ERR(inode))
		return PTR_ERR(inode);

	/* check type */
	li = LAFSI(inode);

	dprintk("Got the inode, type %d %p size %llu\n", li->type,
		inode, inode->i_size);

	switch (li->type) {
		struct la_inode *lai;
		int mdsize;

	default: /* most filetypes are simply ignored */
		break;

	case TypeInodeFile:
		/* The only part of an inode that might be interesting
		 * is embedded data: All metadata changes get logged
		 * as miniblocks.
		 * Further the data can only be interesting for non-directories,
		 * as directory updates are also logged as miniblocks.
		 * So if this is a depth==0 non-directory inode,
		 * treat the data as a miniblock update.
		 */
		if (bytes != fs->blocksize)
			break;
		err = lafs_load_page(fs, p, baddr, 1);
		dprintk("inode load page err %d\n", err);
		if (err)
			break;
		lai = (struct la_inode *)page_address(p);
		mdsize = le16_to_cpu(lai->metadata_size);
		if (lai->filetype >= TypeBase &&
		    lai->filetype != TypeDir  &&
		    lai->depth == 0 &&
		    mdsize > 1 && mdsize < fs->blocksize) {
			u64 sz = le64_to_cpu(lai->metadata[0].file.size);
			if (sz <= fs->blocksize - mdsize)
				err = roll_mini(fs, inum, bnum, -1, 0, 0,
						(int)sz,
						page_address(p) + mdsize);
		}
		break;

	case TypeSegmentMap:
	case TypeQuota:
		/* These only get merged while in a checkpoint. */
		if (fs->qphase == fs->phase)
			break;
		/* FALL THROUGH */
	case TypeFile:
	case TypeSymlink:
		/* merge into the file and possibly extend inode.size
		 * Only extend the size if it was before this block.
		 * i.e. if size was to the middle of this block, we don't
		 * extend the size
		 */
		dprintk("FILE type\n");
		err = -ENOMEM;
		blk = lafs_get_block(inode, bnum, NULL, GFP_KERNEL,
				     MKREF(roll));
		if (!blk)
			break;

		err = lafs_find_block(blk, ADOPT);
		if (err)
			break;
		if (blk->b.physaddr == baddr)
			/* already correctly indexed */
			break;

		if (li->type >= TypeBase && bytes != DescHole &&
		    inode->i_size <= ((loff_t)bnum << inode->i_blkbits)) {
			inode->i_size = ((loff_t)bnum << inode->i_blkbits) + bytes;
			set_bit(I_Dirty, &LAFSI(inode)->iflags);
		}
		if (tstamp) {
			decode_time(&inode->i_mtime, tstamp);
			decode_time(&inode->i_ctime, tstamp);
			set_bit(I_Dirty, &LAFSI(inode)->iflags);
		}

		/* FIXME: we pretend this is a dirty, pinned block
		 * so the lower-level code doesn't get confused.
		 * Is this really the best approach?
		 * Do I need to release some space here?
		 */
		set_bit(B_PinPending, &blk->b.flags); /* Don't need iolock as no io yet */
		lafs_pin_dblock(blk, CleanSpace); /* cannot fail during ! ->rolled */

		lafs_iolock_block(&blk->b);
		/* The '1' in lafs_summary_update assumes SegRef is set, so
		 * assert that it is.
		 */
		LAFS_BUG(!test_bit(B_SegRef, &blk->b.flags), &blk->b);
		lafs_summary_update(fs, blk->b.inode, blk->b.physaddr, baddr,
				    0, fs->phase, 1);
		blk->b.physaddr = baddr;
		lafs_dirty_iblock(blk->b.parent, 0);
		set_bit(B_Writeback, &blk->b.flags);
		lafs_iounlock_block(&blk->b);

		while (lafs_add_block_address(fs, &blk->b) == 0)
			/* Just like in lafs_phase_flip, there is no special
			 * action required here.
			 */
			;

		dprintk("Allocated block %lu to %llu\n",
			(unsigned long)bnum, baddr);
		lafs_writeback_done(&blk->b);

		clear_bit(B_PinPending, &blk->b.flags);
		/* If we had previously read this block for some reason,
		 * the contents are now invalid.  If they are dirty,
		 * we have a real problem as those changes cannot be saved.
		 */
		LAFS_BUG(test_bit(B_Dirty, &blk->b.flags), &blk->b);
		clear_bit(B_Valid, &blk->b.flags);

		break;
	}
	if (blk)
		putdref(blk, MKREF(roll));

	if (inode->i_nlink == 0) {
		struct datablock *db = lafs_inode_get_dblock(inode, MKREF(roll));
		if (db &&
		    list_empty(&db->orphans)) {
			list_add(&db->orphans, &fs->pending_orphans);
			lafs_igrab_fs(inode);
			getdref(db, MKREF(roll_orphan));
		}
		putdref(db, MKREF(roll));
	}
	lafs_iput_fs(inode);
	dprintk("leaving with error %d\n", err);
	return err;
}

static int __must_check
roll_one(struct fs *fs, u64 *addrp, struct page *p, struct page *pg,
	 int max)
{
	u64 addr = *addrp;
	struct cluster_head *ch = (struct cluster_head *)page_address(p);
	struct group_head *gh;
	struct descriptor *desc;
	int i;
	u64 baddr;
	int err;
	int blocksize = fs->blocksize;
	struct segpos seg;
	int header_blocks;

	/* we "know" buf is big enough */
	err = lafs_load_pages(fs, p, addr, max/blocksize);
	if (err)
		return err;

	/* just minimal checks, as we have looked at this already */
	if (!roll_valid(fs, ch, addr))
		return -EIO;
	if (lafs_calc_cluster_csum(ch) != ch->checksum)
		return -EIO;
	*addrp = le64_to_cpu(ch->next_addr);

	if (le16_to_cpu(ch->Hlength) > max)
		return -EIO;

	lafs_seg_setpos(fs, &seg, addr);
	lafs_seg_setsize(fs, &seg, le16_to_cpu(ch->Clength));
	header_blocks = (le16_to_cpu(ch->Hlength) + blocksize - 1) / blocksize;
	for (i = 0; i < header_blocks; i++) {
		baddr = lafs_seg_next(fs, &seg);
		BUG_ON(baddr != addr + i);
	}

	if (!(ch->flags & CH_Checkpoint))
		fs->qphase = fs->phase;

	gh = ch->groups;
	i = 0;
	while (((char *)gh - (char *)ch) < le16_to_cpu(ch->Hlength)) {
		int j = 0;
		int inum = le32_to_cpu(gh->inum);
		int fsnum = le32_to_cpu(gh->fsnum);
		int trunc = le16_to_cpu(gh->truncatenum_and_flag) & 0x7fff;
		int flg   = le16_to_cpu(gh->truncatenum_and_flag) & 0x8000;
		u64 tstamp = le64_to_cpu(gh->timestamp);

		desc = gh->u.desc;
		while (((char *)desc - (char *)gh) <
		       le16_to_cpu(gh->group_size_words)*4) {
			if (le16_to_cpu(desc->block_bytes) <= DescMiniOffset ||
			    le16_to_cpu(desc->block_bytes) == DescIndex) {
				u32 bnum = le32_to_cpu(desc->block_num);
				int cnt = le16_to_cpu(desc->block_cnt);
				int bytes = le16_to_cpu(desc->block_bytes);

				if (le16_to_cpu(desc->block_bytes) == DescIndex
				    && cnt != 1)
					return -EIO; /* FIXME is this
						      * the best
						      * response */
				/* FIXME range check count */
				while (!err && cnt--) {
					if (bytes != DescHole)
						baddr = lafs_seg_next(fs, &seg);
					if (bytes != DescHole &&
					    !baddr) {
						/* We have fallen off the end of
						 * the write-cluster - something
						 * is wrong with the header
						 */
						printk(KERN_WARNING "LAFS: cluster size is wrong\n");
						return -EIO;
					}
					if (!flg && bytes != DescIndex)
						err = roll_block(fs, fsnum, inum, trunc,
								 bnum, baddr,
								 cnt == 0 || bytes == DescHole
								 ? bytes
								 : blocksize,
								 tstamp,
								 pg);
					bnum++;
				}
				desc++;
			} else {
				struct miniblock *mb = (struct miniblock *)desc;
				u32 bnum = le32_to_cpu(mb->block_num);
				int offset = le16_to_cpu(mb->block_offset);
				int len = le16_to_cpu(mb->length)
					- DescMiniOffset;
				if (!flg)
					err = roll_mini(fs, fsnum, inum, trunc,
							bnum, offset, len, (char *)(mb+1));

				mb++;
				mb = (struct miniblock *)(((char*)mb)
							  + ROUND_UP(len));
				desc = (struct descriptor *)mb;
			}
			j++;
			if (err)
				break;
		}
		gh = (struct group_head *)desc;
		i++;
		if (err)
			break;
	}
	if (ch->flags & CH_CheckpointEnd)
		fs->qphase = fs->phase;
	return err;
}

static int roll_forward(struct fs *fs)
{
	u64 first, next = 0, last = 0, seq = 0;
	int max = 0;
	struct page *p, *pg;
	int err;
	int blocksize = fs->blocksize;
	int dev;
	u32 seg;
	u32 offset;
	int order = 0;
	struct list_head pending;

	fs->phase = 1;
	fs->qphase = 0;
	fs->checkpointing = CH_Checkpoint;
	clear_bit(DelayYouth, &fs->fsstate);

	first = fs->checkpointcluster;
	p = alloc_pages(GFP_KERNEL, order);
	if (!p)
		return -ENOMEM;

	err = roll_locate(fs, first, &next, &last, &seq, &max, p);

	max = ((max + blocksize - 1) / blocksize) * blocksize;

	if (!err && max > PAGE_SIZE) {
		__free_pages(p, order);
		order = get_order(max * blocksize);
		p = alloc_pages(order, GFP_KERNEL);
		if (!p)
			err = -EFBIG;
	}
	if (err) {
		__free_pages(p, order);
		return err;
	}

	pg = alloc_page(GFP_KERNEL);
	if (!pg) {
		__free_pages(p, order);
		return -ENOMEM;
	}

	err = lafs_cluster_init(fs, 0, next, last, seq);
	if (err) {
		__free_pages(p, order); put_page(pg);
		return err;
	}
	lafs_cluster_init(fs, 1, 0, 0, 0);

	virttoseg(fs, first, &dev, &seg, &offset);

	while (first != next) {
		int dev2;
		u32 seg2;

		virttoseg(fs, first, &dev2, &seg2, &offset);
		err = roll_one(fs, &first, p, pg, max);
		if (err)
			break;

		if (fs->qphase == fs->phase &&
		    fs->checkpointing) {
			fs->checkpointing = 0;
			clear_bit(DelayYouth, &fs->fsstate);
			lafs_seg_apply_all(fs);
		}

		if (dev2 != dev || seg2 != seg) {
			/* New segment - need to make sure youth is correct */
			dev = dev2;
			seg = seg2;
			/* if fs->checkpointing, seg_apply_all will do the youth
			 * update
			 */
			if (fs->checkpointing == 0)
				lafs_update_youth(fs, dev, seg);
		}
	}
	__free_pages(p, order);
	put_page(pg);

	lafs_add_active(fs, next);

	/* pending_renames will normally be empty, but it is not
	 * impossible that we crashed and an awkward time.  So just
	 * clean up whatever is there 
	 */
	while (fs->pending_renames != NULL) {
		struct rename_roll *rr = fs->pending_renames;
		fs->pending_renames = rr->next;
		iput(rr->dir);
		iput(rr->inode);
		kfree(rr);
	}


	/* Now we release all the nlink==0 inodes that we found */
	INIT_LIST_HEAD(&pending);
	list_splice_init(&fs->pending_orphans, &pending);
	while (!list_empty(&pending)) {
		struct datablock *db = list_first_entry(&pending,
							struct datablock,
							orphans);
		list_del_init(&db->orphans);
		if (db->my_inode->i_nlink == 0)
			lafs_make_orphan(fs, db);
		lafs_iput_fs(db->my_inode);
		putdref(db, MKREF(roll_orphan));
	}
	fs->rolled = 1;
	return err;
}

int
lafs_mount(struct fs *fs)
{
	struct datablock *b = NULL;
	struct inode *rootino;
	struct inode *rootdir;
	struct inode *aino, *oino;
	struct dentry *de;
	int err;
	int d;
	struct sb_key *k = fs->prime_sb->s_fs_info;
	int orphan_count;

	fs->rolled = 0;
	fs->ss[0].root = rootino = iget_locked(fs->prime_sb, 0);
	k->root = rootino;
	LAFSI(rootino)->filesys = rootino;

	err = -ENOMEM;
	if (!rootino)
		goto err;
	b = lafs_get_block(rootino, 0, NULL, GFP_KERNEL, MKREF(mount));
	if (!b)
		goto err;
	set_bit(B_Root, &b->b.flags);
	b->b.physaddr = fs->ss[0].root_addr;
	set_bit(B_PhysValid, &b->b.flags);
	err = lafs_load_block(&b->b, NULL);
	if (err)
		goto err;
	err = lafs_wait_block(&b->b);
	if (err)
		goto err;

	err = lafs_import_inode(rootino, b);
	if (err)
		goto err;
	putdref(b, MKREF(mount));
	b = NULL;

	unlock_new_inode(rootino);

	rootdir = lafs_iget(rootino, 2, SYNC);
	err = PTR_ERR(rootdir);
	if (IS_ERR(rootdir))
		goto err;
	de = d_alloc_root(rootdir);
	err = PTR_ERR(de);
	if (IS_ERR(de)) {
		iput(rootdir);
		goto err;
	}
	fs->prime_sb->s_root = de;

	oino = lafs_iget(rootino, 8, SYNC);
	err = PTR_ERR(oino);
	if (IS_ERR(oino))
		goto err;
	if (LAFSI(oino)->type != TypeOrphanList) {
		iput(oino);
		err = -EINVAL;
		goto err;
	}
	fs->orphans = oino;
	for (d = 0; d < fs->devices ; d++) {
		struct inode *sino = lafs_iget(rootino,
					       fs->devs[d].usage_inum,
					       SYNC);
		err = PTR_ERR(sino);
		if (IS_ERR(sino))
			goto err;
		if (LAFSI(sino)->type != TypeSegmentMap) {
			iput(sino);
			err = -EINVAL;
			goto err;
		}
		fs->devs[d].segsum = sino;
	}
	orphan_count = lafs_count_orphans(fs->orphans);
	LAFSI(fs->orphans)->md.orphan.nextfree = orphan_count;

	lafs_checkpoint_lock(fs);
	err = roll_forward(fs);
	lafs_checkpoint_unlock(fs);

	lafs_add_orphans(fs, fs->orphans, orphan_count);

	for (d = 0; d < 4; d++) {
		struct page *p = alloc_page(GFP_KERNEL);
		if (!p)
			err = -ENOMEM;
		fs->cleaner.seg[d].chead = p;
		INIT_LIST_HEAD(&fs->cleaner.seg[d].cleaning);
	}

	aino = lafs_iget(rootino, 3, SYNC);
	if (!IS_ERR(aino)) {
		if (LAFSI(aino)->type != TypeAccessTime) {
			iput(aino);
			err = -EINVAL;
		} else
			LAFSI(fs->ss[0].root)->md.fs.accesstime = aino;
	} else if (PTR_ERR(aino) != -ENOENT)
		err = PTR_ERR(aino);

err:
	putdref(b, MKREF(mount));
	return err;
}
