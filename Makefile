#
# Makefile for LaFS
#


ARCH=i386
ifneq ($(KERNELRELEASE),)
# kbuild part

EXTRA_CFLAGS += -Werror -DDUMP -DDEBUG_REF=1 -DDEBUG_IOLOCK=1
obj-m += lafs.o

lafs-y := super.o io.o roll.o dir.o inode.o index.o block.o file.o link.o dir-avl.o \
	snapshot.o quota.o summary.o modify.o checkpoint.o cluster.o orphan.o \
	segments.o clean.o thread.o

else

KERNELDIR := /home/src/lafs-2.6.27
KERNELDIR := /home/src/lafs-2.6.34/OBJ
KERNELDIR := /home/src/lafs-2.6.38/OBJ
all::
	$(MAKE) -C $(KERNELDIR) ARCH=i386 M=`pwd`
	nm lafs.o | grep ' T ' | grep -v lafs_

modules ::
	$(MAKE) -C $(KERNELDIR) M=`pwd` CC=$(CC) modules

test: all
	../utils/tools/lafs test/maketest
	sh test/runtty 2>&1 | tee /tmp/log
	reset

endif
