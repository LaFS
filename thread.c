
/*
 * fs/lafs/thread.c
 * Copyright (C) 2005-2010
 * Neil Brown <neilb@suse.de>
 * Released under the GPL, version 2
 */

#include "lafs.h"
#include <linux/kthread.h>


/*
 * This is the management thread that runs whenever the filesystem is
 * mounted writable.
 * It does a lot more than clean, though that is what it does most of.
 * Where possible, the thread does not block on IO, though it might
 * block on memory allocation.
 * Some tasks need to read in data from disk to complete.  These just
 * schedule the read and signal that they should be re-tried later
 * when the read might have completed.
 *
 * Such reads are marked as async and the 'struct fs' tracks how many
 * async reads are pending.  Tasks are retried when this number gets low.
 *
 * The particular tasks are:
 *  Cleaning. This goes through stages.
 *    choose a segment (or a collection of segments)
 *    Read the write-cluster header for that segment (as there can be
 *          multiple write clusters, we might come back here several times)
 *    Follow the indexes to all blocks that could credibly be in that cluster
 *          and load the block if it is found.
 *    As blocks are found, schedule them for the cleaner cluster.
 *    Occasionally flush the cleaner cluster.
 *
 *  Orphan handling
 *    Orphans are kept on 2 lists using the datablock.orphans list.
 *    - orphans that can be processed now
 *    - orphans that can be processed after some async IO has completed
 *    To process an orphan we call a handler based on the inode type.
 *    This can be TypeInodeFile (for truncates, unlinks) and
 *      TypeDirectory for directory cleaning.
 *    These will need to take an i_mutex.  If they fail, they are put on the
 *      delayed list and will be retried after async IO completes, or a
 *      time has passed.
 *
 *  Run a checkpoint
 *    This blocks any other tasks from running until the checkpoint
 *    finishes.  It will block on writing out the clusters.
 *    Any cleaner-segment will be flushed first
 *    This is triggered on a sys_sync or each time a configurable number of
 *    segments has been written.  In the later case we don't start the
 *    checkpoint until the segments currently being cleaned are finished
 *    with.
 *
 *  Scan the segment usage files.
 *    This is a lazy scan which decays youth if needed, and looks for
 *    segments that should be cleaned or re-used.
 *
 *  ?? Call cluster_flush if a cluster has been pending for a while
 *    This really shouldn't be needed....
 *
 *
 * Every time we wake up, we give every task a chance to do work.
 * Each task is responsible for its own rate-limiting.
 * Each task can return a wakeup time.  We set a timeout to wake at the
 * soonest of these.
 * We may be woken sooner by another process requesting action.
 */


static int lafsd(void *data)
{
	struct fs *fs = data;
	long timeout = MAX_SCHEDULE_TIMEOUT;
	long to;
	set_bit(ThreadNeeded, &fs->fsstate);

	while (!kthread_should_stop()) {
		/* We need to wait INTERRUPTIBLE so that
		 * we don't add to the load-average.
		 * That means we need to be sure no signals are
		 * pending
		 */
		if (signal_pending(current))
			flush_signals(current);

		wait_event_interruptible_timeout
			(fs->async_complete,
			 kthread_should_stop() ||
			 test_bit(ThreadNeeded, &fs->fsstate),
			 timeout);
		clear_bit(ThreadNeeded, &fs->fsstate);

		if (test_bit(FlushNeeded, &fs->fsstate) ||
		    test_bit(SecondFlushNeeded, &fs->fsstate)) {
			/* only push a flush now if it can happen
			 * immediately.
			 */
			struct wc *wc = &fs->wc[0];
			if (mutex_trylock(&wc->lock)) {
				int can_flush = 1;
				int which = (wc->pending_next + 3) % 4;
				if (wc->pending_vfy_type[which] == VerifyNext &&
				    atomic_read(&wc->pending_cnt[which]) > 1)
					can_flush = 0;
				which = (which + 3) % 4;
				if (wc->pending_vfy_type[which] == VerifyNext2 &&
				    atomic_read(&wc->pending_cnt[which]) > 1)
					can_flush = 0;
				mutex_unlock(&wc->lock);
				if (can_flush)
					lafs_cluster_flush(fs, 0);
			}
		}

		timeout = MAX_SCHEDULE_TIMEOUT;
		to = lafs_do_checkpoint(fs);
		if (to < timeout)
			timeout = to;

		to = lafs_run_orphans(fs);
		if (to < timeout)
			timeout = to;

		to = lafs_scan_seg(fs);
		if (to < timeout)
			timeout = to;

		to = lafs_do_clean(fs);
		if (to < timeout)
			timeout = to;

		lafs_clusters_done(fs);
		cond_resched();
	}
	return 0;
}

int lafs_start_thread(struct fs *fs)
{
	if (test_and_set_bit(ThreadRunning, &fs->fsstate))
		return 0; /* already running */

	fs->thread = kthread_run(lafsd, fs, "lafsd-%d", fs->prime_sb->s_dev);
	if (fs->thread == NULL)
		clear_bit(ThreadRunning, &fs->fsstate);
	return fs->thread ? 0 : -ENOMEM;
}

void lafs_stop_thread(struct fs *fs)
{
	if (fs->thread)
		kthread_stop(fs->thread);
	fs->thread = NULL;
}

void lafs_wake_thread(struct fs *fs)
{
	set_bit(ThreadNeeded, &fs->fsstate);
	wake_up(&fs->async_complete);
}

void lafs_trigger_flush(struct block *b)
{
	struct fs *fs = fs_from_inode(b->inode);

	if (test_bit(B_Writeback, &b->flags) &&
	    !test_and_set_bit(FlushNeeded, &fs->fsstate))
		lafs_wake_thread(fs);
}
