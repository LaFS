/*
 * fs/lafs/summary.c
 * Copyright (C) 2005-2009
 * Neil Brown <neilb@suse.de>
 * Released under the GPL, version 2
 */
#include "lafs.h"

/*
 * Every time blocks are allocated or deallocated, we must track them:
 *  per-file
 *  per-filesystem
 *  per-user/group/tree
 */
void lafs_summary_update(struct fs *fs, struct inode *ino,
			 u64 oldphys, u64 newphys,
			 int is_index, int phase, int moveref)
{
	/* Whether writing a block or truncating, we hold a reference
	 * to ->iblock, so can access it without locking
	 */
	struct lafs_inode *lai;
	int future;
	int diff;
	if (oldphys && newphys) {
		// FIXME what snapshot should I use?
		lafs_seg_move(fs, oldphys, newphys, 0, phase, moveref);
		return;
	}
	if (oldphys == 0 && newphys == 0)
		return;

	lai = LAFSI(ino);
	future = (phase != !!test_bit(B_Phase1, &lai->iblock->b.flags));
	if (oldphys)
		diff = -1;
	else
		diff = 1;

	/* per-file */
	spin_lock(&ino->i_lock);
	if (future) {
		if (is_index)
			lai->piblocks += diff;
		else
			lai->pblocks += diff;
	} else {
		if (is_index)
			lai->ciblocks += diff;
		else
			lai->cblocks += diff;
	}
	if (!is_index) {
		if (diff > 0)
			lai->ablocks--;
		else
			ino->i_blocks -=
				1 << (fs->blocksize_bits - 9);
	}
	spin_unlock(&ino->i_lock);

	/* per-filesystem */
	lai = LAFSI(lai->filesys);
	spin_lock(&lai->vfs_inode.i_lock);
	if (future)
		lai->md.fs.pblocks_used += diff;
	else
		lai->md.fs.cblocks_used += diff;

	if (!is_index) {
		BUG_ON(lai->md.fs.ablocks_used == 0 && diff > 0);
		if (diff > 0)
			lai->md.fs.ablocks_used--;
	}
	spin_unlock(&lai->vfs_inode.i_lock);

	/* per user/group/tree */

	lafs_qcommit(fs, ino, diff, phase);

	// FIXME what snapshot should I use?
	lafs_seg_move(fs, oldphys, newphys, 0, phase, moveref);
}

int lafs_summary_allocate(struct fs *fs, struct inode *ino, int diff)
{
	/* this is where quota checks happen */
	int err = 0;
	struct lafs_inode *lai = LAFSI(ino);
	lai = LAFSI(LAFSI(ino)->filesys);
	spin_lock(&lai->vfs_inode.i_lock);
	if (lai->md.fs.blocks_allowed &&
	    diff > 0 &&
	    lai->md.fs.cblocks_used +
	    lai->md.fs.pblocks_used +
	    lai->md.fs.ablocks_used + diff
	    > lai->md.fs.blocks_allowed)
		err = -ENOSPC;
	else
		lai->md.fs.ablocks_used += diff;

	spin_unlock(&lai->vfs_inode.i_lock);
	if (err)
		return err;

	err = lafs_quota_allocate(fs, ino, diff * fs->blocksize);
	if (err) {
		spin_lock(&lai->vfs_inode.i_lock);
		lai->md.fs.ablocks_used -= diff;
		spin_unlock(&lai->vfs_inode.i_lock);
		return err;
	}

	lai = LAFSI(ino);
	spin_lock(&ino->i_lock);
	lai->ablocks += diff;
	ino->i_blocks += (blkcnt_t)diff << (fs->blocksize_bits - 9) ;
	spin_unlock(&ino->i_lock);

	return 0;
}
