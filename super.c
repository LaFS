
/*
 * fs/lafs/super.c
 * Copyright (C) 2005-2009
 * Neil Brown <neilb@suse.de>
 * Released under the GPL, version 2
 */

#include	"lafs.h"
#include	<linux/namei.h>
#include	<linux/crc32.h>
#include	<linux/statfs.h>
#include	<linux/mount.h>
#include	<linux/exportfs.h>
#include	<linux/slab.h>

static struct super_operations lafs_sops;
static const struct export_operations lafs_export_ops;

/*---------------------------------------------------------------------
 * Write out state and super blocks
 *  The super blocks only need to be written when the geometry of the
 *  array changes such as when a device is added, removed, or resized.
 *  So we don't bother with that just yet.
 *  The state block needs to be written - twice on each device - whenever
 *  a checkpoint is completed.  All copies are identical and the writes
 *  proceed in parallel.  There are 4 stateblock locations on each device.
 *  2 are typically less recent than the other two.  We over-write the
 *  less-recent copies.
 *  FIXME on a RAID4 we should pad the write to be a full stripe.
 *
 * Locking issues:  This is called from the checkpoint thread and so
 *  it does not race with anything else exclusive to that thread.
 *  The nonlog information needs to be reviewed once that functionality
 *  is implemented.
 */

int lafs_write_state(struct fs *fs)
{
	struct lafs_state *st;
	int i, d;

	fs->seq++;
	st = fs->state;
	st->seq = cpu_to_le32(fs->seq);
	st->nonlog_segment = cpu_to_le32(fs->nonlog_segment);
	st->nonlog_dev = cpu_to_le16(fs->nonlog_dev);
	st->nonlog_offset = cpu_to_le16(fs->nonlog_offset);
	st->nextyouth = cpu_to_le16(fs->youth_next);
	st->checkpointcluster = cpu_to_le64(fs->checkpointcluster);
	for (i = 0; i < fs->maxsnapshot; i++)
		st->root_inodes[i] = cpu_to_le64(fs->ss[i].root_addr);

	st->checksum = 0;
	st->checksum = crc32_le(0, (unsigned char *)st, fs->statesize);

	for (d = 0; d < fs->devices ; d++)
		for (i = (fs->seq & 1); i < 4 ; i += 2)
			lafs_super_write(fs, d, fs->devs[d].stateaddr[i] >> 9,
					 (char *)st, fs->statesize);
	lafs_super_wait(fs);
	/* FIXME what about a write error ??? */
	return 0;
}

static int
valid_devblock(struct lafs_dev *db, sector_t addr, sector_t size)
{
	/* check that this devblock is valid, given that
	 * it was found at sector 'addr'
	 */
	u32 crc, crc2;
	u64 byteaddr;
	sector_t segsize;
	int i;

	if (strncmp(db->idtag, "LaFS-DeviceBlock", 16) != 0)
		return 0;
	if (le32_to_cpu(db->version) != LAFS_DEV_VERS)
		return 0;
	/* uuid can be anything */
	crc = db->checksum;
	db->checksum = 0;
	crc2 = crc32_le(0, (unsigned char *)db, LAFS_DEVBLK_SIZE);
	db->checksum = crc;
	if (crc2 != crc) {
		dprintk("%lx != %lx\n", (unsigned long)crc,
			(unsigned long)crc2);
		return 0;
	}

	byteaddr = (u64)addr << 9; /* convert to byte */
	if (le64_to_cpu(db->devaddr[0]) != byteaddr &&
	    le64_to_cpu(db->devaddr[1]) != byteaddr)
		return 0;

	if (db->statebits < 10 || db->statebits > 16)
		return 0;
	if (db->blockbits < 9 || db->blockbits > 20)
		return 0;
	if (le16_to_cpu(db->width) < 1 || le16_to_cpu(db->width) >= 512)
		return 0;
	if (le32_to_cpu(db->stride) < 1)
		return 0;
	/* devaddr[0] must be early, [1] must be late */
	if (le64_to_cpu(db->devaddr[0]) >=
	    le64_to_cpu(db->segment_offset))
		return 0;

	if (le64_to_cpu(db->devaddr[1]) <
	    le64_to_cpu(db->segment_offset) +
	    ((((sector_t)le32_to_cpu(db->segment_count)
	       * le32_to_cpu(db->segment_size)))
	     << db->blockbits))
		return 0;

	/* 2 is an absolute minimum segment size, a few hundred is more
	 * likely. We'll put a lower limit of 8, and an upper of 800000
	 */
	if (le32_to_cpu(db->segment_size) < 8 ||
	    le32_to_cpu(db->segment_size) > 800000)
		return 0;

	if (le32_to_cpu(db->segment_offset) >
	    (le32_to_cpu(db->segment_size)<<db->blockbits) * 10)
		return 0;

	/* The 4 state blocks live before the first or after the last segment.
	 * The distance from start of first to end of last is either:
	 * - segment_count * segment_size  if width*stride <= segment_size
	 * - (width-1) * stride + segment_size / width * segment_count
	 *                if width * stride > segment_size
	 */
	segsize = le32_to_cpu(db->segment_size);
	segsize *= le32_to_cpu(db->segment_count);
	if (le16_to_cpu(db->width) *  le32_to_cpu(db->stride)
	    > le32_to_cpu(db->segment_size)) {
		int stride = le32_to_cpu(db->stride);
		int width = le16_to_cpu(db->width);

		sector_div(segsize, width);
		segsize += (width - 1) * stride;
	}
	segsize <<= db->blockbits;
	for (i = 0; i < 4; i++) {
		sector_t addr = le64_to_cpu(db->stateaddr[i]);
		int offset = le32_to_cpu(db->segment_offset);
		if (addr + (1<<db->statebits) > offset &&
		    addr < offset + segsize)
			return 0;
		if (addr + (1<<db->statebits) > (size << db->blockbits))
			return 0;
	}

	/* Check all segments fit within device */
	if (le32_to_cpu(db->segment_offset) + segsize > (size << db->blockbits))
		return 0;

	/* I guess it look sane enough... */
	return 1;
}

static int
compare_dev(struct lafs_dev *orig, struct lafs_dev *new)
{
	/* Both these are known to be valid.
	 * Return:
	 *   0 if they are for same filesystem, but 'new' is older
	 *   1 if they are for same filesystem, and 'new' is newer
	 *  -1 if they are for different filesystems
	 */
	if (memcmp(orig->uuid, new->uuid, 16))
		return -1;
	if (u32_after(le32_to_cpu(new->seq),
		      le32_to_cpu(orig->seq)))
		return 1;
	return 0;
}

static int
valid_stateblock(struct lafs_state *st, struct lafs_dev *dv)
{
	/* Given the 'dv' devblock, make sure 'st' is a valid
	 * and consistent stateblock
	 */
	u32 crc;
	if (strncmp(st->idtag, "LaFS-State-Block", 16) != 0)
		return 0;
	if (le32_to_cpu(st->version) != LAFS_STATE_VERS)
		return 0;
	crc = st->checksum;
	st->checksum = 0;
	if (crc32_le(0, (unsigned char *)st, 1<<dv->statebits) != crc)
		return 0;
	st->checksum = crc;

	if (memcmp(st->uuid, dv->uuid, 16))
		return 0;

	if (sizeof(*st) + le32_to_cpu(st->maxsnapshot) * 8
	    > (1<<dv->statebits))
 		return 0;

	/* Don't support RO sharing yet. */
	if (st->alt_seq)
		return 0;

	return 1;
}

static int
compare_state(struct lafs_state *orig, struct lafs_state *new)
{
	/* return 1 if 'new' is actually newer than 'orig'.
	 * We already know they are both valid and have the same
	 * uuid... I don't think there is anything else to be checked
	 */
	return u32_after(le32_to_cpu(new->seq), le32_to_cpu(orig->seq));
}

/*
 * Mount options.
 * As we can have multiple devices, things are slightly non-obvious.
 * The 'devname' can be either a device name, starting '/', or
 * a filesytem name (not starting '/').
 * The 'data' is a standard comma-separated list of options.
 * For 'mount' these are:
 *    dev=/dev/X
 *		- devices in addition to 'dev_name'
 *    new=/dev/X
 *		- A new device, with a superblock already present, to be added.
 *    incomplete
 *		- don't complain if not all devices are given
 *    ?? quota stuff, cleaning parameters,
 *
 * For 'remount', options are
 *    dev=  - add another device
 *    new=  - the device is being added.
 *
 */

struct options {
	int devcnt;
	int curr_dev;
	int statebits, blockbits;
	struct devent {
		const char *dev;
		int is_new;
		int is_name;
		struct block_device *bdev;
		struct lafs_dev *devblock;
		struct lafs_state *stateblock;
		int devchoice, statechoice;
	} *devlist;
	const char *name;
};
static int
count_devs(const char *name, char *data)
{
	int cnt = 0;
	if (*name == '/')
		cnt = 1;
	while (data && *data) {
		if (strncmp(data, "dev=", 4) == 0)
			cnt++;
		if (strncmp(data, "new=", 4) == 0)
			cnt++;
		data = strchr(data, ',');
		if (data)
			data++;
	}
	return cnt;
}

static int
parse_opts(struct options *op, const char *name, char *data)
{
	int dv = 0;
	char *p;

	memset(op, 0, sizeof(*op));
	op->devcnt = count_devs(name, data);
	op->devlist = kzalloc(op->devcnt*sizeof(op->devlist[0]), GFP_KERNEL);

	if (!op->devlist)
		return -ENOMEM;

	op->name = NULL;
	if (*name == '/') {
		op->devlist[dv].is_name = 1;
		op->devlist[dv++].dev = name;
	} else
		op->name = name;
	while ((p = strsep(&data, ",")) != NULL) {
		if (!*p)
			continue;
		if (strncmp(p, "dev=", 4) == 0)
			op->devlist[dv++].dev = p+4;
		else if (strncmp(p, "new=", 4) == 0) {
			op->devlist[dv].is_new = 1;
			op->devlist[dv++].dev = p+4;
		} else {
			printk(KERN_ERR
			       "LaFS: Unrecognised mount option \"%s\"\n", p);
			return -EINVAL;

		}
	}
	op->devcnt = dv;

	return 0;
}

static int
lafs_load_super(struct block_device *bdev, void *opv, int silent)
{
	/* Find the devblock and the stateblock for this device

	 * Only do basic internal consistancy checks.  Inter-device
	 * checks happen later
	 */
	struct options *op = opv;
	struct devent *dv;
	struct page *pg;
	sector_t sect, dev_addr = 0;
	int state_addr = 0;
	int err = 0;
	unsigned int n;
	int i;
	int have_dev = 0, have_state = 0;
	sector_t devsize;

	dv = &op->devlist[op->curr_dev];
	BUG_ON(dv->devblock);
	BUG_ON(dv->stateblock);

	n = queue_logical_block_size(bdev->bd_disk->queue);
	if (n < LAFS_DEVBLK_SIZE)
		n = LAFS_DEVBLK_SIZE;
	BUG_ON(n > PAGE_SIZE);
	dv->devblock = kmalloc(n, GFP_KERNEL);
	if (!dv->devblock)
		return -ENOMEM;
	pg = alloc_page(GFP_KERNEL);
	if (!pg)
		return -ENOMEM;

	devsize = i_size_read(bdev->bd_inode);

	/* Now find a devblock, check the first two possible locations,
	 * and the last two.  If two devblocks are found with different
	 * uuids, we are confused!
	 */
	sect = 0;
	for (i = 0; i < 4; i++) {
		/* try to read block at 'sect' */
		int ok = lafs_sync_page_io(bdev, sect, 0, n, pg, READ);

		if (ok && valid_devblock(page_address(pg), sect, devsize)) {
			if (!have_dev) {
				have_dev = 1;
				memcpy(dv->devblock, page_address(pg), n);
				dev_addr = sect;
			} else switch (compare_dev(dv->devblock,
						   page_address(pg))) {
				case 0: /* older, do nothing */
					break;
				case 1: /* newer, overwrite */
					memcpy(dv->devblock, page_address(pg), n);
					dev_addr = sect;
					break;
				default: /* inconsistent --- HELP */
					printk(KERN_ERR "LaFS: inconsistent device-blocks found.\n");
					err = -EINVAL;
					goto out;
				}
		}

		if (i != 1)
			sect += (n>>9);
		else {
			sect = devsize & ~(sector_t)(n-1);
			sect >>= 9;
			sect -= (n>>9)*2;
		}
	}
	/* FIXME - we've lost the read error, if it was significant */
	err = -EINVAL;
	if (!have_dev) {
		if (!silent)
			printk(KERN_ERR "LaFS - no valid devblock found.\n");
		goto out;
	}

	/* OK, we have a valid devblock, that's nice.
	 * Now we should be able to find some stateblocks.
	 * The locations are in the devblock
	 */
	n = le32_to_cpu(1<<dv->devblock->statebits);
	if ((n & (n-1)) ||
	    n < queue_logical_block_size(bdev->bd_disk->queue) ||
	    n > 128*1024) {
		printk(KERN_ERR "LaFS: statesize of %u not acceptable.\n", n);
		err = -EINVAL;
		goto out;
	}
	dv->stateblock = kmalloc(n, GFP_KERNEL);
	err = -ENOMEM;
	if (!dv->stateblock)
		goto out;
	for (i = 0; i < 4; i++) {
		int ok;
		sect = le64_to_cpu(dv->devblock->stateaddr[i])>>9;
		ok = lafs_sync_page_io(bdev, sect, 0, n, pg, READ);
		if (ok && valid_stateblock(page_address(pg), dv->devblock)) {
			if (!have_state) {
				have_state = 1;
				memcpy(dv->stateblock, page_address(pg), n);
				state_addr = i;
			} else if (compare_state(dv->stateblock,
						 page_address(pg))) {
				memcpy(dv->stateblock, page_address(pg), n);
				state_addr = i;
			}
		}
	}

	if (have_state) {
		err = 0;
		dv->devchoice = dev_addr;
		dv->statechoice = state_addr;
	} else {
		err = -EINVAL;
		if (!silent)
			printk(KERN_ERR "LaFS: no valid stateblock found.\n");
	}
out:
	page_cache_release(pg);
	return err;
}

static int
check_devs(struct options *op)
{
	/* Check we have enough, that they are for the same
	 * uuid, and they they don't overlap
	 * Also check that 'seq' number of devblocks
	 * are within '1'
	 */
	int seqlo = le32_to_cpu(op->devlist[0].devblock->seq);
	int seqhi = le32_to_cpu(op->devlist[0].devblock->seq);
	int newdev = 0;
	int newstate = 0;
	int i, j;

	for (i = 1; i < op->devcnt; i++) {
		if (memcmp(op->devlist[0].stateblock->uuid,
			   op->devlist[i].stateblock->uuid,
			   16) != 0)
			return -EINVAL;

		if (le32_to_cpu(op->devlist[i].devblock->seq) == seqlo)
			;
		else if (le32_to_cpu(op->devlist[i].devblock->seq) == seqlo+1) {
			newdev = i;
			seqhi = seqlo+1;
		} else if (le32_to_cpu(op->devlist[i].devblock->seq) == seqhi-1)
			seqlo = seqhi-1;
		else
			return -EINVAL;

		if (u32_after(le32_to_cpu(op->devlist[i].stateblock->seq),
			      le32_to_cpu(op->devlist[newstate].
					  stateblock->seq)))
			newstate = i;
	}
	if (le32_to_cpu(op->devlist[newstate].stateblock->devices)
	    != op->devcnt)
		return -EINVAL;

	op->statebits = op->devlist[0].devblock->statebits;
	op->blockbits = op->devlist[0].devblock->blockbits;

	/* Now check devices don't overlap in start/size.
	 * We do a simple quadratic search
	 */
	for (i = 0; i < op->devcnt; i++)
		for (j = 0; j < op->devcnt; j++)
			if (i != j)
				if (le64_to_cpu(op->devlist[i].devblock->start) <
				    le64_to_cpu(op->devlist[j].devblock->start) &&

				    le64_to_cpu(op->devlist[i].devblock->start)+
				    le64_to_cpu(op->devlist[i].devblock->size) >
				    le64_to_cpu(op->devlist[j].devblock->start))
					return -EINVAL;
	return newstate;
}

/* we identify lafs superblocks by the filesystem uuid.  This means
 * that block-level snapshots cannot be mounted.  You should use
 * fs-level snapshots instead.
 */
static int sb_test(struct super_block *sb, void *data)
{
	struct sb_key *ptn = data;
	struct sb_key *sk = sb->s_fs_info;
	return memcmp(ptn->fs->state->uuid,
		      sk->fs->state->uuid, 16) == 0;
}

static int sb_set(struct super_block *sb, void *data)
{
	struct sb_key *ptn = data;
	sb->s_fs_info = ptn;
	return set_anon_super(sb, NULL);
}


static int
lafs_load(struct fs *fs, struct options *op, int newest)
{
	/* We seem to have a full set of devices for the filesystem.
	 * Time to create our fs_info structure and fill it out.
	 * This only includes information from the dev and state blocks.
	 * Finding the root-inode comes a bit later.
	 */
	struct lafs_state *st;
	int i;
	int err;
	struct sb_key *k;

	st = fs->state = op->devlist[newest].stateblock;
	op->devlist[newest].stateblock = NULL;
#ifdef DUMP
	dfs = fs;
#endif

	fs->seq = le32_to_cpu(st->seq);
	fs->devices = op->devcnt;
	fs->devs_loaded = fs->devices; /* FIXME use this or lose this */
	fs->statesize = 1 << op->statebits;
	fs->blocksize = 1 << op->blockbits;
	fs->blocksize_bits = op->blockbits;

	fs->nonlog_segment = le32_to_cpu(st->nonlog_segment);
	fs->nonlog_dev = le16_to_cpu(st->nonlog_dev);
	fs->nonlog_offset = le16_to_cpu(st->nonlog_offset);
	fs->youth_next = le16_to_cpu(st->nextyouth);
	fs->checkpoint_youth = fs->youth_next;
	if (fs->youth_next < 8)
		fs->youth_next = 8;
	fs->scan.first_free_pass = 1;
	fs->scan.free_dev = -1;

	fs->maxsnapshot = le32_to_cpu(st->maxsnapshot);

	fs->scan.free_usages = kmalloc(PAGE_SIZE, GFP_KERNEL);
	err = lafs_segtrack_init(fs->segtrack);

	fs->ss = kzalloc(sizeof(struct snapshot)*fs->maxsnapshot, GFP_KERNEL);
	if (!fs->ss || !fs->scan.free_usages || err) {
		if (!err)
			err = -ENOMEM;
		goto abort;
	}

	fs->checkpointcluster = le64_to_cpu(st->checkpointcluster);
	for (i = 0; i < fs->maxsnapshot; i++) {
		fs->ss[i].root_addr =
			le64_to_cpu(st->root_inodes[i]);
		dprintk("root inode %d are %llu\n",
			i, fs->ss[i].root_addr);
	}
	INIT_LIST_HEAD(&fs->pending_orphans);
	INIT_LIST_HEAD(&fs->inode_index);
	INIT_LIST_HEAD(&fs->phase_leafs[0]);
	INIT_LIST_HEAD(&fs->phase_leafs[1]);
	INIT_LIST_HEAD(&fs->clean_leafs);
	INIT_LIST_HEAD(&fs->account_leafs);
	atomic_set(&fs->sb_writes_pending, 0);
	init_waitqueue_head(&fs->sb_writes_wait);
	init_waitqueue_head(&fs->async_complete);
	init_waitqueue_head(&fs->trunc_wait);
	mutex_init(&fs->cleaner.lock);
	spin_lock_init(&fs->stable_lock);
	spin_lock_init(&fs->alloc_lock);
	spin_lock_init(&fs->lock);
	init_waitqueue_head(&fs->phase_wait);

	INIT_WORK(&fs->done_work, lafs_done_work);

	/* FIXME add congention and unplug functions to this bdi */
	err = bdi_init(&fs->bdi);
	if (err)
		goto abort;
	

	fs->phase_locked = 0;
	for (i = 0; i < WC_NUM; i++) {
		int j;
		mutex_init(&fs->wc[i].lock);
		for (j = 0; j < 4 ; j++) {
			atomic_set(&fs->wc[i].pending_cnt[j], 0);
			INIT_LIST_HEAD(&fs->wc[i].pending_blocks[j]);
		}
		init_waitqueue_head(&fs->wc[i].pending_wait);
		fs->wc[i].seg.dev = -1;
	}

	fs->max_newsegs = 32; /* FIXME this should be configurable */

	err = -ENOMEM;
	fs->devs = kzalloc(sizeof(struct fs_dev)*fs->devices, GFP_KERNEL);
	if (!fs->devs)
		goto abort;

	k = kzalloc(sizeof(*k), GFP_KERNEL);
	k->fs = fs;
	fs->prime_sb = sget(&lafs_fs_type, sb_test, sb_set, k);
	if (IS_ERR(fs->prime_sb)) {
		kfree(k);
		err = PTR_ERR(fs->prime_sb);
		goto abort;
	}
	if (fs->prime_sb->s_root) {
		/* filesystem with this uuid already exists */
		deactivate_locked_super(fs->prime_sb);
		kfree(k);
		fs->prime_sb = NULL;
		err = -EBUSY;
		goto abort;
	}
	err = bdi_register_dev(&fs->bdi, fs->prime_sb->s_dev);
	if (err) {
		deactivate_locked_super(fs->prime_sb);
		kfree(k);
		fs->prime_sb = NULL;
		goto abort;
	}
	fs->prime_sb->s_bdi = &fs->bdi;

	fs->prime_sb->s_blocksize = 1 << op->blockbits;
	fs->prime_sb->s_blocksize_bits = op->blockbits;
	fs->prime_sb->s_op = &lafs_sops;
	fs->prime_sb->s_export_op = &lafs_export_ops;
	fs->prime_sb->s_root = NULL;

	/* We allow 29 bits for nanosecs, so they must be even. */
	fs->prime_sb->s_time_gran = 2;

	for (i = 0; i < fs->devices; i++) {
		struct fs_dev *dv = &fs->devs[i];
		struct devent *de = &op->devlist[i];
		int j;
		dv->bdev = de->bdev;
		de->bdev = NULL;

		dv->devblk = de->devblock;
		de->devblock = NULL;

		dv->recent_dev = de->devchoice;
		dv->recent_state = de->statechoice;

		dv->start = le64_to_cpu(dv->devblk->start);
		dv->size = le64_to_cpu(dv->devblk->size);
		dprintk("Dev %d seems to range %llu + %llu\n",
			i, (unsigned long long)dv->start,
			(unsigned long long)dv->size);

		dv->width = le16_to_cpu(dv->devblk->width);
		dv->stride = le32_to_cpu(dv->devblk->stride);
		dv->segment_size = le32_to_cpu(dv->devblk->segment_size);
		dv->segment_offset = le32_to_cpu(dv->devblk->segment_offset);
		dv->segment_count = le32_to_cpu(dv->devblk->segment_count);
		dv->usage_inum = le32_to_cpu(dv->devblk->usage_inum);

		if (dv->segment_size > fs->max_segment)
			fs->max_segment = dv->segment_size;

		if (dv->width * dv->stride <= dv->segment_size) {
			dv->tables_per_seg = dv->segment_size /
				dv->width / dv->stride;
			dv->rows_per_table = dv->stride;
			dv->segment_stride = dv->segment_size;
		} else {
			dv->tables_per_seg = 1;
			dv->rows_per_table = dv->segment_size / dv->width;
			dv->segment_stride = dv->rows_per_table;
		}
		/* table size is the number of blocks in the segment usage
		 * file per snapshot
		 */
		dv->tablesize = (dv->segment_count + (1<<(fs->blocksize_bits-1)) + 1)
			>> (fs->blocksize_bits-1);

		for (j = 0; j < 2; j++)
			dv->devaddr[j] = le64_to_cpu(dv->devblk->devaddr[j]);
		for (j = 0; j < 4; j++)
			dv->stateaddr[j] = le64_to_cpu(dv->devblk->stateaddr[j]);
	}
	return 0;

abort:
	bdi_destroy(&fs->bdi);
	kfree(fs->scan.free_usages);
	lafs_segtrack_free(fs->segtrack);
	kfree(fs->devs);
	kfree(fs->ss);
	kfree(fs);
	return -ENOMEM;
}

static int show_orphans(struct fs *fs)
{
	struct datablock *db;
	printk("Orphans:\n");
	list_for_each_entry(db, &fs->pending_orphans,
			    orphans) {
		struct inode *ino = iget_my_inode(db);
		printk("orphan=%s\n", strblk(&db->b));
		if (ino)
			lafs_print_tree(&LAFSI(ino)->iblock->b, 0);
		iput(ino);
	}
	printk("cleaner active: %d %d\n", fs->cleaner.active,
	       fs->scan.done);
	return 1; /* meaningless, but makes it easy to add to wait_event below */
}

static void lafs_kill_sb(struct super_block *sb)
{
	struct fs *fs = fs_from_sb(sb);
	/* Release the 'struct fs' */
	int i;

	/* FIXME should I refcount this when there are multiple
	 * filesets? How does that work?
	 */

	/* Delay final destruction of the root inode */
	/* FIXME all the sbs... */
	set_bit(I_Deleting, &LAFSI(fs->ss[0].root)->iflags);

	/* FIXME I'm not sure we should be waiting for the
	 * cleaner.  Maybe we should just release all tc->cleaning
	 * blocks instead.
	 */
	set_bit(CleanerDisabled, &fs->fsstate);

	wait_event(fs->async_complete,
		   show_orphans(fs) &&
		   !test_bit(OrphansRunning, &fs->fsstate) &&
		   list_empty(&fs->pending_orphans) &&
		   fs->scan.done == 1 &&
		   fs->cleaner.active == 0);

	if (LAFSI(fs->ss[0].root)->md.fs.accesstime) {
		struct inode *i = LAFSI(fs->ss[0].root)->md.fs.accesstime;
		LAFSI(fs->ss[0].root)->md.fs.accesstime = NULL;
		iput(i);
	}

	kill_anon_super(fs->prime_sb);

	bdi_destroy(&fs->bdi);

	for (i = 0; i < fs->devices; i++) {
		struct fs_dev *dv = &fs->devs[i];
		kfree(dv->devblk);
		blkdev_put(dv->bdev, FMODE_READ|FMODE_WRITE|FMODE_EXCL);
	}

	/* Final checkpoint will have cleared out the leafs lists,
	 * so they should all be empty.
	 */
	/* Lets see what is on the 'leaf' list? */
	for (i = 0; i < 2; i++) {
		struct block *b;
		dprintk("For phase %d\n", i);
	retry:
		list_for_each_entry(b, &fs->phase_leafs[i], lru) {
			/* FIXME this only OK for readonly mounts.
			 */
			getref(b, MKREF(release));
			lafs_refile(b, 0);
			if (test_bit(B_Pinned, &b->flags)) {
				/* didn't fix the pincnt !! */
				printk("This was pinned: %s\n", strblk(b));
				lafs_print_tree(b, 1);
				BUG();
			}
			putref(b, MKREF(release));
			goto retry;
		}
	}
	BUG_ON(!list_empty(&fs->clean_leafs));

	flush_scheduled_work();
	lafs_stop_thread(fs);

	for (i = 0; i < 4; i++)
		if (fs->cleaner.seg[i].chead)
			put_page(fs->cleaner.seg[i].chead);

	kfree(fs->state);
	kfree(fs->ss);
	kfree(fs->devs);
	lafs_segtrack_free(fs->segtrack);
	kfree(fs->scan.free_usages);
	kfree(fs->prime_sb->s_fs_info);
	kfree(fs);
}

static void
lafs_put_super(struct super_block *sb)
{
	struct fs *fs = fs_from_sb(sb);
	int ss;
	struct lafs_inode *li;

	/* If !fs->thread, we never really mounted the fs, so this
	 * cleanup is inappropriate .. and cannot work anyway.
	 */
	if (fs->thread) {
		lafs_checkpoint_lock(fs);
		lafs_checkpoint_start(fs);
		if (sb == fs->prime_sb)
			/* Don't incorporate any more segusage/quota updates. */
			set_bit(FinalCheckpoint, &fs->fsstate);
		lafs_checkpoint_unlock_wait(fs);
		lafs_cluster_wait_all(fs);
	}

	if (sb == fs->prime_sb) {
		int d;
		/* This is the main sb, not a snapshot or
		 * subordinate fs.
		 * Now that all inodes have been invalidated we can do
		 * the final checkpoint.
		 */
		lafs_close_all_segments(fs);
		lafs_empty_segment_table(fs);
		lafs_seg_put_all(fs);

		iput(fs->orphans);
		fs->orphans = NULL;
		for (d=0; d < fs->devices; d++)
			if (fs->devs[d].segsum) {
				iput(fs->devs[d].segsum);
				fs->devs[d].segsum = NULL;
			}
	}

	/* need to break a circular reference... */
	for (ss = 0; ss < fs->maxsnapshot; ss++)
		if (fs->ss[ss].root &&
		    fs->ss[ss].root->i_sb == sb) {
			dprintk("Putting ss %d\n", ss);
			li = LAFSI(fs->ss[ss].root);
			if (test_bit(B_Realloc, &li->dblock->b.flags))
				lafs_dump_tree();
			iput(fs->ss[ss].root);
			fs->ss[ss].root = NULL;
			break;
		}
}

static int
lafs_get_devs(struct fs *fs, struct options *op, int flags)
{
	int err;
	int i;

	for (i = 0; i < op->devcnt; i++) {
		struct block_device *bdev;
		op->curr_dev = i;

		bdev = blkdev_get_by_path(op->devlist[i].dev,
					  FMODE_READ|FMODE_WRITE|FMODE_EXCL,
					  fs);
		err = PTR_ERR(bdev);
		if (IS_ERR(bdev))
			goto out;
		err = lafs_load_super(bdev, op, flags & MS_SILENT ? 1 : 0);
		if (err < 0)
			goto out;
		op->devlist[i].bdev = bdev;
	}
	return 0;

out:
	return err;
}

static int
lafs_get_sb(struct file_system_type *fs_type,
	    int flags, const char *dev_name, void *data,
	    struct vfsmount *mnt)
{
	/* as we may have multiple devices, some in 'data', we cannot just
	 * use get_sb_bdev, we need to roll-our-own.
	 * We call get_sb_bdev on *each* bdev, and make sure the returned
	 * superblocks are either all new, or all for the same filesystem.
	 * If the later, we return the primary.
	 * If the former, we init the filesystem copying static data
	 * to all supers.
	 * First we 'blkdev_get_by_path' each device, exclusive to lafs
	 * Then we 'sget' a superblock that knows any/all the devices.
	 * This may be pre-existing, or may be new
	 * If new, it will be created knowing all devices.
	 * If pre-existing, and don't have correct device list, error
	 */
	struct options op;
	int err;
	int newest;
	struct fs *fs = kzalloc(sizeof(*fs), GFP_KERNEL);
	char *cdata = data;
	if (cdata == NULL)
		cdata = "";

	err = -ENOMEM;
	if (!fs)
		goto out;
	err = parse_opts(&op, dev_name, cdata);
	if (err)
		goto out;

	/* We now have as list of device names.  We call blkdev_get_by_path
	 * on each to collect some superblocks.
	 */
	err = lafs_get_devs(fs, &op, flags);
	if (err)
		goto out;

	/* Each device has a valid dev and state block.  Hopefully they
	 * are all for the same filesystem.  If they don't have the
	 * same uuid, we will bale-out here.  We also check that we have
	 * enough, and that they don't overlap.
	 * While we are looking at state blocks, pick the newest.
	 */
	newest = check_devs(&op);
	if (newest < 0) {
		err = newest;
		goto out;
	}

	/* So they seem to be the same - better create our
	 * 'fs' structure and fill it in
	 */
	err = lafs_load(fs, &op, newest);
	if (err)
		goto out;

	/* Well, all the devices check out.  Now we need to find the
	 * filesystem */
	err = lafs_mount(fs);
	if (err == 0)
		err = lafs_start_thread(fs);
	if (err) {
		/* Don't wait for any scan to finish ... */
		fs->scan.done = 1;
		fs->checkpointing = 0;
		deactivate_locked_super(fs->prime_sb);
	} else {
		fs->prime_sb->s_flags |= MS_ACTIVE;
		simple_set_mnt(mnt, fs->prime_sb);
	}
	/* And there you have it.  Filesystem all mounted, root dir found,
	 * metadata files initialised, all pigs fed, and ready to fly!!!
	 */

out:
	/* Now we clean up 'options'.  Anything that is wanted has
	 * been moved into 'fs', so we just discard anything we find
	 */
	if (op.devlist) {
		int i;
		for (i = 0; i < op.devcnt; i++) {
			kfree(op.devlist[i].devblock);
			kfree(op.devlist[i].stateblock);
			if (op.devlist[i].bdev)
				blkdev_put(op.devlist[i].bdev,
					   FMODE_READ|FMODE_WRITE|FMODE_EXCL);
		}
		kfree(op.devlist);
	}
	return err;
}

static struct dentry *lafs_get_subset_root(struct inode *ino)
{
	/* ino must be a TypeInodeFile inode in the prime filesystem. */
	struct fs *fs = fs_from_inode(ino);
	struct super_block *sb;
	int err = 0;
	struct inode *rootdir, *imapfile;
	struct dentry *root = NULL;

	sb = fs->prime_sb;

	rootdir = lafs_iget(ino, 2, SYNC);
	if (IS_ERR(rootdir) && PTR_ERR(rootdir) == -ENOENT) {
		rootdir = lafs_new_inode(fs, ino, NULL,
					 TypeDir, 2, 0755, NULL);
		/* FIXME could the inode get written before we set
		 * the link count ??*/
		rootdir->i_nlink = 2;
	}
	if (IS_ERR(rootdir))
		err = PTR_ERR(rootdir);
	else {
		root = d_alloc_root(rootdir);
		imapfile = lafs_iget(ino, 1, SYNC);
		if (IS_ERR(imapfile) && PTR_ERR(imapfile) == -ENOENT)
			imapfile = lafs_new_inode(fs, ino, NULL,
						  TypeInodeMap, 1, 0, NULL);

		if (IS_ERR(imapfile))
			err = PTR_ERR(imapfile);
		else
			iput(imapfile);
	}

	if (!err) {
		struct inode *atime = lafs_iget(ino, 3, SYNC);
		if (!IS_ERR(atime)) {
			if (LAFSI(atime)->type != TypeAccessTime) {
				iput(atime);
				err = -EINVAL;
			} else
				LAFSI(ino)->md.fs.accesstime = atime;
		} else if (PTR_ERR(atime) != -ENOENT)
			err = PTR_ERR(ino);
	}
	if (err) {
		dput(root);
		return ERR_PTR(err);
	} else
		return root;
}

static int
lafs_get_subset(struct file_system_type *fs_type,
		int flags, const char *dev_name, void *data,
		struct vfsmount *mnt)
{
	/* mount, possibly creating, a sub-fileset.
	 * dev_name must be an absolute path that leads
	 * to an object in a lafs file-system (or snapshot).
	 * The object must be either an InodeFile or
	 * an empty directory in the main file-system
	 * with mode 0 (though that rule might change).
	 * In the latter case we change the object to an
	 * InodeFile
	 * FIXME must require readonly for snapshots, and readwrite
	 * to create.
	 */

	struct nameidata nd;
	int err;
	struct super_block *sb;
	struct inode *ino;
	struct fs *fs;
	struct dentry *root;

	err = path_lookup(dev_name, LOOKUP_FOLLOW, &nd);
	if (err)
		goto out_noput;
	sb = nd.path.dentry->d_sb;
	err = -EINVAL;
	if (sb->s_type != &lafs_fs_type &&
	    sb->s_type != &lafs_snap_fs_type)
		goto out;
	/* FIXME test not a subset filesystem */
	ino = nd.path.dentry->d_inode;
	if (LAFSI(ino)->type != TypeInodeFile &&
	    LAFSI(ino)->type != TypeDir)
		goto out;
	fs = fs_from_sb(sb);
	down_write(&sb->s_umount);
	mutex_lock(&ino->i_mutex);
	err = 0;
	if (LAFSI(ino)->type == TypeDir) {
		struct datablock *inodb;
		/* maybe convert this to TypeInodeFile */
		err = -EINVAL;
		if (sb->s_type != &lafs_fs_type)
			goto out_unlock;
		if (ino->i_size)
			/* FIXME maybe I should run orphans */
			goto out_unlock;
		if ((ino->i_mode & 07777) != 0)
			goto out_unlock;
		inodb = lafs_inode_dblock(ino, SYNC, MKREF(make_subset));
		err = PTR_ERR(inodb);
		if (IS_ERR(inodb))
			goto out_unlock;
		lafs_iolock_block(&inodb->b);
		set_bit(B_PinPending, &inodb->b.flags);
		lafs_iounlock_block(&inodb->b);
		lafs_checkpoint_lock(fs);
		err = lafs_pin_dblock(inodb, ReleaseSpace);
		if (!err) {
			struct fs_md *md;
			u32 parent = LAFSI(ino)->md.file.parent;
			/* OK, we are good to go making this filesystem */
			LAFSI(ino)->type = TypeInodeFile;
			LAFSI(ino)->metadata_size = (sizeof(struct la_inode) +
						     sizeof(struct fs_metadata));
			ino->i_op = &lafs_subset_ino_operations;
			ino->i_fop = &lafs_subset_file_operations;
			md = &LAFSI(ino)->md.fs;
			md->usagetable = 0;
			ino->i_mtime = current_fs_time(sb);
			md->cblocks_used = 0;
			md->pblocks_used = 0;
			md->ablocks_used = 0;
			md->blocks_allowed = 10000; /* FIXME */
			md->blocks_unalloc = 0;
			/* FIXME should I be using inode_init here */
			md->creation_age = fs->wc[0].cluster_seq;
			md->inodes_used = 0;
			md->parent = parent;
			md->quota_inums[0] = 0;
			md->quota_inums[1] = 0;
			md->quota_inums[2] = 0;
			md->quota_inodes[0] = NULL;
			md->quota_inodes[1] = NULL;
			md->quota_inodes[2] = NULL;
			md->accesstime = NULL;
			md->name = NULL;
			lafs_dirty_dblock(inodb);
			lafs_dirty_inode(ino);
			/* We use a checkpoint to commit this change,
			 * it is too unusual to bother logging
			 */
			lafs_checkpoint_start(fs);
			lafs_checkpoint_unlock_wait(fs);
		} else {
			lafs_checkpoint_unlock(fs);
		}
		putdref(inodb, MKREF(make_subset));
		if (err)
			goto out_unlock;
	}
	/* We have a TypeInodeFile so we can make a superblock */
	root = lafs_get_subset_root(ino);
	iput(ino);

	if (IS_ERR(root))
		err = PTR_ERR(root);
	else {
		mnt->mnt_sb = root->d_sb;
		atomic_inc(&mnt->mnt_sb->s_active);
		mnt->mnt_root = root;
	}
out_unlock:
	mutex_unlock(&ino->i_mutex);
	if (err)
		up_write(&ino->i_sb->s_umount);
out:
	path_put(&nd.path);
out_noput:
	return err;
}

static struct dentry *subset_lookup(struct inode *dir, struct dentry *dentry,
				    struct nameidata *nd)
{
	d_add(dentry, NULL);
	return NULL;
}

static int subset_readdir(struct file *filp, void *dirent, filldir_t filldir)
{
	struct dentry *dentry = filp->f_dentry;
	struct lafs_inode *lai = LAFSI(dentry->d_inode);
	ino_t ino;
	loff_t i = filp->f_pos;

	switch (i) {
	case 0:
		ino = dentry->d_inode->i_ino;
		if (filldir(dirent, ".", 1, i, ino, DT_DIR) < 0)
			break;
		filp->f_pos ++;
		i++;
		/* fallthrough */
	case 1:
		ino = lai->md.fs.parent;
		if (filldir(dirent, "..", 2, i, ino, DT_DIR) < 0)
			break;
		filp->f_pos++;
		i++;
		/* fallthrough */
	default:
		break;
	}
	return 0;
}

const struct file_operations lafs_subset_file_operations = {
	.readdir	= subset_readdir,
};

const struct inode_operations lafs_subset_ino_operations = {
	.lookup		= subset_lookup,
};


struct file_system_type lafs_fs_type = {
	.owner		= THIS_MODULE,
	.name		= "lafs",
	.get_sb		= lafs_get_sb,
	.kill_sb	= lafs_kill_sb,
	.fs_flags	= FS_REQUIRES_DEV,
};

static struct file_system_type lafs_subset_fs_type = {
	.owner		= THIS_MODULE,
	.name		= "lafs_subset",
	.get_sb		= lafs_get_subset,
};

static int __init lafs_init(void)
{
	int err;

	BUILD_BUG_ON(B_NUM_FLAGS > 32);

	err = lafs_ihash_init();
	err = err ?: register_filesystem(&lafs_fs_type);
	err = err ?: register_filesystem(&lafs_snap_fs_type);
	err = err ?: register_filesystem(&lafs_subset_fs_type);
	if (err)
		goto out;
	return 0;

out:
	unregister_filesystem(&lafs_fs_type);
	unregister_filesystem(&lafs_snap_fs_type);
	unregister_filesystem(&lafs_subset_fs_type);
	lafs_ihash_free();
	return err;
}

static void __exit lafs_exit(void)
{
	unregister_filesystem(&lafs_fs_type);
	unregister_filesystem(&lafs_snap_fs_type);
	unregister_filesystem(&lafs_subset_fs_type);
	lafs_ihash_free();
}

static struct inode *lafs_nfs_get_inode(struct super_block *sb,
					u64 ino, u32 generation)
{
	struct fs *fs = fs_from_sb(sb);
	struct inode *inode;

	inode = lafs_iget(fs->ss[0].root, ino, SYNC);
	if (IS_ERR(inode))
		return ERR_CAST(inode);
	if (generation && inode->i_generation != generation) {
		iput(inode);
		return ERR_PTR(-ESTALE);
	}

	return inode;
}

static struct dentry *lafs_fh_to_dentry(struct super_block *sb, struct fid *fid,
					int fh_len, int fh_type)
{
	return generic_fh_to_dentry(sb, fid, fh_len, fh_type,
				    lafs_nfs_get_inode);
}

static struct dentry *lafs_fh_to_parent(struct super_block *sb, struct fid *fid,
					int fh_len, int fh_type)
{
	return generic_fh_to_parent(sb, fid, fh_len, fh_type,
				    lafs_nfs_get_inode);
}

static struct dentry *lafs_get_parent(struct dentry *child)
{
	ino_t inum = 0;
	struct inode *inode;
	switch(LAFSI(child->d_inode)->type) {
	case TypeFile:
		inum = LAFSI(child->d_inode)->md.file.parent;
		break;
	case TypeInodeFile:
		inum = LAFSI(child->d_inode)->md.fs.parent;
		break;
	}
	inode = lafs_iget(LAFSI(child->d_inode)->filesys,
			  inum, SYNC);
	if (IS_ERR(inode))
		return ERR_CAST(inode);
	return d_obtain_alias(inode);
}

static const struct export_operations lafs_export_ops = {
	.fh_to_dentry = lafs_fh_to_dentry,
	.fh_to_parent = lafs_fh_to_parent,
	.get_parent = lafs_get_parent,
};

static struct inode *lafs_alloc_inode(struct super_block *sb)
{
	struct lafs_inode *li;
	li = kmalloc(sizeof(*li), GFP_NOFS);
	if (!li)
		return NULL;
	inode_init_once(&li->vfs_inode);
	li->vfs_inode.i_data.backing_dev_info = sb->s_bdi;
	li->iblock = NULL;
	li->dblock = NULL;
	li->update_cluster = 0;
	li->md.fs.name = NULL;

	init_rwsem(&li->ind_sem);
	INIT_LIST_HEAD(&li->free_index);

	return &li->vfs_inode;
}

static void kfree_inode(struct rcu_head *head)
{
	struct lafs_inode *lai = container_of(head, struct lafs_inode,
					      md.rcu);
	if (lai->type == TypeInodeFile)
		kfree(lai->md.fs.name);
	kfree(lai);
}

void lafs_destroy_inode(struct inode *inode)
{
	struct datablock *db;
	struct inode *fsys = LAFSI(inode)->filesys;

	BUG_ON(!list_empty(&inode->i_sb_list));
	// Cannot test i_list as dispose_list just does list_del
	db = lafs_inode_get_dblock(inode, MKREF(destroy));

	if (db) {
		set_bit(I_Destroyed, &LAFSI(inode)->iflags);
		if (test_and_clear_bit(B_Async, &db->b.flags))
			lafs_wake_thread(fs_from_inode(inode));
		putdref(db, MKREF(destroy));
	} else {
		if (fsys != inode)
			iput(fsys);
		LAFSI(inode)->filesys = NULL;

		spin_lock(&inode->i_data.private_lock);
		if (LAFSI(inode)->iblock)
			LAFS_BUG(atomic_read(&LAFSI(inode)->iblock->b.refcnt),
				 &LAFSI(inode)->iblock->b);
		/* FIXME could there be Async blocks keeps a refcount?
		 * we should free them
		 */
		spin_unlock(&inode->i_data.private_lock);
		lafs_release_index(&LAFSI(inode)->free_index);
		call_rcu(&LAFSI(inode)->md.rcu,
			 kfree_inode);
	}
}

static int lafs_sync_fs(struct super_block *sb, int wait)
{
	if (fs_from_sb(sb)->thread == NULL)
		/* Filesystem in not active - nothing to sync */
		return 0;
	if (!wait)
		/* We only reach here if s_dirt was set, so it
		 * is reasonable to force a checkpoint.
		 */
		lafs_checkpoint_start(fs_from_sb(sb));
	else
		lafs_checkpoint_wait(fs_from_sb(sb));
	return 0;
}

static int lafs_statfs(struct dentry *de, struct kstatfs *buf)
{
	int i;
	u32 fsid;
	u32 *fsuuid;
	struct fs *fs = fs_from_inode(de->d_inode);
	struct lafs_inode *fsroot = LAFSI(LAFSI(de->d_inode)->filesys);
	struct lafs_inode *laroot = LAFSI(fs->ss[0].root);

	fsid = 0;
	fsuuid = (u32 *)fs->state->uuid;
	for (i = 0; i < 16 / 4 ; i++) {
		fsid ^= le32_to_cpu(fsuuid[i]);
		buf->f_fsid.val[i/2] = fsid;
	}
	buf->f_fsid.val[1] ^= fsroot->vfs_inode.i_ino;
	buf->f_type = 0x4C614654; /* "LaFS" */
	buf->f_bsize = fs->blocksize;
	buf->f_blocks = fsroot->md.fs.blocks_allowed;
	if (buf->f_blocks == 0) {
		/* should subtract usage of all other filesystems...*/
		for (i = 0; i < fs->devs_loaded; i++)
			buf->f_blocks += fs->devs[i].size;
	}

	buf->f_files = 0;
	buf->f_ffree = 0;
	buf->f_namelen = 255;
	buf->f_frsize = 0;

	spin_lock(&laroot->vfs_inode.i_lock);
	/* "bavail" is "blocks we could succeed in adding to the filesystem".
	 * "bfree" is effectively total blocks - used blocks
	 */
	buf->f_bavail = fs->free_blocks + fs->clean_reserved - fs->allocated_blocks;
	spin_unlock(&laroot->vfs_inode.i_lock);
	spin_lock(&fsroot->vfs_inode.i_lock);
	buf->f_bfree = buf->f_blocks - (fsroot->md.fs.cblocks_used +
					fsroot->md.fs.pblocks_used +
					fsroot->md.fs.ablocks_used);
	if (buf->f_bfree < buf->f_bavail)
		buf->f_bavail = buf->f_bfree;
	dprintk("df: tot=%ld free=%ld avail=%ld(%ld-%ld-%ld) cb=%ld pb=%ld ab=%ld\n",
		(long)buf->f_blocks, (long)buf->f_bfree, (long)buf->f_bavail,
		(long)fs->free_blocks, (long)fs->clean_reserved,
		(long)fs->allocated_blocks,
		(long)fsroot->md.fs.cblocks_used, (long)fsroot->md.fs.pblocks_used,
		(long)fsroot->md.fs.ablocks_used);
	spin_unlock(&fsroot->vfs_inode.i_lock);
	return 0;
}

static struct super_operations lafs_sops = {
	.alloc_inode	= lafs_alloc_inode,
	.destroy_inode	= lafs_destroy_inode,  /* Inverse of 'alloc_inode' */
	/* Don't use read_inode */
	.dirty_inode	= lafs_dirty_inode,
	/* .write_inode	not needed */
	/* put_inode ?? */

	.evict_inode	= lafs_evict_inode,
	.put_super	= lafs_put_super,
	.sync_fs	= lafs_sync_fs,
	/* write_super_lockfs ?? */
	/* unlockfs ?? */
	.statfs		= lafs_statfs,
	/* remount_fs ?? */
};

MODULE_AUTHOR("Neil Brown");
MODULE_DESCRIPTION("LaFS - Log Structured File System");
MODULE_LICENSE("GPL");
module_init(lafs_init);
module_exit(lafs_exit);
int lafs_trace = 1;
module_param(lafs_trace, int, 0644);

#ifdef DUMP
struct fs *dfs;
static int do_dump(const char *val, struct kernel_param *kp)
{
	extern void lafs_dump_orphans(void);
	extern void lafs_dump_tree(void);
	extern void lafs_dump_cleanable(void);
	extern void lafs_dump_usage(void);

	printk("Want dump of %s\n", val);
	if (strncmp(val, "orphan", 6) == 0)
		lafs_dump_orphans();
	if (strncmp(val, "tree", 4) == 0)
		lafs_dump_tree();
	if (strncmp(val, "cleanable", 9) == 0)
		lafs_dump_cleanable();
	if (strncmp(val, "usage", 5) == 0)
		lafs_dump_usage();
	return 0;
}

static int get_dump(char *buffer, struct kernel_param *kp)
{
	strcpy(buffer, "orphans,tree,cleanable,usage");
	return strlen(buffer);
}

int arg;
module_param_call(dump, do_dump, get_dump, &arg, 0775);
#endif
