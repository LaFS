
/*
 * fs/lafs/link.c
 * Copyright (C) 2005
 * Neil Brown <neilb@suse.de>
 * Released under the GPL, version 2
 *
 * Symlink operations
 */

#include	"lafs.h"

const struct inode_operations lafs_link_ino_operations = {
	.readlink	= generic_readlink,
	.follow_link	= page_follow_link_light,
	.put_link	= page_put_link,
	.getattr	= lafs_getattr,
};
