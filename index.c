
/*
 * Handle index block for LaFS.
 * fs/lafs/index.c
 * Copyright (C) 2005-2009
 * Neil Brown <neilb@suse.de>
 * Released under the GPL, version 2
 */

#include	"lafs.h"
#include	<linux/types.h>
#include	<linux/hash.h>
#include	<linux/slab.h>
/*
 * For each index-block there is an 'indexblock' data structure,
 * and a 'block' of memory, which is a page or part there-of.
 *
 * Index-blocks are stored in a hash table, indexed by
 * (struct inode *, fileaddr, depth).  There is one hash table
 * for all filesystems.
 *
 * There is a single free-list for all index blocks from all
 * filesystems, of all sizes.  Index blocks only get on this list when
 * they are unpinned leafs.  i.e. their 'children' list is empty.  As they
 * are freed, their parents may get added to the list.
 *
 * It's not clear yet how memory pressure will flush out dirty index blocks.
 *
 * All indexblocks for an inode are either attached under the inode via
 * the children/siblings tree, or on a per-inode 'free' list also using
 * the 'siblings' linkage. Thus we are sure that they can be removed when
 * the inode is removed.  Note that data blocks do *not* necessarily live
 * in this tree (though they will when they are dirty).  They can always be
 * found in the radix_tree for the inode's address-space.
 *
 * STILL TO DO:
 *   Consider how 'rcu' can be used for
 *     - hash table
 *     - freelist
 *   Consider gang-freeing for adding to head of lru lists.
 */

/* FIXME these should be configured at runtime based on memory */
#define	HASHBITS	10
static struct hlist_head hash_table[1<<HASHBITS];
spinlock_t lafs_hash_lock;

/* static */ struct freelists freelist;

static int lafs_shrinker(struct shrinker *s,
			 int nr_to_scan, /*gfp_t*/unsigned int gfp_mask)
{
	if (nr_to_scan) {
		LIST_HEAD(togo);
		if (!(gfp_mask & __GFP_FS))
			return -1;
		dprintk("scan %d\n", nr_to_scan);
		spin_lock(&lafs_hash_lock);
		while (nr_to_scan && !list_empty(&freelist.lru)) {
			struct indexblock *ib = list_entry(freelist.lru.next,
							   struct indexblock,
							   b.lru);
			/* Check if it is really free */

			if (atomic_read(&ib->b.refcnt)) {
				list_del_init(&ib->b.lru);
				clear_bit(B_OnFree, &ib->b.flags);
				freelist.freecnt--;
				continue;
			}
			if (test_bit(B_InoIdx, &ib->b.flags))
				LAFSI(ib->b.inode)->iblock = NULL;

			/* Any pinned iblock must have children, be
			 * refcounted, or be on a 'leaf' lru, which gives
			 * a refcount.  So this unreffed block cannot be
			 * pinned.  This BUG_ON is here because previously
			 * the above 'if' also tested for 'Pinned', and that
			 * seems unnecessary, but I want to be sure.
			 */
			LAFS_BUG(test_bit(B_Pinned, &ib->b.flags), &ib->b);
			if (!hlist_unhashed(&ib->hash))
				hlist_del_init(&ib->hash);
			/* delete from inode->free_index */
			list_del_init(&ib->b.siblings);
			list_move(&ib->b.lru, &togo);
			nr_to_scan--;
			freelist.freecnt--;
		}
		spin_unlock(&lafs_hash_lock);
		while (!list_empty(&togo)) {
			struct indexblock *ib = list_entry(togo.next,
							   struct indexblock,
							   b.lru);
			list_del(&ib->b.lru);
			lafs_iblock_free(ib);
		}
	}
	return freelist.freecnt;
}

void lafs_release_index(struct list_head *head)
{
	/* This is the other place that index blocks get freed.
	 * They are freed either by memory pressure in lafs_shrinker
	 * above, or when an inode is destroyed by this code.
	 * They need to both obey the same locking rules, and ensure
	 * blocks are removed from each of siblings, lru, and hash.
	 * This list is an inode's free_index and are linked on
	 * b.siblings.  They are all on the freelist.lru
	 */
	LIST_HEAD(togo);
	spin_lock(&lafs_hash_lock);
	while (!list_empty(head)) {
		struct indexblock *ib = list_entry(head->next,
						   struct indexblock,
						   b.siblings);
		list_del_init(&ib->b.siblings);
		LAFS_BUG(!test_bit(B_OnFree, &ib->b.flags), &ib->b);
		if (!hlist_unhashed(&ib->hash))
			hlist_del_init(&ib->hash);
		list_move(&ib->b.lru, &togo);
		freelist.freecnt--;
	}
	spin_unlock(&lafs_hash_lock);
	while (!list_empty(&togo)) {
		struct indexblock *ib = list_entry(togo.next,
						   struct indexblock,
						   b.lru);
		list_del(&ib->b.lru);
		lafs_iblock_free(ib);
	}
}

struct indexblock *
lafs_iblock_alloc(struct fs *fs, int gfp, int with_buffer, REFARG)
{
	struct indexblock *ib;

	ib = kzalloc(sizeof(*ib), gfp);
	if (!ib)
		return NULL;
	if (with_buffer) {
		ib->data = kmalloc(fs->blocksize, gfp);
		if (!ib->data) {
			kfree(ib);
			return NULL;
		}
	} else
		ib->data = NULL;

	ib->b.flags = 0;
	set_bit(B_Index, &ib->b.flags);
	atomic_set(&ib->b.refcnt, 1);
	add_ref(&ib->b, REF, __FILE__, __LINE__);

	INIT_LIST_HEAD(&ib->children);
	atomic_set(&ib->pincnt[0], 0);
	atomic_set(&ib->pincnt[1], 0);
	ib->uninc = NULL;
	ib->uninc_next = NULL;

	ib->b.parent = NULL;
	INIT_LIST_HEAD(&ib->b.siblings);
	INIT_LIST_HEAD(&ib->b.lru);
	INIT_LIST_HEAD(&ib->b.peers);
	INIT_HLIST_NODE(&ib->hash);
	ib->b.chain = NULL;
	ib->uninc_table.pending_cnt = 0;
	ib->uninc_table.credits = 0;
	ib->b.inode = NULL;
	return ib;
}

void lafs_iblock_free(struct indexblock *ib)
{
	kfree(ib->data);
	kfree(ib);
}

static struct shrinker hash_shrink = {
	.shrink = lafs_shrinker,
	.seeks = DEFAULT_SEEKS,
};

int lafs_ihash_init(void)
{
	int i;
	for (i = 0 ; i < (1<<HASHBITS); i++)
		INIT_HLIST_HEAD(hash_table+i);
	spin_lock_init(&lafs_hash_lock);
	INIT_LIST_HEAD(&freelist.lru);
	register_shrinker(&hash_shrink);

	return 0;
}

void lafs_ihash_free(void)
{
	unregister_shrinker(&hash_shrink);
}

static int
ihash(struct inode *inode, faddr_t addr, int depth)
{
	unsigned long h = hash_ptr(inode, BITS_PER_LONG);
	h = hash_long(h ^ addr, BITS_PER_LONG);
	return hash_long(h ^ depth, HASHBITS);
}

static struct indexblock *
ihash_lookup(struct inode *inode, faddr_t addr, int depth,
	     struct indexblock *new, REFARG)
{
	struct indexblock *ib = NULL;
	struct hlist_node *tmp;
	struct hlist_head *head =  &hash_table[ihash(inode, addr, depth)];

	spin_lock(&lafs_hash_lock);
	hlist_for_each_entry(ib, tmp, head, hash)
		if (ib->b.fileaddr == addr && ib->b.inode == inode &&
		    ib->depth == depth)
			break;
	if (!tmp) {
		if (new) {
			hlist_add_head(&new->hash, head);
			ib = new;
		} else
			ib = NULL;
	}
	if (ib)
		getiref_locked_needsync(ib, REF);
	spin_unlock(&lafs_hash_lock);
	if (ib)
		sync_ref(&ib->b);
	return ib;
}

struct indexblock *lafs_getiref_locked(struct indexblock *ib)
{
	int ok = 0;
	if (!ib)
		return ib;
	LAFS_BUG(!test_bit(B_InoIdx, &ib->b.flags), &ib->b);
	if (spin_is_locked(&ib->b.inode->i_data.private_lock))
		ok = 1;
	if (spin_is_locked(&lafs_hash_lock))
		ok = 1;
	if (spin_is_locked(&fs_from_inode(ib->b.inode)->lock))
		ok = 1;
	LAFS_BUG(!ok, &ib->b);

	if (atomic_inc_return(&ib->b.refcnt) == 1) {
		/* First reference to an InoIdx block implies a reference
		 * to the dblock
		 * If two different threads come here under different locks, one could
		 * exit before the ref on the dblock is taken.  However as each
		 * thread must own an indirect reference to be here, the last indirect ref to
		 * the dblock cannot disappear before all threads have exited this function,
		 * by which time this direct ref will be active.
		 */
		struct datablock *db;
		db = getdref(LAFSI(ib->b.inode)->dblock, MKREF(iblock));
		LAFS_BUG(db == NULL, &ib->b);
	}
	return ib;
}

struct indexblock *
lafs_iblock_get(struct inode *ino, faddr_t addr, int depth, paddr_t phys, REFARG)
{
	/* Find the index block with this inode/depth/address.
	 * If it isn't in the cache, create it with given
	 * phys address
	 */
	struct indexblock *ib = ihash_lookup(ino, addr, depth, NULL, REF);
	struct indexblock *new;
	if (ib)
		return ib;

	if (!phys)
		return NULL;

	new = lafs_iblock_alloc(fs_from_inode(ino), GFP_KERNEL, 1, REF);
	if (!new)
		return NULL;
	new->b.fileaddr = addr;
	new->b.inode = ino;
	new->depth = depth;
	new->b.physaddr = phys;
	set_bit(B_PhysValid, &new->b.flags);

	ib = ihash_lookup(ino, addr, depth, new, REF);
	if (ib != new)
		lafs_iblock_free(new);
	return ib;
}

void lafs_hash_iblock(struct indexblock *ib)
{
	/* This block was an InoIdx block but has just become a real
	 * index block, so hash it.  There cannot be a block with this
	 * address already as we haven't increased the inode depth yet.
	 * Alternately this block was recently split off another,
	 * and has now been incorporated, so a lookup might need
	 * to find it.  So it better be in the hash table.
	 */
	struct hlist_head *head =
		&hash_table[ihash(ib->b.inode, ib->b.fileaddr, ib->depth)];
	spin_lock(&lafs_hash_lock);
	LAFS_BUG(!hlist_unhashed(&ib->hash), &ib->b);
	hlist_add_head(&ib->hash, head);
	spin_unlock(&lafs_hash_lock);
}

void lafs_unhash_iblock(struct indexblock *ib)
{
	/* This block has been emptied and deleted and is no longer
	 * wanted in the hash tables
	 */
	spin_lock(&lafs_hash_lock);
	LAFS_BUG(hlist_unhashed(&ib->hash), &ib->b);
	hlist_del_init(&ib->hash);
	spin_unlock(&lafs_hash_lock);
}

/* adopt blk into parent if possible.
 */
static void
block_adopt(struct block *blk, struct indexblock *parent)
{
	struct address_space *as;

	if (parent == NULL) {
		LAFS_BUG(!test_bit(B_Root, &blk->flags), blk);
		return;
	}

	if (blk->parent) {
		LAFS_BUG(blk->parent != parent, blk);
		return;
	}

	LAFS_BUG(!(parent->b.flags & B_Valid), &parent->b);

	as = &blk->inode->i_data;

	spin_lock(&as->private_lock);
	if (blk->parent) {
		if (blk->parent != parent) {
			printk("blk = %s\n", strblk(blk));
			printk("blk->p = %s\n", strblk(&blk->parent->b));
			printk("parent = %s\n", strblk(&parent->b));
			lafs_dump_tree();
		}
		LAFS_BUG(blk->parent != parent, blk);
	} else {
		if (test_bit(B_EmptyIndex, &parent->b.flags)) {
			LAFS_BUG(!test_bit(B_Index, &parent->b.flags), &parent->b);
			LAFS_BUG(test_bit(B_InoIdx, &parent->b.flags), &parent->b);
			LAFS_BUG(parent->b.fileaddr != parent->b.parent->b.fileaddr, &parent->b);

			clear_bit(B_EmptyIndex, &parent->b.flags);
		}
		LAFS_BUG(parent == iblk(blk), blk);
		blk->parent = parent;
		getiref(parent, MKREF(child));
		/* Remove from the per-inode free list */
		spin_lock(&lafs_hash_lock);
		if (!test_bit(B_InoIdx, &blk->flags))
			list_move(&blk->siblings, &parent->children);
		else {
			/* Remove from ->free_index list */
			list_del_init(&blk->siblings);
			/* Having set ->parent, we take a ref on the dblock.
			 * The dblock must exist because we can only reach here from
			 * lafs_make_iblock which can only be called while holding a ref
			 * on the dblock
			 */
			(void)getdref(LAFSI(blk->inode)->dblock, MKREF(pdblock));
		}
		spin_unlock(&lafs_hash_lock);
	}
	spin_unlock(&as->private_lock);
}

static int __must_check
find_block(struct datablock *b, int adopt, int async);

static int __must_check
setparent(struct datablock *blk, int async)
{
	int err = 0;
	if (blk->b.parent == NULL && !test_bit(B_Root, &blk->b.flags))
		err = find_block(blk, 1, async);
	return err;
}

int __must_check
lafs_setparent(struct datablock *blk)
{
	return setparent(blk, 0);
}

void lafs_pin_block_ph(struct block *b, int ph)
{
	struct fs *fs = fs_from_inode(b->inode);
	struct block *p;
	struct inode *ino;

	LAFS_BUG(b->parent == NULL && !test_bit(B_Root, &b->flags), b);
	if (!test_bit(B_Realloc, &b->flags) &&
	    LAFSI(b->inode)->type != TypeSegmentMap)
		LAFS_BUG(!test_phase_locked(fs), b);

	ino = b->inode;
	spin_lock(&ino->i_data.private_lock);
	for (p = b ;
	     p && !test_bit(B_Pinned, &p->flags) ;
	     p = &(p->parent)->b) {
		set_phase(p, ph);
		if (test_bit(B_InoIdx, &p->flags)) {
			struct datablock *db = LAFSI(p->inode)->dblock;

			spin_unlock(&ino->i_data.private_lock);
			lafs_inode_checkpin(p->inode);
			ino = db->b.inode;
			spin_lock(&ino->i_data.private_lock);
		}
	}
	spin_unlock(&ino->i_data.private_lock);
	lafs_refile(b, 0);
}

static void pin_all_children(struct indexblock *ib)
{
	/* If we cannot get new credits for phase_flip, we need to
	 * pin all dirty data-block children so that they get
	 * written and don't remain to require a phase_flip
	 * on the next checkpoint (at which point there will be
	 * no credit left)
	 * So if any dirty child is found, set phase.  We don't
	 * need PinPending as it is already Dirty.  All other
	 * requirement for pinning will already be met.
	 */
	struct block *b;
	struct indexblock *start = NULL;
	int depth = 0;
	struct fs *fs = fs_from_inode(ib->b.inode);
	int ph = fs->phase;

	/* Locking:
	 * This is called with an IOLock on ib,  So it is unlikely
	 * that children will be added or removed(?), but we really
	 * should hole the inode private_lock anyway.
	 */
	getiref(ib, MKREF(pinall));
	b = NULL;
restart:
	/* We hold a reference on ib, and on 'start' if non-NULL */
	spin_lock(&ib->b.inode->i_data.private_lock);
	b = list_prepare_entry(b, &ib->children, siblings);
	list_for_each_entry_continue(b, &ib->children, siblings) {
		/* FIXME I don't descend through inode blocks to the file below! */
		if (test_bit(B_Index, &b->flags)) {
			/* restart down */
			depth++;
			getref_locked(b, MKREF(pinall));
			spin_unlock(&ib->b.inode->i_data.private_lock);
			putiref(ib, MKREF(pinall));
			ib = iblk(b);
			b = NULL;
			goto restart;
		} else if (test_bit(B_Dirty, &b->flags) &&
			   !test_bit(B_Pinned, &b->flags)) {
			getref_locked(b, MKREF(pinall));
			spin_unlock(&ib->b.inode->i_data.private_lock);
			set_phase(b, ph);
			putref(b, MKREF(pinall));
			/* Slightly safer to restart this level */
			b = NULL;
			goto restart;
		}
	}
	spin_unlock(&ib->b.inode->i_data.private_lock);
	putiref(start, MKREF(pinall));
	if (depth) {
		depth--;
		start = ib;
		ib = getiref(ib->b.parent, MKREF(pinall));
		b = &start->b;
		goto restart;
	}
	putiref(ib, MKREF(pinall));
}

static void flip_phase(struct block *b)
{
	struct indexblock *p;
	int oldphase = !!test_bit(B_Phase1, &b->flags);

	LAFS_BUG(!test_bit(B_Pinned, &b->flags), b);
	if (oldphase)
		clear_bit(B_Phase1, &b->flags);
	else
		set_bit(B_Phase1, &b->flags);

	p = b->parent;

	if (p) {
		atomic_inc(&p->pincnt[1-oldphase]);
		atomic_dec(&p->pincnt[oldphase]);
	} else
		LAFS_BUG(!test_bit(B_Root, &b->flags), b);

	/* move 'next' credits to 'here' */
	if (!test_bit(B_Credit, &b->flags) &&
	    test_and_clear_bit(B_NCredit, &b->flags))
		set_bit(B_Credit, &b->flags);

	if (!test_bit(B_ICredit, &b->flags) &&
	    test_and_clear_bit(B_NICredit, &b->flags))
		set_bit(B_ICredit, &b->flags);
}

/* When the pinning of a block needs to be carried across a
 * checkpoint, we need to 'flip' the phase.
 * This only applies to blocks that can be pinned by a block that
 * may not be written until the next phase.
 * This includes any index block (is it may have some children in one
 * phase and some in the next) and any space-accounting block
 * for the same reason.
 * So indexblocks will need to flip, and use lafs_phase_flip.
 * TypeSegmentMap and TypeQuota also need to flip and use lafs_flip_dblock.
 * TypeInodeFile don't need to be phase_flipped, though their InoIdx
 * block might.  InodeFile blocks are only pinned by metadata transactions
 * which happen inside a checkpoint lock.
 */

void lafs_flip_dblock(struct datablock *db)
{
	/* This is an accounting block (SegmentMap or Quota)
	 * which we need to write out after ->phase has changed
	 * in the tail of the checkpoint.
	 * We always flip the phase and reallocate from AccountSpace.
	 * If all references get dropped, it will then get unpinned
	 * before the next phase finished - we don't unpin here
	 * (unlike lafs_phase_flip for index blocks).
	 */
	flip_phase(&db->b);
	lafs_prealloc(&db->b, AccountSpace);
	/* Parent might need to be on a leaflist now */
	lafs_refile(&db->b.parent->b, 0);
}

void lafs_phase_flip(struct fs *fs, struct indexblock *ib)
{
	/* We are performing a checkpoint, this block has been written
	 * out and now needs to be flipped into the next phase.
	 *
	 * It involves.
	 *  - Processing all uninc_next blocks into uninc_table.
	 *  - adjusting counts on parent
	 *  - moving credits from 'next' to 'this' phase.
	 *  - update block counts to included phase-delayed updates.
	 */
	int oldphase = !!test_bit(B_Phase1, &ib->b.flags);
	struct block *ulist;

	dprintk("FLIP %s\n", strblk(&ib->b));

	LAFS_BUG(!test_bit(B_Pinned, &ib->b.flags), &ib->b);
	LAFS_BUG(!test_bit(B_Index, &ib->b.flags), &ib->b);
	LAFS_BUG(ib->b.parent == NULL && !test_bit(B_Root, &ib->b.flags), &ib->b);
	LAFS_BUG(ib->uninc_table.pending_cnt, &ib->b);
	LAFS_BUG(atomic_read(&ib->pincnt[oldphase]), &ib->b);
	LAFS_BUG(!test_bit(B_IOLock, &ib->b.flags), &ib->b);

	/* If this is the only reference, and pincnts are zero
	 * and all relevant flags are clear, then don't flip
	 * phase but unpin.
	 */
	if (test_bit(B_Pinned, &ib->b.flags) &&
	    !test_bit(B_Dirty, &ib->b.flags) &&
	    !test_bit(B_Realloc, &ib->b.flags) &&
	    atomic_read(&ib->b.refcnt) == 1 && /* This is us */

	    ib->uninc_table.pending_cnt == 0 &&
	    ib->uninc == NULL &&
	    ib->uninc_next == NULL &&
	    atomic_read(&ib->pincnt[0]) == 0 &&
	    atomic_read(&ib->pincnt[1]) == 0 &&

	    (!test_bit(B_InoIdx, &ib->b.flags) ||
	     !(test_bit(B_PinPending, &LAFSI(ib->b.inode)->dblock->b.flags)
	       || test_bit(B_Dirty, &LAFSI(ib->b.inode)->dblock->b.flags)
	       || test_bit(B_Realloc, &LAFSI(ib->b.inode)->dblock->b.flags)))
		) {
		if (test_and_clear_bit(B_Pinned, &ib->b.flags)) {
			if (!test_bit(B_Root, &ib->b.flags)) {
				atomic_dec(&ib->b.parent->pincnt[oldphase]);
				lafs_refile(&ib->b.parent->b, 0);
			}
			lafs_inode_checkpin(ib->b.inode);
		}
		if (test_bit(B_InoIdx, &ib->b.flags))
			/* maybe data block should go in leaf list now */
			lafs_refile(&LAFSI(ib->b.inode)->dblock->b, 0);
		lafs_iounlock_block(&ib->b);
		return;
	}

	flip_phase(&ib->b);

	if (test_bit(B_InoIdx, &ib->b.flags)) {
		struct lafs_inode *lai = LAFSI(ib->b.inode);
		struct datablock *db = lai->dblock;
		struct inode *ino = db->b.inode;

		spin_lock(&ino->i_data.private_lock);
		lai->cblocks += lai->pblocks;
		lai->pblocks = 0;
		lai->ciblocks += lai->piblocks;
		lai->piblocks = 0;
		if (lai->type == TypeInodeFile) {
			/* Root of filesystem */
			lai->md.fs.cblocks_used +=
				lai->md.fs.pblocks_used;
			lai->md.fs.pblocks_used = 0;
		}
		spin_unlock(&ino->i_data.private_lock);

		/* maybe data block needs to be on leaf list */
		lafs_refile(&db->b, 0);
	}

	if (lafs_prealloc(&ib->b, ReleaseSpace) < 0) {
		/* Couldn't get all N*Credit, so
		 * Pin all children to this phase, so they
		 * won't be needed.
		 */
		pin_all_children(ib);
	}

	spin_lock(&ib->b.inode->i_data.private_lock);
	ulist = ib->uninc_next;
	ib->uninc_next = NULL;
	spin_unlock(&ib->b.inode->i_data.private_lock);

	lafs_iounlock_block(&ib->b);

	if (ulist)
		lafs_dirty_iblock(ib, 0);
	while (ulist) {
		struct block *b2 = ulist;
		ulist = b2->chain;
		b2->chain = NULL;
		clear_bit(B_Uninc, &b2->flags);
		while (lafs_add_block_address(fs, b2) == 0)
			/* We had to incorporate b2->parent which might
			 * have split the parent but as that will have made
			 * it dirty, there is nothing extra that we need to
			 * do here
			 */
			;
		putref(b2, MKREF(uninc));
	}
	LAFS_BUG(ib->uninc_next, &ib->b);

	lafs_refile(&ib->b, 0);
	if (ib->b.parent)
		/* Parent may need to be attached to a phase_leafs now */
		lafs_refile(&ib->b.parent->b, 0);
}

/*
 * lafs_refile is called whenever we finish doing stuff
 * to a block.  lafs_refile checks the state of the block and
 * make sure various lists etc are correct.
 * When finished with a block we often want to drop a reference,
 * and this functionality is included in refile so that the
 * decref and associated refiling can be done under a lock.
 *
 * Key issues that lafs_refile deals with are:
 *   Make sure ->lru is on correct list.
 *   Remove ->parent link if no longer needed.
 *   remove B_Lock if no longer needed
 *   trigger a phase change when appropriate
 *   Remove dir parent link if no longer needed.
 *
 * Fleshing these out a bit:
 *   possible lru lists are:
 *      phase_leafs[phase] - for dirty/pinned blocks with no pinned children
 *      clean_leafs - for blocks that are being relocated for cleaning
 *                       and that have no pinned children
 *   The parent link is only needed if this block has a refcount or one
 *     of B_Pinned, B_Dirty, B_Uninc
 *   B_Lock is only needed if one of
 *      B_Dirty
 *    or refcount
 *  though a refcount on a pinned datablock is only significant
 *         while the current phase is locked.
 *
 *  a phase change can (and should) happen for index blocks
 *  in the 'other' phase that are
 *    not Dirty, not Alloc, pincnt[oldphase]==0, uninc-table empty
 *
 *
 * Refiling one block may cause a change in another block which requires
 * a recursive refile.  Naturally we unroll the tail-recursion, but we
 * we need to be sure there are a limited number of tails.
 * The causes of recursion are:
 *   A datablock which no-longer has a PagePrivate page holds a ref
 *      on block0 of that page, which must be dropped on last put.
 *   When any block becomes unpinned, we must refile the parent.
 *   When we clear ->parent we must deref the parent.
 *   When an InoIdx block drops ->parent it is dropped from inode->iblock
 *      so we need to drop the implied refcnt on ->dblock
 * So there can only be 2 block to be recursed on to.
 *  A block0 or dblock, and a parent.
 * A block pointing to a block0 never has a parent, and an iblock has the same
 * parent as the dblock, so there can never be more than 2 outstanding blocks.
 */
int lafs_is_leaf(struct block *b, int ph)
{
	/* This is pinned and not iolocked.  Should it be on a leaf
	 * list?
	 */
	struct inode *myi;
	struct lafs_inode *lai;
	int rv = 1;

	if (!test_bit(B_Pinned, &b->flags) ||
	    test_bit(B_Writeback, &b->flags))
		return 0;

	if (test_bit(B_Index, &b->flags)) {
		if (ph >= 0)
			return atomic_read(&iblk(b)->pincnt[ph]) == 0;
		return (atomic_read(&iblk(b)->pincnt[0]) +
			atomic_read(&iblk(b)->pincnt[1])) == 0;
	}

	/* This is a data block */
	myi = rcu_my_inode(dblk(b));
	if (!myi) {
		/* Non-inode data blocks are always leafs */
		return 1;
	}
	lai = LAFSI(myi);
	spin_lock(&lai->vfs_inode.i_data.private_lock);
	if (ph < 0)
		ph = !!test_bit(B_Phase1, &b->flags);
	if (lai->iblock &&
	    test_bit(B_Pinned, &lai->iblock->b.flags) &&
	    !!test_bit(B_Phase1, &lai->iblock->b.flags) == ph)
		/* iblock still pinned in the same phase, so
		 * this block isn't a leaf
		 */
		rv = 0;

	spin_unlock(&lai->vfs_inode.i_data.private_lock);
	rcu_iput(myi);
	return rv;
}

/*
 * This for debugging and is racy and is probably only safe on UP
 */
static void check_consistency(struct block *b)
{
	/* sanity tests.
	 * 1/ make sure pincnt is right
	 */
	if (test_bit(B_Index, &b->flags)) {
		int c[2];
		struct block *cb;
		c[0] = c[1] = 0;
		list_for_each_entry(cb, &iblk(b)->children, siblings) {
			struct inode *myi = NULL;
			if (test_bit(B_Pinned, &cb->flags)) {
				int pp = !!test_bit(B_Phase1, &cb->flags);
				c[pp]++;
			}
			/* InoIdx block might be pinned but is not a direct
			 * child */
			if (!test_bit(B_Index, &cb->flags) &&
			    (myi = rcu_my_inode(dblk(cb))) != NULL &&
			    LAFSI(myi)->iblock &&
			    test_bit(B_Pinned, &LAFSI(myi)->iblock->b.flags)) {
				int pp = !!test_bit(B_Phase1,
						    &LAFSI(myi)->iblock->b.flags);
				c[pp]++;
			}
			rcu_iput(myi);
		}
		if (c[0] != atomic_read(&iblk(b)->pincnt[0]) ||
		    c[1] != atomic_read(&iblk(b)->pincnt[1])) {
			printk("%d %d\n", c[0], c[1]);
			lafs_dump_tree();
			LAFS_BUG(1, b);
		}
	}

	if (b->parent) {
		struct indexblock *ib = b->parent;
		int c[2];
		struct block *cb;
		c[0] = c[1] = 0;
		list_for_each_entry(cb, &ib->children, siblings) {
			struct inode *myi = NULL;
			if (test_bit(B_Pinned, &cb->flags)) {
				int pp = !!test_bit(B_Phase1, &cb->flags);
				c[pp]++;
			}
			/* InoIdx block might be pinned but is not a direct
			 * child */
			if (!test_bit(B_Index, &cb->flags) &&
			    (myi = rcu_my_inode(dblk(cb))) != NULL &&
			    LAFSI(myi)->iblock &&
			    test_bit(B_Pinned, &(LAFSI(myi)
						 ->iblock->b.flags))) {
				int pp = !!test_bit(B_Phase1,
						    &LAFSI(myi)
						    ->iblock->b.flags);
				c[pp]++;
			}
			rcu_iput(myi);
		}
		if (c[0] != atomic_read(&ib->pincnt[0]) ||
		    c[1] != atomic_read(&ib->pincnt[1])) {
			printk("badcnt %d %d\n", c[0], c[1]);
			LAFS_BUG(1, &ib->b);
		}
	}
}

static void set_lru(struct block *b)
{
	struct fs *fs;
	int ph = !!test_bit(B_Phase1, &b->flags);

	if (!test_bit(B_OnFree, &b->flags) && !list_empty_careful(&b->lru))
		return;
	if (test_bit(B_IOLock, &b->flags) || !lafs_is_leaf(b, ph))
		return;

	/* This is close enough to a leaf that we should put it on a list.
	 * If we raced and it isn't then it will be found and removed
	 */
	if (test_bit(B_OnFree, &b->flags)) {
		spin_lock(&lafs_hash_lock);
		if (test_and_clear_bit(B_OnFree, &b->flags)) {
			list_del_init(&b->lru);
			freelist.freecnt--;
		}
		spin_unlock(&lafs_hash_lock);
	}
	fs = fs_from_inode(b->inode);
	spin_lock(&fs->lock);
	if (list_empty(&b->lru)) {
		if (test_bit(B_Realloc, &b->flags))
			list_add(&b->lru, &fs->clean_leafs);
		else {
			ph = !!test_bit(B_Phase1, &b->flags);
			list_add(&b->lru, &fs->phase_leafs[ph]);
		}

	}
	spin_unlock(&fs->lock);
}

void lafs_refile(struct block *b, int dec)
{
	struct block *next = NULL, *next_parent = NULL;
	struct fs *fs = NULL;
	u64 physref = 0;

	if (!b)
		return;

	fs = fs_from_inode(b->inode);
	check_consistency(b);

/* To (mostly) avoid recursion, we have a loop which may
 * walk up to the parent.
 * The main reason for holding lafs_hash_lock is that it protects
 * ->lru - i.e. all the lists. FIXME that should be fs->lock
 */
	LAFS_BUG(atomic_read(&b->refcnt) == 0, b);
	while (b) {
		int ph;
		int free_me = 0;
		struct inode *checkpin = NULL;
		struct inode *myi = NULL;
		struct datablock *db;

		set_lru(b);

		if (test_bit(B_Pinned, &b->flags) &&
		    !test_bit(B_Index, &b->flags) &&
		    !test_bit(B_PinPending, &b->flags) &&
		    !test_bit(B_Dirty, &b->flags) &&
		    !test_bit(B_Realloc, &b->flags)
			) {
			/* Don't need to be Pinned any more */
			/* FIXME do I need to lock access to ->parent */
			lafs_checkpoint_lock(fs);
			ph = !!test_bit(B_Phase1, &b->flags);
			if (test_and_clear_bit(B_Pinned, &b->flags)) {
				if (test_bit(B_Dirty, &b->flags) ||
				    test_bit(B_Realloc, &b->flags) ||
				    test_bit(B_PinPending, &b->flags))
					/* lost a race */
					set_phase(b, ph);
				else if (!test_bit(B_Root, &b->flags)) {
					struct block *nb;
					atomic_dec(&b->parent->pincnt[ph]);
					nb = &b->parent->b;
					if (next_parent != nb) {
						LAFS_BUG(next_parent, b);
						next_parent = nb;
						atomic_inc(&nb->refcnt);
					}
				}
			}
			lafs_checkpoint_unlock(fs);
		}

		if (dec && atomic_dec_and_lock(&b->refcnt, &b->inode->i_data.private_lock)) {
			/* PinPending disappears with the last non-lru reference,
			 * (or possibly at other times).
			 */
			clear_bit(B_PinPending, &b->flags);

			ph = !!test_bit(B_Phase1, &b->flags);
			/* See if we still need to be pinned */
			if (test_bit(B_Pinned, &b->flags) &&
			    !test_bit(B_Dirty, &b->flags) &&
			    !test_bit(B_Realloc, &b->flags)) {
				/* Don't need to be Pinned any more */
				if (test_and_clear_bit(B_Pinned, &b->flags)) {
					if (!list_empty_careful(&b->lru)) {
						spin_lock(&fs->lock);
						list_del_init(&b->lru);
						spin_unlock(&fs->lock);
					}
					if (test_bit(B_InoIdx, &b->flags) &&
					    b->inode->i_nlink)
						checkpin = b->inode;
					if (!test_bit(B_Root, &b->flags)) {
						struct block *nb;
						atomic_dec(&b->parent->pincnt[ph]);
						if (test_bit(B_InoIdx, &b->flags))
							nb = &LAFSI(b->inode)->dblock->b;
						else
							nb = &b->parent->b;
						if (next_parent != nb) {
							LAFS_BUG(next_parent, b);
							next_parent = nb;
							atomic_inc(&nb->refcnt);
						}
					}
				}
			}

			/* check the ->parent link */
			if (b->parent &&
			    !(b->flags & (
				      (1<<B_Pinned) |
				      (1<<B_Uninc) |
				      (1<<B_Realloc) |
				      (1<<B_Dirty)))
				) {
				int credits;
				/* Don't need ->parent any more */
				if (next_parent == NULL)
					next_parent = &b->parent->b;
				else if (next_parent == &b->parent->b ||
					 next_parent->parent == b->parent) {
					if (atomic_dec_and_test(&b->parent->b.refcnt))
						LAFS_BUG(1, b);
				} else
					LAFS_BUG(1, b);

				del_ref(&b->parent->b, MKREF(child),
					__FILE__, __LINE__);
				b->parent = NULL;

				if (test_bit(B_InoIdx, &b->flags)) {
					/* While an InoIdx has a parent we hold a count on
					 * the dblock.  Now we have dropped one, we must drop the
					 * other
					 */
					struct datablock *db = LAFSI(b->inode)->dblock;
					if (&db->b.parent->b != next_parent &&
					    &db->b != next_parent) {
						printk("db    = %s\n", strblk(&db->b));
						printk("dp->p = %s\n", strblk(&db->b.parent->b));
						printk("np    = %s\n", strblk(next_parent));
						printk("b     = %s\n", strblk(b));
					}
					BUG_ON(&db->b.parent->b != next_parent && &db->b != next_parent);
					if (atomic_dec_and_test(&next_parent->refcnt))
						LAFS_BUG(1, b);
					next_parent = &db->b;
					del_ref(&db->b, MKREF(pdblock), __FILE__, __LINE__);
				}
				if (test_and_clear_bit(B_SegRef, &b->flags))
					physref = b->physaddr;

				LAFS_BUG(test_bit(B_PrimaryRef, &b->flags), b);
				list_del_init(&b->siblings);

				if (test_bit(B_Index, &b->flags))
					list_add(&b->siblings,
						 &LAFSI(b->inode)->free_index);

				if (test_and_clear_bit(B_Prealloc, &b->flags) &&
				    b->physaddr == 0)
					lafs_summary_allocate(fs, b->inode, -1);
				credits = 0;
				if (test_and_clear_bit(B_Credit, &b->flags))
					credits++;
				if (test_and_clear_bit(B_ICredit, &b->flags))
					credits++;
				if (test_and_clear_bit(B_NCredit, &b->flags))
					credits++;
				if (test_and_clear_bit(B_NICredit, &b->flags))
					credits++;
				if (test_and_clear_bit(B_UnincCredit, &b->flags))
					credits++;
				lafs_space_return(fs, credits);
			}
			if (test_bit(B_Index, &b->flags) &&
			    !(b->flags & (
				      (1<<B_Pinned) |
				      (1<<B_Uninc) |
				      (1<<B_Root) |
				      (1<<B_Realloc) |
				      (1<<B_Dirty)))
				) {
				if (b->parent != NULL)
					/* Could this be uninc - where
					 * is refcnt */
					printk("Problem: %s\n", strblk(b));
				LAFS_BUG(b->parent != NULL, b);
				/* put it on the lru */
				spin_lock(&lafs_hash_lock);
				if (!test_and_set_bit(B_OnFree, &b->flags)) {
					LAFS_BUG(!list_empty(&b->lru), b);
					list_move(&b->lru, &freelist.lru);
					freelist.freecnt++;
				}
				spin_unlock(&lafs_hash_lock);
			}
			/* last reference to a dblock with no page
			 * requires special handling
			 * The first block on a page must be freed,
			 * the other blocks hold a reference on that
			 * first block which must be dropped.
			 * However it is possible that a new reference was taken
			 * via _leafs. If so we have now cleared Pinned so
			 * get_flushable will immediately put this again.
			 * so we should leave this cleanup to later.
			 * Unlike other tests, this one isn't idempotent, so
			 * need the check on refcnt
			 */
			db = dblk(b);
			if (!test_bit(B_Index, &b->flags) &&
			    !PagePrivate(db->page) &&
			    atomic_read(&b->refcnt) == 0) {
				int bits = (PAGE_SHIFT -
					    b->inode->i_blkbits);
				int mask = (1<<bits) - 1;
				int bnum = b->fileaddr & mask;

				LAFS_BUG(next, next);
				if (bnum) {
					next = &db[-bnum].b;
					del_ref(next, "lafs_release_0",
						__FILE__, __LINE__);
				} else {
					free_me = 1;
					put_page(db->page);
				}
			}
			/* last reference to an iblock requires that we
			 * deref the dblock.  We don't need to re-check
			 * refcnt here as a racing getiref_locked will take an
			 * extra dblock reference it.
			 */
			if (test_bit(B_InoIdx, &b->flags)) {
				LAFS_BUG(LAFSI(b->inode)->iblock
					 != iblk(b), b);
				LAFS_BUG(next, next);
				next = &LAFSI(b->inode)->dblock->b;
				del_ref(next, MKREF(iblock),
					__FILE__, __LINE__);
			}

			/* Free a delayed-release inode */
			if (!test_bit(B_Index, &b->flags) &&
			    (myi = rcu_my_inode(dblk(b))) != NULL &&
			    (!PagePrivate(dblk(b)->page) ||
			     test_bit(I_Destroyed, &LAFSI(myi)->iflags))) {
				dblk(b)->my_inode = NULL;
				LAFSI(myi)->dblock = NULL;
				/* Don't need lafs_hash_lock to clear iblock as
				 * new refs on iblock are only taken while holding
				 * dblock, and we know dblock has no references.
				 * You would need an iget to get a ref on dblock now,
				 * and because I_Destroyed, we know that isn't possible.
				 */
				LAFSI(myi)->iblock = NULL;
				spin_unlock(&b->inode->i_data.private_lock);
				if (test_bit(I_Destroyed, &LAFSI(myi)->iflags))
					lafs_destroy_inode(myi);
			} else
				spin_unlock(&b->inode->i_data.private_lock);
		}
		rcu_iput(myi);

		if (physref) {
			lafs_seg_deref(fs, physref, 0);
			physref = 0;
		}

		if (checkpin)
			lafs_inode_checkpin(checkpin);

		if (free_me)
			kfree(dblk(b));
		b = NULL;
		if (next) {
			b = next;
			next = NULL;
		} else if (next_parent) {
			b = next_parent;
			next_parent = NULL;
		}
		dec = 1;
	}
}

/*
 * create (if it doesn't already exist) the 'iblock' for an inode.
 * This is a shadow of the dblock but comes into it's own if/when
 * the inode's indexing tree needs to grow.
 * Return a counted reference to the index block.
 * NOTE: we must hold a reference to ->dblock when we call
 * lafs_make_iblock.
 */
struct indexblock * __must_check
lafs_make_iblock(struct inode *ino, int adopt, int async, REFARG)
{
	struct lafs_inode *lai = LAFSI(ino);
	struct indexblock *ib, *new = NULL;
	struct fs *fs = fs_from_inode(ino);
	int err = 0;

	dprintk("MAKE_IBLOCK %d\n", (int)ino->i_ino);

	BUG_ON(lai->dblock == NULL);
	BUG_ON(atomic_read(&lai->dblock->b.refcnt) == 0);
retry:
	spin_lock(&lafs_hash_lock);
	if (lai->iblock)
		ib = getiref_locked_needsync(lai->iblock, REF);
	else if (new) {
		lai->iblock = new;
		ib = new;
		new = NULL;
		(void)getdref(lai->dblock, MKREF(iblock));
	}
	spin_unlock(&lafs_hash_lock);
	if (new)
		lafs_iblock_free(new);
	if (ib) {
		sync_ref(&ib->b);
		if (adopt) {
			err = setparent(lai->dblock, async);
			if (err == 0)
				block_adopt(&ib->b, lai->dblock->b.parent);
		}
		if (err == 0)
			return ib;
		putiref(ib, REF);
		return ERR_PTR(err);
	}

	new = lafs_iblock_alloc(fs, GFP_KERNEL, 0, REF);
	if (!new)
		return ERR_PTR(-ENOMEM);

	new->b.fileaddr = 0;
	new->b.physaddr = lai->dblock->b.physaddr;
	if (test_bit(B_PhysValid, &lai->dblock->b.flags))
		set_bit(B_PhysValid, &new->b.flags);
	LAFS_BUG(!test_bit(B_Valid, &lai->dblock->b.flags), &lai->dblock->b);
	set_bit(B_Valid, &new->b.flags);
	new->b.inode = ino;
	new->depth = lai->depth;
	/* Note: this doesn't get hashed until the index
	 * tree grows and this block is disconnected from
	 * the inode.
	 */
	set_bit(B_InoIdx, &new->b.flags);
	if (test_bit(B_Root, &lai->dblock->b.flags))
		set_bit(B_Root, &new->b.flags);
	new->b.parent = NULL;

	goto retry;
}

static u64
leaf_lookup(void *bf, int len, u32 startaddr, u32 target, u32 *nextp)
{
	/* buf starts with a 2byte header
	 * if 1, then 6byte littleending indirect entries.
	 * if 2, then 12byte extent entries
	 */
	unsigned char *buf = bf;
	u64 p;
	unsigned char *cp;
	int elen;
	int hi, lo;
	u32 addr, next;

	if (nextp)
		*nextp = 0xffffffff;

	if (buf[1])
		return 0;
	switch (buf[0]) {
	default:
		p = 0;
		break;

	case IBLK_INDIRECT: /* indirect */
		dprintk("indirect lookup for %lu from %lu, len %d\n",
			(unsigned long)target, (unsigned long)startaddr, len);
		len -= 2;
		buf += 2;

		if (target < startaddr)
			return 0;

		next = target;
		target -= startaddr;

		/* Need both tests as target could be v.large and
		 * multiply by 6 could overflow
		 */
		if (target > len ||
		    target*6 + 6 > len)
			return 0;
		buf += target*6;
		p = decode48(buf);
		if (nextp) {
			/* find the next allocated block */
			u64 p2;

			len -= target*6;
			len -= 6;
			next++;
			*nextp = 0xffffffff;
			while (len >= 6) {
				p2 = decode48(buf);
				if (p2) {
					*nextp = next;
					break;
				}
				len -= 6;
				next++;
			}
		}
		break;

	case IBLK_EXTENT: /* extent */
		/* 12 byte records: 6byte device, 2 byte length, 4
		 * byte fileaddr */

		len -= 2;
		buf += 2;
		lo = 0;
		hi = len/12;
		while (hi > lo+1) {
			int mid = (lo+hi)/2;
			cp = buf;
			cp += mid*12 + 6;

			elen = decode16(cp);
			addr = decode32(cp);

			if (elen && addr <= target)
				lo = mid;
			else
				hi = mid;
		}

		cp = buf;
		cp += lo*12;
		p = decode48(cp);
		elen = decode16(cp);
		addr = decode32(cp);
		if (target < addr) {
			p = 0;
			cp -= 12;
		} else if (target >= addr + elen)
			p = 0;
		else
			p += target - addr;

		if (nextp) {
			if (lo == len/12)
				*nextp = 0xffffffff; /* next extent */
			else {
				u64 p2;
				p2 = decode48(cp);
				elen = decode16(cp);
				addr = decode32(cp);
				if (elen == 0)
					*nextp = 0xffffffff; /* no more meaningful
							  extents*/
				else
					*nextp = addr;
			}
		}
	}
	return p;
}

u32 lafs_leaf_next(struct indexblock *ib, u32 start)
{
	/* In this leaf index block, find the first address
	 * at-or-after 'start'.  If none, return 0xFFFFFFFF
	 */
	char *buf = map_iblock(ib);
	int blocksize = ib->b.inode->i_sb->s_blocksize;
	struct lafs_inode *li = LAFSI(ib->b.inode);
	u32 next = 0xffffffff;
	u64 phys;

	if (test_bit(B_InoIdx, &ib->b.flags))
		phys = leaf_lookup(buf + li->metadata_size,
				   blocksize - li->metadata_size,
				   ib->b.fileaddr, start, &next);
	else
		phys = leaf_lookup(buf, blocksize,
				   ib->b.fileaddr, start, &next);
	unmap_iblock(ib, buf);
	if (phys)
		return start;
	else
		return next;
}

static u64
index_lookup(void *bf, int len, u32 target, u32 *addrp, u32 *nextp)
{
	unsigned char *buf = bf;
	int lo, hi;
	u32 addr;
	u64 p;
	unsigned char *cp;

	if (buf[1] || buf[0]) {
		dprintk("WARNING: not an index block %02x %02x\n",
			buf[0], buf[1]);
		return 0;
	}

	buf += 2;
	len -= 2;

	/* 10 byte records: 6 byte Device address, 4 byte file address */
	lo = 0; hi = (len/10);
	while (hi > lo+1) {
		int mid = (lo+hi)/2;
		cp = buf;
		cp += mid*10;
		p = decode48(cp);
		addr = decode32(cp);
		dprintk("...addr=%lu target=%lu lo=%d mid=%d hi=%d\n",
			(unsigned long)addr, (unsigned  long)target,
			lo, mid, hi);
		if (p && addr <= target)
			lo = mid;
		else
			hi = mid;
	}

	cp = buf;
	cp += lo*10;
	p = decode48(cp);
	addr = decode32(cp);

	if (p == 0)
		/* Nothing in this block */
		return 0;

	if (addr > target) {
		/* target is before first address */
		p = 0;
		cp -= 10;
	} else
		*addrp = addr;

	if (cp+10 <= (unsigned char *) buf + len && nextp) {
		u64 p2 = decode48(cp);
		u32 nxt = decode32(cp);
		if (p2)
			*nextp = nxt; /* address of next index block */
	}
	return p;
}

int lafs_index_empty(struct indexblock *ib)
{
	char *buf = map_iblock(ib);
	int blocksize = ib->b.inode->i_sb->s_blocksize;
	struct lafs_inode *li = LAFSI(ib->b.inode);
	u32 addr;
	u64 phys;

	if (test_bit(B_InoIdx, &ib->b.flags))
		phys = index_lookup(buf + li->metadata_size,
				    blocksize - li->metadata_size,
				    0xFFFFFFFF, &addr, NULL);
	else
		phys = index_lookup(buf, blocksize,
				    0xFFFFFFFF, &addr, NULL);
	unmap_iblock(ib, buf);

	return phys == 0;
}

static struct indexblock *find_better(struct inode *ino,
				      struct indexblock *ib,
				      u32 addr, u32 *next, REFARG)
{
	struct indexblock *rv = ib;
	spin_lock(&ino->i_data.private_lock);
	list_for_each_entry_continue(ib, &ib->b.parent->children, b.siblings) {
		if (!test_bit(B_PrimaryRef, &ib->b.flags))
			break;
		if (test_bit(B_EmptyIndex, &ib->b.flags))
			continue;
		if (ib->b.fileaddr > addr) {
			if (next && *next > ib->b.fileaddr)
				*next = ib->b.fileaddr;
			break;
		}
		/* This appears to be better. */
		rv = ib;
	}
	getref_locked(&rv->b, REF);
	spin_unlock(&ino->i_data.private_lock);
	return rv;
}

struct indexblock *
lafs_leaf_find(struct inode *inode, u32 addr, int adopt, u32 *next,
	       int async, REFARG)
{
	/* Find the leaf index block which refers to this address (if any do)
	 * Returns the leaf IOLocked.
	 * *next will be set to an address that is higher than addr such
	 * that there are no other leafs before *next.  There might not
	 * actually be a leaf at *next though.
	 */
	struct indexblock *ib, *ib2;
	struct indexblock *hold = NULL;
	u64 iphys;
	u32 iaddr = 0;
	void *buf;
	struct lafs_inode *li = LAFSI(inode);
	int blocksize = inode->i_sb->s_blocksize;
	int err = 0;
	int offset;

	/* Must own a reference to ->dblock */
	BUG_ON(li->dblock == NULL);
	BUG_ON(atomic_read(&li->dblock->b.refcnt) == 0);

	/* We come back to restart if we needed to unlock to
	 * perform IO and we cannot be sure the parent didn't change.
	 * We hold the last seen index block in 'hold' so that if
	 * no changes happened (the common case) we are certain
	 * that no IO will be required to get back to where
	 * we were.
	 */
restart:
	if (next)
		*next = 0xFFFFFFFF;

	ib = lafs_make_iblock(inode, adopt, async, REF);
	err = PTR_ERR(ib);
	if (IS_ERR(ib))
		goto err;
	offset = li->metadata_size;

	err = -EAGAIN;
	if (!async)
		lafs_iolock_block(&ib->b);
	else if (!lafs_iolock_block_async(&ib->b))
		goto err_ib;

	while (ib->depth > 1) {
		/* internal index block */
		/* NOTE:  index block could be empty, or could have
		 * nothing as early as 'addr'.  In that case '0' is
		 * returned implying that the block is either in
		 * cache, or needs to be synthesised as an empty
		 * index block (one level down).
		 */
		struct indexblock *better;
		u32 target = addr;

	retry:
		buf = map_iblock(ib);
		iaddr = ib->b.fileaddr;
		iphys = index_lookup(buf + offset, blocksize - offset,
				     target, &iaddr,
				     target == addr ? next : NULL);
		unmap_iblock(ib, buf);

		ib2 = lafs_iblock_get(inode, iaddr, ib->depth-1, iphys, REF);

		if (!ib2) {
			err = -ENOMEM;
			lafs_iounlock_block(&ib->b);
			goto err_ib;
		}

		better = find_better(inode, ib2, addr, next, REF);
		putiref(ib2, REF);
		ib2 = better;

		if (!test_bit(B_Valid, &ib2->b.flags)) {
			lafs_iounlock_block(&ib->b);
			err = lafs_load_block(&ib2->b, NULL);
			if (err)
				goto err_ib2;
			if (async)
				err = lafs_wait_block_async(&ib2->b);
			else
				err = lafs_wait_block(&ib2->b);
			if (err)
				goto err_ib2;

			err = -EAGAIN;
			if (!async)
				lafs_iolock_block(&ib->b);
			else if (!lafs_iolock_block_async(&ib->b))
				goto err_ib2;

			/* While we did IO, the parent could have been
			 * split, so it is now the wrong parent, or it
			 * could have become EmptyIndex, though that is
			 * much less likely.  If it did, then it must
			 * have had a parent, so the ref we hold means
			 * it still has a parent.
			 * So if ib->b.parent, then we might need to
			 * retry from the top, and holding a ref on ib
			 * means that we don't risk live-locking if
			 * memory for index blocks is very tight.
			 * If there is no parent, then there is
			 * no risk that ib changed.
			 */
			if (ib->b.parent) {
				if (hold)
					putiref(hold, MKREF(hold));
				hold = getiref(ib, MKREF(hold));
				putiref(ib2, REF);
				putiref(ib, REF);
				goto restart;
			}
		}

		err = -EAGAIN;
		if (!async)
			lafs_iolock_block(&ib2->b);
		else if (!lafs_iolock_block_async(&ib2->b))
			goto err_ib2;

		if (ib2->b.fileaddr != ib->b.fileaddr &&
		    test_bit(B_EmptyIndex, &ib2->b.flags)) {
			/* need to look earlier in the parent */
			target = ib2->b.fileaddr - 1;
			lafs_iounlock_block(&ib2->b);
			putiref(ib2, REF);
			goto retry;
		}

#ifdef FIXME
		if (!(ib2->b.flags & B_Linked)) {
			struct block *b2 = peer_find(fs,
						     inode, iaddr,
						     depth, iphys);
			if (b2)
				list_add(&ib2->peers, &b2->peers);
			set_bit(B_Linked, &ib2->b.flags);
		}
#endif
		if (adopt)
			block_adopt(&ib2->b, ib);
		lafs_iounlock_block(&ib->b);
		putiref(ib, REF);
		ib = ib2;
		offset = 0;
	}

	return ib;

err_ib2:
	putiref(ib2, REF);
err_ib:
	putiref(ib, REF);
err:
	if (hold)
		putiref(hold, MKREF(hold));
	return ERR_PTR(err);
}

static int table_find_first(struct uninc *tbl, u32 min, u32 *addr);

static int __lafs_find_next(struct inode *ino, loff_t *addrp)
{
	/* Find the next allocated block in inode at or after *addrp
	 * and set *addrp.
	 * Return:
	 *  -1 -  error
	 *   0 -  there is no block at or after '*addrp'
	 *   1 -  It is worth checking the block at *addrp.
	 *   2 -  *addrp has been updated, check again.
	 */
	struct lafs_inode *lai = LAFSI(ino);
	struct indexblock *leaf;
	struct fs *fs = fs_from_inode(ino);
	struct block *b;
	u32 nexti, nextd, nextl;
	int rv = 0;
	int offset;
	char *buf;
	u64 p;

	/* Must hold a reference to ->dblock */
	BUG_ON(lai->dblock == NULL);
	BUG_ON(atomic_read(&lai->dblock->b.refcnt) == 0);

	if (lai->depth == 0) {
		if (*addrp == 0)
			return 1;
		leaf = lafs_make_iblock(ino, NOADOPT, SYNC, MKREF(find_next));
		if (IS_ERR(leaf))
			return PTR_ERR(leaf);
		nextd = 0xFFFFFFFF;
	} else {
		leaf = lafs_leaf_find(ino, *addrp, NOADOPT, &nexti, SYNC,
				      MKREF(find_next));
		/* FIXME what if 'leaf' is an error?? */
		offset = 0;
		if (lai->depth == 1)
			offset = lai->metadata_size;
		buf = map_iblock(leaf);
		p = leaf_lookup(buf+offset, fs->blocksize-offset,
				leaf->b.fileaddr, (u32)*addrp, &nextd);
		unmap_iblock(leaf, buf);
		if (p) {
			lafs_iounlock_block(&leaf->b);
			putiref(leaf, MKREF(find_next));
			if (*addrp == 0xFFFFFFFF) {
				int j;
				printk("at %s\n", strblk(&lai->dblock->b));
				printk("offset = %d depth=%d\n", offset,
				       lai->depth);
				for (j = 0; j < 16; j++)
					printk("%04x ", buf[offset+j] & 0xffff);
				for (j = 0; j < 16; j++)
					printk("%04x ", buf[1024-16+j] & 0xffff);
				printk("\n");
			}
			BUG_ON(*addrp == 0xFFFFFFFF);
			return 1;
		}
		if (nextd != 0xFFFFFFFF)
			rv = 1;
		else if (nexti != 0xFFFFFFFF) {
			nextd = nexti;
			rv = 2;
		}
	}
	/* If rv==2, nextd may not be high enough as it might
	 * be the address of an EmptyIndex block.  So only
	 * use it if nothing better is found.
	 */
	if (rv == 2)
		nextl = 0xFFFFFFFF;
	else
		nextl = nextd;
	list_for_each_entry(b, &leaf->children, siblings)
		if (b->fileaddr >= *addrp &&
		    b->fileaddr <= nextl &&
		    test_bit(B_Valid, &b->flags)) {
			nextd = nextl = b->fileaddr;
			rv = 1;
		}

	if (leaf->uninc_table.pending_cnt) {
		spin_lock(&leaf->b.inode->i_data.private_lock);
		if (table_find_first(&leaf->uninc_table, *addrp, &nextl)) {
			nextd = nextl;
			rv = 1;
		}
		spin_unlock(&leaf->b.inode->i_data.private_lock);
	}
	lafs_iounlock_block(&leaf->b);
	putiref(leaf, MKREF(find_next));
	*addrp = nextd;
	return rv;
}

/*
 * find the first data block at or after *bnump, and
 * store the address in *bnump.
 * Return 0 if nothing found, -1 on error, or
 * 1 if *bnump is a valid data block address
 *
 * The block could be in the indexing tree, or in
 * an uninc_table, or totally unincorporated.
 * Blocks that are really part of the file, but are not in
 * the indexing tree will still be on a ->children list
 * from an index block, and the index block will be very close
 * to the indexing tree.
 * So once we have found a suitable index block, we need to check
 * all it's children.  This could possibly be optimised using
 * find_get_pages if that was exported.
 */
int lafs_find_next(struct inode *ino, loff_t *bnump)
{
	int rv;
	/* Need to hold a reference to dblock while calling
	 * __lafs_find_next
	 */
	struct datablock *db = lafs_inode_dblock(ino, SYNC, MKREF(find_nexti));

	if (IS_ERR(db))
		return PTR_ERR(db);
	while (1) {
		struct datablock *b;
		int hole;

		rv = __lafs_find_next(ino, bnump);

		if (rv <= 0)
			break;
		if (rv == 2)
			continue;

		b = lafs_get_block(ino, *bnump, NULL, GFP_KERNEL,
				   MKREF(find_next));
		if (!b)
			return -ENOMEM;

		rv = lafs_find_block(b, NOADOPT);
		if (rv) {
			putdref(b, MKREF(find_next));
			return rv;
		}
		hole = (b->b.physaddr == 0 ||
			!test_bit(B_PhysValid, &b->b.flags)) &&
			!test_bit(B_Valid, &b->b.flags);
		if (LAFSI(ino)->depth == 0 &&
		    b->b.fileaddr == 0)
			hole = 0; /* first block lives in inode */
		putdref(b, MKREF(find_next));

		rv = 1;
		if (!hole)
			break;
		*bnump = (*bnump)+1;
	}
	putdref(db, MKREF(find_nexti));
	BUG_ON(rv > 0 && *bnump == 0xFFFFFFFF);
	return rv;
}

static int table_lookup(struct uninc *tbl, u32 addr, u64 *phys)
{
	int i;
	for (i = tbl->pending_cnt; i; ) {
		struct addr *a = &tbl->pending_addr[--i];
		if (a->fileaddr <= addr &&
		    a->fileaddr + a->cnt > addr) {
			*phys =  a->physaddr + (addr - a->fileaddr);
			return 1;
		}
	}
	return 0;
}

static int table_find_first(struct uninc *tbl, u32 min, u32 *addr)
{
	/* Find the earliest allocated block in tbl which is
	 * at-or-after 'min' and before '*addr', and store it in
	 * *addr, returning 1 if something was found.
	 */
	int rv = 0;
	int i;

	for (i = tbl->pending_cnt ; i ; ) {
		struct addr *a = &tbl->pending_addr[--i];
		if (a->fileaddr >= *addr)
			continue;
		if (a->fileaddr + a->cnt < min)
			continue;
		if (a->physaddr == 0)
			continue;
		if (a->fileaddr >= min)
			*addr = a->fileaddr;
		else
			*addr = min;
		rv = 1;
	}
	return rv;
}

static void fill_block_zero(struct datablock *db, struct datablock *inob)
{
	struct lafs_inode *lai = LAFSI(db->b.inode);
	void *baddr = map_dblock(db);
	void *iaddr = map_dblock_2(inob);
	int offset = lai->metadata_size;
	int len = (1<<db->b.inode->i_blkbits) - offset;

	memcpy(baddr, iaddr+offset, len);
	unmap_dblock_2(inob, iaddr);
	memset(baddr+len, 0, (1<<db->b.inode->i_blkbits)-len);
	unmap_dblock(db, baddr);
	set_bit(B_Valid, &db->b.flags);
}

static int __must_check
find_block(struct datablock *b, int adopt, int async)
{
	/* Find where this block is in storage, and
	 * set b->b.physaddr.
	 * If the block lives in the inode, physaddr
	 * gets set to zero, and lafs_load_block works with this.
	 * Otherwise we walk down the index tree
	 * Possible errors:
	 *  -EIO
	 *  -ENOMEM
	 */
	struct lafs_inode *lai;
	struct indexblock *ib;
	struct datablock *db;
	void *buf;
	int  offset;

	LAFS_BUG(test_bit(B_Pinned, &b->b.flags) &&
		 b->b.parent == NULL,
		 &b->b);
	if (b->b.inode == NULL)
		return 0;
	lai = LAFSI(b->b.inode);

	if (lai->depth == 0) {
		/* Data is in the inode if anywhere */
		if (!test_bit(B_PhysValid, &b->b.flags)) {
			b->b.physaddr = 0;
			set_bit(B_PhysValid, &b->b.flags);
		}
		db = lafs_inode_dblock(b->b.inode, async, MKREF(findblock));
		if (IS_ERR(db))
			return PTR_ERR(db);
		if (!test_bit(B_Valid, &b->b.flags) && b->b.fileaddr == 0) {
			/* Now is the best time to copy data from inode into
			 * datablock, as we know we have a valid inode block
			 * now.
			 */
			LAFS_BUG(b->b.physaddr != 0, &b->b);
			fill_block_zero(b, db);
		}
		if (adopt) {
			ib = lafs_make_iblock(b->b.inode, adopt, async,
					      MKREF(findblock));
			putdref(db, MKREF(findblock));
			if (IS_ERR(ib))
				return PTR_ERR(ib);
			block_adopt(&b->b, ib);
			putiref(ib, MKREF(findblock));
		} else
			putdref(db, MKREF(findblock));
		return 0;
	}

	if (test_bit(B_PhysValid, &b->b.flags)
	    && (!adopt || b->b.parent || test_bit(B_Root, &b->b.flags))) {
		return 0;
	}

	db = lafs_inode_dblock(b->b.inode, async, MKREF(findblock));
	if (IS_ERR(db))
		return PTR_ERR(db);

	/* Ok, there is some indexing information we need to
	 * look through.  Find the leaf first
	 */
	ib = lafs_leaf_find(b->b.inode, b->b.fileaddr, adopt, NULL, async,
			    MKREF(findblock));
	putdref(db, MKREF(findblock));

	if (IS_ERR(ib)) {
		dprintk("lafs_leaf_find returned error to lafs_find_block\n");
		goto out;
	}

	if (!test_bit(B_PhysValid, &b->b.flags)
	    && ib->uninc_table.pending_cnt) {
		spin_lock(&b->b.inode->i_data.private_lock);
		if (table_lookup(&ib->uninc_table, b->b.fileaddr,
				 &b->b.physaddr))
			set_bit(B_PhysValid, &b->b.flags);
		spin_unlock(&b->b.inode->i_data.private_lock);
	}
	if (!test_bit(B_PhysValid, &b->b.flags)) {
		buf = map_iblock(ib);
		offset = lai->depth > 1 ? 0 : lai->metadata_size;
		if (test_bit(B_InoIdx, &ib->b.flags))
			dprintk("InoIdx!!!\n");
		dprintk("offset %d: %02x %02x\n", offset,
			((char *)buf)[offset], ((char *)buf)[offset+1]);
		b->b.physaddr = leaf_lookup(buf+offset,
					    b->b.inode->i_sb->s_blocksize
					    - offset,
					    ib->b.fileaddr, b->b.fileaddr,
					    NULL);
		set_bit(B_PhysValid, &b->b.flags);
		unmap_iblock(ib, buf);
	}
	dprintk("lafs_find_block: lookup for %lu of %lu at %lu found %llu\n",
		(unsigned long)b->b.fileaddr,
		(unsigned long)b->b.inode->i_ino,
		(unsigned long)ib->b.fileaddr,
		b->b.physaddr);
	/* FIXME should peer_find go here or in load_block? */

	if (adopt)
		block_adopt(&b->b, ib);
	lafs_iounlock_block(&ib->b);
out:
	if (IS_ERR(ib))
		return PTR_ERR(ib);
	if (ib)
		putiref(ib, MKREF(findblock));
	return 0;
}

int __must_check
lafs_find_block(struct datablock *b, int adopt)
{
	return find_block(b, adopt, SYNC);
}

int __must_check
lafs_find_block_async(struct datablock *b)
{
	return find_block(b, ADOPT, ASYNC);
}

/*
 * lafs_allocated_block records that a block has been written at a new
 * address (or found at an address during roll-forward). This involves
 * updating some summary information (quota etc) and making sure
 * the change gets recorded in the parent block
 * This must be called before B_Dirty or B_Realloc are cleared,
 * to ensure that the index block is marked accordingly.
 *  B_UnincCredit should still be set, and we clear it.
 */

int lafs_add_block_address(struct fs *fs, struct block *blk)
{
	struct indexblock *p = blk->parent;
	struct addr *a;

	spin_lock(&blk->inode->i_data.private_lock);
	if (test_bit(B_Index, &blk->flags)) {
		if (!test_and_set_bit(B_Uninc, &blk->flags)) {
			blk->chain = p->uninc;
			LAFS_BUG(p->depth == 1, blk);
			p->uninc = blk;
			getref(blk, MKREF(uninc));
		}
		spin_unlock(&blk->inode->i_data.private_lock);
		return 1;
	}

	/* same phase and leaf; add the address to uninc_table */

	/* the only place we try to merge is at the end
	 * of the embedded table, and then only for data
	 * blocks
	 * It is essential that uninc_table is sorted and,
	 * in particular, has no overlapping extents.
	 * If this block threatens problems we incorporate first.
	 */

	LAFS_BUG(!test_bit(B_Dirty, &p->b.flags) &&
		 !test_bit(B_Realloc, &p->b.flags), blk);

	a = &p->uninc_table.pending_addr
		[p->uninc_table.pending_cnt-1];
	if (p->uninc_table.pending_cnt >= 1 &&
	    a->fileaddr+a->cnt == blk->fileaddr &&
	    a->physaddr != 0 &&
	    a->physaddr+a->cnt == blk->physaddr) {
		a->cnt++;
		if (test_and_clear_bit(B_UnincCredit, &blk->flags))
			p->uninc_table.credits++;
		spin_unlock(&blk->inode->i_data.private_lock);
		return 1;
	} else if ((p->uninc_table.pending_cnt == 0 ||
		    a->fileaddr + a->cnt <= blk->fileaddr) &&
		   /* properly ordered, maybe add */
		   (p->uninc_table.pending_cnt <
		    ARRAY_SIZE(p->uninc_table.pending_addr))) {
		a++;
		a->fileaddr = blk->fileaddr;
		a->physaddr = blk->physaddr;
		a->cnt = 1;
		p->uninc_table.pending_cnt++;
		if (test_and_clear_bit(B_UnincCredit,
				       &blk->flags))
			p->uninc_table.credits++;
		spin_unlock(&blk->inode->i_data.private_lock);

		return 1;
	} else {
		spin_unlock(&blk->inode->i_data.private_lock);
		/* We own a ref through blk->parent now, but
		 * after lafs_incorporate, we might not
		 */
		getiref(p,MKREF(addblock));
		lafs_iolock_written(&p->b);
		lafs_incorporate(fs, p);
		lafs_iounlock_block(&p->b);
		putiref(p,MKREF(addblock));
		return 0;
	}
}

int lafs_allocated_block(struct fs *fs, struct block *blk, u64 phys)
{
	int ph;
	struct indexblock *p;

	dprintk("Allocated %s -> %llu\n",  strblk(blk), phys);

	/* If phys is 0, I don't need uninc credit.
	 * Also, Index block that are not near full do not need UnincCredits
	 */
	LAFS_BUG(test_bit(B_InoIdx, &blk->flags), blk);
	if (!test_bit(B_Dirty, &blk->flags) &&
	    !test_bit(B_Realloc, &blk->flags) &&
	    phys != 0)
		printk("something missing %s\n", strblk(blk));
	LAFS_BUG(!test_bit(B_UnincCredit, &blk->flags) && phys &&
		 !test_bit(B_Index, &blk->flags), blk);
	LAFS_BUG(!test_bit(B_Dirty, &blk->flags) &&
		 !test_bit(B_Realloc, &blk->flags) &&
		 phys != 0, blk);

	LAFS_BUG(!test_bit(B_Writeback, &blk->flags), blk);

	if (test_bit(B_Root, &blk->flags)) {
		int i;
		struct lafs_inode *lai;

		LAFS_BUG(test_bit(B_Index, &blk->flags), blk);

		for (i = 0; i < fs->maxsnapshot; i++)
			if (fs->ss[i].root == blk->inode)
				break;
		LAFS_BUG(i == fs->maxsnapshot, blk);
		blk->physaddr = phys; /* superblock doesn't get
				       * counted in summaries */
		LAFS_BUG(test_bit(B_SegRef, &blk->flags), blk);
		set_bit(B_PhysValid, &blk->flags);
		fs->ss[i].root_addr = phys;
		lai = LAFSI(blk->inode);
		if (i == 0 && lai->md.fs.usagetable > 1)
			/* snapshot checkpoint is happening.  Record root
			 * address in main fs and in snapshot
			 */
			fs->ss[lai->md.fs.usagetable-1].root_addr = phys;

		/* Don't bother to clear the B_UnincCredit in the root.
		 * It'll just get set again.  This way there is less churn.
		 */
		return 0;
	}
	if (test_bit(FinalCheckpoint, &fs->fsstate) &&
	    !test_bit(B_Index, &blk->flags) &&
	    (LAFSI(blk->inode)->type == TypeSegmentMap ||
	     LAFSI(blk->inode)->type == TypeQuota)) {
		/* These blocks will be picked up by roll-forward,
		 * so don't need to have their address recorded,
		 * and doing so would leave the index blocks
		 * dirty after the checkpoint.
		 * So just return
		 */
		if (test_and_clear_bit(B_SegRef, &blk->flags))
			lafs_seg_deref(fs, blk->physaddr, 0);
		blk->physaddr = phys;
		set_bit(B_PhysValid, &blk->flags);
		return 0;
	}
	p = blk->parent;

	LAFS_BUG(!p, blk);

	/* We need to work out if this block is in the current phase or
	 * the next one.  This can only be an issue if a phase change
	 * (aka checkpoint) is underway. But if it is, we need to
	 * ensure the right phase is used
	 */
	lafs_checkpoint_lock(fs);
	if (test_bit(B_Pinned, &blk->flags))
		ph = !!test_bit(B_Phase1, &blk->flags);
	else if (test_bit(B_Index, &blk->flags))
		LAFS_BUG(1, blk); /* Index blocks must always be pinned when dirty (*/
	else if (p->uninc_table.pending_cnt) {
		/* attach data block into previous phase as incorporation of
		 * this phase is still to happen.
		 * This is needed for flush-if-cannot-preallocate to work.
		 */
		LAFS_BUG(!test_bit(B_Pinned, &p->b.flags), &p->b);
		ph = !!test_bit(B_Phase1, &p->b.flags);
		lafs_pin_block_ph(blk, ph);
	} else {
		lafs_pin_block(blk);
		ph = !!test_bit(B_Phase1, &blk->flags);
	}
	lafs_checkpoint_unlock(fs);
	if (!test_bit(B_PhysValid, &blk->flags))
		blk->physaddr = 0;
	lafs_summary_update(fs, blk->inode, blk->physaddr, phys,
			    test_bit(B_Index, &blk->flags), ph,
			    test_bit(B_SegRef, &blk->flags));
	clear_bit(B_Prealloc, &blk->flags);

	blk->physaddr = phys;
	set_bit(B_PhysValid, &blk->flags);

	if (ph != !!test_bit(B_Phase1, &p->b.flags)) {
		/* in the next phase */
		dprintk("uninc next phase\n");
		if (test_and_set_bit(B_Uninc, &blk->flags))
			/* This block was already on the 'unincorporated'
			 * list, (it must be changing quickly) so there
			 * is nothing else to do.
			 */
			return 0;
		getref(blk, MKREF(uninc));

		spin_lock(&blk->inode->i_data.private_lock);
		blk->chain = p->uninc_next;
		p->uninc_next = blk;
		spin_unlock(&blk->inode->i_data.private_lock);
		/* We will try again after the phase change.
		 * That will be when we clean B_UnincCredit
		 */
		return 0;
	}

new_parent:
	lafs_dirty_iblock(p, !(test_bit(B_Dirty, &blk->flags) || phys == 0));
	dprintk("Uninc same phase\n");

	BUG_ON(!test_bit(B_Pinned, &blk->parent->b.flags));

	if (!lafs_add_block_address(fs, blk)) {
		p = blk->parent;

		LAFS_BUG(!p, blk);
		LAFS_BUG(!test_bit(B_Pinned, &p->b.flags), blk);
		goto new_parent;
	}

	/* That credit has now been added to the uninc_table and
	 * thus made available to the parent
	 */

	lafs_refile(&p->b, 0);
	return 0;
}
