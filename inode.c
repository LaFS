
/*
 * fs/lafs/inode.c
 * Copyright (C) 2005-2009
 * Neil Brown <neilb@suse.de>
 * Released under the GPL, version 2
 *
 * generic inode handling
 *
 */

#include	"lafs.h"
#include <linux/random.h>
#include <linux/delay.h>
#include <linux/slab.h>

static void check_atime_ref(struct inode *ino, int async);

/* Supporting an async 'iget' - as required by the cleaner -
 * is slightly non-trivial.
 * iget*_locked will normally wait for any inode with one
 * of the flags I_FREEING I_CLEAR I_WILL_FREE I_NEW
 * to either be unhashed or have the flag cleared.
 * We cannot afford that wait in the cleaner as we could deadlock.
 * So we use iget5_locked and provide a test function that fails
 * if it finds the inode with any of those flags set.
 * If it does see the inode like that it sets a flag in the 'ikey'
 * that is passed in by reference so that it knows to continue
 * failing (for consistency) and so that the 'set' function
 * we provide can know to fail the 'set'.
 * The result of this is that if iget finds an inode it would
 * have to wait on, a flag is set and NULL is returned.
 * An unfortunate side effect is that an inode will be allocated
 * and then destroyed to no avail.
 * This is avoided by calling ilookup5 first.  This also allows
 * us to only allocate/load the data block if there really seems
 * to be a need.
 */
struct ikey {
	ino_t inum;
	struct inode *fsys;
	bool was_busy;
};

static int sync_itest(struct inode *inode, void *data)
{
	struct ikey *ik = data;

	if (inode->i_ino != ik->inum ||
	    LAFSI(inode)->filesys != ik->fsys)
		return 0;
	return 1;
}

static int async_itest(struct inode *inode, void *data)
{
	struct ikey *ik = data;

	if (ik->was_busy)
		/* found and is freeing */
		return 0;
	if (!sync_itest(inode, data))
		return 0;
	if (inode->i_state & (I_FREEING|I_CLEAR|I_WILL_FREE|I_NEW)) {
		ik->was_busy = true;
		return 0;
	}
	return 1;
}

static int iset(struct inode *inode, void *data)
{
	struct ikey *ik = data;
	if (ik->was_busy)
		return -EBUSY;
	inode->i_ino = ik->inum;
	LAFSI(inode)->filesys = ik->fsys;
	return 0;
}

struct inode *
lafs_iget(struct inode *fsys, ino_t inum, int async)
{
	/* find, and load if needed, this inum */
	struct inode *ino = NULL;
	struct inode *oldino;
	struct datablock *b = NULL;
	struct ikey ik = { .inum = inum, .fsys = fsys, };
	int err = 0;
	struct super_block *sb = fsys->i_sb;

	if (async) {
		/* We cannot afford to block on 'freeing_inode'
		 * So use iget5_locked and refuse to match such
		 * inodes.
		 * If the inode is 'freeing', inum gets set to NO_INO.
		 * ilookup5 is used first to avoid an unnecessary
		 * alloc/free if the inode is locked in some way.
		 */
		while (!ino) {

			err = 0;
			ino = ilookup5(sb, inum, async_itest, &ik);
			if (ino)
				break;

			if (ik.was_busy)
				err = -EAGAIN;

			/* For async we will always want the dblock loaded,
			 * and we need to load it first as we cannot afford
			 * to fail -EAGAIN once we have an I_NEW inode.
			 */
			if (!b)
				b = lafs_get_block(fsys, inum, NULL,
						   GFP_NOFS, MKREF(iget));
			if (!b)
				return ERR_PTR(-ENOMEM);

			if (!err)
				err = lafs_read_block_async(b);

			if (!err) {
				/* Have the block, so safe to iget */
				ino = iget5_locked(sb, inum,
						   async_itest, iset,
						   &ik);
				if (!ino) {
					if (ik.was_busy)
						err = -EAGAIN;
					else
						err = -ENOMEM;
				}
			}
			if (err) {
				if (test_and_set_bit(B_Async, &b->b.flags)) {
					putdref(b, MKREF(iget));
					return ERR_PTR(err);
				}
				getdref(b, MKREF(async));
			}
		}
	} else
		ino = iget5_locked(sb, inum, sync_itest, iset, &ik);

	if (!ino) {
		putdref(b, MKREF(iget));
		return ERR_PTR(-ENOMEM);
	}

	if (!(ino->i_state & I_NEW)) {
		putdref(b, MKREF(iget));
		if (ino->i_mode) {
			check_atime_ref(ino, async);
			return ino;
		}
		iput(ino);
		return ERR_PTR(-ENOENT);
	}

	igrab(LAFSI(ino)->filesys);

	/* surprisingly the inode bdi does not default to the
	 * super_blocks bdi...
	 */
	ino->i_data.backing_dev_info = sb->s_bdi;
	/* Need to load block 'inum' from an inode file...
	 */
	if (!b) {
		b = lafs_get_block(fsys, inum, NULL, GFP_KERNEL, MKREF(iget));
		if (!b)
			err = -ENOMEM;
		else
			err = lafs_read_block(b);
	}
	if (err)
		goto err;

	oldino = rcu_my_inode(b);
	if (oldino) {
		/* The inode is new, but the block thinks it has an
		 * old inode, so we must be in the process of destroying
		 * the old one.
		 * So fail the lookup without even looking at the content
		 * of the block (Which might not be clear yet).
		 */
		spin_lock(&oldino->i_data.private_lock);
		if (!test_bit(I_Deleting, &LAFSI(oldino)->iflags)) {
			b->my_inode = NULL;
			LAFSI(oldino)->dblock = NULL;
			LAFSI(oldino)->iblock = NULL;
		}
		spin_unlock(&oldino->i_data.private_lock);
	}
	rcu_iput(oldino);
	if (b->my_inode) {
		err = -ENOENT;
		goto err;
	}

	err = lafs_import_inode(ino, b);
	if (err) {
		if (err != -ENOENT)
			printk("lafs_import_inode failed %d\n", err);
		goto err;
	}
	check_atime_ref(ino, async);
	unlock_new_inode(ino);
out:
	if (b && test_and_clear_bit(B_Async, &b->b.flags)) {
		putdref(b, MKREF(async));
		lafs_wake_thread(fs_from_inode(fsys));
	}
	putdref(b, MKREF(iget));
	return ino;
err:
	ino->i_nlink = 0;
	unlock_new_inode(ino);
	iput(ino);
	ino = ERR_PTR(err);
	goto out;
}

struct inode *
lafs_iget_fs(struct fs *fs, int fsnum, int inum, int async)
{
	struct super_block *sb;
	struct inode *rv;

	sb = fs->prime_sb;

	if (fsnum) {
		/* Need to locate or load the superblock for this
		 * subordinate filesystem
		 */
		struct inode *filesys;

		filesys = lafs_iget(fs->ss[0].root, fsnum, async);
		if (IS_ERR(filesys))
			return filesys;
		if (LAFSI(filesys)->type != TypeInodeFile) {
			iput(filesys);
			return ERR_PTR(-ENOENT);
		}
		rv = lafs_iget(filesys, inum, async);
	} else if (inum) {
		rv = lafs_iget(fs->ss[0].root, inum, async);
		if (!IS_ERR(rv))
			atomic_inc(&sb->s_active);
	} else {
		rv = igrab(fs->ss[0].root);
		atomic_inc(&sb->s_active);
	}
	return rv;
}

int __must_check
lafs_import_inode(struct inode *ino, struct datablock *b)
{
	struct la_inode *lai = map_dblock(b);
	struct lafs_inode *li = LAFSI(ino);
	int err = -ENOENT;

	if (lai->filetype == 0) {
		li->type = 0;
		ino->i_mode = 0;
		ino->i_nlink = 0;
		goto out;
	}

	ino->i_mode = S_IFREG;
	ino->i_nlink = 1; /* For special file, set nlink so they
			   * never appear unlinked */

	err = -EINVAL;

	LAFS_BUG(ino->i_ino != b->b.fileaddr, &b->b);
	li->cblocks = le32_to_cpu(lai->data_blocks);
	li->pblocks = li->ablocks = 0;
	li->vfs_inode.i_blocks = ((blkcnt_t)li->cblocks
				  << (ino->i_sb->s_blocksize_bits - 9));
	li->ciblocks = le32_to_cpu(lai->index_blocks);
	li->piblocks = 0;
	li->iflags = 0;

	ino->i_generation = le16_to_cpu(lai->generation);
	li->trunc_gen = lai->trunc_gen;
	li->flags = lai->flags;
	li->type = lai->filetype;
	li->metadata_size = le16_to_cpu(lai->metadata_size);
	li->depth = lai->depth;

	dprintk("inode %lu type is %d\n", (unsigned long)ino->i_ino, li->type);

	ino->i_data.a_ops = &lafs_file_aops;
	li->trunc_next = 0;

	switch (li->type) {
	case TypeInodeFile:
	{
		struct fs_md *i = &li->md.fs;
		struct fs_metadata *l = &lai->metadata[0].fs;
		int nlen;

		i->usagetable = le16_to_cpu(l->snapshot_usage_table);
		decode_time(&ino->i_mtime, le64_to_cpu(l->update_time));
		i->cblocks_used = le64_to_cpu(l->blocks_used);
		i->pblocks_used = i->ablocks_used = 0;
		i->blocks_allowed = le64_to_cpu(l->blocks_allowed);
		i->blocks_unalloc = 0;
		i->creation_age = le64_to_cpu(l->creation_age);
		i->inodes_used = le32_to_cpu(l->inodes_used);
		i->parent = le32_to_cpu(l->parent);
		i->quota_inums[0] = le32_to_cpu(l->quota_inodes[0]);
		i->quota_inums[1] = le32_to_cpu(l->quota_inodes[1]);
		i->quota_inums[2] = le32_to_cpu(l->quota_inodes[2]);
		i->quota_inodes[0] = i->quota_inodes[1]
			= i->quota_inodes[2] = NULL;
		nlen = li->metadata_size - offsetof(struct la_inode,
						    metadata[0].fs.name);
		i->accesstime = NULL;
		if (i->name)
			kfree(i->name);
		if (nlen == 0)
			i->name = NULL;
		else {
			/* Need to unmap the dblock to kmalloc because
			 * the mapping makes us 'atomic'
			 */
			unmap_dblock(b, lai);
			i->name = kmalloc(nlen+1, GFP_KERNEL);
			lai = map_dblock(b);
			l = &lai->metadata[0].fs;

			err = -ENOMEM;
			if (!i->name)
				goto out;
			memcpy(i->name, l->name, nlen);
			i->name[nlen] = 0;
		}
		/* Make this look like a directory */
		ino->i_mode = S_IFDIR;
		ino->i_uid = 0;
		ino->i_gid = 0;
		ino->i_size = 0;
		ino->i_op = &lafs_subset_ino_operations;
		ino->i_fop = &lafs_subset_file_operations;
		break;
	}

	case TypeInodeMap:
	{
		struct inodemap_md *m = &li->md.inodemap;
		struct inodemap_metadata *s = &lai->metadata[0].inodemap;
		m->size = le32_to_cpu(s->size);
		m->thisblock = NoBlock;
		m->nextbit = 0;
		break;
	}

	case TypeSegmentMap:
	{
		struct su_md *m = &li->md.segmentusage;
		struct su_metadata *s = &lai->metadata[0].segmentusage;
		m->table_size = le32_to_cpu(s->table_size);
		break;
	}

	case TypeQuota:
	{
		struct quota_md *m = &li->md.quota;
		struct quota_metadata *s = &lai->metadata[0].quota;
		m->gracetime = le32_to_cpu(s->gracetime);
		m->graceunits = le32_to_cpu(s->graceunits);
		break;
	}
	case TypeOrphanList:
	{
		struct orphan_md *m = &li->md.orphan;
		/* This will be set via lafs_count_orphans */
		m->nextfree = 0;
		m->reserved = 0;
		break;
	}
	case TypeAccessTime:
		break;

	default: /* TypeBase or larger */
	{
		struct file_md *i = &li->md.file;
		struct file_metadata *l = &lai->metadata[0].file;
		struct dir_metadata *d = &lai->metadata[0].dir;
		struct special_metadata *s = &lai->metadata[0].special;

		if (li->type < TypeBase)
			goto out;
		i->flags = le16_to_cpu(l->flags);
		ino->i_mode = le16_to_cpu(l->mode);
		ino->i_uid = le32_to_cpu(l->userid);
		ino->i_gid = le32_to_cpu(l->groupid);
		i->treeid = le32_to_cpu(l->treeid);
		i->creationtime = le64_to_cpu(l->creationtime);
		decode_time(&ino->i_mtime, le64_to_cpu(l->modifytime));
		decode_time(&ino->i_ctime, le64_to_cpu(l->ctime));
		decode_time(&i->i_accesstime, le64_to_cpu(l->accesstime));
		ino->i_atime = i->i_accesstime;
		i->atime_offset = 0; /* Will be filled-in later probably */
		lafs_add_atime_offset(&ino->i_atime, i->atime_offset);
		ino->i_size = le64_to_cpu(l->size);
		i->parent = le32_to_cpu(l->parent);
		ino->i_nlink = le32_to_cpu(l->linkcount);
		if (ino->i_nlink == 0 && list_empty(&b->orphans) &&
		    fs_from_inode(ino)->rolled) {
			/* This block should already be on the orphan
			 * list, otherwise there is a filesystem
			 * inconsistency.
			 * Either the orphan file is wrong, or the
			 * linkcount is wrong.
			 * It is safest to assume the later - either
			 * way an FS check would be needed to fix it.
			 * Note: while roll-forward is happening, this
			 * situation is perfectly possible and is handled
			 * correctly.
			 */
			/* FIXME set a superblock flag requesting
			 * directory linkage checking
			 */
			ino->i_nlink = 1;
		}

		dprintk("  mode = 0%o uid %d size %lld\n",
			ino->i_mode, ino->i_uid, ino->i_size);
		switch (li->type) {
		case TypeFile:
			ino->i_op = &lafs_file_ino_operations;
			ino->i_fop = &lafs_file_file_operations;
			ino->i_mode = (ino->i_mode & 07777)  | S_IFREG;
			break;
		case TypeDir:
			i->seed = le32_to_cpu(d->hash_seed);
			ino->i_op = &lafs_dir_ino_operations;
			ino->i_fop = &lafs_dir_file_operations;
			ino->i_mode = (ino->i_mode & 07777)  | S_IFDIR;
			{
				u32 *b = (u32 *)lai;
				dprintk("Hmm. %d %d %d\n",
					(int)b[24],
					(int)b[25],
					(int)b[26]);
			}
			break;
		case TypeSymlink:
			ino->i_op = &lafs_link_ino_operations;
			ino->i_mode = (ino->i_mode & 07777)  | S_IFLNK;
			break;
		case TypeSpecial:
			/* the data had better be in the inode ... */
			ino->i_rdev = MKDEV(le32_to_cpu(s->major),
					    le32_to_cpu(s->minor));
			ino->i_op = &lafs_special_ino_operations;
			init_special_inode(ino, ino->i_mode, ino->i_rdev);
			break;
		}
		break;
	}
	}

	ino->i_blkbits = ino->i_sb->s_blocksize_bits;
	/* FIXME i_blocks and i_byte - used for quota?? */
	err = 0;

	/* Note: no refcount yet.  Either will remove the reference to the
	 * other when freed
	 */
	li->dblock = b;
	rcu_assign_pointer(b->my_inode, ino);

out:
	if (err && li->type)
		printk("inode %lu type is %d\n",
		       (unsigned long)ino->i_ino, li->type);
	unmap_dblock(b, lai);
	return err;
}

static void check_atime_ref(struct inode *ino, int async)
{
	/* If there is an atime file in this filesystem the inode
	 * should hold a reference to the relevant block in
	 * that file.
	 */
	struct inode *root, *at;
	u32 bnum;
	struct datablock *b;
	if (async)
		/* Never bother for async lookups */
		return;
	if (LAFSI(ino)->type < TypeBase)
		return;
	if (test_bit(I_AccessTime, &LAFSI(ino)->iflags))
		return;
	root = LAFSI(ino)->filesys;
	at = LAFSI(root)->md.fs.accesstime;
	if (at == NULL)
		return;

	if (LAFSI(ino)->md.file.atime_offset)
		LAFSI(ino)->md.file.atime_offset = 0;

	/* "* 2" to get byte number, then shift to get block
	 * number.  So just shift by 1 less than blkbits.
	 */
	bnum = ino->i_ino >> (at->i_blkbits-1);
	b = lafs_get_block(at, bnum, NULL, GFP_NOFS, MKREF(atime));
	if (b) {
		if (lafs_read_block(b) == 0) {
			u16 *atp;
			int i;
			atp = map_dblock(b);
			i = (ino->i_ino * 2) & ((1<<at->i_blkbits)-1);
			LAFSI(ino)->md.file.atime_offset = le16_to_cpu(atp[i]);
			set_bit(I_AccessTime, &LAFSI(ino)->iflags);
			unmap_dblock(b, atp);
			lafs_add_atime_offset(&ino->i_atime,
					      LAFSI(ino)->md.file.atime_offset);
		} else
			putdref(b, MKREF(atime));
	}
}

void lafs_add_atime_offset(struct timespec *atime, int offset)
{
	int expon;
	time_t mantissa;
	if (offset == 0)
		return;

	expon = offset & 0x1f;
	if (expon)
		mantissa = (offset >> 5) | 0x800;
	else
		mantissa = (offset >> 5);
	if (expon >= 11) {
		/* seconds */
		mantissa <<= expon-11;
		atime->tv_sec += mantissa;
	} else {
		/* milliseconds */
		if (expon)
			mantissa <<= expon-1;
		timespec_add_ns(atime, (s64)mantissa * 1000000);
	}
}

static int normalise(int *mantissa)
{
	/* Shift down until value can be stored in 12 bits:
	 * Top bit will be '1', so only 11 bits needed.
	 * Not used on values below 2048.
	 */
	int shift = 0;
	while (*mantissa >= 4096) {
		*mantissa >>= 1;
		shift ++;
	}
	return shift;
}

static int update_atime_delta(struct inode *ino)
{
	/* calculate new delta to show the difference between
	 * i_atime and i_accesstime
	 */
	int rv;
	if (LAFSI(ino)->type < TypeBase)
		return 0;
	if (timespec_compare(&ino->i_atime,
			     &LAFSI(ino)->md.file.i_accesstime) <= 0) {
		/* We cannot store negative delta so if i_atime is in the
		 * past, just store zero
		 */
		rv = 0;
	} else {
		struct timespec diff;
		int shift;

		diff = timespec_sub(ino->i_atime,
				    LAFSI(ino)->md.file.i_accesstime);
		if (diff.tv_sec >= 2048) {
			/* Just store the seconds */
			rv = diff.tv_sec;
			shift = normalise(&rv) + 11;
		} else {
			/* Store the milliseconds */
			int rv = diff.tv_nsec / 1000000;
			rv += diff.tv_sec * 1000;
			if (rv >= 2048)
				shift = normalise(&rv) + 1;
			else
				shift = 0;
		}
		if (shift > 31)
			rv = 0xFFFF;
		else {
			rv &= 0x7ff;
			rv <<= 5;
			rv |= shift;
		}
	}
	if (LAFSI(ino)->md.file.atime_offset == rv)
		return 0;

	LAFSI(ino)->md.file.atime_offset = rv;
	return 1;
}

static void store_atime_delta(struct inode *ino)
{
	struct inode *at;
	u32 bnum;
	struct datablock *b;
	u16 *atp;
	int i;

	if (!test_bit(I_AccessTime, &LAFSI(ino)->iflags))
		/* sorry, nothing we can do here */
		return;

	/* We own a reference, so this lookup must succeed */
	at = LAFSI(LAFSI(ino)->filesys)->md.fs.accesstime;
	bnum = ino->i_ino >> (at->i_blkbits-1);
	b = lafs_get_block(at, bnum, NULL, GFP_NOFS, MKREF(store_atime));
	BUG_ON(!b);
	atp = map_dblock(b);
	i = (ino->i_ino * 2) & ((1<<at->i_blkbits)-1);
	if (le16_to_cpu(atp[i]) != LAFSI(ino)->md.file.atime_offset) {
		atp[i] = cpu_to_le16(LAFSI(ino)->md.file.atime_offset);
		/* FIXME - I could lose an update here - do I care? */
		/* Can only reserve NewSpace with checkpoint locked... */
		lafs_checkpoint_lock(fs_from_inode(ino));
		if (lafs_reserve_block(&b->b, NewSpace) == 0)
			lafs_dirty_dblock(b);
		lafs_checkpoint_unlock(fs_from_inode(ino));
	}
	unmap_dblock(b, atp);
	putdref(b, MKREF(store_atime));
}

void lafs_inode_checkpin(struct inode *ino)
{
	/* Make sure I_Pinned is set correctly.
	 * It should be set precisely if i_nlink is non-zero,
	 * and ->iblock is B_Pinned.
	 * When it is set, we own a reference to the inode.
	 *
	 * This needs to be called whenever we change
	 * i_nlink, and whenever we pin or unpin an InoIdx
	 * block.
	 */
	if (ino->i_nlink == 0) {
		/* I_Pinned should not be set */
		if (test_and_clear_bit(I_Pinned, &LAFSI(ino)->iflags)) {
			if (ino->i_sb->s_type == &lafs_fs_type)
				iput(ino);
			else
				lafs_iput_fs(ino);
		}
	} else {
		/* Need to check if iblock is Pinned. */
		struct indexblock *ib = NULL;
		if (LAFSI(ino)->iblock) {
			spin_lock(&ino->i_data.private_lock);
			ib = LAFSI(ino)->iblock;
			if (ib && !test_bit(B_Pinned, &ib->b.flags))
				ib = NULL;
			spin_unlock(&ino->i_data.private_lock);
		}
		if (ib) {
			if (!test_and_set_bit(I_Pinned, &LAFSI(ino)->iflags)) {
				if (ino->i_sb->s_type == &lafs_fs_type)
					igrab(ino);
				else
					lafs_igrab_fs(ino);
			}
		} else {
			if (test_and_clear_bit(I_Pinned, &LAFSI(ino)->iflags)) {
			if (ino->i_sb->s_type == &lafs_fs_type)
				iput(ino);
			else
				lafs_iput_fs(ino);
			}
		}
	}
}

struct datablock *lafs_inode_get_dblock(struct inode *ino, REFARG)
{
	struct datablock *db;

	spin_lock(&ino->i_data.private_lock);
	db = LAFSI(ino)->dblock;
	if (db) {
		if (db->b.inode == ino)
			getdref_locked(db, REF);
		else {
			spin_lock_nested(&db->b.inode->i_data.private_lock, 1);
			getdref_locked(db, REF);
			spin_unlock(&db->b.inode->i_data.private_lock);
		}
	}
	spin_unlock(&ino->i_data.private_lock);
	return db;
}

struct datablock *lafs_inode_dblock(struct inode *ino, int async, REFARG)
{
	struct datablock *db;
	int err;

	db = lafs_inode_get_dblock(ino, REF);

	if (!db)
		db = lafs_get_block(LAFSI(ino)->filesys, ino->i_ino, NULL,
				    GFP_KERNEL, REF);
	if (!db)
		return ERR_PTR(-ENOMEM);

	LAFSI(ino)->dblock = db;
	rcu_assign_pointer(db->my_inode, ino);
	if (async)
		err = lafs_read_block_async(db);
	else
		err = lafs_read_block(db);
	if (err == 0)
		return db;

	putdref(db, REF);
	return ERR_PTR(err);
}

void lafs_inode_init(struct datablock *b, int type, int mode, struct inode *dir)
{
	/* A new block has been allocated in an inode file to hold an
	 * inode.  We get to fill in initial values so that when
	 * 'iget' calls lafs_import_inode, the correct inode is
	 * loaded.
	 */
	struct fs *fs = fs_from_inode(b->b.inode);
	struct la_inode *lai = map_dblock(b);
	int size;

	lai->data_blocks = cpu_to_le32(0);
	lai->index_blocks = cpu_to_le32(0);
	get_random_bytes(&lai->generation, sizeof(lai->generation));
	lai->depth = 1;
	lai->trunc_gen = 0;
	lai->filetype = type;
	lai->flags = 0;

	switch(type) {
	case TypeInodeFile:
	{
		struct fs_metadata *l = &lai->metadata[0].fs;
		size = sizeof(struct fs_metadata);
		l->update_time = 0;
		l->blocks_used = 0;
		l->blocks_allowed = 0;
		l->creation_age = fs->wc[0].cluster_seq;
		l->inodes_used = 0;
		l->parent = 0;
		l->quota_inodes[0] = 0;
		l->quota_inodes[1] = 0;
		l->quota_inodes[2] = 0;
		l->snapshot_usage_table = 0;
		l->pad = 0;
		/* name will be zero length and not used */
		break;
	}
	case TypeInodeMap:
	{
		struct inodemap_metadata *l = &lai->metadata[0].inodemap;
		l->size = 0;
		size = sizeof(struct inodemap_metadata);
		break;
	}
	case TypeSegmentMap:
		size = sizeof(struct su_metadata);
		break;
	case TypeQuota:
		size = sizeof(struct quota_metadata);
		break;
	case TypeOrphanList:
		size = 0;
		break;
	case TypeAccessTime:
		size = 0;
		break;
	default:
	{
		struct file_metadata *l = &lai->metadata[0].file;
		struct timespec now = CURRENT_TIME;

		l->flags = cpu_to_le16(0);
		l->userid = cpu_to_le32(current->cred->fsuid);
		if (dir && (dir->i_mode & S_ISGID)) {
			l->groupid = cpu_to_le32(dir->i_gid);
			if (type == TypeDir)
				mode |= S_ISGID;
		} else
			l->groupid = cpu_to_le32(current->cred->fsgid);
		if (dir && LAFSI(dir)->md.file.treeid)
			l->treeid = cpu_to_le32(LAFSI(dir)->md.file.treeid);
		else
			l->treeid = l->userid;

		l->mode = cpu_to_le16(mode);
		l->creationtime = encode_time(&now);
		l->modifytime = l->creationtime;
		l->ctime = l->creationtime;
		l->accesstime = l->creationtime;
		l->size = 0;
		l->parent = dir ? cpu_to_le32(dir->i_ino) : 0;
		l->linkcount = 0;
		l->attrinode = 0;
		if (type == TypeDir) {
			struct dir_metadata *l = &lai->metadata[0].dir;
			u32 seed;
			get_random_bytes(&seed,
					 sizeof(seed));
			seed = (seed & ~7) | 1;
			l->hash_seed = cpu_to_le32(seed);
			size = sizeof(struct dir_metadata);
		} else if (type == TypeSpecial) {
			struct special_metadata *s = &lai->metadata[0].special;
			s->major = s->minor = 0;
			size = sizeof(struct special_metadata);
		} else
			size = sizeof(struct file_metadata);
	}
	}
	size += sizeof(struct la_inode);
	lai->metadata_size = cpu_to_le32(size);
	memset(((char *)lai)+size, 0, fs->blocksize-size);
	*(u16 *)(((char *)lai)+size) = cpu_to_le16(IBLK_EXTENT);

	unmap_dblock(b, lai);
	set_bit(B_Valid, &b->b.flags);
	LAFS_BUG(!test_bit(B_Pinned, &b->b.flags), &b->b);
	lafs_dirty_dblock(b);
}

static int inode_map_free(struct fs *fs, struct inode *fsys, u32 inum);

void lafs_evict_inode(struct inode *ino)
{
	struct fs *fs = fs_from_inode(ino);
	struct lafs_inode *li = LAFSI(ino);

	if (ino->i_mode == 0) {
		/* There never was an inode here,
		 * so nothing to do.
		 * We just call end_writeback to get the
		 * flags set properly.
		 */
		end_writeback(ino);
		return;
	}

	dprintk("EVICT INODE %d\n", (int)ino->i_ino);


	/* Normal truncation holds an igrab, so we cannot be
	 * deleted until any truncation finishes
	 */
	BUG_ON(test_bit(I_Trunc, &LAFSI(ino)->iflags));

	if (ino->i_nlink == 0) {
		struct datablock *b =
			lafs_inode_dblock(ino, SYNC, MKREF(delete_inode));
		i_size_write(ino, 0);
		truncate_inode_pages(&ino->i_data, 0);
		LAFSI(ino)->trunc_next = 0;
		set_bit(I_Deleting, &LAFSI(ino)->iflags);
		set_bit(I_Trunc, &LAFSI(ino)->iflags);
		lafs_igrab_fs(ino);
		if (!IS_ERR(b)) {
			set_bit(B_Claimed, &b->b.flags);
			lafs_add_orphan(fs, b);
			dprintk("PUNCH hole for %d\n", (int)b->b.fileaddr);
			putdref(b, MKREF(delete_inode));
		}
		inode_map_free(fs, LAFSI(ino)->filesys,  ino->i_ino);
	} else
		truncate_inode_pages(&ino->i_data, 0);
	end_writeback(ino);

	dprintk("CLEAR INODE %d\n", (int)ino->i_ino);

	li->type = 0;

	/* Now is a good time to break the linkage between
	 * inode and dblock - but not if the file is
	 * being deleted
	 */
	if (!test_bit(I_Deleting, &li->iflags)) {
		struct datablock *db;
		spin_lock(&ino->i_data.private_lock);
		db = li->dblock;
		if (db) {
			struct indexblock *ib = li->iblock;
			LAFS_BUG(ib && atomic_read(&ib->b.refcnt), &db->b);
			db->my_inode = NULL;
			li->dblock = NULL;
			li->iblock = NULL;
		}
		spin_unlock(&ino->i_data.private_lock);
	}

	/* FIXME release quota inodes if filesystem */
}

static int prune(void *data, u32 addr, u64 paddr, int len)
{
	/* This whole index block is being pruned, just account
	 * for everything and it will be cleared afterwards
	 */
	struct indexblock *ib = data;
	struct inode *ino = ib->b.inode;
	struct fs *fs = fs_from_inode(ino);
	int ph = !!test_bit(B_Phase1, &ib->b.flags);
	int i;
	dprintk("PRUNE %d for %d at %lld\n", addr, len, (long long)paddr);
	if (paddr == 0 || len == 0)
		return 0;
	for (i = 0 ; i < len ; i++)
		lafs_summary_update(fs, ino, paddr+i, 0, 0, ph, 0);
	return len;
}

static int prune_some(void *data, u32 addr, u64 paddr, int len)
{
	/* Part of this index block is being pruned.  Copy
	 * what addresses we can into uninc_table so that
	 * it can be 'incorporated'
	 * We should probably share some code with
	 * lafs_allocated_block??
	 */
	struct indexblock *ib = data;
	struct inode *ino = ib->b.inode;
	struct fs *fs = fs_from_inode(ino);
	int ph = !!test_bit(B_Phase1, &ib->b.flags);
	int i;

	if (paddr == 0 || len == 0)
		return 0;
	dprintk("PRUNE2 %d for %d at %lld\n", addr, len, (long long)paddr);
	for (i = 0 ; i < len ; i++) {
		/* FIXME should allow longer truncation ranges in uninc_table
		 * as they are easy to handle.
		 */
		struct addr *a;
		if (addr + i < LAFSI(ino)->trunc_next)
			continue;
		spin_lock(&ino->i_data.private_lock);
		a = &ib->uninc_table.pending_addr
			[ib->uninc_table.pending_cnt - 1];
		if (ib->uninc_table.pending_cnt <
		    ARRAY_SIZE(ib->uninc_table.pending_addr)) {
			a++;
			a->fileaddr = addr + i;
			a->physaddr = 0;
			a->cnt = 1;
			LAFS_BUG(!test_bit(B_Pinned, &ib->b.flags), &ib->b);
			ib->uninc_table.pending_cnt++;
		} else {
			spin_unlock(&ino->i_data.private_lock);
			break;
		}
		spin_unlock(&ino->i_data.private_lock);
		lafs_summary_update(fs, ino, paddr+i, 0, 0, ph, 0);
	}
	return i;
}

int lafs_inode_handle_orphan(struct datablock *b)
{
	/* Don't need rcu protection for my_inode run_orphan
	 * holds a reference
	 */
	struct indexblock *ib, *ib2;
	struct inode *ino = b->my_inode;
	struct fs *fs = fs_from_inode(ino);
	u32 trunc_next, next_trunc;
	int loop_cnt = 20;
	int err = -ENOMEM;

	if (!test_bit(I_Trunc, &LAFSI(ino)->iflags)) {
		if (test_bit(I_Deleting, &LAFSI(ino)->iflags)) {
			LAFS_BUG(ino->i_nlink, &b->b);
			if (LAFSI(ino)->cblocks +
			    LAFSI(ino)->pblocks +
			    LAFSI(ino)->ablocks +
			    LAFSI(ino)->ciblocks +
			    LAFSI(ino)->piblocks)
			printk("Deleting inode %lu: %ld+%ld+%ld %ld+%ld\n",
			       ino->i_ino,
			       LAFSI(ino)->cblocks,
			       LAFSI(ino)->pblocks,
			       LAFSI(ino)->ablocks,
			       LAFSI(ino)->ciblocks,
			       LAFSI(ino)->piblocks);
			BUG_ON(LAFSI(ino)->cblocks +
			       LAFSI(ino)->pblocks +
			       LAFSI(ino)->ablocks +
			       LAFSI(ino)->ciblocks +
			       LAFSI(ino)->piblocks);
			if (lafs_erase_dblock_async(b))
				lafs_orphan_release(fs, b);
		} else if (ino->i_nlink || LAFSI(ino)->type == 0)
			lafs_orphan_release(fs, b);
		else
			lafs_orphan_forget(fs, b);
		return 0;
	}

	ib = lafs_make_iblock(ino, ADOPT, SYNC, MKREF(inode_handle_orphan));
	if (IS_ERR(ib))
		return PTR_ERR(ib);

	/* Here is the guts of 'truncate'.  We find the next leaf index
	 * block and discard all the addresses there-in.
	 */
	trunc_next = LAFSI(ino)->trunc_next;

	if (trunc_next == 0xFFFFFFFF) {
		/* truncate has finished in that all data blocks
		 * have been removed and all index block are either
		 * gone or pending incorporation at which point they will
		 * go.
		 * If we hit a phase change, we will need to postpone
		 * the rest of the cleaning until it completes.
		 * If there is a checkpoint happening, then all the work
		 * that we can do now, it will do for us.  So just
		 * let it.
		 */
		struct indexblock *tmp;
		struct indexblock *next;
		u32 lastaddr;

		if (!test_bit(B_Pinned, &ib->b.flags)) {
			/* must be finished */
			LAFS_BUG(test_bit(B_Dirty, &ib->b.flags), &ib->b);
			clear_bit(I_Trunc, &LAFSI(ino)->iflags);
			lafs_iput_fs(ino);
			wake_up(&fs->trunc_wait);
			err = -ERESTARTSYS;
			goto out2;
		}
		if (fs->checkpointing) {
			/* This cannot happen with current code,
			 * but leave it in case we ever have
			 * orphan handling parallel with checkpoints
			 */
			err = -EBUSY; /* Try again after the checkpoint */
			goto out2;
		}

		lastaddr = (i_size_read(ino) +
			    fs->blocksize - 1)
			>> fs->blocksize_bits;
		/* Find a Pinned descendent of ib which has no
		 * Pinned descendents and no PrimaryRef dependent
		 * (so take the last).
		 * Prefer blocks that are beyond EOF (again, take the last).
		 * If there are none, descend the last block that
		 * is not after EOF and look at its children.
		 */
		ib2 = next = ib;
		spin_lock(&ib->b.inode->i_data.private_lock);
		while (next) {
			ib2 = next;
			next = NULL;
			list_for_each_entry(tmp, &ib2->children, b.siblings) {
				if (!test_bit(B_Index, &tmp->b.flags) ||
				    !test_bit(B_Pinned, &tmp->b.flags))
					continue;
				if (next == NULL ||
				    tmp->b.fileaddr > next->b.fileaddr)
					next = tmp;
			}
		}
		if (ib2->b.fileaddr < lastaddr) {
			/* Must be all done */
			spin_unlock(&ib->b.inode->i_data.private_lock);
			clear_bit(I_Trunc, &LAFSI(ino)->iflags);
			lafs_iput_fs(ino);
			wake_up(&fs->trunc_wait);
			err = -ERESTARTSYS;
			goto out2;
		}
		getiref(ib2, MKREF(inode_handle_orphan2));
		spin_unlock(&ib->b.inode->i_data.private_lock);

		/* ib2 is an index block beyond EOF with no
		 * Pinned children.
		 * Incorporating it should unpin it.
		 */
		if (!list_empty(&ib2->children)) {
			lafs_print_tree(&ib2->b, 3);
			LAFS_BUG(1, &ib2->b);
		}

		if (!lafs_iolock_written_async(&ib2->b)) {
			putiref(ib2, MKREF(inode_handle_orphan2));
			err = -EAGAIN;
			goto out2;
		}
		while (ib2->uninc_table.pending_cnt || ib2->uninc)
			lafs_incorporate(fs, ib2);

		if (test_bit(B_Dirty, &ib2->b.flags) ||
		    test_bit(B_Realloc, &ib2->b.flags))
			lafs_cluster_allocate(&ib2->b, 0);
		else
			lafs_iounlock_block(&ib2->b);

		if (!list_empty(&ib2->b.siblings)) {
			printk("looping on %s\n", strblk(&ib2->b));
			loop_cnt--;
			if (loop_cnt < 0)
				BUG();
		}
		putiref(ib2, MKREF(inode_handle_orphan2));
		err = -ERESTARTSYS;
		if (ib->uninc) {
			if (lafs_iolock_written_async(&ib->b)) {
				while (ib->uninc)
					lafs_incorporate(fs, ib);
				lafs_iounlock_block(&ib->b);
			} else
				err = -EAGAIN;
		}
	out2:
		putiref(ib, MKREF(inode_handle_orphan));
		return err;
	}

	putiref(ib, MKREF(inode_handle_orphan));

	ib = lafs_leaf_find(ino, trunc_next, ADOPT, &next_trunc,
			    ASYNC, MKREF(inode_handle_orphan3));
	if (IS_ERR(ib))
		return PTR_ERR(ib);
	/* now hold an iolock on ib */

	/* Ok, trunc_next seems to refer to a block that exists.
	 * We need to erase it..
	 *
	 * So we open up the index block ourselves, call
	 * lafs_summary_update with each block address, and then
	 * erase the block.
	 */

	if (LAFSI(ino)->depth == 0) {
		/* Nothing to truncate */
		clear_bit(I_Trunc, &LAFSI(ino)->iflags);
		lafs_iput_fs(ino);
		if (test_bit(B_Pinned, &ib->b.flags))
			/* Need to move the dirtiness which keeps this
			 * pinned to the data block.
			 */
			lafs_cluster_allocate(&ib->b, 0);
		else
			lafs_iounlock_block(&ib->b);
		err = -ERESTARTSYS;
		goto out_put;
	}

	lafs_checkpoint_lock(fs);
	err = lafs_reserve_block(&ib->b, ReleaseSpace);
	if (err < 0)
		goto out;

	if (!test_bit(B_Valid, &ib->b.flags) &&
	    test_bit(B_InoIdx, &ib->b.flags)) {
		/* still invalid, just re-erase to remove
		 * pinning */
		LAFSI(ino)->trunc_next = next_trunc;
		lafs_cluster_allocate(&ib->b, 0);
		err = -ERESTARTSYS;
		goto out_unlocked;
	}

	lafs_pin_block(&ib->b);

	/* It might be that this can happen, in which case
	 * we simply update trunc_next and loop.  But I'd like
	 * to be sure before I implement that
	 */
	if (!test_bit(B_Valid, &ib->b.flags)) {
		printk("Not Valid: %s\n", strblk(&ib->b));
		printk("depth = %d\n", LAFSI(ino)->depth);
		if (test_bit(B_InoIdx, &ib->b.flags))
			printk("DB: %s\n", strblk(&LAFSI(ib->b.inode)->dblock->b));
		LAFSI(ino)->trunc_next = next_trunc;
		//BUG_ON(!test_bit(B_Valid, &ib->b.flags));
		err = -ERESTARTSYS;
		goto out;
	}

	if (ib->b.fileaddr < trunc_next &&
	    lafs_leaf_next(ib, 0) < trunc_next) {
		/* We only want to truncate part of this index block.
		 * So we copy addresses into uninc_table and then
		 * call lafs_incorporate.
		 * This might cause the index tree to grow, so we
		 * cannot trust next_trunc
		 */
		if (ib->uninc_table.pending_cnt == 0 &&
		    ib->uninc == NULL) {
			lafs_dirty_iblock(ib, 0);
			/* FIXME this just removes 8 blocks at a time,
			 * which is not enough
			 */
			lafs_walk_leaf_index(ib, prune_some, ib);
		}
		if (test_bit(B_Dirty, &ib->b.flags))
			lafs_incorporate(fs, ib);
		err = -ERESTARTSYS;
		goto out;
	}
	LAFSI(ino)->trunc_next = next_trunc;

	while (ib->uninc_table.pending_cnt || ib->uninc) {
		/* There should be no Realloc data blocks here
		 * but index blocks might be realloc still.
		 */
		LAFS_BUG(!test_bit(B_Dirty, &ib->b.flags) &&
			 !test_bit(B_Realloc, &ib->b.flags), &ib->b);
		lafs_incorporate(fs, ib);
	}
	if (test_bit(B_InoIdx, &ib->b.flags) ||
	    !test_bit(B_PhysValid, &ib->b.flags) ||
	    ib->b.physaddr != 0) {
		lafs_walk_leaf_index(ib, prune, ib);
		lafs_clear_index(ib);
		lafs_dirty_iblock(ib, 0);
	}
	if (test_bit(B_Dirty, &ib->b.flags))
		lafs_incorporate(fs, ib);
	if (!list_empty(&ib->children))
		lafs_print_tree(&ib->b, 2);
	LAFS_BUG(!list_empty(&ib->children), &ib->b);
	err = -ERESTARTSYS;
out:
	lafs_iounlock_block(&ib->b);
out_unlocked:
	lafs_checkpoint_unlock(fs);
out_put:
	putiref(ib, MKREF(inode_handle_orphan3));
	return err;
}

void lafs_dirty_inode(struct inode *ino)
{
	/* this is called in one of three cases:
	 * 1/ by lafs internally when dblock or iblock is pinned and
	 *    ready to be dirtied
	 * 2/ by writeout before requesting a write - to update mtime
	 * 3/ by read to update atime
	 *
	 * We want to handle atime updates carefully as they may not change
	 * the stored inode itself.
	 * For all other updates, the inode dblock exists and is pinned.
	 * In those cases we will be updating the inode and so can store
	 * the atime exactly.
	 * For an atime update, the dblock may not exists, or may not be
	 * Pinned.  If it isn't then we don't want to make the inode dirty
	 * but only want to update the delta stored in the atime file.
	 * The block for that should already be pinned.
	 *
	 *
	 * We mustn't update the data block as it could be in
	 * writeout and we cannot always wait safely.
	 * So require that anyone who really cares, dirties the datablock
	 * or a child themselves.
	 * When cluster_allocate eventually gets called, it will update
	 * the datablock from the inode.
	 * If an update has to wait for the next phase, lock_dblock
	 * (e.g. in setattr) will do that.
	 *
	 * We also use this opportunity to update the filesystem modify time.
	 */
	struct timespec now;
	struct inode *filesys;
	int atime_only = 1;

	if (LAFSI(ino)->dblock) {
		struct datablock *db;
		spin_lock(&ino->i_data.private_lock);
		db = LAFSI(ino)->dblock;
		if (db && test_bit(B_Pinned, &db->b.flags))
			atime_only = 0;
		spin_unlock(&ino->i_data.private_lock);
	}

	if (atime_only) {
		if (update_atime_delta(ino))
			store_atime_delta(ino);
		return;
	}

	set_bit(I_Dirty, &LAFSI(ino)->iflags);
	ino->i_sb->s_dirt = 1;

	if (LAFSI(ino)->type < TypeBase)
		return;
	LAFSI(ino)->md.file.i_accesstime = ino->i_atime;
	if (LAFSI(ino)->md.file.atime_offset) {
		LAFSI(ino)->md.file.atime_offset = 0;
		store_atime_delta(ino);
	}

	now = current_fs_time(ino->i_sb);
	filesys = LAFSI(ino)->filesys;
	if (!timespec_equal(&filesys->i_mtime, &now)) {
		filesys->i_mtime = now;
		set_bit(I_Dirty, &LAFSI(filesys)->iflags);
	}
}

int lafs_sync_inode(struct inode *ino, int wait)
{
	/* fsync has been called on this file so we need
	 * to sync any inode updates to the next cluster.
	 *
	 * If we cannot create an update record,
	 * we wait for a phase change, which writes everything
	 * out.
	 */
	struct datablock *b;
	struct fs *fs = fs_from_inode(ino);
	struct update_handle uh;
	int err;

	if (wait) {
		if (LAFSI(ino)->update_cluster > 1)
			lafs_cluster_wait(fs, LAFSI(ino)->update_cluster);
		if (LAFSI(ino)->update_cluster == 1) {
			lafs_checkpoint_lock(fs);
			lafs_checkpoint_unlock_wait(fs);
		}
		return 0;
	}

	LAFSI(ino)->update_cluster = 0;
	if (!test_bit(I_Dirty, &LAFSI(ino)->iflags))
		return 0;
	b = lafs_inode_dblock(ino, SYNC, MKREF(write_inode));
	if (IS_ERR(b))
		return PTR_ERR(b);

	lafs_iolock_written(&b->b);
	lafs_inode_fillblock(ino);
	lafs_iounlock_block(&b->b);

	err = lafs_cluster_update_prepare(&uh, fs, LAFS_INODE_LOG_SIZE);
	if (err)
		lafs_cluster_update_abort(&uh);
	else {
		lafs_checkpoint_lock(fs);
		if (lafs_cluster_update_pin(&uh) == 0) {
			if (test_and_clear_bit(B_Dirty, &b->b.flags))
				lafs_space_return(fs, 1);
			LAFSI(ino)->update_cluster =
				lafs_cluster_update_commit
				(&uh, b, LAFS_INODE_LOG_START,
				 LAFS_INODE_LOG_SIZE);
		} else	
			lafs_cluster_update_abort(&uh);
		lafs_checkpoint_unlock(fs);
	}
	if (test_bit(B_Dirty, &b->b.flags)) {
		/* FIXME need to write out the data block...
		 * Is that just lafs_cluster_allocate ?
		 */
	}

	if (LAFSI(ino)->update_cluster == 0) {
		lafs_checkpoint_lock(fs);
		if (test_bit(B_Dirty, &b->b.flags))
			LAFSI(ino)->update_cluster = 1;
		lafs_checkpoint_start(fs);
		lafs_checkpoint_unlock(fs);
	}
	putdref(b, MKREF(write_inode));
	return 0; /* FIXME should I return some error message??? */
}

void lafs_inode_fillblock(struct inode *ino)
{
	/* copy data from ino into the related data block */

	struct lafs_inode *li = LAFSI(ino);
	struct datablock *db = li->dblock;
	struct la_inode *lai;

	clear_bit(I_Dirty, &LAFSI(ino)->iflags);

	lai = map_dblock(db);
	lai->data_blocks = cpu_to_le32(li->cblocks);
	lai->index_blocks = cpu_to_le32(li->ciblocks);
	lai->generation = cpu_to_le16(ino->i_generation);
	lai->trunc_gen = li->trunc_gen;
	lai->flags = li->flags;
	lai->filetype = li->type;
	if (lai->metadata_size != cpu_to_le16(li->metadata_size)) {
		/* Changing metadata size is wierd.
		 * We will need to handle this somehow for xattrs
		 * For now we just want to cope with
		 * Dir -> InodeFile changes, and that guarantees us
		 * there is no index info - so just clear the index 
		 * area.
		 */
		u16 *s = (u16*)(((char*)lai) + li->metadata_size);
		BUG_ON(li->type != TypeInodeFile);
		lai->metadata_size = cpu_to_le16(li->metadata_size);
		memset(s, 0, ino->i_sb->s_blocksize - li->metadata_size);
		*s = cpu_to_le16(IBLK_INDIRECT);
	}
	lai->depth = li->depth;

	switch (li->type) {
	case TypeInodeFile:
	{
		struct fs_md *i = &li->md.fs;
		struct fs_metadata *l = &lai->metadata[0].fs;
		int nlen;

		l->snapshot_usage_table = cpu_to_le16(i->usagetable);
		l->update_time = cpu_to_le64(encode_time(&ino->i_mtime));
		l->blocks_used = cpu_to_le64(i->cblocks_used);
		l->blocks_allowed = cpu_to_le64(i->blocks_allowed);
		l->creation_age = cpu_to_le64(i->creation_age);
		l->inodes_used = cpu_to_le32(i->inodes_used);
		l->parent = cpu_to_le32(i->parent);
		l->quota_inodes[0] = cpu_to_le32(i->quota_inums[0]);
		l->quota_inodes[1] = cpu_to_le32(i->quota_inums[1]);
		l->quota_inodes[2] = cpu_to_le32(i->quota_inums[2]);
		nlen = lai->metadata_size - offsetof(struct la_inode,
						     metadata[0].fs.name);
		memset(l->name, 0, nlen);
		if (i->name == NULL)
			nlen = 0;
		else if (strlen(i->name) < nlen)
			nlen = strlen(i->name);
		memcpy(l->name, i->name, nlen);
		break;
	}

	case TypeInodeMap:
	{
		struct inodemap_md *m = &li->md.inodemap;
		struct inodemap_metadata *s = &lai->metadata[0].inodemap;
		s->size = cpu_to_le32(m->size);
		break;
	}

	case TypeSegmentMap:
	{
		struct su_md *m = &li->md.segmentusage;
		struct su_metadata *s = &lai->metadata[0].segmentusage;
		s->table_size = cpu_to_le32(m->table_size);
		break;
	}

	case TypeQuota:
	{
		struct quota_md *m = &li->md.quota;
		struct quota_metadata *s = &lai->metadata[0].quota;
		s->gracetime = cpu_to_le32(m->gracetime);
		s->graceunits = cpu_to_le32(m->graceunits);
		break;
	}
	case TypeOrphanList:
	case TypeAccessTime:
		break;

	default: /* TypeBase or larger */
	{
		struct file_md *i = &li->md.file;
		struct file_metadata *l = &lai->metadata[0].file;
		struct dir_metadata *d = &lai->metadata[0].dir;
		struct special_metadata *s = &lai->metadata[0].special;

		if (li->type < TypeBase)
			break;
		l->flags = cpu_to_le16(i->flags);
		l->mode = cpu_to_le16(ino->i_mode);
		l->userid = cpu_to_le32(ino->i_uid);
		l->groupid = cpu_to_le32(ino->i_gid);
		l->treeid = cpu_to_le32(i->treeid);
		l->creationtime = cpu_to_le64(i->creationtime);
		l->modifytime = cpu_to_le64(encode_time(&ino->i_mtime));
		l->ctime = cpu_to_le64(encode_time(&ino->i_ctime));
		l->accesstime = cpu_to_le64(encode_time(&i->i_accesstime));
		l->size = cpu_to_le64(ino->i_size);
		l->parent = cpu_to_le32(i->parent);
		l->linkcount = cpu_to_le32(ino->i_nlink);

		switch (li->type) {
		case TypeFile:
			break;
		case TypeDir:
			d->hash_seed = cpu_to_le32(i->seed);
			break;
		case TypeSymlink:
			break;
		case TypeSpecial:
			s->major = cpu_to_le32(MAJOR(ino->i_rdev));
			s->minor = cpu_to_le32(MINOR(ino->i_rdev));
			break;
		}
	}
	}
	unmap_dblock(db, lai);
}

/*-----------------------------------------------------------------------
 * Inode allocate map handling.
 * Inode 1 of each fileset is a bitmap of free inode numbers.
 * Whenever the file is extended in size, new bits are set to one.  They
 * are then cleared when the inode is allocated.  When a block becomes
 * full of zeros, we don't need to store it any more.
 *
 * We don't clear the bit until we are committed to creating an inode
 * This means we cannot clear it straight away, so two different threads
 * might see the same inode number as being available.  We have two
 * approaches to guard against this.
 * Firstly we have a 'current' pointer into the inodemap file and
 * increase that past the inode we return.  This discourages multiple
 * hits but as the pointer would need to be rewound occasionally it
 * isn't a guarantee.  The guarantee against multiple allocations is done
 * via a flag in the block representing an inode.  This is set
 * while an inode is being allocated.
 */

/* inode number allocation has the prealloc/pin/commit/abort structure
 * so it can be committed effectively
 */

static int
choose_free_inum(struct fs *fs, struct inode *fsys, u32 *inump,
		 struct datablock **bp, int *restarted)
{
	struct inode *im = lafs_iget(fsys, 1, SYNC);
	loff_t bnum;
	struct datablock *b;
	char *buf;
	int err;
	int bit;

	if (*bp) {
		struct inode *i = (*bp)->b.inode;
		putdref(*bp, MKREF(cfi_map));
		iput(i);
		*bp = NULL;
	}

	mutex_lock_nested(&im->i_mutex, I_MUTEX_QUOTA);
retry:
	bnum = LAFSI(im)->md.inodemap.thisblock;

	if (bnum == NoBlock ||
	    LAFSI(im)->md.inodemap.nextbit >= (fs->blocksize<<3)) {
		if (bnum == NoBlock)
			bnum = LAFSI(im)->md.inodemap.size;

		if (bnum+1 < LAFSI(im)->md.inodemap.size)
			bnum++;
		else if (!*restarted) {
			bnum = 0;
			*restarted = 1;
		} else {
			/* Need to add a new block to the file */
			bnum = LAFSI(im)->md.inodemap.size;
			b = lafs_get_block(im, bnum, NULL, GFP_KERNEL,
					   MKREF(cfi_map));
			err = -ENOMEM;
			if (!b)
				goto abort;
			lafs_iolock_written(&b->b);
			set_bit(B_PinPending, &b->b.flags);
			lafs_iounlock_block(&b->b);
		retry2:
			lafs_checkpoint_lock(fs);
			err = lafs_pin_dblock(b, NewSpace);
			if (err == -EAGAIN) {
				lafs_checkpoint_unlock_wait(fs);
				goto retry2;
			}
			if (err < 0)
				goto abort_unlock;

			buf = map_dblock(b);
			/* Set block to "all are free" */
			memset(buf, 0xff, fs->blocksize);
			unmap_dblock(b, buf);
			set_bit(B_Valid, &b->b.flags);
			LAFSI(im)->md.inodemap.size = bnum+1;
			lafs_dirty_inode(im);
			lafs_dirty_dblock(b);
			lafs_checkpoint_unlock(fs);
			putdref(b, MKREF(cfi_map));
		}
		b = NULL;
		err = lafs_find_next(im, &bnum);
		if (err < 0)
			goto abort;
		if (err == 0)
			bnum = 0;

		LAFSI(im)->md.inodemap.nextbit = 0;
		LAFSI(im)->md.inodemap.thisblock = bnum;
		goto retry;
	}
	b = lafs_get_block(im, bnum, NULL, GFP_KERNEL, MKREF(cfi_map));
	err = -ENOSPC;
	if (!b)
		goto abort;
	err = lafs_find_block(b, NOADOPT);
	if (err)
		goto abort;
	if (b->b.physaddr == 0 && !test_bit(B_Valid, &b->b.flags)) {
		LAFSI(im)->md.inodemap.nextbit =
			(fs->blocksize<<3) + 1;
		putdref(b,MKREF(cfi_map));
		goto retry;
	}
	err = lafs_read_block(b);
	if (err)
		goto abort;

	bit = LAFSI(im)->md.inodemap.nextbit;
	LAFSI(im)->md.inodemap.thisblock = bnum;
	buf = map_dblock(b);
	while (bnum == 0 && bit < 16) {
		/* Never return an inum below 16 - they are special */
		if (!generic_test_le_bit(bit, (unsigned long *)buf))
			generic___clear_le_bit(bit, (unsigned long *)buf);
		bit++;
	}

	bit = generic_find_next_le_bit((unsigned long *)buf,
				       fs->blocksize<<3, bit);
	unmap_dblock(b, buf);
	LAFSI(im)->md.inodemap.nextbit = bit+1;
	if (bit >= fs->blocksize<<3) {
		putdref(b,MKREF(cfi_map));
		goto retry;
	}
	mutex_unlock(&im->i_mutex);
	*bp = b;
	*inump = bit + (bnum << (im->i_blkbits + 3));
	return 0;

abort_unlock:
	lafs_checkpoint_unlock(fs);
abort:
	putdref(b, MKREF(cfi_map));
	*bp = NULL;
	mutex_unlock(&im->i_mutex);
	iput(im);
	return err;
}

struct inode_map_new_info {
	struct datablock *ib, *mb;
};

static int
inode_map_new_prepare(struct fs *fs, int inum, struct inode *fsys,
		      struct inode_map_new_info *imni)
{
	int choice = inum;
	int restarted = 0;
	int err = 0;
	struct datablock *b;

	imni->ib = imni->mb = NULL;
retry:
	if (inum == 0)
		/* choose a possibly-free inode number */
		err = choose_free_inum(fs, fsys, &choice,
				       &imni->mb, &restarted);
	if (err)
		return err;

	b = lafs_get_block(fsys, choice, NULL, GFP_KERNEL,
			   MKREF(cfi_ino));
	if (!b)
		return -ENOMEM;

	if (test_and_set_bit(B_Claimed, &b->b.flags)) {
		putdref(b, MKREF(cfi_ino));
		if (inum)
			return -EEXIST;
		goto retry;
	}
	if (imni->mb) {
		lafs_iolock_written(&imni->mb->b);
		set_bit(B_PinPending, &imni->mb->b.flags);
		lafs_iounlock_block(&imni->mb->b);
	}
	set_bit(B_PinPending, &b->b.flags);
	b->my_inode = NULL;
	imni->ib = b;
	return 0;
}

static int
inode_map_new_pin(struct inode_map_new_info *imni)
{
	int err = 0;
	if (imni->mb)
		err = lafs_pin_dblock(imni->mb, NewSpace);
	err = err ?: lafs_pin_dblock(imni->ib, NewSpace);
	return err;
}

static void
inode_map_new_commit(struct inode_map_new_info *imni)
{
	unsigned long *buf;

	if (imni->mb) {
		int blksize = imni->ib->b.inode->i_sb->s_blocksize;
		int bit = imni->ib->b.fileaddr & (blksize*8 - 1);
		int hole = 0;
		struct inode *ino = imni->mb->b.inode;

		mutex_lock_nested(&ino->i_mutex, I_MUTEX_QUOTA);
		buf = map_dblock(imni->mb);
		generic___clear_le_bit(bit, buf);
		if (buf[blksize/sizeof(*buf)-1] == 0 &&
		    generic_find_next_le_bit(buf, blksize*8, 0) == blksize*8)
			/* block is empty, punch a hole */
			hole = 1;

		unmap_dblock(imni->mb, buf);
		if (hole)
			lafs_erase_dblock(imni->mb);
		else
			lafs_dirty_dblock(imni->mb);

		putdref(imni->mb, MKREF(cfi_map));
		mutex_unlock(&ino->i_mutex);
		iput(ino);
	}
	putdref(imni->ib, MKREF(cfi_ino));
}

static void
inode_map_new_abort(struct inode_map_new_info *imni)
{
	if (imni->ib) {
		clear_bit(B_Claimed, &imni->ib->b.flags);
		clear_bit(B_PinPending, &imni->ib->b.flags);
		lafs_orphan_release(fs_from_inode(imni->ib->b.inode),
				    imni->ib);
	}
	putdref(imni->ib, MKREF(cfi_ino));
	if (imni->mb) {
		struct inode *ino = imni->mb->b.inode;
		putdref(imni->mb, MKREF(cfi_map));
		iput(ino);
	}
}

struct inode *
lafs_new_inode(struct fs *fs, struct inode *fsys, struct inode *dir,
	       int type, int inum, int mode, struct datablock **inodbp)
{
	/* allocate and instantiate a new inode.  If inum is non-zero,
	 * choose any number, otherwise we are creating a special inode
	 * and have to use the given number.
	 * This creation is committed independently of any name that might
	 * subsequently be given to the inode.  So we register it as an
	 * orphan so that it will be cleaned up if the name isn't
	 * successfully created
	 *
	 */
	struct inode *ino;
	struct datablock *b;
	struct inode_map_new_info imni;
	struct update_handle ui;
	int err;

	err = inode_map_new_prepare(fs, inum, fsys, &imni);
	err = lafs_cluster_update_prepare(&ui, fs, sizeof(struct la_inode))
		?: err;
	if (err == 0)
		err = lafs_make_orphan(fs, imni.ib);
	if (err)
		goto abort;
retry:
	lafs_checkpoint_lock(fs);

	err = inode_map_new_pin(&imni);

	if (err == -EAGAIN) {
		lafs_checkpoint_unlock_wait(fs);
		goto retry;
	}
	if (err < 0)
		goto abort_unlock;

	b = getdref(imni.ib, MKREF(inode_new));

	lafs_iolock_block(&b->b); /* make sure we don't race with the cleaner
				   * and zero this inode while trying to load it
				   */
	lafs_inode_init(b, type, mode, dir);
	lafs_iounlock_block(&b->b);

	inode_map_new_commit(&imni);
	ino = lafs_iget(fsys, b->b.fileaddr, SYNC);
	if (IS_ERR(ino)) {
		lafs_cluster_update_abort(&ui);
		LAFS_BUG(1, &b->b);
	} else
		lafs_cluster_update_commit(&ui, b, 0,
					   LAFSI(ino)->metadata_size);
	LAFS_BUG(LAFSI(ino)->dblock != b, &b->b);
	LAFS_BUG(b->my_inode != ino, &b->b);
	lafs_checkpoint_unlock(fs);

	if (inodbp)
		*inodbp = b;
	else
		putdref(b, MKREF(inode_new));
	return ino;

abort_unlock:
	lafs_checkpoint_unlock(fs);
	err = -ENOSPC;
abort:
	inode_map_new_abort(&imni);
	lafs_cluster_update_abort(&ui);
	dprintk("After abort %d: %s\n", err, strblk(&imni.ib->b));
	return ERR_PTR(err);
}

static int inode_map_free(struct fs *fs, struct inode *fsys, u32 inum)
{
	struct inode *im = lafs_iget(fsys, 1, SYNC);
	int bit;
	unsigned long *buf;
	struct datablock *b;
	u32 bnum;
	int err;

	mutex_lock_nested(&im->i_mutex, I_MUTEX_QUOTA);

	bnum = inum >> (3 + fs->blocksize_bits);
	bit = inum - (bnum << (3 + fs->blocksize_bits));
	b = lafs_get_block(im, bnum, NULL, GFP_KERNEL, MKREF(inode_map_free));
	if (!b) {
		mutex_unlock(&im->i_mutex);
		iput(im);
		return -ENOMEM;
	}
	err = lafs_read_block(b);
	if (err) {
		putdref(b, MKREF(inode_map_free));
		mutex_unlock(&im->i_mutex);
		iput(im);
		return err;
	}
	lafs_iolock_written(&b->b);
	set_bit(B_PinPending, &b->b.flags);
	lafs_iounlock_block(&b->b);
retry:
	lafs_checkpoint_lock(fs);
	err = lafs_pin_dblock(b, ReleaseSpace);
	if (err == -EAGAIN) {
		lafs_checkpoint_unlock_wait(fs);
		goto retry;
	}
	BUG_ON(err < 0);
	buf = map_dblock(b);
	generic___set_le_bit(bit, buf);
	unmap_dblock(b, buf);
	lafs_dirty_dblock(b);
	putdref(b, MKREF(inode_map_free));
	lafs_checkpoint_unlock(fs);
	mutex_unlock(&im->i_mutex);
	iput(im);
	return 0;
}

int lafs_inode_inuse(struct fs *fs, struct inode *fsys, u32 inum)
{
	/* This is used during roll-forward to register a newly created
	 * inode in the inode map
	 */
	struct inode *im = lafs_iget(fsys, 1, SYNC);
	int bit;
	unsigned long *buf;
	struct datablock *b;
	u32 bnum;
	int err;

	mutex_lock_nested(&im->i_mutex, I_MUTEX_QUOTA);

	bnum = inum >> (3 + fs->blocksize_bits);
	bit = inum - (bnum << (3 + fs->blocksize_bits));
	if (bnum > LAFSI(im)->md.inodemap.size) {
		/* inum to unbelievably big */
		mutex_unlock(&im->i_mutex);
		iput(im);
		return -EINVAL;
	}
	b = lafs_get_block(im, bnum, NULL, GFP_KERNEL, MKREF(inode_map_free));
	if (!b) {
		mutex_unlock(&im->i_mutex);
		iput(im);
		return -ENOMEM;
	}

	err = lafs_read_block(b);
	if (err) {
		putdref(b, MKREF(inode_map_free));
		mutex_unlock(&im->i_mutex);
		iput(im);
		return err;
	}

	lafs_iolock_written(&b->b);
	set_bit(B_PinPending, &b->b.flags);
	lafs_iounlock_block(&b->b);
retry:
	lafs_checkpoint_lock(fs);
	err = lafs_pin_dblock(b, CleanSpace);
	if (err == -EAGAIN) {
		lafs_checkpoint_unlock_wait(fs);
		goto retry;
	}
	BUG_ON(err < 0);
	buf = map_dblock(b);
	if (bnum == LAFSI(im)->md.inodemap.size) {
		/* need to add a new block to the file */
		memset(buf, 0xff, fs->blocksize);
		LAFSI(im)->md.inodemap.size = bnum + 1;
		lafs_dirty_inode(im);
	}
	generic___clear_le_bit(bit, buf);
	unmap_dblock(b, buf);
	lafs_dirty_dblock(b);
	putdref(b, MKREF(inode_map_free));
	lafs_checkpoint_unlock(fs);
	mutex_unlock(&im->i_mutex);
	iput(im);
	return 0;
}



int lafs_setattr(struct dentry *dentry, struct iattr *attr)
{
	int err;
	struct inode *ino = dentry->d_inode;
	struct fs *fs = fs_from_inode(ino);
	struct datablock *db;

	err = inode_change_ok(ino, attr);
	db = lafs_inode_dblock(ino, SYNC, MKREF(setattr));
	if (IS_ERR(db))
		err = PTR_ERR(db);
	if (err)
		return err;

	/* We don't need iolock_written here as we don't
	 * actually change the inode block yet
	 */
	lafs_iolock_block(&db->b);
	set_bit(B_PinPending, &db->b.flags);
	lafs_iounlock_block(&db->b);

	/* FIXME quota stuff */

again:
	lafs_checkpoint_lock(fs);
	err = lafs_pin_dblock(db, ReleaseSpace);
	if (err == -EAGAIN) {
		lafs_checkpoint_unlock_wait(fs);
		goto again;
	}

	if (!err) {
		if ((attr->ia_valid & ATTR_SIZE) &&
		    attr->ia_size != i_size_read(ino))
			truncate_setsize(ino, attr->ia_size);
		setattr_copy(ino, attr);
		mark_inode_dirty(ino);

		lafs_dirty_dblock(db);
	}
	clear_bit(B_PinPending, &db->b.flags);
	putdref(db, MKREF(setattr));
	lafs_checkpoint_unlock(fs);

	return err;
}

void lafs_truncate(struct inode *ino)
{
	/* Want to truncate this file.
	 * i_size has already been changed, and the address space
	 * has been cleaned up.
	 * So just start the background truncate
	 */
	struct fs *fs = fs_from_inode(ino);
	struct datablock *db = lafs_inode_dblock(ino, SYNC, MKREF(trunc));
	loff_t trunc_block;
	DEFINE_WAIT(wq);

	if (IS_ERR(db))
		return;

	trunc_block = ((i_size_read(ino) + fs->blocksize - 1)
		       >> fs->blocksize_bits);
	/* We hold i_mutex, so regular orphan processing cannot
	 * contine - we have to push it forward ourselves.
	 */
	while (test_bit(I_Trunc, &LAFSI(ino)->iflags) &&
	       LAFSI(ino)->trunc_next < trunc_block) {
		prepare_to_wait(&fs->async_complete, &wq,
				TASK_UNINTERRUPTIBLE);
		lafs_inode_handle_orphan(db);
		if (test_bit(B_Orphan, &db->b.flags))
			schedule();
	}
	finish_wait(&fs->async_complete, &wq);

	/* There is nothing we can do about errors here.  The
	 * most likely are ENOMEM which itself is very unlikely.
	 * If this doesn't get registered as an orphan .... maybe
	 * it will have to wait until something else truncates it.
	 */
	lafs_make_orphan(fs, db);

	if (!test_and_set_bit(I_Trunc, &LAFSI(ino)->iflags))
		lafs_igrab_fs(ino);
	if (trunc_block == 0)
		LAFSI(ino)->trunc_gen++;
	LAFSI(ino)->trunc_next = trunc_block;
	putdref(db, MKREF(trunc));
}

const struct inode_operations lafs_special_ino_operations = {
	.setattr	= lafs_setattr,
	.getattr	= lafs_getattr,
	.truncate	= lafs_truncate,
};
