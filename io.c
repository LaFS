
/*
 * IO routines for LaFS
 * fs/lafs/io.c
 * Copyright (C) 2006-2009
 * NeilBrown <neilb@suse.de>
 * Released under the GPL, version 2
 */

/*
 * There are quite separate sets of routines here.
 * One set is used for reading and writing filesystem blocks.
 * Reading is generally asynchronous, but can be waited for.
 * Writing is sequential into write-clusters.  It is not possible
 * to wait for a particular write, but only to wait for a write-cluster
 * to by safe.
 * The other set is for all other IO such as reading/writing superblocks
 * and stateblocks, and for reading cluster-heads during roll-forward.
 * These reads are always synchronous while write allow all devices
 * to be written in parallel.
 */

#include	"lafs.h"
#include	<linux/blkdev.h>
#include	<linux/bit_spinlock.h>

int
lafs_dev_find(struct fs *fs, u64 virt)
{
	int i;
	for (i = 0; i < fs->devices; i++)
		if (virt >= fs->devs[i].start &&
		    virt < fs->devs[i].start + fs->devs[i].size)
			return i;
	printk("%llu not found:\n", (unsigned long long) virt);
	for (i = 0; i < fs->devices; i++)
		printk(" %d: %llu+%llu\n", i,
		       (unsigned long long)fs->devs[i].start,
		       (unsigned long long)fs->devs[i].size);
	BUG();
	return -1;
}

static void bi_complete(struct bio *bio, int error)
{
	complete((struct completion *)bio->bi_private);
}

int
lafs_sync_page_io(struct block_device *bdev, sector_t sector,
		  int offset, int size,
		  struct page *page, int rw)
{
	struct bio *bio = bio_alloc(GFP_NOIO, 1);
	struct completion event;
	int ret;

	rw |= REQ_UNPLUG;

	bio->bi_bdev = bdev;
	bio->bi_sector = sector;
	bio_add_page(bio, page, size, offset);
	init_completion(&event);
	bio->bi_private = &event;
	bio->bi_end_io = bi_complete;
	submit_bio(rw, bio);
	wait_for_completion(&event);

	ret = !!test_bit(BIO_UPTODATE, &bio->bi_flags);
	bio_put(bio);
	return ret;
}

int
lafs_load_page(struct fs *fs, struct page *p, u64 vaddr, int blocks)
{
	int dev;
	sector_t sect;
	struct block_device *bdev;

	virttophys(fs, vaddr, &dev, &sect);

	if (dev < 0 || dev >= fs->devs_loaded) {
		dprintk("dev %d not in [0..%d)\n", dev, fs->devs_loaded);
		return -EIO;
	}

	bdev = fs->devs[dev].bdev;
	return lafs_sync_page_io(bdev, sect, 0,
				 blocks << fs->blocksize_bits,
				 p, 0) ? 0 : -EIO;
}

int
lafs_load_pages(struct fs *fs, struct page *p, u64 vaddr, int blocks)
{
	/* load 1 or more pages which are consecutive in memory
	 * from 'p'
	 * FIXME make this async - then wait.
	 */
	int blocks_per_page = (PAGE_SIZE >> fs->blocksize_bits);
	int rv = 0;

	while(blocks && rv == 0) {
		int b = blocks;
		if (b > blocks_per_page)
			b = blocks_per_page;
		rv = lafs_load_page(fs, p, vaddr, b);
		blocks -= b;
		vaddr += blocks_per_page;
		p++;
	}
	return rv;
}

static void
bi_async_complete(struct bio *bio, int error)
{
	struct async_complete *ac = bio->bi_private;

	if (test_bit(BIO_UPTODATE, &bio->bi_flags))
		ac->state = 3;
	else
		ac->state = 4;
	bio_put(bio);
	lafs_wake_thread(ac->fs);
}

static void
async_page_io(struct block_device *bdev, sector_t sector, int offset, int size,
	      struct page *page, int rw, struct async_complete *ac)
{
	struct bio *bio = bio_alloc(GFP_NOIO, 1);

	rw |= REQ_UNPLUG;

	bio->bi_bdev = bdev;
	bio->bi_sector = sector;
	bio_add_page(bio, page, size, offset);
	bio->bi_private = ac;
	bio->bi_end_io = bi_async_complete;
	submit_bio(rw, bio);
}

int
lafs_load_page_async(struct fs *fs, struct page *p, u64 vaddr,
		     int blocks, struct async_complete *ac)
{
	int dev;
	sector_t sect;
	struct block_device *bdev;

	virttophys(fs, vaddr, &dev, &sect);

	if (dev < 0 || dev >= fs->devs_loaded) {
		dprintk("dev %d not in [0..%d)\n", dev, fs->devs_loaded);
		return -EIO;
	}
	if (ac->state == 2)
		return -EAGAIN;
	if (ac->state == 3)
		return 0;
	if (ac->state == 4)
		return -EIO;

	bdev = fs->devs[dev].bdev;
	ac->state = 2; /* loading */
	ac->fs = fs;
	async_page_io(bdev, sect, 0,
		      blocks << fs->blocksize_bits,
		      p, 0, ac);
	return -EAGAIN;
}

static void
bi_write_done(struct bio *bio, int error)
{
	struct fs *fs = bio->bi_private;

	if (atomic_dec_and_test(&fs->sb_writes_pending))
		wake_up(&fs->sb_writes_wait);
	bio_put(bio);
	/* FIXME didn't do anything with error */
}

void
lafs_super_write(struct fs *fs, int dev, u64 addr, char *buf, int size)
{
	struct bio *bio = bio_alloc(GFP_NOIO, 1);
	int rw = WRITE | REQ_UNPLUG;

	bio->bi_bdev = fs->devs[dev].bdev;
	bio->bi_sector = addr;
	bio_add_page(bio, virt_to_page(buf), size, offset_in_page(buf));
	bio->bi_private = fs;
	bio->bi_end_io = bi_write_done;
	atomic_inc(&fs->sb_writes_pending);
	submit_bio(rw, bio);
}

int
lafs_super_wait(struct fs *fs)
{
	wait_event(fs->sb_writes_wait,
		   atomic_read(&fs->sb_writes_pending) == 0
		);
	return 0; /* FIXME should be an error flag */
}

static int sched(void *flags)
{
	io_schedule();
	return 0;
}

void _lafs_iolock_block(struct block *b)
{
	while (test_and_set_bit(B_IOLock, &b->flags)) {
#ifdef DEBUG_IOLOCK
		printk("iolock wait for %s:%d: %s\n",
		       b->iolock_file, b->iolock_line,
		       strblk(b));
#endif
		wait_on_bit(&b->flags, B_IOLock,
			    sched, TASK_UNINTERRUPTIBLE);
	}
}

int _lafs_iolock_block_async(struct block *b)
{
	for(;;) {
		if (!test_and_set_bit(B_IOLock, &b->flags)) {
			/* just got the lock! */
			if (test_and_clear_bit(B_Async, &b->flags))
				putref(b, MKREF(async));
			return 1;
		}
		if (test_and_set_bit(B_Async, &b->flags))
			/* already have async set */
			return 0;
		getref(b, MKREF(async));
	}
}

void
lafs_iounlock_block(struct block *b)
{
	/* Unlock this block, and if it is the last locked block
	 * for the page, unlock the page too.
	 * This only applied to data blocks.
	 */

	if (test_bit(B_Index, &b->flags))
		clear_bit(B_IOLock, &b->flags);
	else
		lafs_iocheck_block(dblk(b), 1);

	wake_up_bit(&b->flags, B_IOLock);
	if (test_bit(B_Async, &b->flags))
		lafs_wake_thread(fs_from_inode(b->inode));
}

void lafs_writeback_done(struct block *b)
{
	/* remove writeback flag on this block.
	 * If it is last on page, release page as well.
	 */

	if (test_bit(B_Index, &b->flags)) {
		clear_bit(B_Writeback, &b->flags);
		wake_up_bit(&b->flags, B_Writeback);
		if (test_bit(B_Async, &b->flags))
			lafs_wake_thread(fs_from_inode(b->inode));
	} else
		lafs_iocheck_writeback(dblk(b), 1);
}

void lafs_iocheck_block(struct datablock *db, int unlock)
{
	struct page *page = db->page;
	struct datablock *blist;
	int n, i;
	int locked = 0;
	int havelock = 0;

	if (!page)
		return;
	blist = (struct datablock *)page->private;
	if (!blist)
		return;

	n = 1<<(PAGE_CACHE_SHIFT - blist->b.inode->i_blkbits);
	bit_spin_lock(B_IOLockLock, &blist->b.flags);
	if (unlock)
		clear_bit(B_IOLock, &db->b.flags);
	for (i = 0 ; i < n; i++) {
		if (test_bit(B_IOLock, &blist[i].b.flags))
			locked++;
	}
	if (!locked && test_and_clear_bit(B_HaveLock, &blist->b.flags))
		havelock = 1;
	bit_spin_unlock(B_IOLockLock, &blist->b.flags);

	if (havelock) {
		if (!PageError(page))
			SetPageUptodate(page);
		unlock_page(page);
	}
}

void lafs_iocheck_writeback(struct datablock *db, int unlock)
{
	struct page *page = db->page;
	struct datablock *blist;
	int n, i;
	int locked = 0;
	int havewrite = 0;

	if (!page)
		return;
	blist = (struct datablock *)page->private;
	if (!blist)
		return;

	n = 1<<(PAGE_CACHE_SHIFT - blist->b.inode->i_blkbits);
	bit_spin_lock(B_IOLockLock, &blist->b.flags);
	if (unlock)
		clear_bit(B_Writeback, &db->b.flags);
	for (i = 0 ; i < n; i++) {
		if (test_bit(B_Writeback, &blist[i].b.flags))
			locked++;
		/* FIXME what about checking uptodate ?? */
	}
	if (!locked && test_and_clear_bit(B_HaveWriteback, &blist->b.flags))
		havewrite = 1;
	bit_spin_unlock(B_IOLockLock, &blist->b.flags);

	if (havewrite)
		end_page_writeback(page);
	if (unlock) {
		wake_up_bit(&db->b.flags, B_Writeback);
		if (test_bit(B_Async, &db->b.flags))
			lafs_wake_thread(fs_from_inode(db->b.inode));
	}
}

static int sched_valid(void *flags)
{
	if (test_bit(B_Valid, flags))
		return -EINTR;

	schedule();
	return 0;
}

int __must_check
lafs_wait_block(struct block *b)
{
	if (test_bit(B_IOLock, &b->flags) &&
	    !test_bit(B_Valid, &b->flags))
		wait_on_bit(&b->flags, B_IOLock, 
			    sched_valid, TASK_UNINTERRUPTIBLE);

	return test_bit(B_Valid, &b->flags) ? 0 : -EIO;
}

int __must_check
lafs_wait_block_async(struct block *b)
{
	for (;;) {
		if (!test_bit(B_IOLock, &b->flags) ||
		    test_bit(B_Valid, &b->flags)) {
			if (test_and_clear_bit(B_Async, &b->flags))
				putref(b, MKREF(async));
			if (test_bit(B_Valid, &b->flags))
				return 0;
			else
				return -EIO;
		}
		if (test_and_set_bit(B_Async, &b->flags))
			return -EAGAIN;
		getref(b, MKREF(async));
	}
}

static void wait_writeback(struct block *b)
{
	if (test_bit(B_Writeback, &b->flags)) {
#ifdef DEBUG_IOLOCK
		printk("writeback wait for %s:%d: %s\n",
		       b->iolock_file, b->iolock_line,
		       strblk(b));
#endif
		lafs_trigger_flush(b);
		wait_on_bit(&b->flags, B_Writeback,
			    sched, TASK_UNINTERRUPTIBLE);
	}
}

void _lafs_iolock_written(struct block *b)
{
	_lafs_iolock_block(b);
	wait_writeback(b);
}

int _lafs_iolock_written_async(struct block *b)
{
	for (;;) {
		if (!test_bit(B_Writeback, &b->flags) &&
		    !test_and_set_bit(B_IOLock, &b->flags)) {
			if (!test_bit(B_Writeback, &b->flags)) {
				/* Have lock without writeback */
				if (test_and_clear_bit(B_Async, &b->flags))
					putref(b, MKREF(async));
				return 1;
			}
			/* Writeback was set by a racing thread.. */
			lafs_iounlock_block(b);
		}
		lafs_trigger_flush(b);
		if (test_and_set_bit(B_Async, &b->flags))
			return 0;

		getref(b, MKREF(async));
	}
}

static void
block_loaded(struct bio *bio, int error)
{
	struct block *b = bio->bi_private;

	dprintk("loaded %d of %d\n", (int)b->fileaddr, (int)b->inode->i_ino);
	if (test_bit(BIO_UPTODATE, &bio->bi_flags)) {
		set_bit(B_Valid, &b->flags); /* FIXME should I set
						an error too? */
	} else if (!test_bit(B_Index, &b->flags) && dblk(b)->page) {
		ClearPageUptodate(dblk(b)->page);
		SetPageError(dblk(b)->page);
	} else
		dprintk("Block with no page!!\n");
	lafs_iounlock_block(b);
}

static void
blocks_loaded(struct bio *bio, int error)
{
	struct block *bhead = bio->bi_private;

	while (bhead->chain) {
		struct block *b = bhead->chain;
		bhead->chain = b->chain;
		b->chain = NULL;
		bio->bi_private = b;
		block_loaded(bio, error);
	}
	bio->bi_private = bhead;
	block_loaded(bio, error);
}

int __must_check
lafs_load_block(struct block *b, struct bio *bio)
{
	int dev;
	sector_t sect;
	struct block_device *bdev;
	struct fs *fs = fs_from_inode(b->inode);
	struct page *page;
	struct block *headb;
	int offset;

	if (!test_bit(B_PhysValid, &b->flags))
		b->physaddr = 0;
	if (test_bit(B_Valid, &b->flags))
		return 0;
	lafs_iolock_block(b);
	if (test_bit(B_Valid, &b->flags)) {
		lafs_iounlock_block(b);
		return 0;
	}
	LAFS_BUG(test_bit(B_InoIdx, &b->flags), b);
	if (test_bit(B_Index, &b->flags)) {
		struct indexblock *ib = iblk(b);

		if (b->physaddr == 0) {
			/* An empty index block.  One doesn't
			 * see many of these as it means we trimmed
			 * out some blocks, but not all following
			 * block, and block in the hole is being
			 * looked for.  Just Create a valid clear
			 * index block.
			 */
			lafs_clear_index(ib);
			lafs_iounlock_block(b);
			return 0;
		}

		page = virt_to_page(ib->data);
		offset = offset_in_page(ib->data);
	} else {
		struct datablock *db = dblk(b);
		if (b->physaddr == 0) {
			/* block is either in the inode, or
			 * non-existent (all 'nul').
			 */
			struct lafs_inode *lai = LAFSI(b->inode);
			void *baddr = map_dblock(db);

			/* This case is handled in find_block */
			LAFS_BUG(lai->depth == 0 && b->fileaddr == 0, b);

			memset(baddr, 0, (1<<b->inode->i_blkbits));
			unmap_dblock(db, baddr);
			set_bit(B_Valid, &b->flags);
			lafs_iounlock_block(b);
			return 0;
		}
		page = db->page;
		offset = dblock_offset(db);
	}

	virttophys(fs, b->physaddr, &dev, &sect);

	if (dev < 0) {
		lafs_iounlock_block(b);
		return -EIO;
	}

	bdev = fs->devs[dev].bdev;

	if (!bio) {
		bio = bio_alloc(GFP_NOIO, 1);

		bio->bi_bdev = bdev;
		bio->bi_sector = sect;
		bio_add_page(bio, page, fs->blocksize, offset);

		bio->bi_private = b;
		bio->bi_end_io = block_loaded;
		submit_bio(READ, bio);

		return 0;
	}
	LAFS_BUG(b->chain != NULL, b);
	if (bio->bi_size == 0) {
		bio->bi_sector = sect;
		bio->bi_bdev = bdev;
		bio_add_page(bio, page, fs->blocksize, offset);
		bio->bi_private = b;
		bio->bi_end_io = blocks_loaded;
		return 0;
	}
	if (bio->bi_sector + (bio->bi_size / 512) != sect
	    || bio->bi_bdev != bdev
	    || bio_add_page(bio, page, fs->blocksize, offset) == 0)
		return -EINVAL;
	/* added the block successfully */
	headb = bio->bi_private;
	b->chain = headb->chain;
	headb->chain = b;
	return 0;
}

int __must_check
lafs_read_block(struct datablock *b)
{
	int rv;

	if (test_bit(B_Valid, &b->b.flags))
		return 0;

	rv = lafs_find_block(b, NOADOPT);
	if (rv)
		return rv;
	rv = lafs_load_block(&b->b, NULL);
	if (rv)
		return rv;
	return lafs_wait_block(&b->b);
}

int __must_check
lafs_read_block_async(struct datablock *b)
{
	int rv;

	if (test_bit(B_Valid, &b->b.flags))
		return 0;

	rv = lafs_find_block_async(b);
	if (rv)
		return rv;
	rv = lafs_load_block(&b->b, NULL);
	if (rv)
		return rv;
	return lafs_wait_block_async(&b->b);
}

/*------------------------------------------------------------------
 * Writing filesystem blocks and cluster headers.
 * The endio function is found from lafs_cluster_endio_choose.
 * We need to increment the pending_cnt for this cluster and,
 * if this is a header block, possibly for earlier clusters.
 *
 * Later should attempt to combine multiple blocks into the
 * one bio ... if we can manage the bi_end_io function properly
 */

static void write_block(struct fs *fs, struct page *p, int offset,
			u64 virt, struct wc *wc, int head)
{
	struct bio *bio;
	sector_t uninitialized_var(sect);
	int which = wc->pending_next;
	int dev;
	int nr_vecs;

	virttophys(fs, virt, &dev, &sect);

	bio = wc->bio;
	if (bio && virt == wc->bio_virt &&
	    bio->bi_bdev == fs->devs[dev].bdev &&
	    which == wc->bio_which &&
	    bio_add_page(bio, p, fs->blocksize, offset) > 0) {
		/* Added the current bio - too easy */
		wc->bio_virt++;
		return;
	}

	if (bio) {
		int w = wc->bio_which;
		/* need to submit the pending bio and add to pending counts */
		atomic_inc(&wc->pending_cnt[w]);
		if (wc->bio_head) {
			w = (w+3) % 4;
			if (wc->pending_vfy_type[w] == VerifyNext ||
			    wc->pending_vfy_type[w] == VerifyNext2)
				atomic_inc(&wc->pending_cnt[w]);
			w = (w+3) % 4;
			if (wc->pending_vfy_type[w] == VerifyNext2)
				atomic_inc(&wc->pending_cnt[w]);
		}
		wc->bio = NULL;
		if (wc->bio_queue && wc->bio_queue != bdev_get_queue(bio->bi_bdev))
			blk_unplug(wc->bio_queue);
		wc->bio_queue = bdev_get_queue(bio->bi_bdev);
		submit_bio(WRITE, bio);
		bio = NULL;
	}
	if (!virt && !head) {
		/* end of cluster */
		if (wc->bio_queue)
			blk_unplug(wc->bio_queue);
		wc->bio_queue = NULL;
		return;
	}
	nr_vecs = 128; /* FIXME */
	while (!bio && nr_vecs) {
		bio = bio_alloc(GFP_NOIO, nr_vecs);
		nr_vecs /= 2;
	}
	wc->bio = bio;
	wc->bio_virt = virt + 1;
	wc->bio_head = head;
	wc->bio_which = which;
	bio->bi_bdev = fs->devs[dev].bdev;
	bio->bi_sector = sect;
	bio_add_page(bio, p, fs->blocksize, offset);

	bio->bi_private = wc;
	bio->bi_end_io = lafs_cluster_endio_choose(which, head);
}

void lafs_write_head(struct fs *fs, struct cluster_head *head, u64 virt,
		     struct wc *wc)
{
	write_block(fs, virt_to_page(head), offset_in_page(head),
		    virt, wc, 1);
}

void lafs_write_block(struct fs *fs, struct block *b, struct wc *wc)
{
	if (test_bit(B_Index, &b->flags))
		write_block(fs, virt_to_page(iblk(b)->data),
			    offset_in_page(iblk(b)->data),
			    b->physaddr, wc, 0);
	else
		write_block(fs, dblk(b)->page, dblock_offset(dblk(b)),
			    b->physaddr, wc, 0);
}

void lafs_write_flush(struct fs *fs, struct wc *wc)
{
	write_block(fs, NULL, 0, 0, wc, 0);
}
