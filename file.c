
/*
 * fs/lafs/file.c
 * Copyright (C) 2005-2009
 * Neil Brown <neilb@suse.de>
 * Released under the GPL, version 2
 *
 * File operations
 */

#include	"lafs.h"
#include	<linux/bit_spinlock.h>
#include	<linux/writeback.h>
#include	<linux/version.h>

struct readpage_data {
	struct bio *bio;
	int pages_remaining;
};

static int
readpage_filler(void *data, struct page *page)
{
	struct inode *ino = page->mapping->host;
	struct readpage_data *rpd = data;
	struct datablock *b0;
	int n = 1 << (PAGE_SHIFT - ino->i_blkbits);
	int i;
	int err = 0;

	rpd->pages_remaining--;
	b0 = lafs_get_block(ino, 0, page, GFP_KERNEL,
			    MKREF(readpage0));

	if (!b0)
		return -ENOMEM;

	LAFS_BUG(test_bit(B_HaveLock, &b0->b.flags), &b0->b);

	dprintk("read page %p for %d blocks\n", page, n);
	for (i = 0; i < n; i++) {
		struct datablock *b = lafs_get_block(ino, i, page, GFP_KERNEL,
						     MKREF(readpage));
		BUG_ON(!b);

		err = lafs_find_block(b, NOADOPT);
		if (err) {
			putdref(b, MKREF(readpage));
			break;
		}
		if (test_bit(B_Valid, &b->b.flags)) {
			putdref(b, MKREF(readpage));
			continue;
		}
	retry:
		if (rpd->bio == NULL)
			rpd->bio = bio_alloc(GFP_NOFS, rpd->pages_remaining + 1);
		err = lafs_load_block(&b->b, rpd->bio);
		if (err == -EINVAL) {
			submit_bio(READ, rpd->bio);
			rpd->bio = NULL;
			goto retry;
		}
		putdref(b, MKREF(readpage));
		if (err)
			break;
	}
	set_bit(B_HaveLock, &b0->b.flags);
	lafs_iocheck_block(b0, 0);
	putdref(b0, MKREF(readpage0));
	return err;
}

static int
lafs_readpage(struct file *file, struct page *page)
{
	/* Page is locked.  We want to read it all in,
	 * and then unlock the page.
	 * For this we get all the blocks and load them
	 */
	int err = 0;
	struct readpage_data rpd;

	rpd.bio = NULL;
	rpd.pages_remaining = 1;
	err = readpage_filler(&rpd, page);

	if (rpd.bio) {
		if (rpd.bio->bi_size)
			submit_bio(READ, rpd.bio);
		else
			bio_put(rpd.bio);
	}
	return err;
}

static int
lafs_readpages(struct file *file, struct address_space *mapping,
	       struct list_head *pages, unsigned nr_pages)
{
	int err = 0;
	struct readpage_data rpd;

	rpd.bio = NULL;
	rpd.pages_remaining = nr_pages;
	err = read_cache_pages(mapping, pages, readpage_filler, &rpd);

	if (rpd.bio) {
		if (rpd.bio->bi_size)
			submit_bio(READ, rpd.bio);
		else
			bio_put(rpd.bio);
	}
	return err;
}


static int
lafs_write_begin(struct file *file, struct address_space *mapping,
		 loff_t pos, unsigned len, unsigned flags,
		 struct page **pagep, void **fsdata)
{
	/* The range covers one or more blocks in this page
	 * and may partially cover the first or last.
	 * If there is partial coverage, we need to read in
	 * those blocks.
	 * Then we 'reserve' those datablocks to ensure that
	 * a write will succeed.
	 */
	struct inode *ino = mapping->host;
	struct fs *fs = fs_from_inode(ino);
	int bits = ino->i_blkbits;
	int first, last, loading = -1;
	struct datablock *fb;
	int err = 0;
	int i;
	pgoff_t index;
	unsigned from, to;
	struct page *page;
	DEFINE_WAIT(wq);

	index = pos >> PAGE_CACHE_SHIFT;
	from = pos & (PAGE_CACHE_SIZE - 1);
	to = from + len;

	first = from >> bits;
	last = (to-1) >> bits;

	while (test_bit(I_Trunc, &LAFSI(ino)->iflags) &&
	       LAFSI(ino)->trunc_next <= last) {
		struct datablock *db = lafs_inode_dblock(ino,
							 SYNC, MKREF(writetrunc));
		prepare_to_wait(&fs->async_complete, &wq,
				TASK_UNINTERRUPTIBLE);
		lafs_inode_handle_orphan(db);
		if (test_bit(B_Orphan, &db->b.flags))
			schedule();
		putdref(db, MKREF(writetrunc));
	}
	finish_wait(&fs->async_complete, &wq);

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,29)
	page = __grab_cache_page(mapping, index);
#else
	page = grab_cache_page_write_begin(mapping, index, flags);
#endif
	if (!page)
		return -ENOMEM;
	*pagep = page;

	fb = lafs_get_block(ino, first, page, GFP_KERNEL, MKREF(write));
	dprintk("PREPARE %p\n", fb);
	if (!fb) {
		err = -ENOMEM;
		goto fail;
	}
	/* Further lafs_get_block calls cannot fail as both the page
	 * and the block structures exist
	 */
	for (i = first + 1 ; i <= last ; i++)
		lafs_get_block(ino, i, page, GFP_KERNEL, MKREF(write));

	if (from != (first << bits)) {
		err = lafs_find_block(fb, NOADOPT);
		if (!err)
			err = lafs_load_block(&fb->b, NULL);
		if (err)
			goto fail;
		loading = first;
	}
	if (last != loading &&
	    to != ((last+1) << bits)) {
		struct datablock *lb = fb + last - first;

		err = lafs_find_block(lb, ADOPT);
		if (!err)
			err = lafs_load_block(&lb->b, NULL);
		if (err)
			goto fail;

		err = lafs_wait_block(&fb[last-first].b);
		if (err)
			goto fail;
	}
	if (loading == first)
		err = lafs_wait_block(&fb->b);
	if (err)
		goto fail;
retry:
	lafs_checkpoint_lock(fs);
	for (i = first ; err == 0 && i <= last ; i++) {
		/* FIXME need PinPending or something to make sure
		 * credits don't disappear */
		err = lafs_reserve_block(&fb[i - first].b, NewSpace);
		LAFS_BUG(fb[i-first].b.parent == NULL, &fb[i-first].b);
	}
	if (err == -EAGAIN) {
		lafs_checkpoint_unlock_wait(fs);
		err = 0;
		goto retry;
	}
	if (err < 0)
		goto fail_unlock;
	*fsdata = fb;
	return 0;

fail_unlock:
	lafs_checkpoint_unlock(fs);
fail:
	if (fb)
		for (i = first; i <= last ; i++)
			putdref(&fb[i-first], MKREF(write));
	unlock_page(page);
	page_cache_release(page);
	return err;
}

static int
lafs_write_end(struct file *file,
	       struct address_space *mapping,
	       loff_t pos, unsigned len, unsigned copied,
	       struct page *page, void *fsdata)
{
	struct inode *ino = mapping->host;
	struct fs *fs = fs_from_inode(ino);
	int bits = ino->i_blkbits;
	unsigned first, last;
	struct datablock *fb;
	unsigned i;
	pgoff_t index;
	unsigned from, to;

	index = pos >> PAGE_CACHE_SHIFT;
	from = pos & (PAGE_CACHE_SIZE - 1);
	to = from + len;

	first = from >> bits;
	last = (to-1) >> bits;

	fb = fsdata;

	if (unlikely(copied < len)) {
		if (!PageUptodate(page))
			copied = 0;
	}

	/*
	 * No need to use i_size_read() here, the i_size
	 * cannot change under us because we hold i_sem.
	 */
	if (pos + len > ino->i_size) {
		i_size_write(ino, pos + len);
		/* note that we deliberately don't call
		 * mark_inode_dirty(ino);
		 * The inode will automatically be written
		 * when the data blocks have been,
		 * and there is no rush before that.
		 * Just set I_Dirty and allow
		 * __set_page_dirty_nobuffers to do the rest
		 */
		set_bit(I_Dirty, &LAFSI(ino)->iflags);
	}
	__set_page_dirty_nobuffers(page);
	for (i = first ; i <= last ; i++) {
		if (copied > 0) {
			set_bit(B_Valid, &(fb+i-first)->b.flags);
			lafs_dirty_dblock(fb+i-first);
		}
		putdref(fb+i-first, MKREF(write));
	}
	lafs_checkpoint_unlock(fs);
	unlock_page(page);
	page_cache_release(page);
	return copied;
}

static int
lafs_writepage(struct page *page, struct writeback_control *wbc)
{
	struct inode *ino = page->mapping->host;
	struct datablock *b0 = NULL;
	int blocks = PAGE_SIZE >> ino->i_blkbits;
	int i;
	int redirty = 0;
	dprintk("WRITEPAGE %lu/%lu/%lu\n", LAFSI(ino)->filesys->i_ino, ino->i_ino, page->index * blocks);
	for (i = 0 ; i < blocks; i++) {
		struct datablock *b = lafs_get_block(ino, i, page, GFP_KERNEL,
						     MKREF(writepage));
		if (!b)
			continue;
		if (i == 0)
			b0 = getdref(b, MKREF(writepage0));

		/* We need to check PinPending, otherwise we might be called
		 * to flush out a page that is currently part of a transaction.
		 * Need to be careful with inodes too.
		 */
		if (test_bit(B_Dirty, &b->b.flags)) {
			struct inode *myi = NULL;
			if (test_bit(B_PinPending, &b->b.flags))
				redirty = 1;
			else if ((myi = rcu_my_inode(b)) != NULL &&
				 LAFSI(myi)->iblock)
				redirty = 1;
			else {
				rcu_iput(myi); myi = NULL;
				lafs_iolock_written(&b->b);
				/* block might have been invalidated,
				 * or Pinned, while we waited */
				if (!test_bit(B_Dirty, &b->b.flags))
					lafs_iounlock_block(&b->b);
				else if (test_bit(B_PinPending, &b->b.flags) ||
					 ((myi = rcu_my_inode(b)) != NULL &&
					  LAFSI(myi)->iblock)) {
					redirty = 1;
					lafs_iounlock_block(&b->b);
				} else {
					rcu_iput(myi); myi = NULL;
					lafs_cluster_allocate(&b->b, 0);
				}
			}
			rcu_iput(myi);
		}
		putdref(b, MKREF(writepage));
	}
	if (redirty)
		redirty_page_for_writepage(wbc, page);
	else {
		if (b0) {
			set_page_writeback(page);
			set_bit(B_HaveWriteback, &b0->b.flags);
			lafs_iocheck_writeback(b0, 0);
		}
	}
	unlock_page(page);

	putdref(b0, MKREF(writepage0));
	if (!b0 || redirty)
		return 0;

	if (LAFSI(ino)->depth == 0) {
		/* We really want the data to be safe soon, not just
		 * the page to be clean.  And the data is in the inode.
		 * So write the inode.
		 */
		struct datablock *b = lafs_inode_dblock(ino, SYNC,
							MKREF(writepageflush));
		if (test_bit(B_Dirty, &b->b.flags)) {
			lafs_iolock_written(&b->b);
			/* block might have been invalidated while we waited */
			if (test_bit(B_Dirty, &b->b.flags))
				lafs_cluster_allocate(&b->b, 0);
			else
				lafs_iounlock_block(&b->b);
		}
		putdref(b, MKREF(writepageflush));
	}
	return 0;
}

static void lafs_sync_page(struct page *page)
{
	/* If any block is dirty, flush the cluster so that
	 * writeback completes.
	 */
	struct inode *ino;
	struct address_space *mapping;
	struct fs *fs;
	int bits;
	int i;
	int want_flush = 0;

	if (!PageWriteback(page))
		/* Presumably page is locked - nothing
		 * we can do
		 * FIXME should unplug the queue if page
		 * is not up to date, and has blocks.
		 */
		return;

	mapping = page->mapping;
	if (!mapping)
		return;
	ino = mapping->host;
	fs = fs_from_inode(ino);
	bits = PAGE_SHIFT - ino->i_blkbits;

	spin_lock(&mapping->private_lock);
	if (PagePrivate(page)) {
		struct datablock *bl = (struct datablock *)page->private;

		for (i = 0; i < (1<<bits); i++) {
			/* If this block is still dirty though the page is in
			 * writeback, the block must be in the current cluster
			 */
			if (test_bit(B_Dirty, &bl[i].b.flags)) {
				want_flush = 2;
				break;
			}
			if (test_bit(B_Writeback, &bl[i].b.flags))
				want_flush = 1;
		}
	}
	spin_unlock(&mapping->private_lock);
	if (want_flush == 2)
		set_bit(FlushNeeded, &fs->fsstate);
	else if (want_flush == 1)
		set_bit(SecondFlushNeeded, &fs->fsstate);
	lafs_wake_thread(fs);
}

static int
lafs_sync_file(struct file *file, int datasync)
{
	struct inode *ino = file->f_dentry->d_inode;
	int err = lafs_sync_inode(ino, 0);

	/* FIXME I ignore datasync, just like file_fsync */
	err = write_inode_now(ino, 0) ?: err;

	if (err == 0)
		lafs_sync_inode(ino, 1);
	return err;
}

void lafs_fillattr(struct inode *ino, struct kstat *stat)
{
	/* This makes sure the reported 'atime' is a time that
	 * we can store and return after a clean restart
	 */
	generic_fillattr(ino, stat);

	if (!test_bit(I_AccessTime, &LAFSI(ino)->iflags))
		return;

	stat->atime = LAFSI(ino)->md.file.i_accesstime;
	lafs_add_atime_offset(&stat->atime, LAFSI(ino)->md.file.atime_offset);
}

int lafs_getattr(struct vfsmount *mnt, struct dentry *dentry,
		 struct kstat *stat)
{
	lafs_fillattr(dentry->d_inode, stat);
	return 0;
}

const struct file_operations lafs_file_file_operations = {
	.llseek		= generic_file_llseek,
	.read		= do_sync_read,
	.write		= do_sync_write,
	.aio_read	= generic_file_aio_read,
	.aio_write	= generic_file_aio_write,
/*	.ioctl		= lafs__ioctl,*/
	.mmap		= generic_file_mmap,
	.open		= generic_file_open,
/*	.release	= lafs__release_file,*/
	.fsync		= lafs_sync_file,
	.splice_read	= generic_file_splice_read,
	.splice_write	= generic_file_splice_write,
};

const struct inode_operations lafs_file_ino_operations = {
	.setattr	= lafs_setattr,
	.getattr	= lafs_getattr,
};

const struct address_space_operations lafs_file_aops = {
	.readpage	= lafs_readpage,
	.readpages	= lafs_readpages,
	.writepage	= lafs_writepage,
	.write_begin	= lafs_write_begin,
	.write_end	= lafs_write_end,
	.invalidatepage	= lafs_invalidate_page,
	.releasepage	= lafs_release_page,
	.sync_page	= lafs_sync_page,
};
