
/*
 * routines to create a checkpoint for LaFS
 * fs/lafs/checkpoint.c
 * Copyright (C) 2006-2009
 * NeilBrown <neilb@suse.de>
 * Released under the GPL, version 2
 */

/* A checkpoint is a series of write clusters
 * from which roll-forward can start when the filesystem
 * is restarted.
 * By creating a new checkpoint, all write clusters before
 * the checkpoint become free to participate in cleaning.
 * Thus the two motivations for creating a checkpoint are
 * - to limit the amount of device space that roll-forward will
 *   have to scan on a restart
 * - to make more space available in a very full filesystem.
 * - to allow creation of a snapshot.
 * So when the filesystem is very full a checkpoint should be
 * triggered every time a new segment is started.
 * Otherwise a checkpoint should be triggered once periodically based
 * on the number of segments written - probably once for every
 * few dozen segments - experimentation and possible tuning needed.
 *
 * Checkpoint creation is not needed for any data integrity
 * requirements.  It is not even needed at unmount, but is
 * done at that point anyway to minimise startup time.
 *
 * Taking a checkpoint involves:
 * - stopping all cleaning and defrag activities
 * - synchronising with various threads that might want to get
 *   a number of changes into the same phase
 * - synchronising with writecluster creation on the main writecluster
 *   sequence
 * - flipping the 'phase' bit.
 * - Arranging that the next writecluster will start the checkpoint
 * - flush out all blocks in the old phase, starting at the leaves, and
 *   ultimately writing the root.
 * - writing out any changed blocks in the segment usage files and quota
 *   files.
 * - marking the end of the checkpoint.
 *
 * The synchronisation and bit flipping is from anywhere that decides
 * a checkpoint is needed.  The flush and finish-up is handled by
 * a thread that is created for the purpose.
 */

#include "lafs.h"
#include <linux/kthread.h>

#ifdef DUMP
static char *strflags(struct block *b)
{
	static char ans[200];
	ans[0] = 0;
	if (test_bit(B_Index, &b->flags))
		sprintf(ans, "Index(%d),", iblk(b)->depth);
	if (test_bit(B_Pinned, &b->flags)) {
		strcat(ans, "Pinned,");
		if (test_bit(B_Phase1, &b->flags))
			strcat(ans, "Phase1,");
		else
			strcat(ans, "Phase0,");
	}
	if (test_bit(B_PinPending, &b->flags))
		strcat(ans, "PinPending,");
	if (test_bit(B_InoIdx, &b->flags))
		strcat(ans, "InoIdx,");
	if (test_bit(B_Valid, &b->flags))
		strcat(ans, "Valid,");
	if (test_bit(B_Dirty, &b->flags))
		strcat(ans, "Dirty,");
	if (test_bit(B_Writeback, &b->flags))
		strcat(ans, "Writeback,");
	if (test_bit(B_Linked, &b->flags))
		strcat(ans, "Linked,");
	if (test_bit(B_Realloc, &b->flags))
		strcat(ans, "Realloc,");
	if (test_bit(B_Async, &b->flags))
		strcat(ans, "Async,");
	if (test_bit(B_Root, &b->flags))
		strcat(ans, "Root,");
	if (test_bit(B_SegRef, &b->flags))
		strcat(ans, "SegRef,");
	if (test_bit(B_Credit, &b->flags))
		strcat(ans, "C,");
	if (test_bit(B_ICredit, &b->flags))
		strcat(ans, "CI,");
	if (test_bit(B_NCredit, &b->flags))
		strcat(ans, "CN,");
	if (test_bit(B_NICredit, &b->flags))
		strcat(ans, "CNI,");
	if (test_bit(B_UnincCredit, &b->flags))
		strcat(ans, "UninCredit,");
	if (test_bit(B_IOLock, &b->flags))
		strcat(ans, "IOLock,");
	if (test_bit(B_IOLockLock, &b->flags))
		strcat(ans, "IOLockLock,");
	if (test_bit(B_HaveLock, &b->flags))
		strcat(ans, "HaveLock,");
	if (test_bit(B_HaveWriteback, &b->flags))
		strcat(ans, "HaveWriteback,");
	if (test_bit(B_WriteError, &b->flags))
		strcat(ans, "WriteError,");
	if (test_bit(B_Claimed, &b->flags))
		strcat(ans, "Claimed,");
	if (test_bit(B_OnFree, &b->flags))
		strcat(ans, "OnFree,");
	if (test_bit(B_PhysValid, &b->flags))
		strcat(ans, "PhysValid,");
	if (test_bit(B_PrimaryRef, &b->flags))
		strcat(ans, "PrimaryRef,");
	if (test_bit(B_EmptyIndex, &b->flags))
		strcat(ans, "EmptyIndex,");

	if (test_bit(B_Uninc, &b->flags))
		strcat(ans, "Uninc,");
	if (test_bit(B_Orphan, &b->flags))
		sprintf(ans+strlen(ans), "Orphan(%d),", dblk(b)->orphan_slot);
	if (test_bit(B_Prealloc, &b->flags))
		strcat(ans, "Prealloc,");
	if (ans[0])
		ans[strlen(ans)-1] = 0;
	else
		strcpy(ans, "-nil-");
	return ans;
}

char *strblk(struct block *b)
{
	static char ans[400];
	unsigned long ino = 0;

	if (!b)
		return "(NULL block)";
	if (b->inode)
		ino = b->inode->i_ino;
	if (test_bit(B_PhysValid, &b->flags))
		sprintf(ans, "[%p]%lu/%lu(%llu)r%d%c:%s",
			b, ino, b->fileaddr,
			b->physaddr, atomic_read(&b->refcnt),
			list_empty(&b->lru) ? 'E' : 'F',
			strflags(b));
	else
		sprintf(ans, "[%p]%lu/%lu(NoPhysAddr)r%d%c:%s",
			b, ino, b->fileaddr,
			atomic_read(&b->refcnt),
			list_empty(&b->lru) ? 'E' : 'F',
			strflags(b));
	if (test_bit(B_Pinned, &b->flags) &&
	    test_bit(B_Index, &b->flags))
		sprintf(ans+strlen(ans), "{%d,%d}",
			atomic_read(&iblk(b)->pincnt[0]),
			atomic_read(&iblk(b)->pincnt[1]));
	if (test_bit(B_Index, &b->flags)) {
		sprintf(ans+strlen(ans), "[%d%s%s]",
			iblk(b)->uninc_table.pending_cnt,
			iblk(b)->uninc ? "*" : "",
			iblk(b)->uninc_next ? "+" : "");
	}
#ifdef DEBUG_IOLOCK
	if (test_bit(B_IOLock, &b->flags)) {
		sprintf(ans+strlen(ans), "<%s:%d>",
			strrchr(b->iolock_file, '/'),
			b->iolock_line);
	}
#endif
	if (!b->parent)
		sprintf(ans+strlen(ans), " NP");
#if DEBUG_REF
	{
		int i;
		for (i = 0; i < 16; i++)
			if (b->holders[i].cnt)
				sprintf(ans+strlen(ans),
					" %s(%d)", b->holders[i].name,
					b->holders[i].cnt);
	}
#endif
	return ans;
}

int lafs_print_tree(struct block *b, int depth)
{
	struct indexblock *ib = iblk(b);
	int i;
	int j;
	struct block *b2;
	int credits = 0;
	struct inode *ino = NULL;

	if (depth > 20)	{
		printk("... aborting at %d\n", depth);
		BUG();
		return 0;
	}

	printk("%*s", depth, "");
	printk("%s", strblk(b));
	if (!b) {
		printk("\n");
		return 0;
	}

	for (i = 0; i < 2; i++) {
		j = 0;
		list_for_each_entry(b2, &dfs->phase_leafs[i], lru) {
			if (b2 == b) {
				printk(" Leaf%d(%d) ", i, j);
				break;
			}
			j++;
		}
	}
	j = 0;
	list_for_each_entry(b2, &dfs->clean_leafs, lru) {
		if (b2 == b) {
			printk(" Clean(%d) ", j);
			break;
		}
		j++;
	}
	list_for_each_entry(b2, &dfs->account_leafs, lru) {
		if (b2 == b) {
			printk(" Account(%d) ", j);
			break;
		}
		j++;
	}
	list_for_each_entry(b2, &freelist.lru, lru)
		if (b2 == b) {
			printk(" on free ");
			break;
		}
	for (i = 0 ; i < 2; i++) {
		int j, k;
		j = 0;
		list_for_each_entry(b2, &dfs->wc[i].clhead, lru) {
			if (b2 == b)
				printk(" wc[%d][%d] ", i, j);
			j++;
		}
		for (k = 0; k < 4; k++) {
			j = 0;
			list_for_each_entry(b2, &dfs->wc[i].pending_blocks[k], lru) {
				if (b2 == b)
					printk(" pending[%d][%d][%d] ", i, k, j);
				j++;
			}
		}
	}

	printk("\n");
	if (test_bit(B_Index, &b->flags) && ib->uninc_table.pending_cnt) {
		lafs_print_uninc(&ib->uninc_table);
		credits += ib->uninc_table.credits;
	}

	if (test_bit(B_Credit, &b->flags))
		credits++;
	if (test_bit(B_ICredit, &b->flags))
		credits++;
	if (test_bit(B_NCredit, &b->flags))
		credits++;
	if (test_bit(B_NICredit, &b->flags))
		credits++;
	if (test_bit(B_UnincCredit, &b->flags))
		credits++;
	if (test_bit(B_Dirty, &b->flags))
		credits++;
	if (test_bit(B_Realloc, &b->flags))
		credits++;

	if (test_bit(B_Index, &b->flags)) {
		list_for_each_entry(b, &ib->children, siblings) {
			LAFS_BUG(b->parent != ib, b);
			credits += lafs_print_tree(b, depth+2);
		}
	} else if ((ino = rcu_my_inode(dblk(b))) != NULL &&
		   LAFSI(ino)->iblock &&
		   LAFSI(ino)->iblock->b.parent
		) {
		LAFS_BUG(LAFSI(ino)->iblock->b.parent != b->parent,
			 b);
		credits += lafs_print_tree(&LAFSI(ino)->iblock->b,
					   depth+1);
	}
	rcu_iput(ino);
	return credits;
}

void lafs_dump_tree(void)
{
	int credits;
	printk("=================== Index Dump ===================\n");
	printk("inum dpth (refcnt) addr [phys] {[lk cnt]} flags\n");
	credits = lafs_print_tree(&LAFSI(dfs->ss[0].root)->dblock->b, 0);
	credits += lafs_print_tree(&LAFSI(dfs->ss[0].root)->iblock->b, 0);
	printk("====================%d/%d-%d credits ==============================\n",
	       credits, (int)dfs->allocated_blocks, dfs->cluster_updates);
}
#endif

static int prepare_checkpoint(struct fs *fs)
{
	int oldphase;
	int youth;

	spin_lock(&fs->lock);
	wait_event_lock(fs->phase_wait,
			fs->phase_locked == 0,
			fs->lock);

	oldphase = fs->phase;
	fs->phase = !oldphase;
	fs->checkpointing = CH_CheckpointStart | CH_Checkpoint;
	clear_bit(CheckpointNeeded, &fs->fsstate);
	spin_unlock(&fs->lock);

	youth = fs->youth_next - 1;
	return youth;
}

/* Get a block that should be written out.
 * If phase is '-1', then only return cleanable blocks, else
 * also return blocks for that phase.
 * The returned block is locked for IO.
 */
struct block *lafs_get_flushable(struct fs *fs, int phase)
{
	struct block *b = NULL;

retry:
	spin_lock(&fs->lock);
	if (!list_empty(&fs->clean_leafs))
		b = list_entry(fs->clean_leafs.next, struct block, lru);
	else if (phase >= 0 && !list_empty(&fs->phase_leafs[phase]))
		b = list_entry(fs->phase_leafs[phase].next, struct block, lru);
	else {
		spin_unlock(&fs->lock);
		return NULL;
	}

	getref_locked_needsync(b, MKREF(flushable));
	list_del_init(&b->lru);
	spin_unlock(&fs->lock);

	sync_ref(b);

	/* Need an iolock, but if the block is in writeback,
	 * or unpinned or otherwise not a leaf, we don't want it.
	 */
	lafs_iolock_block(b);

	if (!lafs_is_leaf(b, phase)) {
		lafs_iounlock_block(b);
		putref(b, MKREF(flushable));
		goto retry;
	}

	return b;
}

static void do_checkpoint(void *data)
{
	struct fs *fs = data;
	int oldphase = !fs->phase;
	struct block *b;
	int loops = 0;
#ifdef DUMP
	dfs = fs;
#endif

	dprintk("Start Checkpoint\n");
	if (lafs_trace)
		lafs_dump_tree();
again:
	while ((b = lafs_get_flushable(fs, oldphase)) != NULL) {
		int unlock = 1;
		dprintk("Checkpoint Block %s\n", strblk(b));

		if (test_and_clear_bit(B_Async, &b->flags)) {
			/* this shouldn't normally happen, but
			 * it is possible, so check anyway
			 */
			putref(b, MKREF(async));
			lafs_wake_thread(fs);
		}

		if (!!test_bit(B_Phase1, &b->flags) != oldphase)
			/* lafs_pin_dblock flipped phase for us */;
		else if (test_bit(B_PinPending, &b->flags) &&
		    !test_bit(B_Index, &b->flags) &&
		    (LAFSI(b->inode)->type == TypeSegmentMap ||
		     LAFSI(b->inode)->type == TypeQuota)) {
			/* Need to delay handling of this block until
			 * the phase change has finished, to just
			 * before we finish the checkpoint.
			 * Note that we don't check if they are dirty -
			 * they might not be yet - the whole point of the
			 * delay is that changes are still arriving.
			 */
			lafs_flip_dblock(dblk(b));
			/* access to account_leafs is single-threaded
			 * by the cleaner thread so no locking needed
			 */
			getref(b, MKREF(accounting));
			list_add(&b->lru, &fs->account_leafs);
		} else if (test_bit(B_Index, &b->flags) &&
		    (iblk(b)->uninc_table.pending_cnt ||
		     iblk(b)->uninc)) {
			lafs_incorporate(fs, iblk(b));
		} else if ((test_bit(B_Dirty, &b->flags) ||
			    test_bit(B_Realloc, &b->flags))) {
			lafs_cluster_allocate(b, 0);
			unlock = 0;
		} else if (test_bit(B_Index, &b->flags)) {
			if (atomic_read(&iblk(b)->pincnt[oldphase])
			    == 0) {
				lafs_phase_flip(fs, iblk(b));
				unlock = 0;
			}
		} else {
			/* Data block is still PinPending at
			 * checkpoint so must be a directory block.
			 * We want to keep PinPending until the operation
			 * completes, but need to unpin.
			 * So we drop/refile/retake.
			 * PinPending is mainly needed to keep
			 * writepage at bay and it will take iolock,
			 * so dropping PinPending while holding
			 * iolock is safe.
			 */
			LAFS_BUG(!test_bit(B_PinPending, &b->flags), b);
			clear_bit(B_PinPending, &b->flags);
			lafs_refile(b, 0);
			set_bit(B_PinPending, &b->flags);
		}

		if (unlock)
			lafs_iounlock_block(b);

		putref(b, MKREF(flushable));
	}
	if ((test_bit(B_Pinned, &LAFSI(fs->ss[0].root)->iblock->b.flags) &&
	     !!test_bit(B_Phase1, &LAFSI(fs->ss[0].root)->iblock->b.flags)
	     != fs->phase)
	    ||
	    (test_bit(B_Pinned, &LAFSI(fs->ss[0].root)->dblock->b.flags) &&
	     !!test_bit(B_Phase1, &LAFSI(fs->ss[0].root)->dblock->b.flags)
	     != fs->phase)) {
		if (loops == 20) {
			printk("Cannot escape PHASE=%d\n", oldphase);
			lafs_print_tree(&LAFSI(fs->ss[0].root)->dblock->b, 0);
			lafs_print_tree(&LAFSI(fs->ss[0].root)->iblock->b, 0);
			lafs_trace = 1;
		}

		lafs_cluster_flush(fs, 0);
		lafs_cluster_wait_all(fs);
		lafs_clusters_done(fs);
		if (loops == 20) {
			printk("After cluster_flush...\n");
			lafs_print_tree(&LAFSI(fs->ss[0].root)->dblock->b, 0);
			lafs_print_tree(&LAFSI(fs->ss[0].root)->iblock->b, 0);
			BUG();
		}
		loops++;
		goto again;
	}
	lafs_clusters_done(fs);
}

static void flush_accounting(struct fs *fs)
{
	while (!list_empty(&fs->account_leafs)) {
		struct block *b = list_first_entry(&fs->account_leafs,
						   struct block,
						   lru);
		list_del_init(&b->lru);
		lafs_iolock_block(b);
		lafs_cluster_allocate(b, 0);
		putref(b, MKREF(accounting));
	}
	fs->qphase = fs->phase;
}

static void finish_checkpoint(struct fs *fs, int youth)
{
	/* Don't want to change the youth block while writing them out */
	set_bit(DelayYouth, &fs->fsstate);

	flush_accounting(fs);

	/* if we are creating a snapshot, special handling is needed */
	if (LAFSI(fs->ss[0].root)->md.fs.usagetable > 1) {
		/* duplicate the usage table */
		if (lafs_seg_dup(fs, LAFSI(fs->ss[0].root)->md.fs.usagetable)) {
			/* FIXME need to abort the snapshot somehow */
		}
	}

	fs->checkpointing |= CH_CheckpointEnd;
	dprintk("FinalFlush %d\n", fs->seq);
	lafs_cluster_flush(fs, 0);

	clear_bit(DelayYouth, &fs->fsstate);

	if (!test_bit(FinalCheckpoint, &fs->fsstate))
		lafs_seg_apply_all(fs);
	lafs_clean_free(fs);
	if (test_bit(EmergencyPending, &fs->fsstate)) {
		set_bit(EmergencyClean, &fs->fsstate);
		clear_bit(EmergencyPending, &fs->fsstate);
	}

	lafs_write_state(fs);
	dprintk("State written, all done %d\n", fs->seq);

	if (!test_bit(CleanerDisabled, &fs->fsstate)) {
		fs->scan.done = 0;
		if (youth > 65536 - 2 * fs->max_newsegs) {
			/* time to decay youth */
			spin_lock(&fs->lock);
			youth = decay_youth(youth);
			fs->youth_next = decay_youth(fs->youth_next);
			fs->scan.do_decay = 1;
			spin_unlock(&fs->lock);
		}
	}
	fs->cleaner.cleaning = 0;

	fs->checkpoint_youth = youth;
	fs->newsegments = 0;
	lafs_wake_thread(fs);
}

unsigned long lafs_do_checkpoint(struct fs *fs)
{
	int y;
	if (!test_bit(CheckpointNeeded, &fs->fsstate) &&
	    !test_bit(FinalCheckpoint, &fs->fsstate)) {
		/* not compulsory, but maybe just time */
		if (fs->cleaner.active == 0 &&
		    fs->newsegments > fs->max_newsegs)
			set_bit(CheckpointNeeded, &fs->fsstate);
		else if (test_bit(CheckpointOpen, &fs->fsstate)) {
			unsigned long j = jiffies;
			if (time_after_eq(j, fs->next_checkpoint))
				set_bit(CheckpointNeeded, &fs->fsstate);
			else
				return fs->next_checkpoint - j;
		} else
			return MAX_SCHEDULE_TIMEOUT;
	}

	if (!test_bit(CheckpointNeeded, &fs->fsstate))
		/* Must have done final checkpoint */
		return MAX_SCHEDULE_TIMEOUT;

	if (fs->cleaner.active || ! fs->scan.done)
		return MAX_SCHEDULE_TIMEOUT;

	/* Make sure all cleaner blocks are flushed as if anything lingers
	 * in the cleaner cluster, they will stop the checkpoint from making
	 * progress.
	 */
	lafs_cluster_flush(fs, 1);

	/* OK, time for some work. */
	dprintk("############################ start checkpoint\n");
	y = prepare_checkpoint(fs);

	do_checkpoint(fs);

	finish_checkpoint(fs, y);
	dprintk("############################ finish checkpoint\n");

	return MAX_SCHEDULE_TIMEOUT;
}

unsigned long long lafs_checkpoint_start(struct fs *fs)
{
	unsigned long long cp = fs->wc[0].cluster_seq;
	WARN_ON(test_bit(FinalCheckpoint, &fs->fsstate));
	set_bit(CheckpointNeeded, &fs->fsstate);
	/* Whenever we do a checkpoint, get anyone waiting on
	 * space to check again */
	clear_bit(CleanerBlocks, &fs->fsstate);
	fs->prime_sb->s_dirt = 0;
	lafs_wake_thread(fs);
	return cp;
}

void lafs_checkpoint_lock(struct fs *fs)
{
	spin_lock(&fs->lock);
	fs->phase_locked++;
	spin_unlock(&fs->lock);
}

void lafs_checkpoint_unlock(struct fs *fs)
{
	int l;
	spin_lock(&fs->lock);
	l = --fs->phase_locked;
	spin_unlock(&fs->lock);
	if (l == 0)
		wake_up(&fs->phase_wait);
}

void lafs_checkpoint_unlock_wait(struct fs *fs)
{
	/* FIXME it is a bug if we haven't done something to
	 * make a checkpoint happen, else we could wait forever.
	 * As we still hold the lock prepare_checkpoint cannot
	 * have progressed, so CheckpointNeeded must be set.
	 */
	BUG_ON(!test_bit(CheckpointNeeded, &fs->fsstate) &&
	       !test_bit(CleanerBlocks, &fs->fsstate));
	lafs_checkpoint_unlock(fs);

	wait_event(fs->phase_wait,
		   !test_bit(CleanerBlocks, &fs->fsstate) &&
		   !test_bit(CheckpointNeeded, &fs->fsstate) &&
		   fs->checkpointing == 0);
}

void lafs_checkpoint_wait(struct fs *fs)
{
	/* Wait until there is no checkpoint requested or happening.
	 * This is used to implement filesystem-wide sync
	 */
	wait_event(fs->phase_wait,
		   !test_bit(CheckpointNeeded, &fs->fsstate) &&
		   fs->checkpointing == 0);
}
