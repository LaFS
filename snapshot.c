
/*
 * fs/lafs/super.c
 * Copyright (C) 2005-2009
 * Neil Brown <neilb@suse.de>
 * Released under the GPL, version 2
 */

#include	"lafs.h"
#include	<linux/namei.h>
#include	<linux/crc32.h>
#include	<linux/slab.h>

/*
 * Mounting a snapshot is very different from mounting a new
 * filesystem.
 * The 'dev' passed is really a path to the mountpoint of the
 * original - or atleast a file within the original.
 * We find that filesystem, make sure it is a LaFS, find the
 * named snapshot, and mount it read_only
 */

struct options {
	char  *snapshot;
};

static int parse_opts(struct options *op, const char *dev_name, char *data)
{
	/* dev_name must start with '/',
	 * data must have snapshot=....
	 */
	char *p;

	if (dev_name[0] != '/')
		return -EINVAL;
	op->snapshot = NULL;
	while ((p = strsep(&data, ",")) != NULL) {
		if (!*p)
			continue;
		if (strncmp(p, "snapshot=", 9) == 0) {
			op->snapshot = p+9;
		} else {
			printk(KERN_ERR
			       "LaFS: Unrecognised mount option \"%s\"\n", p);
			return -EINVAL;
		}
	}
	if (op->snapshot == NULL || op->snapshot[0] == 0) {
		printk(KERN_ERR
		       "LaFS: No snaphot name given.\n");
		return -EINVAL;
	}
	return 0;
}

/* s_fs_info for snapshots contains the snapshot number as well */
struct snap_key {
	struct sb_key k;
	int ssnum;
};

static int snap_test(struct super_block *sb, void *data)
{
	struct snap_key *ptn = data;
	struct snap_key *sk = container_of(sb->s_fs_info, struct snap_key, k);
	return sk->k.fs == ptn->k.fs && sk->ssnum == ptn->ssnum;
}

static int snap_set(struct super_block *sb, void *data)
{
	struct snap_key *sk = data;
	sb->s_fs_info = &sk->k;
	return set_anon_super(sb, NULL);
}

static int
lafs_snap_get_sb(struct file_system_type *fstype,
		 int flags, const char *dev_name, void *data,
		 struct vfsmount *mnt)
{
	/* The 'dev_name' is a path to an object in the
	 * parent filesystem (typically the mountpoint).
	 * "data" contains 'snapshot=name' giving the name
	 * of the snapshot to be found and mounted
	 */
	char *cdata = data;
	struct nameidata nd;
	int err;
	struct super_block *sb;
	struct fs *fs;
	int s;
	struct la_inode *lai;
	struct page *p;
	struct options op;
	struct snap_key *sk;

	err = parse_opts(&op, dev_name, cdata);
	if (err)
		return err;

	p = alloc_page(GFP_KERNEL);
	if (!p)
		return -ENOMEM;

	err = path_lookup(dev_name, LOOKUP_FOLLOW, &nd);
	if (err) {
		put_page(p);
		return err;
	}
	sb = nd.path.dentry->d_sb;
	err = -EINVAL;
	if (sb->s_type != &lafs_fs_type)
		goto fail;
	fs = fs_from_sb(sb);

	for (s = 1; s < fs->maxsnapshot; s++) {
		int nlen;

		if (fs->ss[s].root_addr == 0)
			continue;
		if (fs->ss[s].root) {
			struct lafs_inode *li = LAFSI(fs->ss[s].root);
			if (li->md.fs.name &&
			    strcmp(li->md.fs.name, op.snapshot) == 0)
				/* found it */
				break;
		}
		err = lafs_load_page(fs, p, fs->ss[s].root_addr, 1);
		if (err)
			continue;
		lai = (struct la_inode *)page_address(p);
		nlen = le16_to_cpu(lai->metadata_size) -
			offsetof(struct la_inode, metadata[0].fs.name);
		printk("ss %d is %.*s\n", s, nlen, lai->metadata[0].fs.name);
		if (strncmp(lai->metadata[0].fs.name, op.snapshot, nlen) != 0)
			continue;
		/* FIXME more checks? */
		/* Found it */
		break;
	}
	if (s == fs->maxsnapshot) {
		err = -ENOENT;
		goto fail;
	}
	/* Ok, we have the right snapshot... now we need a superblock */
	sk = kzalloc(sizeof(*sk), GFP_KERNEL);
	if (!sk) {
		err = -ENOMEM;
		goto fail;
	}
	sk->k.fs = fs;
	sk->ssnum = s;
	
	sb = sget(&lafs_snap_fs_type, snap_test, snap_set, NULL);
	if (IS_ERR(sb)) {
		kfree(sk);
		err = PTR_ERR(sb);
		goto fail;
	}
	if (sb->s_root) {
		/* already existed */
		kfree(sk);
	} else {
		struct inode *rootino, *rootdir;
		struct datablock *b;
		sb->s_flags = flags | MS_RDONLY;

		atomic_inc(&fs->prime_sb->s_active);

		rootino = iget_locked(sb, 0);
		fs->ss[s].root = sk->k.root = rootino;
		LAFSI(rootino)->filesys = rootino;
		b = lafs_get_block(rootino, 0, NULL, GFP_KERNEL,
				   MKREF(snap));
		b->b.physaddr = fs->ss[s].root_addr;
		set_bit(B_PhysValid, &b->b.flags);
		LAFS_BUG(test_bit(B_Valid, &b->b.flags), &b->b);
		printk("ss root at %llu\n", b->b.physaddr);
		err = lafs_load_block(&b->b, NULL);
		if (!err)
			err = lafs_wait_block(&b->b);
		if (!err)
			err = lafs_import_inode(fs->ss[s].root, b);
		putdref(b, MKREF(snap));
		unlock_new_inode(rootino);
		if (err) {
			deactivate_locked_super(sb);
			goto fail;
		}
		rootdir = lafs_iget(rootino, 2, SYNC);
		sb->s_root = d_alloc_root(rootdir);
		sb->s_op = fs->prime_sb->s_op;
		sb->s_flags |= MS_ACTIVE;
	}
	up_write(&sb->s_umount);
	path_put(&nd.path);
	put_page(p);
	simple_set_mnt(mnt, sb);
	return 0;

fail:
	put_page(p);
	path_put(&nd.path);
	return err;
}

static void lafs_snap_kill_sb(struct super_block *sb)
{
	struct fs *fs = fs_from_sb(sb);

	printk("Generic_shutdown_super being called....\n");
	kill_anon_super(sb);
	kfree(sb->s_fs_info);
	printk("Generic_shutdown_super called\n");
	deactivate_super(fs->prime_sb);
}

struct file_system_type lafs_snap_fs_type = {
	.owner		= THIS_MODULE,
	.name		= "lafs_snap",
	.get_sb		= lafs_snap_get_sb,
	.kill_sb	= lafs_snap_kill_sb,
};

