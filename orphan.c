
/*
 * fs/lafs/inode.c
 * Copyright (C) 2006-2009
 * Neil Brown <neilb@suse.de>
 * Released under the GPL, version 2
 *
 * Orphan file management
 *
 * Inode 8 in the primary fileset is the orphan file.
 * It records blocks in files that might need special care
 * after a restart.  The particular 'special care' depends on
 * the block and the type of file.  Some example are:
 * InodeFile
 *   The inode may have a linkcount of 0 and if so, it should
 *   be truncated and removed
 *   The inode might have blocks beyond EOF in which case they
 *   should be truncated.
 * Directory
 *   An internal chain from this block might be being released...
 *   see dir.c for further details.
 *
 * Each entry in the orphan file is 16 bytes holding 4 u32s
 * in little_endian.
 * These are fsnumber, inodenumber, blocknumber, data
 * The data can be set by whoever creates the orphan.
 *
 * The orphan file is dense - no holes.  When an entry
 * is freed the last entry is moved down.
 *
 * When a block is in the orphan file, it has it's 'orphan_slot'
 * set to record where the orphan entry is.
 *
 * When updating a block of the orphan file we must first make sure
 * that it isn't dirty in the previous phase.  If it is, we must wait
 * for it to be written out.  It is permitted to wait for this
 * while holding the checkpoint lock.
 */

/*
 * Orphan entries are always created as part of a commit transaction
 * so we need the prepare/pin/commit/abort functions
 */

#include "lafs.h"

#ifdef DUMP
void lafs_dump_orphans(void)
{
	struct orphan_md *om;
	int slot;

	if (!dfs || !dfs->orphans) {
		printk("No orphan FS !!\n");
		return;
	}

	mutex_lock_nested(&dfs->orphans->i_mutex, I_MUTEX_QUOTA);

	om = &LAFSI(dfs->orphans)->md.orphan;
	printk("nextfree = %u\n", (unsigned)om->nextfree);
	printk("reserved = %u\n", (unsigned)om->reserved);

	for (slot = 0; slot < om->nextfree; slot++) {
		struct orphan *or;
		struct datablock *ob;
		u32 bnum;
		int ent;

		bnum = slot >> (dfs->blocksize_bits-4);
		ob = lafs_get_block(dfs->orphans, bnum, NULL, GFP_KERNEL,
				    MKREF(dumpo));
		or = map_dblock(ob);
		ent = slot - (bnum << (dfs->blocksize_bits-4));
		printk("%3d: %d %u %u %u\n", slot,
		       or[ent].type, or[ent].filesys, or[ent].inum,
		       or[ent].addr);
		unmap_dblock(ob, or);
		putdref(ob, MKREF(dumpo));
	}

	mutex_unlock(&dfs->orphans->i_mutex);
}
#endif

/*
 * Guide to MKREF reference names:
 * "orphan_reserve".  This is a block in the orphan file, and someone
 *         owns a reservation which would be commited by updating this block.
 *         There are "om->reserved" of these, typically all on the one block.
 * "orphan"   This is a block in the orphan file which holds an orphan
 *         record for some committed orphan.  ->orphan_slot points here.
 * "orphan_flag" This is a block (not in orphan file) which is an orphan, and
 *         has the B_Orphan flag set.
 * "orphanx", "orphan_release", "orphan_release2", "orphan_move"
 * "orphan_fs", "orphan_ino", "orphan_blk"   Temporary references
 */

/* Before locking the checkpoint, we need to reserve space to
 * store the orphan record, and make sure we hold a reference
 * to the block.
 */
static int orphan_prepare(struct fs *fs, int async)
{
	struct orphan_md *om = &LAFSI(fs->orphans)->md.orphan;
	u32 bnum;
	int err = 0;
	struct datablock *b;

#ifdef DUMP
	dfs = fs;
#endif
	mutex_lock_nested(&fs->orphans->i_mutex, I_MUTEX_QUOTA);
	bnum = (om->nextfree + om->reserved) >>
		(fs->blocksize_bits-4);
	b = lafs_get_block(fs->orphans, bnum, NULL, GFP_KERNEL,
			   MKREF(orphan_reserve));
	om->reserved++;
	mutex_unlock(&fs->orphans->i_mutex);
	if (b) {
		if (async)
			err = lafs_read_block_async(b);
		else
			err = lafs_read_block(b);
		if (err)
			putdref(b, MKREF(orphan_reserve));
	} else
		err = -ENOMEM;
	if (err) {
		mutex_lock_nested(&fs->orphans->i_mutex, I_MUTEX_QUOTA);
		om->reserved--;
		mutex_unlock(&fs->orphans->i_mutex);
	}
	lafs_iolock_written(&b->b);
	set_bit(B_PinPending, &b->b.flags);
	lafs_iounlock_block(&b->b);
	return err;
}

static struct datablock *orphan_pin(struct fs *fs, struct datablock *b)
{
	struct orphan_md *om = &LAFSI(fs->orphans)->md.orphan;
	u32 slot;
	int err;
	u32 bnum;
	struct datablock *ob;

	slot = om->nextfree;
	bnum = slot >> (fs->blocksize_bits-4);
	dprintk("slot=%d bnum=%d\n", slot, bnum);
	ob = lafs_get_block(fs->orphans, bnum, NULL, GFP_KERNEL, MKREF(orphan));
	/* that *must* succeed as we own a reference on the block already */
	LAFS_BUG(ob == NULL, &b->b);

	err = lafs_pin_dblock(ob, ReleaseSpace);
	if (err) {
		putdref(ob, MKREF(orphan));
		return ERR_PTR(err);
	}
	return ob;
}

static void orphan_commit(struct fs *fs, struct datablock *b, struct datablock *ob)
{
	struct orphan_md *om = &LAFSI(fs->orphans)->md.orphan;
	struct orphan *or;
	int ent;

	/* Committed to being an orphan now */
	b->orphan_slot = om->nextfree++;
	set_bit(B_Orphan, &b->b.flags);
	getdref(b, MKREF(orphan_flag));
	lafs_igrab_fs(b->b.inode);
	lafs_add_orphan(fs, b);

	dprintk("%p->orphan_slot=%d (%lu,%lu,%lu) %s\n", b, b->orphan_slot,
		LAFSI(b->b.inode)->filesys->i_ino,
		b->b.inode->i_ino, b->b.fileaddr, strblk(&b->b));

	om->reserved--;
	putdref(ob, MKREF(orphan_reserve));

	or = map_dblock(ob);
	ent = b->orphan_slot - (ob->b.fileaddr
				<< (fs->blocksize_bits-4));
	or[ent].type = cpu_to_le32(1);
	or[ent].filesys = cpu_to_le32(LAFSI(b->b.inode)->filesys->i_ino);
	or[ent].inum = cpu_to_le32(b->b.inode->i_ino);
	or[ent].addr = cpu_to_le32(b->b.fileaddr);
	unmap_dblock(ob, or);
	lafs_dirty_dblock(ob);
}

/* We failed to allocate space to write the update to the orphan
 * block so we need to revert the reservation done in orphan_pin.
 * Note that we don't 'unmark' the block from being an orphan.
 * We let regular orphan processing find that out and release it.
 * This avoids races.
 */
static void orphan_abort(struct fs *fs)
{
	u32 bnum;
	struct datablock *b;
	struct orphan_md *om = &LAFSI(fs->orphans)->md.orphan;

	om->reserved--;
	bnum = (om->nextfree + om->reserved) >>
		(fs->blocksize_bits-4);
	b = lafs_get_block(fs->orphans, bnum, NULL, GFP_KERNEL,
			   MKREF(orphanx));
	/* Note that we now own two references to this block, one
	 * we just got and one we are trying to get rid of
	 */
	putdref(b, MKREF(orphanx));

	/* If this was the last block in the file,
	 * we need to punch a hole.
	 * i.e. if the first unneeded slot is at or before
	 * the first slot in this block, we don't want
	 * this block.
	 */
	if ((om->nextfree + om->reserved) <=
	    (bnum << (fs->blocksize_bits-4))
		) {
		LAFS_BUG(!test_bit(B_PinPending, &b->b.flags), &b->b);
		lafs_erase_dblock(b);
	}

	putdref(b, MKREF(orphan_reserve));
}

/* When we are about to make a change which might make a block
 * into an 'orphan', we call lafs_make_orphan to record the fact.
 * This sets B_Orphan, put the block on the fs->pending_orphans list
 * and stores the fs/inode/block number in the orphan file.
 * The handle_orphans routine for the relevant file type must ensure
 * that orphan handling doesn't happen until the block is genuinely
 * an orphan.
 * For directories, i_mutex is used for this.
 * For inodes, B_IOLock is used.
 */
int lafs_make_orphan(struct fs *fs, struct datablock *db)
{
	int err;
	struct datablock *ob;

	if (test_bit(B_Orphan, &db->b.flags))
		return 0;

	err = orphan_prepare(fs, 0);
	if (err)
		return err;
retry:
	lafs_checkpoint_lock(fs);
	mutex_lock_nested(&fs->orphans->i_mutex, I_MUTEX_QUOTA);
	ob = orphan_pin(fs, db);
	if (IS_ERR(ob) && PTR_ERR(ob) == -EAGAIN) {
		mutex_unlock(&fs->orphans->i_mutex);
		lafs_checkpoint_unlock_wait(fs);
		goto retry;
	}
	if (IS_ERR(ob)) {
		orphan_abort(fs);
		err = PTR_ERR(ob);
	} else
		orphan_commit(fs, db, ob);

	mutex_unlock(&fs->orphans->i_mutex);
	lafs_checkpoint_unlock(fs);
	return err;
}

/* non-blocking version of make_orphan */
int lafs_make_orphan_nb(struct fs *fs, struct datablock *db)
{
	int err;
	struct datablock *ob;

	if (test_bit(B_Orphan, &db->b.flags))
		return 0;

	err = orphan_prepare(fs, 1);
	if (err)
		return err;

	lafs_checkpoint_lock(fs);
	if (!mutex_trylock(&fs->orphans->i_mutex)) {
		lafs_checkpoint_unlock(fs);
		return -EAGAIN;
	}
	ob = orphan_pin(fs, db);
	if (IS_ERR(ob)) {
		orphan_abort(fs);
		err = PTR_ERR(ob);
	} else
		orphan_commit(fs, db, ob);

	mutex_unlock(&fs->orphans->i_mutex);
	lafs_checkpoint_unlock(fs);
	return err;
}

static void lafs_drop_orphan(struct fs *fs, struct datablock *db);
/*
 * When any processing of an orphan makes it not an orphan any more
 * (e.g. link is created for a file, directory block is cleaned)
 * we need to delete the entry from the orphan file.
 * If this isn't the last entry in the file we swap the last entry
 * in thus keeping the file dense.
 * We don't have the blocks in the orphan file reserved.  If we
 * cannot get a reservation we fail to release the orphan status
 * so we can try again later.
 * All IO requests here must be async as we run from the cleaner
 * thread.
 * We can block in orphan->i_mutex or even in erase_dblock in
 * orphan_abort, but i_mutex is only held for very short periods
 * like a spinlock, and erase_dblock should not block as the block
 * should be PinPending;
 */
void lafs_orphan_release(struct fs *fs, struct datablock *b)
{
	struct datablock *ob1, *ob2;
	int shift = b->b.inode->i_blkbits - 4;
	struct orphan_md *om = &LAFSI(fs->orphans)->md.orphan;
	struct orphan *or, *lastor, last;
	int ent, lastent;

	if (!test_bit(B_Orphan, &b->b.flags))
		return;

	ob1 = lafs_get_block(fs->orphans, b->orphan_slot >> shift, NULL,
			     GFP_KERNEL, MKREF(orphan_release));
	/* We already own an 'orphan' reference on this block, so we
	 * don't need this one
	 */
	LAFS_BUG(ob1 == NULL, &b->b);
	putdref(ob1, MKREF(orphan_release));

	BUG_ON(!test_bit(B_PinPending, &ob1->b.flags));

	lafs_checkpoint_lock(fs);

	if (lafs_pin_dblock(ob1, ReleaseSpace) < 0)
		goto out;

	/* Note that orphan_slot can change spontaneously unless
	 * orphans->i_mutex is held
	 */
	mutex_lock_nested(&fs->orphans->i_mutex, I_MUTEX_QUOTA);
	ent = b->orphan_slot & ((1<<shift)-1);

	dprintk("os=%d sh=%d ent=%d nf=%d\n", b->orphan_slot, shift,
		ent, om->nextfree);
again:
	if (b->orphan_slot != om->nextfree-1) {
		/* need to swap in the last entry */
		struct inode *bino;
		struct datablock *bbl;

		/* All blocks in the orphan file a referenced either by
		 * an orphan or by a reservation, so this must succeed.
		 */
		ob2 = lafs_get_block(fs->orphans, (om->nextfree-1) >> shift,
				     NULL, GFP_KERNEL, MKREF(orphan_move));
		LAFS_BUG(ob2 == NULL, &b->b);

		LAFS_BUG(!test_bit(B_Valid, &ob2->b.flags), &ob2->b);
		LAFS_BUG(!test_bit(B_PinPending, &ob2->b.flags), &ob2->b);

		if (lafs_pin_dblock(ob2, ReleaseSpace) < 0) {
			putdref(ob2, MKREF(orphan_move));
			goto out_unlock;
		}

		lastor = map_dblock_2(ob2);
		lastent = (om->nextfree-1) & ((1<<shift)-1);
		dprintk("lastor=%p lastent=%d\n", lastor, lastent);
		last = lastor[lastent];
		lastor[lastent].type = 0;
		unmap_dblock_2(ob2, lastor);

		if (last.type == 0) {
			/* this entry was invalidated during read-in.
			 * lets just forget it
			 */
			om->nextfree--;
			om->reserved++;
			orphan_abort(fs);
			putdref(ob2, MKREF(orphan_move));
			goto again;
		}

		/* The block, being an orphan, must exist */
		bino = lafs_iget_fs(fs,
				    le32_to_cpu(last.filesys),
				    le32_to_cpu(last.inum), ASYNC);
		LAFS_BUG(!bino, &b->b);
		bbl = lafs_get_block(bino,
				     le32_to_cpu(last.addr),
				     NULL, GFP_KERNEL, MKREF(orphan_blk));
		LAFS_BUG(!bbl, &b->b);

		dprintk("Q %d %d\n", bbl->orphan_slot, om->nextfree);
		LAFS_BUG(bbl->orphan_slot != om->nextfree-1, &bbl->b);

		or = map_dblock(ob1);
		or[ent] = last;
		unmap_dblock(ob1, or);
		bbl->orphan_slot = b->orphan_slot;
		putdref(bbl, MKREF(orphan_blk));
		lafs_iput_fs(bino);

		lafs_dirty_dblock(ob1);
		lafs_dirty_dblock(ob2);

		/* The orphan reference on ob2 has moved to ob1
		 * and ob2 is now a 'reservation' reference.
		 * So we drop ob2 and extra time, but not ob1 at all.
		 */
		getdref(ob2, MKREF(orphan_reserve));
		putdref(ob2, MKREF(orphan_move));
		putdref(ob2, MKREF(orphan));
	} else {
		or = map_dblock(ob1);
		or[ent].type = 0;
		unmap_dblock(ob1, or);
		lafs_dirty_dblock(ob1);
		getdref(ob1, MKREF(orphan_reserve));
		putdref(ob1, MKREF(orphan));
	}
	om->nextfree--;
	om->reserved++;
	clear_bit(B_Orphan, &b->b.flags);
	lafs_iput_fs(b->b.inode);
	lafs_drop_orphan(fs, b);
	putdref(b, MKREF(orphan_flag));

	/* Now drop the reservation we just synthesised */
	orphan_abort(fs);
out_unlock:
	mutex_unlock(&fs->orphans->i_mutex);
out:
	lafs_checkpoint_unlock(fs);
}

static struct inode *orphan_inode(struct datablock *db)
{
	struct inode *ino, *rv = NULL;
	switch (LAFSI(db->b.inode)->type) {
	case TypeInodeFile:
		ino = rcu_my_inode(db);
		if (ino &&
		    test_bit(I_Deleting, &LAFSI(ino)->iflags)) {
			/* don't need rcu while I_Deleting is set */
			rcu_iput(ino);
			return ino;
		}
		if (ino)
			rv = igrab(ino);
		rcu_iput(ino);
		return rv;
	case TypeDir:
		return igrab(db->b.inode);
	default:
		BUG();
	}
}

static void orphan_iput(struct inode *ino)
{
	if (!ino)
		return;
	if (test_bit(I_Deleting, &LAFSI(ino)->iflags))
		return;
	iput(ino);
}

long lafs_run_orphans(struct fs *fs)
{
	struct datablock *db;
	struct list_head done;
	long timeout = MAX_SCHEDULE_TIMEOUT;

	if (list_empty_careful(&fs->pending_orphans))
		return MAX_SCHEDULE_TIMEOUT;

	INIT_LIST_HEAD(&done);
	/* Now process the orphans.  Move each one to 'done',
	 * then handle it.  Those which remain on 'done' get
	 * moved back at the end.
	 */

	set_bit(OrphansRunning, &fs->fsstate);
	spin_lock(&fs->lock);
	while (!list_empty(&fs->pending_orphans)) {
		struct inode *ino;
		db = list_entry(fs->pending_orphans.next,
				struct datablock,
				orphans);
		list_move(&db->orphans, &done);
		getdref(db, MKREF(run_orphans));
		spin_unlock(&fs->lock);
		ino = orphan_inode(db);
		if (!ino && !test_bit(B_Claimed, &db->b.flags))
			/* OK not to have an inode */;
		else if (!ino || !mutex_trylock(&ino->i_mutex)) {
			/* we cannot get a wakeup when mutex
			 * is unlocked, so we need timeout.
			 */
			orphan_iput(ino);
			timeout = msecs_to_jiffies(500);
			putdref(db, MKREF(run_orphans));
			spin_lock(&fs->lock);
			continue;
		}

		if (!ino)
			lafs_orphan_forget(fs, db);
		else {
			int err = -ERESTARTSYS;
			while (err == -ERESTARTSYS) {
				switch(LAFSI(db->b.inode)->type) {
				case TypeInodeFile:
					err = lafs_inode_handle_orphan(db);
					break;
				case TypeDir:
					err = lafs_dir_handle_orphan(db);
					break;
				}
				if (err == -ENOMEM)
					timeout = msecs_to_jiffies(500);
			}
			mutex_unlock(&ino->i_mutex);
		}

		orphan_iput(ino);
		putdref(db, MKREF(run_orphans));
		spin_lock(&fs->lock);
	}
	list_splice(&done, &fs->pending_orphans);
	spin_unlock(&fs->lock);
	clear_bit(OrphansRunning, &fs->fsstate);

	if (test_bit(CleanerDisabled, &fs->fsstate)) {
		struct datablock *db;
		printk("Orphan sleeping:\n");
		list_for_each_entry(db, &fs->pending_orphans,
				    orphans) {
			printk("orph=%s\n", strblk(&db->b));
		}
	}
	if (list_empty_careful(&fs->pending_orphans))
		/* unmount might be waiting... */
		wake_up(&fs->async_complete);
	return timeout;
}

static void lafs_drop_orphan(struct fs *fs, struct datablock *db)
{
	/* This block was an orphan but isn't any more.
	 * Remove it from the list.
	 * It must be IOlocked, and this call must not come
	 * from lafs_run_orphans.
	 */
	struct inode *ino = orphan_inode(db);

	BUG_ON(ino && !mutex_is_locked(&ino->i_mutex));
	orphan_iput(ino);

	if (test_bit(B_Orphan, &db->b.flags))
		return;
	spin_lock(&fs->lock);
	if (!test_bit(B_Orphan, &db->b.flags) &&
	    !list_empty_careful(&db->orphans) &&
	    !test_bit(B_Cleaning, &db->b.flags))
		list_del_init(&db->orphans);
	spin_unlock(&fs->lock);
}

void lafs_add_orphan(struct fs *fs, struct datablock *db)
{
	/* This block is an orphan block but might not be
	 * on the list any more.
	 * It now needs orphan processing (probably nlink
	 * has changed, or I_Deleting), so put it back
	 * on the list.
	 */
	LAFS_BUG(!test_bit(B_Orphan, &db->b.flags), &db->b);
	spin_lock(&fs->lock);
	if (list_empty_careful(&db->orphans))
		list_add_tail(&db->orphans, &fs->pending_orphans);
	spin_unlock(&fs->lock);
	lafs_wake_thread(fs);
}

void lafs_orphan_forget(struct fs *fs, struct datablock *db)
{
	/* This is still an orphan, but we don't want to handle
	 * it just now.  When we do, lafs_add_orphan will be called */
	LAFS_BUG(!test_bit(B_Orphan, &db->b.flags), &db->b);
	spin_lock(&fs->lock);
	if (!test_bit(B_Cleaning, &db->b.flags) &&
	    !list_empty_careful(&db->orphans))
		list_del_init(&db->orphans);
	spin_unlock(&fs->lock);
}

struct datablock *lafs_find_orphan(struct inode *ino)
{
	/* I could walk the child tree of the inode, or
	 * walk the pending_orphan list looking for an
	 * orphan for this inode.
	 * The latter seems easier.
	 * Performance will be quadratic in the size of the
	 * orphan list, so we could possibly consider
	 * improvements later.
	 */
	struct fs *fs = fs_from_inode(ino);
	struct datablock *db;

	spin_lock(&fs->lock);
	list_for_each_entry(db, &fs->pending_orphans, orphans)
		if (db->b.inode == ino) {
			spin_unlock(&fs->lock);
			return db;
		}
	spin_unlock(&fs->lock);
	return NULL;
}

int lafs_count_orphans(struct inode *ino)
{
	/* Count how many active orphan slots there are
	 * in this file (which is the orphan file).
	 * We simply iterate all the slots until we
	 * find an empty one.
	 * We keep references to the blocks so lafs_add_orphans
	 * is certain to find them.
	 */
	int bnum = -1;
	int slots = 1 << (ino->i_blkbits - 4);
	int slot;

	do {
		struct datablock *db;
		struct orphan *or;

		bnum++;
		slot = 0;

		db = lafs_get_block(ino, bnum, NULL, GFP_KERNEL, MKREF(count_orphans));
		if (!db)
			break;
		if (lafs_read_block(db) < 0) {
			putdref(db, MKREF(count_orphans));
			break;
		}

		or = map_dblock(db);
		for (slot = 0; slot < slots ; slot++)
			if (or[slot].type == 0)
				break;

		unmap_dblock(db, or);
		if (slot == 0)
			putdref(db, MKREF(count_orphans));
	} while (slot == slots);

	return bnum * slots + slot;
}

void lafs_add_orphans(struct fs *fs, struct inode *ino, int count)
{
	int slots = 1 << (ino->i_blkbits - 4);
	int last_block = (count + slots - 1) >> (ino->i_blkbits - 4);
	int bnum;

	for (bnum = 0; bnum < last_block; bnum++) {
		struct datablock *db = lafs_get_block(ino, bnum, NULL, GFP_KERNEL, MKREF(add_orphans));
		int bslot = bnum * slots;
		struct orphan *or;
		int slot;

		/* We already own a count_orphans ref on this block */
		BUG_ON(!db);
		LAFS_BUG(!test_bit(B_Valid, &db->b.flags), &db->b);
		putdref(db, MKREF(count_orphans));

		or = map_dblock(db);
		for (slot = 0; slot < slots && bslot + slot < count; slot++) {
			struct inode *oino;
			struct datablock *ob;

			LAFS_BUG(or[slot].type == 0, &db->b);
			
			oino = lafs_iget_fs(fs,
					    le32_to_cpu(or[slot].filesys),
					    le32_to_cpu(or[slot].inum),
					    SYNC);
			if (IS_ERR(oino)) {
				or[slot].type = 0;
				continue;
			}

			ob = lafs_get_block(oino, le32_to_cpu(or[slot].addr), NULL,
					    GFP_KERNEL, MKREF(add_orphan));

			if (!IS_ERR(ob)) {
				if (!test_and_set_bit(B_Orphan, &ob->b.flags)) {
					getdref(ob, MKREF(orphan_flag));
					getdref(db, MKREF(orphan));
					ob->orphan_slot = bslot + slot;
					lafs_igrab_fs(ob->b.inode);
					lafs_add_orphan(fs, ob);
				} else
					or[slot].type = 0;
				putdref(ob, MKREF(add_orphan));
			}
			lafs_iput_fs(oino);
		}
		unmap_dblock(db, or);
		putdref(db, MKREF(add_orphans));
	}
}
