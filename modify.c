#include "lafs.h"

/*
 * fs/lafs/modify.c
 * Copyright (C) 2005-2009
 * Neil Brown <neilb@suse.de>
 * Released under the GPL, version 2
 *
 * Index incorporation.
 *  We have an index block (indirect, extent, or index), which may contain
 *  some addresses already.
 *  It has an 'uninc_table' which is a short list of addresses to
 *  be incorporated.
 *
 * Best option is that all the new addresses fit and we don't need to
 * split or even change between indirect and extent.
 * But some times format changing and splitting are needed.
 *
 * To handle format changing we generally create a new table and
 * copy all data into it.  We maintain some fast paths for the
 * really simple updates such as indirect when all new blocks fit in the
 * range, or index where all new ranges come at the end, and fit.
 *
 * In general, the first result of incorporation is a new block
 * with new addresses, a possibly empty old block, and a possibly empty
 * list of addresses that didn't fit into the new block.
 *
 * If the old block and the list of addresses are empty, we simply
 * swizzle the block pointers and are done.  If not, we have to split the
 * index block and we need to inform the parent.  If there is no parent
 * to inform (we are an inode) then we have to grow the depth of the
 * index tree.
 *
 * Awkward details:
 * - When an index block is split, we need to scan all children and
 *   reparent those with a new parent.
 * - If an index block becomes empty, we can mark it a Hole.
 * - If the inode block has only one index, maybe we should
 *   shrink the tree.
 *
 * Memory allocation issues.
 *  We are potentially in the write path for freeing memory when
 *  performing incorporation so we have to be careful about memory
 *  allocations.
 *  Sometimes we need to allocate a new page for a page split.  It seems
 *  likely that the old page will be able to be written and freed soon
 *  but there is no guarantee as new pages might be dirtied at any time
 *  and so pin the index page back in memory.
 *  The key to avoiding memory exhaustion is to block the dirtying of
 *  new pages early enough... We will have to worry about that later.
 */

static void sort_blocks(struct block **blkp)
{
	/* sort blocks list in *blkp by ->fileaddr
	 * use merge sort
	 */
	int cnt = 0;

	struct block *bl[2];
	bl[0] = *blkp;
	bl[1] = NULL;

	do {
		struct block **blp[2], *b[2];
		int curr = 0;
		long prev = 0;
		int next = 0;
		cnt++;
		blp[0] = &bl[0];
		blp[1] = &bl[1];
		b[0] = bl[0];
		b[1] = bl[1];

		/* take least of b[0] and b[1]
		 * if it is larger than prev, add to
		 * blp[curr], else swap curr then add
		 */
		while (b[0] || b[1]) {
			if (b[next] == NULL ||
			    (b[1-next] != NULL &&
			     !((prev <= b[1-next]->fileaddr)
			       ^(prev <= b[next]->fileaddr)
			       ^(b[next]->fileaddr <= b[1-next]->fileaddr)))
				)
				next = 1 - next;

			if (b[next]->fileaddr < prev)
				curr = 1 - curr;
			prev = b[next]->fileaddr;
			*blp[curr] = b[next];
			blp[curr] = &b[next]->chain;
			b[next] = b[next]->chain;
		}
		*blp[0] = NULL;
		*blp[1] = NULL;
	} while (bl[0] && bl[1]);
	if (bl[0])
		*blkp = bl[0];
	else
		*blkp = bl[1];
}

static int incorp_index(struct block *b)
{
	/* This is an index block which we have just incorporated.
	 * Update flags accordingly.
	 */
	int cr = 0;
	clear_bit(B_Uninc, &b->flags);
	if (test_and_clear_bit(B_UnincCredit, &b->flags)) {
		temp_credits++;
		cr++;
	}
	if (test_and_clear_bit(B_PrimaryRef, &b->flags)) {
		struct block *pb = list_entry(b->siblings.prev,
					      struct block, siblings);
		LAFS_BUG(b->siblings.prev == &b->parent->children, b);
		putref(pb, MKREF(primary));
		if (!test_bit(B_EmptyIndex, &b->flags))
			lafs_hash_iblock(iblk(b));
	}
	putref(b, MKREF(uninc));
	return cr;
}

static int incorporate_indirect(struct uninc *ui, char *buf, u32 addr, int len)
{
	/* See if this uninc info can be incorporated directly into
	 * this indirect block, and if it can: do it.
	 */
	int i;
	int credits = 0;

	if ( ui->pending_addr[ui->pending_cnt-1].fileaddr
	    +ui->pending_addr[ui->pending_cnt-1].cnt
	     > addr + (len/6))
		return 0;

	for (i = 0; i < ui->pending_cnt; i++) {
		u32 ad = ui->pending_addr[i].fileaddr - addr;
		u32 cnt = ui->pending_addr[i].cnt;
		u64 phys = ui->pending_addr[i].physaddr;
		credits += cnt;
		while (cnt--) {
			char *p = buf + 6*ad;
			encode48(p, phys);
			ad++;
			phys++;
		}
	}
	return credits;
}

static int incorporate_extent(struct uninc *ui, char *buf, int size)
{
	/* See if the common case of new data being added to the file
	 * applies.  i.e. is there enough space in the buf to add the
	 * extents listed in ui.  If so, add them. */
	/* Find the last valid extent. */
	int e = (size)/12;
	int i;
	u32 last = 0;
	int credits = 0;

	while (e > 0) {
		char *b;
		int size;
		b = buf + (e-1)*12 + 6;
		size = decode16(b);
		if (size != 0) {
			last = decode32(b)+size;
			break;
		}
		e--;
	}
	/* there are 'e' valid extents; */
	if (size/12 - e < ui->pending_cnt
	    || ui->pending_addr[0].fileaddr < last)
		return 0;

	/*Ok, they fit. */
	buf += e*12;
	for (i = 0; i < ui->pending_cnt; i++)
		if (ui->pending_addr[i].physaddr) {
			dprintk("AddedX %llu %u %lu\n",
				(unsigned long long)ui->pending_addr[i].physaddr,
				(unsigned) ui->pending_addr[i].cnt,
				(unsigned long)ui->pending_addr[i].fileaddr);
			credits += ui->pending_addr[i].cnt;
			encode48(buf, ui->pending_addr[i].physaddr);
			encode16(buf, ui->pending_addr[i].cnt);
			encode32(buf, ui->pending_addr[i].fileaddr);
		}
	return credits;
}

static int incorporate_index(struct block *uninc, char *buf, int size)
{
	/* see if we can merge the addresses of uninc blocks
	 * with those in buf, by counting the total number,
	 * and if they fit, merge backwards.
	 * Return -1 on failure, or number of credits collected.
	 */
	int ucnt = 0;
	int icnt = size/10;
	int ncnt;
	int delcnt = 0;
	int repcnt = 0;
	int i;
	char *b;
	u32  uaddr, iaddr;
	struct block *blk;
	int credits;

	while (icnt > 0) {
		u64 phys;
		b = buf + (icnt-1)*10;
		phys = decode48(b);
		if (phys != 0)
			break;
		icnt--;
	}
	for (blk = uninc ; blk ; blk = blk->chain)
		ucnt++;
	i = 0;
	ncnt = 0;
	LAFS_BUG(ucnt == 0, uninc);
	uaddr = uninc->fileaddr;
	blk = uninc;

	while (blk || i < icnt) {
		if (blk)
			uaddr = blk->fileaddr;
		else
			uaddr = 0xffffffff;
		if (i < icnt) {
			b = buf + i*10 + 6;
			iaddr = decode32(b);
		} else
			iaddr = 0xffffffff;

		if (uaddr == iaddr) {
			/* replacement */
			if (blk->physaddr)
				ncnt++;
			else
				delcnt++;
			repcnt++;
			i++;
			blk = blk->chain;
		} else if (uaddr < iaddr) {
			/* New entry, unlikely to have a phys of 0... */
			if (blk->physaddr)
				ncnt++;
			else
				delcnt++; /* not strictly a delete, but too
					   * confusing to handle.
					   */
			blk = blk->chain;
		} else {/* Old unchanged entry */
			ncnt++;
			i++;
		}
	}
	if (ncnt > (size/10))
		return -1;
	if (repcnt == icnt) {
		/* all currently incorporated addresses are
		 * replaced or removed, so just install the
		 * new addresses
		 */
		credits = 0;
		b = buf;
		i = 0;
		temp_credits = 0;
		while (uninc) {
			blk = uninc;
			uninc = uninc->chain;
			if (blk->physaddr) {
				encode48(b, blk->physaddr);
				encode32(b, blk->fileaddr);
				i++;
			}
			credits += incorp_index(blk);
		}
		while (i < icnt) {
			encode48(b, 0ULL);
			encode32(b, 0);
			i++;
		}
		temp_credits = 0;
		return credits;
	}
	if (delcnt) {
		/* some addresses have been deleted.  Too hard to do
		 * quickly
		 */
		return -1;
	}
	/* OK, have n addresses, and we can write them from last
	 * to first without overwriting anything, so let's do it
	 */
	blk = NULL;
	while (uninc) {
		struct block *t = blk;
		blk = uninc;
		uninc = uninc->chain;
		blk->chain = t;
	}
	uninc = blk;
	credits = 0;
	temp_credits = 0;
	while (i || uninc) {
		int drop_one = 0;
		if (uninc)
			uaddr = uninc->fileaddr;
		else
			uaddr = 0;
		if (i) {
			b = buf + (i-1)*10 + 6;
			iaddr = decode32(b);
		} else
			iaddr = 0;

		if (uaddr == iaddr && uaddr == 0) {
			/* '0' is both least value and end marker, so
			 * be careful.  We know there is no deletion, so
			 * ->physaddr cannot be zero.
			 */
			ncnt--;
			if (uninc) {
				BUG_ON(uninc->physaddr == 0);
				b = buf + ncnt*10;
				encode48(b, uninc->physaddr);
				encode32(b, uaddr);
				drop_one = 1;
			}
			if (i)
				i--;
		} else if (uaddr == iaddr) {
			BUG_ON(uninc->physaddr == 0);
			ncnt--;
			b = buf + ncnt*10;
			encode48(b, uninc->physaddr);
			encode32(b, uaddr);
			i--;
			drop_one = 1;
		} else if (uaddr > iaddr) {
			if (uninc->physaddr) {
				ncnt--;
				b = buf + ncnt*10;
				encode48(b, uninc->physaddr);
				encode32(b, uaddr);
			}
			drop_one = 1;
		} else {
			u64 p;
			u32 a;
			i--; ncnt--;
			b = buf + i*10;
			p = decode48(b);
			a = decode32(b);
			b = buf + ncnt*10;
			encode48(b, p);
			encode32(b, a);
		}
		if (drop_one) {
			blk = uninc;
			uninc = uninc->chain;
			credits += incorp_index(blk);
		}
	}
	temp_credits = 0;
	BUG_ON(ncnt);
	return credits;
}

void lafs_clear_index(struct indexblock *ib)
{
	int len = 1 << ib->b.inode->i_blkbits;
	int offset = 0;
	char *buf;
	if (test_bit(B_InoIdx, &ib->b.flags))
		offset = LAFSI(ib->b.inode)->metadata_size;
	buf = map_iblock(ib);
	memset(buf+offset, 0, len - offset);

	if (ib->depth == 1)
		*(u16 *)(buf+offset) = cpu_to_le16(IBLK_INDIRECT);
	else if (ib->depth > 1)
		*(u16 *)(buf+offset) = cpu_to_le16(IBLK_INDEX);
	else
		LAFS_BUG(1, &ib->b);
       
	if (offset) {
		/* just to be on the safe side ... */
		((struct la_inode*)(buf))->depth = ib->depth;
	}
	unmap_iblock(ib, buf);
	set_bit(B_Valid, &ib->b.flags);
}

static void grow_index_tree(struct indexblock *ib, struct indexblock *new)
{
	/* leaf_incorporate or internal_incorporate was working on an inode
	 * and didn't find enough space.
	 * So demote 'ib' to a regular Index block and make 'new' a new
	 * InoIdx block (inode->iblock);
	 * We have IOLock on ib to ensure exclusivity.
	 */

	int blksize = ib->b.inode->i_sb->s_blocksize;
	int offset = LAFSI(ib->b.inode)->metadata_size;
	char *ibuf;

	ib->data = new->data;  new->data = NULL;
	clear_bit(B_InoIdx, &ib->b.flags);
	lafs_hash_iblock(ib);
	set_bit(B_InoIdx, &new->b.flags);
	if (test_and_clear_bit(B_Root, &ib->b.flags))
		set_bit(B_Root, &new->b.flags);

	new->b.fileaddr = 0;
	new->b.inode = ib->b.inode;
	new->b.parent = ib->b.parent;
	ib->b.parent = new;
	getiref(new, MKREF(child));

	clear_bit(B_PhysValid, &ib->b.flags);
	ib->b.physaddr = 0;

	ibuf = map_iblock(new);
	memcpy(ib->data, ibuf + offset, blksize - offset);
	memset(ib->data + blksize - offset, 0, offset);
	LAFS_BUG(ib->depth == 0, &ib->b);
	new->depth = ib->depth + 1;
	LAFSI(new->b.inode)->depth++;
	((struct la_inode*)(ibuf))->depth = LAFSI(new->b.inode)->depth;

	/* There is no need to set PrimaryRef as the first child in an
	 * index block is implicitly incorporated - at least enough to be
	 * found.  So the ->parent link holds our place in the tree
	 */
	unmap_iblock(new, ibuf);

	LAFS_BUG(LAFSI(new->b.inode)->iblock != ib, &ib->b);
	LAFSI(new->b.inode)->iblock = new;
	list_add(&ib->b.siblings, &new->children);
	INIT_LIST_HEAD(&new->b.siblings);

	LAFS_BUG(new->depth != LAFSI(new->b.inode)->depth, &ib->b);
	lafs_clear_index(new);

	/* Make sure pin count is correct */
	LAFS_BUG(!test_bit(B_Pinned, &ib->b.flags), &ib->b);
	set_bit(B_Pinned, &new->b.flags);
	set_phase(&new->b, test_bit(B_Phase1, &ib->b.flags));
	atomic_set(&new->pincnt[!!test_bit(B_Phase1, &ib->b.flags)], 1);
}

static void print_index(char *buf, u32 start, int len)
{
	int type = le16_to_cpu(*(u16 *)(buf));
	char *p = buf+2;
	u32 addr;
	int size;
	u64 phys;
	len -= 2;

	switch (type) {
	case IBLK_INDIRECT:
		printk(" Indirect:\n");
		while (len >= 6) {
			phys = decode48(p);
			if (phys)
				printk(" %u:%lu", start, (unsigned long)phys);
			start++;
			len -= 6;
		}
		printk("\n");
		break;
	case IBLK_EXTENT:
		printk(" Extent:\n");
		while (len >= 12) {
			phys = decode48(p);
			size = decode16(p);
			addr = decode32(p);
			if (size)
				printk(" %u-%u: %llu\n", addr, addr+size-1,
				       (unsigned long long) phys);
			len -= 12;
		}
		break;
	case IBLK_INDEX:
		printk(" Index:\n");
		while (len >= 10) {
			phys = decode48(p);
			addr = decode32(p);
			if (phys)
				printk(" %u:%lu  ", addr, (unsigned long)phys);
			len -= 10;
		}
		printk("\n");
		break;
	}
}

void lafs_print_uninc(struct uninc *ui)
{
	int i;
	printk(" pending=%d\n", ui->pending_cnt);
	for (i = 0; i < ui->pending_cnt; i++)
		printk("  [%d] %u-%u : %lu\n", i,
		       ui->pending_addr[i].fileaddr,
		       ui->pending_addr[i].fileaddr
		       +ui->pending_addr[i].cnt - 1,
		       (unsigned long)ui->pending_addr[i].physaddr);
}

struct layoutinfo {
	char *data;
	int size;
	u32 nextaddr; /* on failure, set to the first address that doesn't fit */

	u64 lastphys;
	u32 lastaddr;
	int esize;
};

static int add_index(void *data, u32 addr, u64 phys)
{
	struct layoutinfo *li = data;
	char *cp = li->data;

	if (phys == 0) {
		/*initialise */
		encode16(cp, IBLK_INDEX);
		li->size -= 2;
		li->data = cp;
		return 0;
	}
	if (phys == ~0LL)
		return 0;

	if (li->size < 10) {
		li->nextaddr = addr;
		return 0;
	}

	cp = li->data;
	/* Each entry is 10 bytes: 6byte dev, 4byte file */
	encode48(cp, phys);
	encode32(cp, addr);

	li->data = cp;
	li->size -= 10;
	return 1;
}

static int add_indirect(void *data, u32 addr, u64 phys, int len)
{
	struct layoutinfo *li = data;
	u32 lastaddr;
	char *p;
	int i;

	if (phys == 0) {
		/* initialise */
		li->lastaddr = addr;
		p = li->data;
		encode16(p, IBLK_INDIRECT);
		li->size -= 2;
		li->data = p;
		return 0;
	}
	if (len == 0)
		return 0; /* finalise */

	p = li->data + (addr - li->lastaddr) * 6;
	lastaddr = li->lastaddr + (li->size/6);
	for (i = 0; i < len && addr < lastaddr ; i++) {
		encode48(p, phys);
		addr++;
		phys++;
	}
	if (i < len)
		li->nextaddr = addr;
	return i;
}

static int add_extent(void *data, u32 addr, u64 phys, int len)
{
	struct layoutinfo *li = data;
	char *p;

	if (phys == 0) {
		/* initialise */
		p = li->data;
		encode16(p, IBLK_EXTENT);
		li->size -= 2;
		li->data = p;
		li->esize = 0;
		return 0;
	}
	if (len == 0) {
		/* finalise */
		/* close off an extent if there is one */
		dprintk("Finalise: %d\n", (int)li->esize);
		if (li->esize) {
			p = li->data - 6;
			encode16(p, li->esize);
			li->esize = 0;
		}
		return 0;
	}

	if (li->esize && li->lastaddr == addr &&
	    li->lastphys == phys) {
		/* just extend the extent */
		dprintk("Extending %llu %u %lu\n",
			(unsigned long long)phys,
			(unsigned) len,
			(unsigned long)addr);
		li->esize += len;
		li->lastaddr += len;
		li->lastphys += len;
		return len;
	}
	if (li->esize) {
		/* need to close old extent */
		p = li->data - 6;
		encode16(p, li->esize);
		li->esize = 0;
	}
	if (li->size < 12) {
		/* No room */
		dprintk("Extent says 'no room'\n");
		li->nextaddr = li->lastaddr;
		return 0;
	}
	p = li->data;
	encode48(p, phys);
	encode16(p, len);
	encode32(p, addr);
	dprintk("Added %llu %u %lu\n",
		(unsigned long long)phys,
		(unsigned) len,
		(unsigned long)addr);
	li->esize = len;
	li->data = p;
	li->size -= 12;
	li->lastaddr = addr + len;
	li->lastphys = phys + len;
	return len;
}

struct leafinfo {
	u32	firstaddr;
	u32	nextaddr;
	u64	nextphys;
	int	extents;
	int	esize;
	int	choice;
	int	nofit; /* bit mask of which layouts don't fit */
	int	blksize;
	int	cnt;
};

static int check_leaf(void *data, u32 addr, u64 phys, int len)
{
	struct leafinfo *li = data;
	int origlen = len;

	/* We are gathering data to see whether an indirect block or
	 * an extent block is best - depending on which fills the block
	 * first (that one is not good choice).
	 * We also see if either will fit at all.
	 * For indirect, we give up as soon as an address will not fit in
	 *  the block.
	 * For extent, we track how many extents will be needed, and give up
	 *  when that exceeds the size of the block.
	 */

	if (phys == 0) {
		/* We are initialising the structure */
		memset(li, 0, sizeof(*li));
		li->firstaddr = addr;
		li->blksize = len - 2; /* exclude type field */
		return 0;
	}
	if (len == 0)
		return 0; /* nothing to finalise */

	li->cnt++;
	/* Check for indirect layout */
	if (((addr + len) - li->firstaddr) * 6 > li->blksize) {
		/* Indirect no longer fits */
		if (!li->choice)
			li->choice = IBLK_EXTENT;
		li->nofit |= (1 << IBLK_INDIRECT);
	}
	/* Check for extent layout */
	if (addr == li->nextaddr && phys == li->nextphys) {
		/* This fits in the current extent, at least partly */
		int size = len;
		if (li->esize + size >= 0x10000) {
			/* extent too large */
			len = 0x10000 - li->esize - 1;
		}
		li->esize += size;
		li->nextaddr += size;
		li->nextphys += size;
		len -= size;
		addr += size;
		phys += size;
	}
	if (len) {
		/* need a new extent */
		li->extents++;
		if (li->extents * 12 > li->blksize) {
			/* But it won't fit */
			if (!li->choice)
				li->choice = IBLK_INDIRECT;
			li->nofit |= (1 << IBLK_EXTENT);
		}
		li->nextaddr = addr+1;
		li->nextphys = phys+1;
		li->esize = len;
	}
	if (li->choice &&
	    (li->nofit & (1 << IBLK_INDIRECT)) &&
	    (li->nofit & (1 << IBLK_EXTENT)))
		/* stop looking */
		return 0;
	return origlen;
}

static u32 walk_index(u32 addr, char **bufp, int len, struct block *uninc,
		      int (*handle)(void*, u32, u64),
		      void *data)
{
	/* Pass each entry in buf or uninc to "handle" (which is
	 * always add_index).
	 * Must pass addresses in order.
	 * 'handle' returns 1 if the address was added.
	 * We return 1 if all addressed handled, else 0.
	 *
	 * Entries are 10 bytes: 6 byte dev address, 4 byte file address.
	 */
	char *buf = *bufp;
	int found_end = 0;

	handle(data, addr, 0); /* initialise */

	while ((len >= 10 && !found_end) || uninc != NULL) {
		unsigned long addr = 0;
		u64 phys = 0;

		if (len >= 10) {
			phys = decode48(buf);
			addr = decode32(buf);
			len -= 10;
			if (phys == 0)
				found_end = 1;
		}

		while (uninc &&
		       (phys == 0 || uninc->fileaddr <= addr)) {
			/* New block at or before current address */
			if (uninc->physaddr &&
			    !handle(data, uninc->fileaddr, uninc->physaddr)) {
				if (addr > uninc->fileaddr)
					buf -= 10;
				*bufp = buf;
				return 0;
			}
			if (uninc->fileaddr == addr)
				/* Don't record the old value from the index */
				phys = 0;
			uninc = uninc->chain;
		}

		if (phys && !handle(data, addr, phys)) {
			*bufp = buf - 10;
			return 0;
		}
	}
	handle(data, 0, ~(u64)0); /* finalise */
	return 1;
}

static u32 walk_indirect(u32 addr, char **bufp, int len, struct uninc *ui,
			 int (*handle)(void*, u32, u64, int),
			 void *data)
{
	/* Pass each entry in buf or ui to "handle".
	 * Must pass addresses in order.
	 * 'handle' returns number of addresses handled.
	 * If this isn't all, we stop and return 0
	 * else 1 if all are handled.
	 */
	int uinum = 0;
	int uioffset = 0;
	char *buf = *bufp;

	handle(data, addr, 0, len); /* initialise */

	while (len >= 6 || uinum < ui->pending_cnt) {
		u64 phys = 0;

		if (len >= 6) {
			phys = decode48(buf);
			len -= 6;
		} else if (uinum < ui->pending_cnt &&
			   addr < ui->pending_addr[uinum].fileaddr + uioffset)
			/* Skip ahead quickly. */
			addr = ui->pending_addr[uinum].fileaddr + uioffset;

		if (uinum < ui->pending_cnt &&
		    ui->pending_addr[uinum].fileaddr + uioffset == addr) {
			phys = ui->pending_addr[uinum].physaddr + uioffset;
			uioffset++;
			if (uioffset >= ui->pending_addr[uinum].cnt) {
				uinum++;
				uioffset = 0;
			}
			/* FIXME what if pending address ranges overlap !! */
			/* FIXME should an pending_addr range of holes be allowed?*/
			/* FIXME should we check that addr is never > fileaddr+offset ? */
		}
		if (phys) {
			if (!handle(data, addr, phys, 1)) {
				*bufp = buf - 6;
				return 0;
			}
		}
		addr++;
	}

	handle(data, 0, ~(u64)0, 0); /* finalise */
	return 1;
}

static u32 walk_extent(u32 addr, char **bufp, int len, struct uninc *ui,
		       int (*handle)(void*, u32, u64, int),
		       void *data)
{
	/* pass each extent in buf or ui to handle.  Extents
	 * can overlap, so care is needed
	 */
	int uinum = 0;
	int uioffset = 0;
	u64 ephys = 0;
	u32 eaddr = 0;
	int elen = 0;
	int found_end = 0;
	int handled;
	char *buf = *bufp;

	handle(data, addr, 0, len); /* initialise */

	while (uinum < ui->pending_cnt || ! found_end) {
		if (elen == 0 && !found_end) {
			if (len >= 12) {
				ephys = decode48(buf);
				elen = decode16(buf);
				eaddr = decode32(buf);
				len -= 12;
				BUG_ON(ephys == 0 && elen != 0); // FIXME fail gracefully
				dprintk("Found %llu %u %lu at %d\n",
					(unsigned long long) ephys,
					(unsigned) elen,
					(unsigned long)eaddr, len);
				if (ephys == 0) {
					eaddr = 0xFFFFFFFFUL;
					found_end = 1;
				}
			} else {
				eaddr = 0xFFFFFFFFUL;
				elen = 0;
				found_end = 1;
			}
		}

		/* Handle all pending extents that start before (or at) here */
		while (uinum < ui->pending_cnt &&
		       ui->pending_addr[uinum].fileaddr + uioffset <= eaddr &&
		       (elen > 0 || eaddr == 0xFFFFFFFFUL)
			) {
			/* just submit next extent from ui */
			s32 overlap;
			int hlen = ui->pending_addr[uinum].cnt - uioffset;
			/* If there is an overlap with the current extent, only
			 * add the overlapped portion.
			 */
			dprintk("(%llu %lu %u) (%llu %lu %u) + %d = %d (%d)\n",
				ephys, (unsigned long)eaddr, elen,
				ui->pending_addr[uinum].physaddr,
				(unsigned long)ui->pending_addr[uinum].fileaddr,
				ui->pending_addr[uinum].cnt,
				uioffset, hlen, uinum);
			if (elen &&
			    ui->pending_addr[uinum].fileaddr + uioffset + hlen
			    > eaddr + elen)
				hlen = (eaddr + elen) -
					(ui->pending_addr[uinum].fileaddr + uioffset);
			if (ui->pending_addr[uinum].physaddr)
				handled = handle(data,
						 ui->pending_addr[uinum].fileaddr + uioffset,
						 ui->pending_addr[uinum].physaddr + uioffset,
						 hlen);
			else
				handled = hlen;
			/* That might replace some of current extent */
			overlap = (ui->pending_addr[uinum].fileaddr + uioffset +
				   handled) - eaddr;
			if (elen && overlap > 0) {
				if (overlap > elen)
					overlap = elen;
				elen -= overlap;
				eaddr += overlap;
				ephys += overlap;
			}
			if (handled < hlen) {
				if (elen)
					*bufp = buf - 12;
				else
					*bufp = buf;
				return 0;
			}
			uioffset += hlen;
			if (uioffset >= ui->pending_addr[uinum].cnt) {
				uinum++;
				uioffset = 0;
			}
		}
		BUG_ON(elen && uioffset);
		/* If uninc extent intersects this extent, just submit
		 * partial extent and loop
		 */
		if (elen && uinum < ui->pending_cnt &&
		    ui->pending_addr[uinum].fileaddr < eaddr + elen) {
			int elen2 = ui->pending_addr[uinum].fileaddr - eaddr;
			handled = handle(data, eaddr, ephys, elen2);
			if (handled < elen2) {
				*bufp = buf - 12;
				return 0;
			}
			eaddr += elen2;
			ephys += elen2;
			elen -= elen2;
		} else if (elen) {
			/* Ok, just submit this extent */
			handled = handle(data, eaddr, ephys, elen);
			if (handled < elen) {
				*bufp = buf - 12;
				return 0;
			}

			elen = 0;
		}
	}
	*bufp = buf;
	handle(data, 0, ~(u64)0, 0); /* finalise */
	return 1;
}

void lafs_walk_leaf_index(struct indexblock *ib,
			  int (*handle)(void*, u32, u64, int),
			  void *data)
{
	char *ibuf, *buf;
	int offset;
	int len;
	int current_layout;
	struct fs *fs = fs_from_inode(ib->b.inode);
	struct uninc ui;
	ui.pending_cnt = 0;

	/* Cannot use kmap_atomic as handle might sleep when
	 * looking up segusage blocks
	 */
	if (test_bit(B_InoIdx, &ib->b.flags)) {
		ibuf = kmap(LAFSI(ib->b.inode)->dblock->page);
		ibuf += dblock_offset(LAFSI(ib->b.inode)->dblock);
	} else
		ibuf = map_iblock(ib);
	if (test_bit(B_InoIdx, &ib->b.flags))
		offset = LAFSI(ib->b.inode)->metadata_size;
	else
		offset = 0;
	len = fs->blocksize - offset;

	current_layout = le16_to_cpu(*(u16 *)(ibuf+offset));
	buf = ibuf + offset + 2;
	dprintk("CURRENT=%d\n", current_layout);
	switch (current_layout) {
	case IBLK_INDIRECT:
		walk_indirect(ib->b.fileaddr,
			      &buf, len-2, &ui,
			      handle, data);
		break;
	case IBLK_EXTENT:
		walk_extent(ib->b.fileaddr,
			    &buf, len-2, &ui,
			    handle, data);
		break;
	default:
		printk("CURRENT=%d\n", current_layout);
		LAFS_BUG(1, &ib->b); // FIXME should be IO error ??
	}
	if (test_bit(B_InoIdx, &ib->b.flags))
		kunmap(LAFSI(ib->b.inode)->dblock->page);
}

static void share_list(struct block **ibp, struct block **newp, u32 next)
{
	struct block *uin = *ibp;
	*ibp = NULL;
	while (uin) {
		struct block *b = uin;
		uin = b->chain;

		if (b->fileaddr < next) {
			*ibp = b;
			ibp = &b->chain;
		} else {
			*newp = b;
			newp = &b->chain;
		}
	}
	*ibp = NULL;
	*newp = NULL;
}

static void share_uninc(struct uninc *from, struct uninc *to,
			 u32 next)
{
	/* Any addresses in 'from' that are at-or-after 'next' go to 'to'.
	 * Credits need to be shared somehow too...
	 */
	int i, j;
	struct addr *ia, *ja;
	u32 len;
	int moved = 0;

	for (i = 0; i < from->pending_cnt; i++) {
		ia = &from->pending_addr[i];
		if (ia->fileaddr +
		    ia->cnt <= next)
			/* This one stays put */
			continue;
		if (ia->fileaddr < next) {
			/* This one gets split */
			j = to->pending_cnt;
			ja = &to->pending_addr[j];
			len = next - ia->fileaddr;

			ja->fileaddr = next;
			ja->cnt = ia->cnt - len;
			ja->physaddr = ia->physaddr + len;
			moved += ia->cnt - len;

			ia->cnt = len;
			to->pending_cnt = j+1;
			continue;
		}
		/* This one gets moved across. */
		j = to->pending_cnt;
		ja = &to->pending_addr[j];
		*ja = *ia;
		to->pending_cnt = j+1;
		moved = ia->cnt;

		/* Move the last one into this spot, and try again */
		j = from->pending_cnt-1;
		ja = &from->pending_addr[j];
		*ia = *ja;
		from->pending_cnt = j;
		i--;
	}
	/* FIXME this doesn't make sense.
	 * if the test can fail, we must have a better answer
	 */
	if (from->credits >= moved) {
		to->credits += moved;
		from->credits -= moved;
	} else {
		int c = from->credits / 2;
		to->credits += c;
		from->credits -= c;
		BUG();
	}
}

static void share_children(struct indexblock *ib, struct indexblock *new)
{
	/* Some of the children for *ib must now become children
	 * of *new.  When moving children we must be careful that
	 * blocks with B_PrimaryRef stay with their primary,
	 * or drop the flag and the reference
	 */
	struct block *b, *tmp;

	list_for_each_entry_safe(b, tmp, &ib->children, siblings) {
		if (b->fileaddr == new->b.fileaddr &&
		    test_bit(B_Index, &b->flags) &&
		    test_and_clear_bit(B_PrimaryRef, &b->flags)) {
			/* This block had a 'Primary' reference on
			 * the previous block.  We must drop that
			 * before it can be moved.
			 */
			putref(list_entry(b->siblings.prev,
					  struct block, siblings),
			       MKREF(primary));
		}
		if (b->fileaddr >= new->b.fileaddr) {
			list_move_tail(&b->siblings, &new->children);
			b->parent = new;
			getiref(new, MKREF(child));
			if (test_bit(B_Pinned, &b->flags)) {
				int ph = test_bit(B_Phase1, &b->flags);
				atomic_inc(&new->pincnt[ph]);
				atomic_dec(&ib->pincnt[ph]);
			}
			putiref(ib, MKREF(child));
		}
	}
}

static int do_incorporate_leaf(struct fs *fs, struct indexblock *ib,
			       struct uninc *ui,
			       struct indexblock *new)
{
	/* Incorporate all the changes in ib->uninc_table into ib,
	 * with any new address appearing in new.
	 * Return value is one of:
	 * 0: uninc_table removed everything, block is now empty.
	 * 1: everything fits in 'ib'.  New can be discarded.  uninc_table
	 *    is empty.
	 * 2: the addresses are in ib, new, and possibly new->uninc_table.
	 *    'new' has ->fileaddr set.
	 * 3: ib is InoIdx and the addresses didn't all fit, so everything
	 *    was copied to new and ib is now an IBLK_INDEX block pointing
	 *    at new.  'new' still needs incorporation.
	 *
	 * We first simulate laying out the info in both
	 * Indirect and Extent layouts and choose the best.
	 * Then we perform the layout into new. Then we swap data
	 * between ib and new as appropriate and return the result.
	 */
	char *ibuf, *nbuf;
	char *buf, *b2, *sbuf;
	int offset;
	int len, slen;
	struct leafinfo leafinfo;
	int current_layout;
	int choice;
	struct layoutinfo layout;
	u32 next;
	int uinxt, uinum;
	int ok;

	u32 eaddr;
	u64 ephys;
	int elen;

	ibuf = map_iblock(ib);
	if (test_bit(B_InoIdx, &ib->b.flags))
		offset = LAFSI(ib->b.inode)->metadata_size;
	else
		offset = 0;
	len = fs->blocksize - offset;

	check_leaf(&leafinfo, ib->b.fileaddr, 0, len);

	current_layout = le16_to_cpu(*(u16 *)(ibuf+offset));
	buf = ibuf + offset + 2;
	switch (current_layout) {
	case IBLK_INDIRECT:
		walk_indirect(ib->b.fileaddr,
			      &buf, len-2, ui,
			      check_leaf, &leafinfo);
		break;
	case IBLK_EXTENT:
		walk_extent(ib->b.fileaddr,
			    &buf, len-2, ui,
			    check_leaf, &leafinfo);
		break;
	default:
		printk("Current is %d\n", current_layout);
		LAFS_BUG(1, &ib->b); // FIXME should be IO error ??
	}

	if (leafinfo.cnt == 0) {
		/* no addresses need to be stored here. */
		unmap_iblock(ib, ibuf);
		return 0;
	}
	choice = leafinfo.choice ?: IBLK_EXTENT; /* default to Extents */
	if (offset && (leafinfo.nofit & (1<<choice))) {
		/* addresses won't fit in this inode, so copy into new,
		 * and set inode up as an index block
		 */
		unmap_iblock(ib, ibuf);
		grow_index_tree(ib, new);
		return 3;
	}

	nbuf = map_iblock_2(new);
	layout.data = nbuf;
	layout.size = len;
	memset(nbuf, 0, fs->blocksize);
	set_bit(B_Valid, &new->b.flags);

	buf = ibuf + offset + 2;
	switch (current_layout) {
	case IBLK_INDIRECT:
		ok = walk_indirect(ib->b.fileaddr,
				   &buf, len-2, ui,
				   (choice == IBLK_EXTENT)
				   ? add_extent : add_indirect,
				   &layout);
		break;
	case IBLK_EXTENT:
		ok = walk_extent(ib->b.fileaddr,
				 &buf, len-2, ui,
				 (choice == IBLK_EXTENT)
				 ? add_extent : add_indirect,
				 &layout);
		break;
	default: BUG();
	}
	dprintk("walk_leaf only got as far as %d\n", (int)layout.nextaddr);
	// print_index(ibuf+offset, ib->b.fileaddr, len);
	if (ok) {
		/* it all fit perfectly.
		 * Copy from 'new' into 'ib' and zero the uninc list
		 */
		//printk("Offset=%d len=%d\n", offset, len);
		memcpy(ibuf+offset, nbuf, len);
		unmap_iblock_2(new, nbuf);
		unmap_iblock(ib, ibuf);
		return 1;
	}
	next = layout.nextaddr;
	LAFS_BUG(offset, &ib->b);

	/* It didn't fit, and this is a regular index block, so
	 * we need to do a horizontal split.
	 * 'new' already has the early addresses.
	 * All addresses before 'next' in 'ui' and 'ib' need to be
	 * cleared out.
	 * new->fileaddr must be set to 'next'.
	 * data buffers for new and ib need to be swapped
	 */

	/* There is nowhere that we can safely put any index info
	 * that is still in 'ui' except into the new block with the
	 * remains of the 'ib' addresses.
	 * It had better fit.  And it will.
	 * Here are the cases for current_layout and choice.
	 * INDIRECT -> INDIRECT
	 *  The start of ib hasn't changed, so all the addresses that
	 *  were there are now in new.  So ib is empty and can fit everything
	 *  in ui, as ui is <<< ib
	 *
	 * INDIRECT -> EXTENT
	 *  More fitted as EXTENTS, so we must have included all remaining
	 *  addressed from ib, so only some ui addresses remain.
	 * EXTENT -> EXTENT
	 *  If extents have been added into new, there can have been at most
	 *  two for each extent in ui (the new extent, and the extra piece from
	 *  a split extent).  So worst case there are 16 extents left to encode,
	 *  twice the number in ui.
	 * EXTENT -> INDIRECT
	 *  The INDIRECT must be storing more addresses than the EXTENT option,
	 *  so there is even less remainder to be encoded.
	 *
	 * In each case, the remainder of ib must use less than half the space,
	 * and it must be in EXTENT format if it is not empty.
	 * So we can move them up to the top of the buffer, then merge with
	 * anything remaining in ui.
	 */

	/* Shuffle remaining extents in ui down so they are easy to access */
	uinxt = 0;
	for (uinum = 0; uinum < ui->pending_cnt; uinum++) {
		if (ui->pending_addr[uinum].cnt == 0)
			continue;
		if (ui->pending_addr[uinum].fileaddr
		    + ui->pending_addr[uinum].cnt < next)
			continue;
		if (ui->pending_addr[uinum].fileaddr < next) {
			int cnt = ui->pending_addr[uinum].cnt
				- (next - ui->pending_addr[uinum].fileaddr);
			ui->pending_addr[uinxt].physaddr =
				ui->pending_addr[uinum].physaddr
				+ (next - ui->pending_addr[uinum].fileaddr);
			ui->pending_addr[uinxt].fileaddr = next;
			ui->pending_addr[uinxt].cnt = cnt;
		} else {
			ui->pending_addr[uinxt].fileaddr =
				ui->pending_addr[uinum].fileaddr;
			ui->pending_addr[uinxt].physaddr =
				ui->pending_addr[uinum].physaddr;
			ui->pending_addr[uinxt].cnt =
				ui->pending_addr[uinum].cnt;
		}
		uinxt++;
	}
	ui->pending_cnt = uinxt;

	switch (current_layout) {
	case IBLK_INDIRECT:
		/* buf must now be effectively empty, and there must
		 * be at least one extent still in ui.
		 * So zero the buff and encode those extents as IBLK_EXTENT
		 */
		LAFS_BUG(next < ib->b.fileaddr + (len-2)/6, &ib->b);
		LAFS_BUG(ui->pending_cnt == 0, &ib->b);
		memset(ibuf, 0, len);
		sbuf = NULL;
		slen = 0;
		break;
	case IBLK_EXTENT:
		/* The combination of the remaining extents in buf, and
		 * those in ui will not fill buf.
		 * So shift the buf extents up to the top of the buffer,
		 * then merge the remaining ui extents in.
		 * The first extent may be partially processed already, so
		 * we might need to adjust it.
		 */
		/* Find end of index list.  Could use a binary search,
		 * but not now.
		 */
		b2 = buf;
		while (b2 + 12 <= ibuf + len) {
			ephys = decode48(b2);
			elen = decode16(b2);
			eaddr = decode32(b2);
			if (elen == 0) {
				b2 -= 12;
				break;
			}
			if (eaddr < next) {
				int handled = next - eaddr;
				BUG_ON(eaddr + elen <= next);
				b2 -= 12;
				BUG_ON(b2 != buf);
				encode48(b2, ephys + handled);
				encode16(b2, elen - handled);
				encode32(b2, next);
			}
		}
		/* Data we want extends from buf up to b2
		 * Move it to end of ibuf
		 */
		slen = b2 - buf;
		sbuf = ibuf + len - slen;
		memmove(sbuf, buf, slen);
		break;
	default:
		LAFS_BUG(1, &ib->b);
	}
	layout.data = ibuf;
	layout.size = len;
	ok = walk_extent(next, &sbuf, slen, ui, add_extent, &layout);
	LAFS_BUG(!ok, &ib->b);
	if (slen && layout.data > sbuf) {
		printk("slen=%d ld-sb=%d layout.data=%p sbuf=%p "
		       "buf=%p ibuf=%p len=%d\n",
		       slen, (int)(layout.data-sbuf), layout.data, sbuf,
		       buf, ibuf, len);
	}
	LAFS_BUG(slen && layout.data > sbuf, &ib->b);
	memset(layout.data, 0, layout.size);

	new->b.fileaddr = next;
	new->data = ibuf;
	ib->data = nbuf;
	unmap_iblock_2(new, nbuf);
	unmap_iblock(ib, ibuf);

	/* new needs to be a sibling of ib, and children need to be shared out,
	 *   distributing pincnt appropriately.
	 * uninc_next need to be shared too
	 */
	share_children(ib, new);

	spin_lock(&ib->b.inode->i_data.private_lock);
	if (ib->uninc_next)
		share_list(&ib->uninc_next, &new->uninc_next, next);
	new->uninc_table.pending_cnt = 0;
	new->uninc_table.credits = 0;
	share_uninc(&ib->uninc_table, &new->uninc_table, next);
	spin_unlock(&ib->b.inode->i_data.private_lock);

	new->depth = ib->depth;
	if (ib->b.siblings.next != &ib->b.parent->children &&
	    test_bit(B_PrimaryRef, &list_entry(ib->b.siblings.next,
					       struct block, siblings)->flags))
		/* Inserting into a PrimaryRef chain */
		getiref(new, MKREF(primary));
	else
		getiref(ib, MKREF(primary));
	set_bit(B_PrimaryRef, &new->b.flags);
	list_add(&new->b.siblings, &ib->b.siblings);
	new->b.parent = ib->b.parent;
	(void)getiref(new->b.parent, MKREF(child));
	new->b.inode = ib->b.inode;
	LAFS_BUG(!test_bit(B_Pinned, &ib->b.flags), &ib->b);
	set_phase(&new->b, test_bit(B_Phase1, &ib->b.flags));
	return 2;
}

static int do_incorporate_internal(struct fs *fs, struct indexblock *ib,
				   struct block *uninc, int *credits,
				   struct indexblock *new)
{
	/* Incorporate all the changes in 'uninc' into ib,
	 * with any new address appearing in new.
	 * Return value is one of:
	 * 0: uninc removed everything, block is now empty.
	 * 1: everything fits in 'ib'.  New can be discarded.  uninc_table
	 *    is empty.
	 * 2: the addresses are in ib, new, and possibly new->uninc
	 *    'new' has ->fileaddr set.
	 * 3: ib is InoIdx and the addresses didn't all fit, so everything
	 *    was copied to new and ib is now an IBLK_INDEX block pointing
	 *    at new.  'new' still needs incorporation.
	 *
	 */
	char *ibuf, *nbuf;
	char *buf;
	int offset;
	int len;
	int current_layout;
	struct layoutinfo layout;
	u32 next = 0;
	int ok;
	int cr;

	ibuf = map_iblock(ib);
	if (test_bit(B_InoIdx, &ib->b.flags))
		offset = LAFSI(ib->b.inode)->metadata_size;
	else
		offset = 0;
	len = fs->blocksize - offset;

	current_layout = le16_to_cpu(*(u16 *)(ibuf+offset));
	buf = ibuf + offset + 2;
	LAFS_BUG(current_layout != IBLK_INDEX,
		 &ib->b);

	nbuf = map_iblock_2(new);
	layout.data = nbuf;
	layout.size = len;
	memset(nbuf, 0, fs->blocksize);

	ok = walk_index(ib->b.fileaddr,
			&buf, len-2, uninc,
			add_index,
			&layout);

	dprintk("walk_index only got as far as %d\n", (int)next);

	if (ok) {
		/* it all fit perfectly.
		 * Copy from 'new' into 'ib' and free all uninc blocks
		 */
		//printk("Offset=%d len=%d\n", offset, len);
		memcpy(ibuf+offset, nbuf, len);
		unmap_iblock_2(new, nbuf);
		unmap_iblock(ib, ibuf);
		cr = 0;
		temp_credits = 0;
		while (uninc) {
			struct block *b = uninc;
			uninc = uninc->chain;
			cr += incorp_index(b);
		}
		temp_credits = 0;
		*credits += cr;
		return 1;
	}
	if (offset) {
		unmap_iblock(ib, ibuf);
		grow_index_tree(ib, new);
		return 3;
	}
	next = layout.nextaddr;
	/* It didn't fit, and this is a regular index block, so
	 * we need to do a horizontal split.
	 * 'new' already has the early addresses.
	 * All addresses before 'next' in 'ib' need to be
	 * cleared out.
	 * new->fileaddr must be set to 'next'.
	 * data buffers for new and ib need to be swapped
	 */

	/* shuffle ibuf down.
	 * 'buf' is the starting point.
	 */
	memcpy(ibuf+2, buf,  ibuf + len - buf);
	memset(ibuf+2 + len - (buf - ibuf), 0, buf - (ibuf+2));

	new->b.fileaddr = next;
	new->data = ibuf;
	ib->data = nbuf;
	unmap_iblock_2(new, nbuf);
	unmap_iblock(ib, ibuf);
	set_bit(B_Valid, &new->b.flags);

	/* new needs to be a sibling of ib, and children need to be shared out,
	 *   distributing pincnt appropriately.
	 * All of uninc that is remaining goes to 'new', while
	 * ib->uninc and ib->uninc_next need to be shared out.
	 * We need to be careful to preserve the relative order of children
	 * as they might not all be incorporated yet.
	 */
	share_children(ib, new);

	cr = 0;
	temp_credits = 0;
	while (uninc && uninc->fileaddr < next) {
		struct block *b = uninc;
		uninc = uninc->chain;
		cr += incorp_index(b);
	}
	temp_credits = 0;
	*credits += cr;
	new->uninc = uninc;
	spin_lock(&ib->b.inode->i_data.private_lock);
	if (ib->uninc_next)
		share_list(&ib->uninc_next, &new->uninc_next, next);
	if (ib->uninc)
		share_list(&ib->uninc, &new->uninc, next);
	spin_unlock(&ib->b.inode->i_data.private_lock);

	new->depth = ib->depth;
	if (ib->b.siblings.next != &ib->b.parent->children &&
	    test_bit(B_PrimaryRef, &list_entry(ib->b.siblings.next,
					       struct block, siblings)->flags))
		/* Inserting into a PrimaryRef chain */
		getiref(new, MKREF(primary));
	else
		getiref(ib, MKREF(primary));
	set_bit(B_PrimaryRef, &new->b.flags);
	list_add(&new->b.siblings, &ib->b.siblings);
	new->b.parent = ib->b.parent;
	(void)getiref(new->b.parent, MKREF(child));
	new->b.inode = ib->b.inode;
	LAFS_BUG(!test_bit(B_Pinned, &ib->b.flags), &ib->b);
	set_phase(&new->b, test_bit(B_Phase1, &ib->b.flags));

	if (new->uninc->fileaddr == next) {
		struct block *b;
		/* This block really needs to be incorporated, or
		 * we might not be able to find it - it might be
		 * a primary for other blocks too.
		 * So do a minimal incorporation of this block.
		 * That must succeed as it will be an addition to an
		 * empty block, or a replacement.
		 */
		b = new->uninc;
		new->uninc = b->chain;
		b->chain = NULL;
		nbuf = map_iblock(new);
		cr = incorporate_index(b, nbuf, len);
		unmap_iblock(new, nbuf);
		LAFS_BUG(cr < 0, &ib->b);
		*credits += cr;
	}

	return 2;
}

static void filter_empties(struct block **uninc)
{
	/* If any block has phys==0 and there is another block
	 * with same address, incorp_index the phys==0 block
	 */
	struct block *b = *uninc;
	while (b && b->chain) {
		if (b->fileaddr == b->chain->fileaddr) {
			struct block *t;
			if (b->physaddr == 0) {
				t = b;
				*uninc = b->chain;
			} else if (b->chain->physaddr == 0) {
				t = b->chain;
				b->chain = t->chain;
			} else
				LAFS_BUG(1, b);
			t->chain = NULL;
			incorp_index(t);
		} else {
			uninc = &b->chain;
			b = *uninc;
		}
	}
}

/* Incorporate all the addresses in uninc_table into this
 * indexblock.  Each address carried a credit to allow for
 * indexblock splitting etc.
 * The block is already Dirty if it should go in the main
 * cluster, or Realloc if it should go in a cleaning cluster.
 */
void lafs_incorporate(struct fs *fs, struct indexblock *ib)
{
	int offset;	/* start of block where indexing is */
	struct indexblock *new = NULL;
	struct block *uninc = NULL;
	int rv = 0;
	char *buf;
	struct uninc uit;
	int blocksize = fs->blocksize;

	LAFS_BUG(!test_bit(B_IOLock, &ib->b.flags), &ib->b);

	LAFS_BUG(!test_bit(B_Dirty, &ib->b.flags) &&
		 !test_bit(B_Realloc, &ib->b.flags), &ib->b);

	LAFS_BUG(!test_bit(B_Valid, &ib->b.flags) && ib->depth > 0, &ib->b);

	if (ib->depth <= 1) {
		/* take a copy of the uninc_table so we can work while
		 * more changes can be made
		 */
		char *b;
		u16 type;
		u32 start;

		if (ib->uninc) {
			struct block *x = ib->uninc;
			while (x) {
				printk("x=%s\n", strblk(x));
				x = x->chain;
			}
		}
		LAFS_BUG(ib->uninc, &ib->b);

		spin_lock(&ib->b.inode->i_data.private_lock);
		memcpy(&uit, &ib->uninc_table, sizeof(uit));
		ib->uninc_table.pending_cnt = 0;
		ib->uninc_table.credits = 0;
		spin_unlock(&ib->b.inode->i_data.private_lock);

		if (test_bit(B_InoIdx, &ib->b.flags)) {
			offset = LAFSI(ib->b.inode)->metadata_size;
			if (LAFSI(ib->b.inode)->depth == 0) {
				/* data has already been copied
				 * out.. I hope - FIXME */
				LAFSI(ib->b.inode)->depth = 1;
				ib->depth = 1;
				lafs_clear_index(ib);
			}
		} else
			offset = 0;

		buf = map_iblock(ib) + offset;
		b = buf;
		type = decode16(b);
		start = decode32(b);

		if (uit.pending_cnt == 0) {
			unmap_iblock(ib, buf-offset);
			goto out;
		}
		/* Check it really is sorted */
		{
			int i;
			for (i=1; i<uit.pending_cnt; i++)
				if (uit.pending_addr[i].fileaddr <
				    uit.pending_addr[i-1].fileaddr +
				    uit.pending_addr[i-1].cnt)
					BUG();
		}

		if(!test_bit(B_Dirty, &ib->b.flags) &&
		   !test_bit(B_Realloc, &ib->b.flags))
			printk("bad %s\n", strblk(&ib->b));
		LAFS_BUG(!test_bit(B_Dirty, &ib->b.flags) &&
			 !test_bit(B_Realloc, &ib->b.flags), &ib->b);

		/* OK, we genuinely have work to do. */
		/* find size and type of current block */

		/* Maybe a direct update of an indirect block */
		if (type == IBLK_INDIRECT &&
		    incorporate_indirect(&uit, buf+2,
					 ib->b.fileaddr,
					 blocksize-(offset+2))) {
			unmap_iblock(ib, buf-offset);
			goto out;
		}

		/* Maybe a fairly direct update of an extent block */
		if (type == IBLK_EXTENT &&
		    incorporate_extent(&uit, buf+2,
				       blocksize-(offset+2))) {
			unmap_iblock(ib, buf-offset);
			goto out;
		}

		dprintk("Index contains:\n");
		if (lafs_trace)
			print_index(buf, ib->b.fileaddr, blocksize - offset);
		dprintk("uninc contains:\n");
		if (lafs_trace)
			lafs_print_uninc(&uit);

		unmap_iblock(ib, buf-offset);

	} else {
		int cred;
		uit.credits = 0;

		LAFS_BUG(ib->uninc_table.pending_cnt, &ib->b);
		spin_lock(&ib->b.inode->i_data.private_lock);
		uninc = ib->uninc;
		ib->uninc = NULL;
		spin_unlock(&ib->b.inode->i_data.private_lock);

		if (uninc == NULL)
			goto out;

		sort_blocks(&uninc);
		filter_empties(&uninc);

		LAFS_BUG(!test_bit(B_Dirty, &ib->b.flags) &&
			 !test_bit(B_Realloc, &ib->b.flags), &ib->b);

		/* OK, we genuinely have work to do. */
		/* find size and type of current block */
		buf = map_iblock(ib);

		if (test_bit(B_InoIdx, &ib->b.flags)) {
			offset = LAFSI(ib->b.inode)->metadata_size;
			buf += offset;
		} else
			offset = 0;

		dprintk("Index contains:\n");
		if (lafs_trace) {
			struct block *b;

			print_index(buf, ib->b.fileaddr, blocksize - offset);
			printk("uninc list:\n");
			for (b = uninc; b ; b=b->chain)
				printk("  %lu: %llu\n",
				       (unsigned long) b->fileaddr,
				       (unsigned long long) b->physaddr);
		}

		/* internal index block.  Might be able to merge in-place */
		if (*(u16 *)(buf) == cpu_to_le16(IBLK_INDEX) &&
		    (cred = incorporate_index(uninc, buf+2,
					      blocksize-(offset+2))) >= 0) {
			unmap_iblock(ib, buf-offset);
			uit.credits = cred;
			goto out;
		}
		unmap_iblock(ib, buf-offset);

	}
	/* Ok, those were the easy options. Now we need to allocate a
	 * new index block.
	 */
retry:
	new = lafs_iblock_alloc(fs, GFP_NOFS, 1, MKREF(inc));
	/* FIXME need to preallocate something for a fall-back?? */

	/* We lock the block despite that fact that it isn't
	 * shared, so that map_iblock won't complain */
	lafs_iolock_block(&new->b);

	if (ib->depth < 1)
		printk("small depth %s\n", strblk(&ib->b));
	LAFS_BUG(ib->depth < 1, &ib->b);
	if (ib->depth == 1)
		rv = do_incorporate_leaf(fs, ib, &uit, new);
	else
		rv = do_incorporate_internal(fs, ib, uninc, &uit.credits, new);

	switch (rv) {
	case 0:
		/* There is nothing in this block any more.
		 * If it is an inode, clear it, else punch a hole
		 */
		dprintk("incorp to empty off=%d %s\n",
			(int)offset, strblk(&ib->b));
		lafs_iounlock_block(&new->b);
		lafs_iblock_free(new);
		lafs_clear_index(ib);
		break;

	case 1: /* everything was incorporated - hurray */
		lafs_iounlock_block(&new->b);
		lafs_iblock_free(new);
		LAFS_BUG(!test_bit(B_Dirty, &ib->b.flags) &&
			 !test_bit(B_Realloc, &ib->b.flags), &ib->b);
		/* Don't need to dirty, it is already dirty */
		break;

	case 2: /* Simple split */
		/* The addresses are now in 'ib', 'new' and possibly
		 * new->uninc_table.  'new' has been linked in to the
		 * parent.
		 */
		uit.credits --;
		set_bit(B_Credit, &new->b.flags);
		if (uit.credits > 0) {
			set_bit(B_ICredit, &new->b.flags);
			uit.credits--;
		}

		lafs_dirty_iblock(new, !test_bit(B_Dirty, &ib->b.flags));
		lafs_iounlock_block(&new->b);
		temp_credits = uit.credits;
		putiref(new, MKREF(inc));
		break;
	case 3: /* Need to grow */
		/* new needs a B_Credit and a B_ICredit.
		 */

		uit.credits--;
		set_bit(B_Credit, &new->b.flags);
		if (uit.credits > 0){
			set_bit(B_ICredit, &new->b.flags);
			uit.credits--;
		}

		lafs_dirty_iblock(new, !test_bit(B_Dirty, &ib->b.flags));
		dprintk("Just Grew %s\n", strblk(&new->b));
		dprintk("     from %s\n", strblk(&ib->b));
		lafs_iounlock_block(&new->b);
		temp_credits = uit.credits;
		putiref(new, MKREF(inc));

		goto retry;
	}

out:
	if (uit.credits > 0 && !test_and_set_bit(B_UnincCredit, &ib->b.flags))
		uit.credits--;
	if (uit.credits > 0 && !test_and_set_bit(B_ICredit, &ib->b.flags))
		uit.credits--;
	if (uit.credits > 0 && !test_and_set_bit(B_NICredit, &ib->b.flags))
		uit.credits--;
	if (uit.credits < 0) {
		printk("Credits = %d, rv=%d\n", uit.credits, rv);
		printk("ib = %s\n", strblk(&ib->b));
	}
	temp_credits = 0;
	lafs_space_return(fs, uit.credits);

	/* If this index block is now empty and has no
	 * children which are not EmptyIndex, we flag it
	 * as EmptyIndex so index lookups know to avoid it.
	 * Dirty children also delay empty-handling.  This
	 * is important at the InoIdx level as we cannot drop
	 * level to 1 while there are still children that
	 * might want to be incorporated.
	 */
	if (ib->depth > 1 && ! lafs_index_empty(ib))
		goto out2;
	if (ib->depth > 1) {
		int non_empty = 0;
		struct block *b;
		spin_lock(&ib->b.inode->i_data.private_lock);
		list_for_each_entry(b, &ib->children, siblings)
			if (!test_bit(B_EmptyIndex, &b->flags) ||
			    test_bit(B_Dirty, &b->flags) ||
			    test_bit(B_Realloc, &b->flags)) {
				non_empty = 1;
				break;
			}
		spin_unlock(&ib->b.inode->i_data.private_lock);
		if (non_empty)
			goto out2;
	}
	if (test_bit(B_InoIdx, &ib->b.flags)) {
		/* Empty InoIdx blocks are allowed.  However depth must
		 * be 1.  This is where we suddenly collapse a now-empty
		 * indexing tree.
		 */
		if (ib->depth > 1) {
			ib->depth = 1;
			LAFSI(ib->b.inode)->depth = 1;
			lafs_clear_index(ib);
		}
		/* Don't need to dirty the inode - the fact that we just
		 * did incorporation should ensure it is already dirty
		 */
		goto out2;
	}
	if (ib->depth == 1 &&
	    (lafs_leaf_next(ib, 0) != 0xFFFFFFFF ||
	     !list_empty(&ib->children)))
		goto out2;

	/* Block is really really empty */
	set_bit(B_EmptyIndex, &ib->b.flags);
out2:
	return;
}

/***************************************************************
 * Space pre-allocation
 * We need to make sure that the block and all parents
 * have a full complement of space credits.
 * If this cannot be achieved, we can fail with either
 * -ENOSPC if we were allocating more space, or -EAGAIN.
 */

int __must_check lafs_prealloc(struct block *blk, int why)
{
	struct fs *fs = fs_from_inode(blk->inode);
	struct block *b;
	int need;
	int credits = 0;

	LAFS_BUG(why != CleanSpace && why != AccountSpace
		 && !test_phase_locked(fs)
		 && !fs->checkpointing, blk);

retry:
	need = 0;
	b = blk;

	while (b) {
		if (!test_bit(B_Dirty, &b->flags)) {
			if (credits <= 0)
				need += !test_bit(B_Credit, &b->flags);
			else if (!test_and_set_bit(B_Credit, &b->flags))
				credits--;
		}
		if (!test_bit(B_UnincCredit, &b->flags)) {
			if (credits <= 0)
				need += !test_bit(B_ICredit, &b->flags);
			else if (!test_and_set_bit(B_ICredit, &b->flags))
				credits--;
		}
		/* We need N*Credits if this block might need to be
		 * phase-flipped and remain pinned in the next
		 * phase.  i.e. index blocks and accounting blocks.
		 */
		if (test_bit(B_Index, &b->flags) ||
		    LAFSI(b->inode)->type == TypeQuota ||
		    LAFSI(b->inode)->type == TypeSegmentMap) {
			if (credits <= 0)
				need += !test_bit(B_NCredit, &b->flags);
			else if (!test_and_set_bit(B_NCredit, &b->flags))
				credits--;
			if (credits <= 0)
				need += !test_bit(B_NICredit, &b->flags);
			else if (!test_and_set_bit(B_NICredit, &b->flags))
				credits--;
		}
		if (test_bit(B_InoIdx, &b->flags))
			b = &LAFSI(b->inode)->dblock->b;
		else
			b = &b->parent->b;
	}
	dprintk("need=%d credits=%d for %s\n", need, credits, strblk(blk));
	if (need == 0) {
		/* all needed credits allocated */
		lafs_space_return(fs, credits);
		return 0;
	}

	if (lafs_space_alloc(fs, need, why)) {
		dprintk("Got them for %s\n", strblk(blk));
		/* We got the credits we asked for */
		credits = need;
		goto retry;
	}
	lafs_space_return(fs, credits);

	return -ENOSPC;
}
