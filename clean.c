
/*
 * fs/lafs/clean.c
 * Copyright (C) 2005-2010
 * Neil Brown <neilb@suse.de>
 * Released under the GPL, version 2
 */

#include "lafs.h"

/* mark_cleaning
 * Given a block that is in a segment that is being cleaner, mark
 * and pin it so that it gets cleaner.
 * This is written to cope with failure when allocating space, but in
 * the current design, that should never happen.
 */
static int mark_cleaning(struct block *b)
{
	int err;
	int credits;

	if (test_bit(B_Realloc, &b->flags))
		/* As cleaning is single threaded,
		 * we cannot race with another
		 * pin_dblock_clean here - the
		 * previous attempt must have succeeded
		 */
		return 0;
	err = lafs_reserve_block(b, CleanSpace);
	if (err)
		return err;
	LAFS_BUG(!test_bit(B_Valid, &b->flags), b);
	credits = lafs_space_alloc(fs_from_inode(b->inode), 2, CleanSpace);
	if (credits == 0)
		return -EAGAIN;
	if (!test_and_set_bit(B_Realloc, &b->flags))
		credits--;

	lafs_pin_block(b);
	if (test_bit(B_Dirty, &b->flags)) {
		/* If dirty, then now that it is pinned,
		 * it will get written, so don't need
		 * Realloc
		 */
		if (test_and_clear_bit(B_Realloc, &b->flags))
			credits++;
	}

	if (!test_and_set_bit(B_UnincCredit, &b->flags))
		credits--;
	lafs_space_return(fs_from_inode(b->inode), credits);

	lafs_refile(b, 0);
	lafs_refile(&b->parent->b, 0);
	return 0;
}

/* first_in_seg
 * Find the first block among this and its ancenstor which
 * is in the nominated segment - if any are.
 * As the cluster header does not differentiate between
 * index blocks of different depths, we need to check them
 * all.
 */
static struct block *first_in_seg(struct block *b, struct fs *fs,
				  int dev, u32 seg, REFARG)
{
	struct address_space *as = &b->inode->i_data;
	struct block *p;
	if (in_seg(fs, dev, seg, b->physaddr))
		return getref(b, REF);

	spin_lock(&as->private_lock);
	for (p = b;
	     p && !in_seg(fs, dev, seg, p->physaddr);
	     p = &(p->parent)->b) {
		if (test_bit(B_InoIdx, &p->flags)) {
			struct datablock *db = LAFSI(p->inode)->dblock;

			spin_unlock(&as->private_lock);
			as = &db->b.inode->i_data;
			spin_lock(&as->private_lock);
		}
	}
	if (p && test_bit(B_InoIdx, &p->flags)) {
		struct datablock *db = LAFSI(p->inode)->dblock;
		spin_unlock(&as->private_lock);
		as = &db->b.inode->i_data;
		spin_lock(&as->private_lock);
		p = &db->b;
	}

	if (p)
		getref_locked(p, REF);

	spin_unlock(&as->private_lock);
	return p;
}

/* To 'flush' the cleaner, anything on the fs->clean_leafs needs
 * to be either allocated to a cleaning-cluster or incorporated then added.
 * If the list gets empty we flush out the cleaning cluster and return.
 * The next time the cleaner runs it will come back here and do
 * some more flushing.
 */
static void cleaner_flush(struct fs *fs)
{
	struct block *b;
	int err = 0;
	dprintk("Start cleaner_flush\n");
	while (!err &&
	       (b = lafs_get_flushable(fs, -1)) != NULL) {
		int unlock = 1;

		dprintk("cleaning %s\n", strblk(b));

		if (test_bit(B_PinPending, &b->flags)) {
			/* Cannot safely clean this now.  Just mark
			 * it Dirty (it probably will be soon anyway)
			 * so it gets written to the new-data segment
			 * which will effectively clean it.
			 */
			if (!test_and_set_bit(B_Dirty, &b->flags))
				if (!test_and_clear_bit(B_Realloc, &b->flags))
					if (!test_and_clear_bit(B_Credit, &b->flags))
						LAFS_BUG(1, b);
		}
		if (test_bit(B_Dirty, &b->flags)) {
			/* Ignore this, checkpoint will take it */
			if (test_and_clear_bit(B_Realloc, &b->flags))
				if (test_and_set_bit(B_Credit, &b->flags))
					lafs_space_return(fs, 1);
		} else if (test_bit(B_Index, &b->flags) &&
			   (iblk(b)->uninc ||
			    iblk(b)->uninc_table.pending_cnt)) {
			lafs_incorporate(fs, iblk(b));
		} else {
			err = lafs_cluster_allocate(b, 1);
			unlock = 0;
		}
		if (unlock)
			lafs_iounlock_block(b);
		putref(b, MKREF(flushable));
	}
	lafs_cluster_flush(fs, 1);
}

/*
 * Load the next cluster header for this segment and perform
 * some validity checks.  If there is a failure, simply
 * update the 'tc' state so that it looks like there are
 * no more clusters to find.
 */
static void cleaner_load(struct fs *fs, struct toclean *tc)
{
	int err;
	err = lafs_load_page_async(fs, tc->chead,
				   tc->haddr,
				   (PAGE_SIZE /
				    fs->blocksize),
				   &tc->ac);

	BUG_ON(err && err != -EAGAIN && err != -EIO);

	if (err == -EAGAIN)
		return;
	if (err)
		//printk("CLEANER got IO error !!\n");
		// FIXME adjust youth to so as not to touch this again
		goto bad_header;

	tc->ch = page_address(tc->chead);
	if (memcmp(tc->ch->idtag, "LaFSHead", 8) != 0)
		goto bad_header;
	if (memcmp(tc->ch->uuid, fs->state->uuid, 16) != 0)
		goto bad_header;
	if (tc->seq == 0)
		tc->seq = le64_to_cpu(tc->ch->seq);
	else {
		tc->seq++;
		if (tc->seq != le64_to_cpu(tc->ch->seq)) {
			printk("Bad seq number\n");
			goto bad_header;
		}
	}
	if (lafs_calc_cluster_csum(tc->ch) != tc->ch->checksum) {
		//printk("Cluster header checksum is wrong!!\n");
		goto bad_header;
	}
	dprintk("try_clean: got header %d\n", (int)tc->haddr);
	return;
	
bad_header:
	tc->ac.state = 0;
	tc->have_addr = 0;
}

/* Parse a cluster header and identify blocks that might need cleaning.
 * Each time through we start parsing from the start.
 * As we find blocks, or reject inodes we update the header so that
 * the next time through we don't try those again.
 * Once we have started IO on 16 different inodes, we take a break
 * and let some of the IO complete.
 * As we find blocks, we put them on a list to be processed later.
 */
static void cleaner_parse(struct fs *fs, struct toclean *tc)
{
	u32 bnum;
	int bcnt;
	u32 inum, fsnum;
	u16 trunc;
	struct inode *ino = NULL;
	int again = 0;

	/* Always start at the beginning - we invalidate entries
	 * that we have finished with
	 */
	tc->gh = tc->ch->groups;
	tc->desc = tc->gh->u.desc;

	while (again < 16) {
		/* Load the index block for each described data or index block.
		 * For data blocks, get the address and possibly load the
		 * data block too.
		 * As blocks are loaded, they will be checked for membership
		 * in a current cleaning-segment and flagged for reallocation
		 * if appropriate.
		 */
		if ((((char *)tc->gh) - (char *)tc->ch)
		    >= le16_to_cpu(tc->ch->Hlength)) {
			/* Finished with that cluster, try another. */
			u64 next;
			if (again)
				/* Need to come back later */
				break;
			next = le64_to_cpu(tc->ch->next_addr);
			if (next > tc->haddr &&
			    in_seg(fs, tc->dev, tc->seg, next)) {
				tc->haddr = next;
				tc->ac.state = 1;
			} else {
				tc->ac.state = 0;
				tc->have_addr = 0;
			}
			tc->ch = NULL;
			lafs_wake_thread(fs);
			break;
		}
		if (((((char *)tc->desc) - (char *)tc->gh)+3)/4
		    >= le16_to_cpu(tc->gh->group_size_words)) {
			/* Finished with that group, try another */
			/* FIXME what if group has padding at end?
			 * this might be fixed, but need to be certain
			 * of all possibilities. */
			BUG_ON(le16_to_cpu(tc->gh->group_size_words) == 0);
			tc->gh = (struct group_head *)(((char *)tc->gh) +
						       le16_to_cpu(tc->gh->group_size_words)*4);
			tc->desc = tc->gh->u.desc;
			continue;
		}
		if (le16_to_cpu(tc->desc->block_bytes) > DescMiniOffset &&
		    tc->desc->block_bytes != DescIndex) {
			/* This is a miniblock, skip it. */
			int len = le16_to_cpu(tc->desc->block_bytes)
				- DescMiniOffset;
			tc->desc++;
			tc->desc = (struct descriptor *)
				(((char *)tc->desc)
				 + roundup(len, 4));
			continue;
		}
		/* Ok, desc seems to be a valid descriptor in this group */
		/* Try to load the index info for block_num in inode in filesys.
		 */
		// FIXME track phys block number for comparison
		// FIXME try to optimise out based on youth and snapshot age

		bnum = le32_to_cpu(tc->desc->block_num);
		bcnt = le16_to_cpu(tc->desc->block_cnt);
		if (bcnt == 0) {
			tc->desc++;
			continue;
		}
		inum = le32_to_cpu(tc->gh->inum);
		fsnum = le32_to_cpu(tc->gh->fsnum);
		trunc = le16_to_cpu(tc->gh->truncatenum_and_flag) & 0x7fff;

		if (inum == 0xFFFFFFFF &&
		    fsnum == 0xFFFFFFFF) {
			tc->desc++;
			continue;
		}
		dprintk("Cleaner looking at %d/%d %d+%d (%d)\n",
			(int)fsnum, (int)inum, (int)bnum, (int)bcnt,
			(int)le16_to_cpu(tc->desc->block_bytes));

		if (fsnum == 0 && inum == 0 && bnum == 0)
			goto skip;

		if (ino == NULL ||
		    ino->i_ino != inum ||
		    LAFSI(ino)->filesys->i_ino != fsnum) {
			if (ino)
				lafs_iput_fs(ino);
			if (fsnum) {
				struct inode *fsino =
					lafs_iget_fs(fs, 0, fsnum, ASYNC);
				if (IS_ERR(fsino))
					ino = fsino;
				else if (LAFSI(fsino)->md.fs.creation_age > tc->seq) {
					/* skip this inode as filesystem
					 * is newer
					 */
					lafs_iput_fs(fsino);
					ino = NULL;
					goto skip_inode;
				} else
					lafs_iput_fs(fsino);
			}
			ino = lafs_iget_fs(fs, fsnum, inum, ASYNC);
		}
		if (!IS_ERR(ino)) {
			int itrunc;
			struct datablock *b;
			dprintk("got the inode\n");
			/* Minor optimisation for files that have shrunk */
			/* Actually this is critical for handling truncation
			 * properly.  We don't want to even 'get' a block beyond
			 * EOF, certainly not after the truncate_inode_pages.
			 */
			if (LAFSI(ino)->type == 0 ||
			    (LAFSI(ino)->type >= TypeBase &&
			     ((loff_t)bnum << ino->i_blkbits) >= i_size_read(ino))) {
				/* skip the whole descriptor */
				bcnt = 1;
				goto skip;
			}
			itrunc = ((ino->i_generation<<8) |
				  (LAFSI(ino)->trunc_gen & 0xff)) & 0x7fff;
			if (itrunc != trunc) {
				/* file has been truncated or replaced since
				 * this cluster
				 */
				goto skip_inode;
			}
			b = lafs_get_block(ino, bnum, NULL, GFP_NOFS,
					   MKREF(cleaning));
			if (b == NULL)
				break;

			if (!test_and_set_bit(B_Cleaning, &b->b.flags)) {
				getdref(b, MKREF(cleaning));
				lafs_igrab_fs(ino);
			}
			if (LAFSI(ino)->type == TypeInodeFile ||
			    LAFSI(ino)->type == TypeDir) {
				/* Could become an orphan just now, so need
				 * to protect b->cleaning 
				 */
				spin_lock(&fs->lock);
				list_move_tail(&b->cleaning, &tc->cleaning);
				spin_unlock(&fs->lock);
			} else {
				/* No locking needed */
				if (list_empty(&b->cleaning))
					list_add_tail(&b->cleaning, &tc->cleaning);
			}

			/* We can race with truncate here, so need to check
			 * i_size again now that b->cleaning is non-empty.
			 * The thread doing the truncate will have to lock
			 * the page holding this block, which should be enough
			 * of a barrier so that if it sets i_size after now,
			 * it will see that b->cleaning is non-empty.
			 * We need to be sure that if it sets it before now,
			 * we get to see i_size.
			 * So I think a memory barrier is a good idea...
			 */
			mb();

			if (LAFSI(ino)->type == 0 ||
			    (LAFSI(ino)->type >= TypeBase &&
			     ((loff_t)bnum << ino->i_blkbits)
			     >= i_size_read(ino))) {
				list_del_init(&b->cleaning);
				if (test_and_clear_bit(B_Cleaning, &b->b.flags)) {
					putdref(b, MKREF(cleaning));
					lafs_iput_fs(ino);
				}
			}
			putdref(b, MKREF(cleaning));
		} else  {
			int err = PTR_ERR(ino);
			ino = NULL;
			dprintk("iget gives error %d\n", err);
			if (err == -EAGAIN) {
				again++;
				tc->desc++;
				continue;
			}
			/* inode not found, make sure we never
			 * look for it again
			 */
		skip_inode:
			tc->gh->inum = 0xFFFFFFFF;
			tc->gh->fsnum = 0xFFFFFFFF;
			tc->desc++;
			continue;
		}
	skip:
		/* We modify the descriptor in-place to track where
		 * we are up to.  This is a private copy.  The real
		 * descriptor doesn't change.
		 */
		tc->desc->block_num = cpu_to_le32(bnum+1);
		tc->desc->block_cnt = cpu_to_le16(bcnt-1);
	}
	if (ino)
		lafs_iput_fs(ino);
}

/* Process all blocks that have been found to possibly need to be
 * moved from their current address (i.e. cleaned).
 * We initiate async index lookup, then if the block really
 * is in the target segment we initiate async read.  Once
 * that is complete we mark the block for cleaning and
 * releaes it.
 */
static int cleaner_process(struct fs *fs, struct toclean *tc)
{
	struct datablock *b, *tmp;
	int rv = 0;
	struct inode *ino;

	dprintk("start processing list\n");
	list_for_each_entry_safe(b, tmp, &tc->cleaning, cleaning) {
		struct block *cb = NULL;
		int err = lafs_find_block_async(b);
		dprintk("find_async gives %d %s\n", err, strblk(&b->b));
		if (err == -EAGAIN)
			continue;
		if (err)
			/* Eeek, what do I do?? */
			goto done_cleaning;

		cb = first_in_seg(&b->b, fs, tc->dev, tc->seg, MKREF(clean2));

		if (cb == NULL) {
			/* Moved, don't want this. */
			dprintk("Not in seg\n");
			goto done_cleaning;
		}
		err = lafs_load_block(cb, NULL);
		if (err)
			goto done_cleaning;

		err = lafs_wait_block_async(cb);
		if (err == -EAGAIN) {
			putref(cb, MKREF(clean2));
			continue;
		}
		if (err)
			goto done_cleaning;

		err = mark_cleaning(cb);
		dprintk("Want to clean %s (%d)\n",
			strblk(cb), err);
		if (err)
			rv = -1;

		/* as cb is B_Pinned, it holds an effective
		 * ref on the inode, so it is safe to drop our
		 * ref now
		 */
	done_cleaning:
		clear_bit(B_Cleaning, &b->b.flags);

		list_del_init(&b->cleaning);
		if (test_bit(B_Orphan, &b->b.flags)) {
			spin_lock(&fs->lock);
			if (test_bit(B_Orphan, &b->b.flags) &&
			    list_empty(&b->orphans)) {
				list_add(&b->orphans, &fs->pending_orphans);
				lafs_wake_thread(fs);
			}
			spin_unlock(&fs->lock);
		}

		ino = b->b.inode;
		putdref(b, MKREF(cleaning));
		lafs_iput_fs(ino);
		putref(cb, MKREF(clean2));
		if (rv)
			break;
	}
	return rv;
}

/*
 * Try to advance the process of cleaning the given segment.
 * This may require loading a cluster head, parsing or reparsing
 * that head, or loading some blocks.
 */
static int try_clean(struct fs *fs, struct toclean *tc)
{
	/* return 1 if everything has been found, -ve if we need to flush */
	int rv = 0;

	mutex_lock(&fs->cleaner.lock);
	dprintk("try_clean: state = %d\n", tc->ac.state);
	if (tc->ch == NULL && tc->have_addr)
		cleaner_load(fs, tc);

	if (tc->ch)
		cleaner_parse(fs, tc);

	if (!list_empty(&tc->cleaning))
		rv = cleaner_process(fs, tc);
	if (!rv)
		rv = (tc->ch == NULL && !tc->have_addr &&
		      list_empty(&tc->cleaning));
	mutex_unlock(&fs->cleaner.lock);
	return rv;
}

/*
 * When we truncate a file, and block that is in the
 * process of being cleaned must have that cleaning
 * cancelled.  That is done by lafs_erase_dblock calling
 * lafs_unclean.
 */
void lafs_unclean(struct datablock *db)
{
	if (!list_empty_careful(&db->cleaning)) {
		struct fs *fs = fs_from_inode(db->b.inode);
		mutex_lock(&fs->cleaner.lock);
		if (test_and_clear_bit(B_Cleaning, &db->b.flags)) {
			/* This must be on the cleaner list, so
			 * it is safe to delete without a spinlock
			 */
			list_del_init(&db->cleaning);
			putdref(db, MKREF(cleaning));
			lafs_iput_fs(db->b.inode);
			if (test_and_clear_bit(B_Async, &db->b.flags)) {
				putdref(db, MKREF(async));
				lafs_wake_thread(fs);
			}
			if (test_bit(B_Orphan, &db->b.flags)) {
				spin_lock(&fs->lock);
				if (test_bit(B_Orphan, &db->b.flags) &&
				    list_empty(&db->orphans)) {
					list_add(&db->orphans, &fs->pending_orphans);
					lafs_wake_thread(fs);
				}
				spin_unlock(&fs->lock);
			}
		}
		mutex_unlock(&fs->cleaner.lock);
	}
}

unsigned long lafs_do_clean(struct fs *fs)
{
	/*
	 * If the cleaner is inactive, we need to decide whether to
	 * activate.  This depends on the amount of free space that
	 * is tied up in dirty segments.
	 * If we decide to activate, we collect a few segments and
	 * start work on them.
	 *
	 * If the cleaner is active we simply try to progress any activity.
	 * Any activity may trigger an async read in which case we
	 * leave it and move on.
	 *
	 * (Most of this happens in try_clean() )
	 * - If we have chosen a segment/cluster but haven't loaded the
	 *   cluster head, load the cluster head.
	 * - If we have loaded a cluster head but haven't processed all
	 *   of it, process some more into a list of block addresses
	 * - If we have loaded a cluster head and have processed all
	 *   of it, select the next cluster in the segment, or forget
	 *   the segment.
	 * - If we have un-processed block addresses in some snapshots
	 *   Try to find the current addresses of those blocks
	 * - If we found blocks in the current segment, read them in
	 * - If we have them read in, mark them for reallocation.
	 * - If all done, process realloc_leafs and allocate to clean cluster.
	 *
	 */
	if (!fs->cleaner.active &&
	    !test_bit(CheckpointNeeded, &fs->fsstate) &&
	    !test_bit(CleanerDisabled, &fs->fsstate)) {
		/* Choose to clean when the fraction of all space that is clean
		 * is below the fraction of free space that is not clean.
		 * i.e. if T is total space, C is clean space, F is free space,
		 * then clean when C/T < (F-C)/F
		 * So as the amount of clean space decreases we are less tolerant
		 * of unavailable free space.
		 * Avoiding division, this is
		 *       C * F < T * (F - C)
		 * As we always reserve 3 clean segments for accounting overhead
		 * and 1 to ensure we can handle deletions,  we exclude those
		 * clean segments from the calculations.
		 * i.e. subtract 4 segments from T, C and F
		 *
		 * T we know from the size of the devices
		 * C we know by counting the clean segments
		 * F we count each time we scan the segments. (total_free)
		 *  We used the largest count of last pass and this pass.
		 *
		 * We need to avoid cleaning too much in one checkpoint as
		 *  the free counts will start to get misleading.
		 * Maybe every time we choose to clean a segment, we add the
		 * size of the segment to some counter and add that to C in the
		 * above calculations.
		 *
		 * For now, clean up to 4 segments at a time.
		 */
		int i, max_segs;
		u64 T = 0;
		int force_checkpoint_after_clean = 0;

		for (i = 0; i < fs->devices; i++)
			T += fs->devs[i].size;

		T -= TOTAL_RESERVED * fs->max_segment;

		max_segs = lafs_alloc_cleaner_segs(fs, CLEANER_SEGS);
		if (max_segs < 1) {
			/* If we can only clean to main segment, we may
			 * have to.  However:
			 *  - only do one segment at a time
			 *  - only if there are no clean (but not yet free) segments
			 *  - if CleanerBlocks, then clean.
			 *  otherwise don't.
			 */
			if (fs->segtrack->clean.cnt == 0
			    && test_bit(CleanerBlocks, &fs->fsstate)) {
				max_segs = 1;
				force_checkpoint_after_clean = 1;
			}
		}
		for (i = 0; i < max_segs; i++) {
			struct toclean *tc = &fs->cleaner.seg[i];
			u64 C = fs->free_blocks + fs->clean_reserved
				+ fs->cleaner.cleaning;
			u64 F = max(fs->total_free, fs->total_free_prev);

			if (TOTAL_RESERVED * fs->max_segment < C)
				C -= TOTAL_RESERVED * fs->max_segment; /* adjust to unusable space FIXME adjust F too? */
			else
				C = 0;
			if (TOTAL_RESERVED * fs->max_segment < F)
				F -= TOTAL_RESERVED * fs->max_segment; /* adjust to unusable space FIXME adjust F too? */
			else
				F = 0;

			dprintk("C=%llu F=%llu T=%llu\n", C, F, T);
			if ((F < C || C * F >= T * (F - C)) &&
			    !test_bit(EmergencyClean, &fs->fsstate) &&
			    !test_bit(EmergencyPending, &fs->fsstate) &&
			    !test_bit(CleanerBlocks, &fs->fsstate)) {
				dprintk("CLEANER: enough cleaning with %d segments\n",
					i);
				break;
			}

			if (tc->chead == NULL)
				continue;

			/* OK, we are good to keep cleaning */
			tc->have_addr = lafs_get_cleanable(fs, &tc->dev, &tc->seg);
			if (!tc->have_addr) {
				dprintk("CLEANER: Nothing found to clean at %d :-(\n",
					i);
				if (i == 0 && (test_bit(EmergencyPending, &fs->fsstate) ||
					       test_bit(CleanerBlocks, &fs->fsstate)))
					lafs_checkpoint_start(fs);
				break;
			}
			printk("CLEANER: clean %d/%d\n", tc->dev, tc->seg);
			fs->cleaner.cleaning += fs->devs[tc->dev].segment_size /* - 1*/;
			tc->haddr = segtovirt(fs, tc->dev, tc->seg);
			tc->gh = NULL;
			tc->desc = NULL;
			tc->ac.state = 1;
			tc->seq = 0;
			INIT_LIST_HEAD(&tc->cleaning);
			fs->cleaner.active = 1;
			if (force_checkpoint_after_clean)
				lafs_checkpoint_start(fs);
		}
		if (i == CLEANER_SEGS)
			dprintk("CLEANER: found %d segments to clean\n", i);
	}
	if (fs->cleaner.active) {
		int cnt = 0;
		int i;
		int doflush = 1;
		for (i = 0; i < CLEANER_SEGS ; i++) {
			struct toclean *tc = &fs->cleaner.seg[i];
			if (tc->have_addr || !list_empty(&tc->cleaning)) {
				/* Might be something to do here */
				int done = try_clean(fs, tc);
				if (done < 0)
					doflush = 2;
				if (done == 0 && doflush == 1)
					doflush = 0;
				cnt++;
			}
		}
		if (doflush)
			cleaner_flush(fs);
		if (cnt == 0) {
			fs->cleaner.active = 0;
			lafs_wake_thread(fs);
		}
	}
	if (test_bit(CleanerBlocks, &fs->fsstate)) {
		int any_clean;
		int clean = lafs_clean_count(fs, &any_clean);
		dprintk("clean=%d max_seg=%d need=%d act=%d any=%d\n", (int)clean,
			(int)fs->max_segment, (int)fs->cleaner.need, fs->cleaner.active, any_clean);
		if (any_clean) {
			/* If there is enough clean space for everything to move
			 * forward, or the cleaner has done all it can, then
			 * push out a checkpoint so threads waiting on the cleaner
			 * can proceed
			 */
			if (clean * fs->max_segment
			    >= fs->allocated_blocks + fs->cleaner.need
			    ||
			    !fs->cleaner.active)
				lafs_checkpoint_start(fs);
		}
	}
	return MAX_SCHEDULE_TIMEOUT;
}
