
Future ideas for LaFS
Things worth exploring, but not on the immediate horizon.

- Make sure all data structures are versioned.  This allows
  more experimentation.

- Duplicate all metadata blocks and include a checksum in the index.
  This means that an index block stores:
    fileaddr  phys1   phys2   checksum.

  If phys1 cannot be read or doesn't match checksum, try phys2.
  Need to figure how best to write second copy at some distance
  efficiently.

  Also, indirect blocks would need a version that stored an array of
    phys1 phys2 checksum.
  Maybe just use same format as checksumed-index blocks so sparse directories
  are handled well.

- If top bit of 'phys' is set, then the address is that of a cluster header.
  Read that to find the data.  It could be a miniblock for a tail, or it
  could be compressed.

- non-logged files without non-logged segments.
  These do not benefit from snapshots or from better management of RAID4
  But could have advantages.

  Possibly do a non-logged write if old location is more recent than
  most recent snapshot.

- Single-block segments.
  The segment usage counter would be one bit - plus error info.
  So make it 32 (16?) bits to cover all snapshots, and have 2 bits
  for error flags?

  Could use these only for cleaned data if the level-0 device is
  NVRAM or similar.

  Could have a different cluster header that explicitly lists all
  physical blocks.  Possibly use compression to use only a couple of bytes most of the time.
