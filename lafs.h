
/*
 * fs/lafs/lafs.h
 * Copyright (C) 2005-2009
 * Neil Brown <neilb@suse.de>
 * Released under the GPL, version 2
 */

#include	<linux/blkdev.h>
#include	<linux/fs.h>
#include	<linux/highmem.h>

#include	"layout.h"
#include	"state.h"

extern int lafs_trace;
#define dprintk(x...) do { if(lafs_trace)printk(x); }while(0)

#define LAFS_BUG(cond, b) do { if (cond) { printk(KERN_ERR "%s:%d: %s\n", __FILE__,__LINE__,strblk(b));BUG();}}while(0)

#ifdef DUMP
extern struct fs *dfs;
extern struct freelists {
	struct list_head	lru;
	unsigned long		freecnt;
} freelist;
extern void lafs_dump_tree(void);
#endif

#if DEBUG_REF
#define REFARG char *__refname
#define REF     __refname
#define MKREF(name) #name

#define add_ref(a,b,c,d) lafs_add_ref(a,b,c,d)
#define del_ref(a,b,c,d) lafs_del_ref(a,b,c,d)
#define has_ref(a,b) lafs_has_ref(a,b)

#define lafs_get_block(a,b,c,d,e) _lafs_get_block(a,b,c,d,e)
#define first_in_seg(a,b,c,d,e) _first_in_seg(a,b,c,d,e)
#define dir_lookup_blk(a,b,c,d,e,f,g) _dir_lookup_blk(a,b,c,d,e,f,g)
#define lafs_iblock_alloc(a,b,c,d) _lafs_iblock_alloc(a,b,c,d)
#define ihash_lookup(a,b,c,d,e) _ihash_lookup(a,b,c,d,e)
#define iblock_get(a,b,c,d,e) _iblock_get(a,b,c,d,e)
#define lafs_make_iblock(a,b,c,d) _lafs_make_iblock(a,b,c,d)
#define lafs_leaf_find(a,b,c,d,e,f) _lafs_leaf_find(a,b,c,d,e,f)
#define lafs_inode_dblock(a,b,c) _lafs_inode_dblock(a,b,c)
#define lafs_inode_get_dblock(a,b) _lafs_inode_get_dblock(a,b)
#else
#define REFARG void
#define REF    0
#define MKREF(name) 0

#define add_ref(a,b,c,d) do {} while (0)
#define del_ref(a,b,c,d) do {} while (0)
#define has_ref(a,b) (-1)

#define lafs_get_block(a,b,c,d,e) _lafs_get_block(a,b,c,d)
#define first_in_seg(a,b,c,d,e) _first_in_seg(a,b,c,d)
#define dir_lookup_blk(a,b,c,d,e,f,g) _dir_lookup_blk(a,b,c,d,e,f)
#define lafs_iblock_alloc(a,b,c,d) _lafs_iblock_alloc(a,b,c)
#define ihash_lookup(a,b,c,d,e) _ihash_lookup(a,b,c,d)
#define iblock_get(a,b,c,d,e) _iblock_get(a,b,c,d)
#define lafs_make_iblock(a,b,c,d) _lafs_make_iblock(a,b,c)
#define lafs_leaf_find(a,b,c,d,e,f) _lafs_leaf_find(a,b,c,d,e)
#define lafs_inode_dblock(a,b,c) _lafs_inode_dblock(a,b)
#define lafs_inode_get_dblock(a,b) _lafs_inode_get_dblock(a)
#endif
#define strblk(a) lafs_strblk(a)

static inline int
u32_after(u32 a, u32 b)
{
	return ((long)a-(long)b)>0;
}

const extern struct inode_operations lafs_file_ino_operations;
const extern struct inode_operations lafs_dir_ino_operations;
const extern struct inode_operations lafs_subset_ino_operations;
const extern struct inode_operations lafs_link_ino_operations;
const extern struct inode_operations lafs_special_ino_operations;
const extern struct file_operations lafs_file_file_operations;
const extern struct file_operations lafs_dir_file_operations;
const extern struct file_operations lafs_subset_file_operations;
const extern struct address_space_operations lafs_file_aops;
const extern struct address_space_operations lafs_index_operations;

#define SYNC 0
#define ASYNC 1

#define NOADOPT 0
#define ADOPT 1

typedef u32 faddr_t;
typedef u64 paddr_t;
typedef u64 vaddr_t;
int lafs_sync_page_io(struct block_device *bdev, sector_t sector, int offset,
		      int size, struct page *page, int rw);
int lafs_load_page(struct fs *fs, struct page *p, u64 vaddr, int blocks);
int lafs_load_pages(struct fs *fs, struct page *p, u64 vaddr, int blocks);
int lafs_load_page_async(struct fs *fs, struct page *p, u64 vaddr, int blocks,
			 struct async_complete *ac);
int __must_check lafs_load_block(struct block *b, struct bio *bio);
int __must_check lafs_wait_block(struct block *b);
int __must_check lafs_wait_block_async(struct block *b);
int __must_check lafs_find_block(struct datablock *b, int adopt);
int __must_check lafs_find_block_async(struct datablock *b);
int __must_check lafs_read_block(struct datablock *b);
int __must_check lafs_read_block_async(struct datablock *b);
int __must_check lafs_find_next(struct inode *b, loff_t *bnum);
struct indexblock *lafs_leaf_find(struct inode *inode, u32 addr,
				  int adopt, u32 *next, int async, REFARG);
u32 lafs_leaf_next(struct indexblock *ib, u32 start);
int lafs_index_empty(struct indexblock *ib);
#ifdef DEBUG_IOLOCK
#define set_iolock_info(b) ( (b)->iolock_file = __FILE__, (b)->iolock_line = __LINE__)
#else
#define set_iolock_info(b) (0)
#endif
#define lafs_iolock_block(b) do { _lafs_iolock_block(b); set_iolock_info(b); } while(0)
#define lafs_iolock_block_async(b) ( _lafs_iolock_block_async(b) ? ( set_iolock_info(b), 1) : 0)
#define lafs_iolock_written(b) do { _lafs_iolock_written(b); set_iolock_info(b); } while(0)
#define lafs_iolock_written_async(b) ( _lafs_iolock_written_async(b) ? ( set_iolock_info(b), 1) : 0)

void _lafs_iolock_block(struct block *b);
void _lafs_iolock_written(struct block *b);
int _lafs_iolock_block_async(struct block *b);
int _lafs_iolock_written_async(struct block *b);

void lafs_iounlock_block(struct block *b);
void lafs_iocheck_block(struct datablock *db, int unlock);
void lafs_iocheck_writeback(struct datablock *db, int unlock);
void lafs_writeback_done(struct block *b);

void lafs_super_write(struct fs *fs, int dev, u64 addr, char *buf, int size);
int lafs_super_wait(struct fs *fs);

/* inode.c */
void lafs_add_atime_offset(struct timespec *atime, int offset);
int __must_check lafs_mount(struct fs *fs);
struct inode *lafs_iget(struct inode *filesys, ino_t inum, int async);
struct inode *lafs_iget_fs(struct fs *fs, int fsnum, int inum, int async);
int __must_check lafs_import_inode(struct inode *ino, struct datablock *b);
void lafs_inode_checkpin(struct inode *ino);
void lafs_evict_inode(struct inode *ino);
void lafs_dirty_inode(struct inode *ino);
int lafs_sync_inode(struct inode *ino, int wait);
struct inode *lafs_new_inode(struct fs *fs, struct inode *fsys,
			     struct inode *dir, int type,
			     int inum, int mode, struct datablock **inodbp);
int lafs_lock_inode(struct inode *ino);
void lafs_inode_fillblock(struct inode *ino);
struct datablock *lafs_inode_dblock(struct inode *ino, int async, REFARG);
struct datablock *lafs_inode_get_dblock(struct inode *ino, REFARG);
int lafs_inode_handle_orphan(struct datablock *b);
int lafs_inode_inuse(struct fs *fs, struct inode *fsys, u32 inum);

static inline void lafs_iput_fs(struct inode *ino)
{
	struct super_block *sb = ino->i_sb;
	if (!test_bit(I_Deleting, &LAFSI(ino)->iflags))
		iput(ino);
	deactivate_super(sb);
}

static inline void lafs_igrab_fs(struct inode *ino)
{
	if (igrab(ino) == NULL &&	
	    !test_bit(I_Deleting, &LAFSI(ino)->iflags))
		BUG();
	atomic_inc(&ino->i_sb->s_active);
}

struct datablock *lafs_get_block(struct inode *ino, unsigned long index,
				 struct page *p, int gfp, REFARG);
#if DEBUG_REF
void add_ref(struct block *b, char *ref, char *file, int line);
void del_ref(struct block *b, char *ref, char *file, int line);
int has_ref(struct block *b, char *ref);
#endif

int lafs_setattr(struct dentry *dentry, struct iattr *attr);
int lafs_getattr(struct vfsmount *mnt, struct dentry *denty,
		 struct kstat *stat);
void lafs_fillattr(struct inode *ino, struct kstat *stat);

char *strblk(struct block *b);
int lafs_print_tree(struct block *b, int depth);
int lafs_dev_find(struct fs *fs, u64 virt);

static inline void
virttoseg(struct fs *fs, u64 virt, int *devp, u32 *segp, u32 *offsetp)
{
	int d = lafs_dev_find(fs, virt);
	struct fs_dev *dv = &fs->devs[d];
	BUG_ON(d<0);
	virt -= dv->start;
	if (dv->segment_size >= dv->width * dv->stride) {
		*offsetp = do_div(virt, dv->segment_size);
		*segp = virt;
	} else {
		u64 v2 = virt;
		int of = do_div(v2,  dv->stride);
		int strp;
		v2 = virt;
		do_div(v2, dv->width * dv->stride);
		strp = v2;

		*segp = (strp * dv->stride + of) /
			(dv->segment_size / dv->width);
		*offsetp = virt - dv->segment_stride * *segp;
	}
	*devp = d;
}

static inline u64 segtovirt(struct fs *fs, int dev, u32 segnum)
{
	return fs->devs[dev].start +
		(u64)fs->devs[dev].segment_stride * segnum;
}

static inline int
in_seg(struct fs *fs, int d, u32 seg, u64 virt)
{
	struct fs_dev *dv = &fs->devs[d];

	if (virt == 0)
		return 0;
	if (virt < dv->start ||
	    virt >= dv->start + dv->size)
		return 0;

	virt -= dv->start;
	if (dv->segment_size >= dv->width * dv->stride) {
		do_div(virt, dv->segment_size);
		return seg == virt;
	} else {
		u64 v2 = virt;
		int of = do_div(v2,  dv->stride);
		int strp;
		v2 = virt;
		do_div(v2, dv->width * dv->stride);
		strp = v2;

		return seg == ((strp * dv->stride + of) /
			       (dv->segment_size / dv->width));
	}
}

static inline void
virttophys(struct fs *fs, u64 virt, int *devp, sector_t *sectp)
{
	int d = lafs_dev_find(fs, virt);
	struct fs_dev *dv = &fs->devs[d];

	*devp = d;
	if (d < 0) return;

	virt -= dv->start;
	virt <<= (fs->blocksize_bits - 9);
	virt += (dv->segment_offset)>>9;
	*sectp = virt;
}

static inline int dblock_offset(struct datablock *b)
{
	return (b - (struct datablock*)b->page->private)
		<< b->b.inode->i_blkbits;
}

#if 0
static inline int iblock_offset(struct indexblock *b)
{
	return (b - (struct indexblock*)b->b.page->private)
		<< b->b.inode->i_blkbits;
}

static inline int block_offset(struct block *b)
{
	if (test_bit(B_Index, &b->flags))
		return iblock_offset(iblk(b));
	else
		return dblock_offset(dblk(b));
}
#endif

static inline void *map_dblock(struct datablock *b)
{
	void *a = kmap_atomic(b->page, KM_USER0);
	a += dblock_offset(b);
	return a;
}

static inline void unmap_dblock(struct datablock *b, void *buf)
{
	kunmap_atomic(buf - dblock_offset(b), KM_USER0);
}

static inline void *map_dblock_2(struct datablock *b)
{
	void *a = kmap_atomic(b->page, KM_USER1);
	a += dblock_offset(b);
	return a;
}

static inline void unmap_dblock_2(struct datablock *b, void *buf)
{
	kunmap_atomic(buf - dblock_offset(b), KM_USER1);
}

static inline void *map_iblock(struct indexblock *b)
{
	LAFS_BUG(!test_bit(B_IOLock, &b->b.flags), &b->b);
	if (test_bit(B_InoIdx, &b->b.flags))
		return map_dblock(LAFSI(b->b.inode)->dblock);
	return b->data;
}

static inline void unmap_iblock(struct indexblock *b, void *buf)
{
	if (test_bit(B_InoIdx, &b->b.flags))
		unmap_dblock(LAFSI(b->b.inode)->dblock, buf);
}

static inline void *map_iblock_2(struct indexblock *b)
{
	if (test_bit(B_InoIdx, &b->b.flags))
		return map_dblock_2(LAFSI(b->b.inode)->dblock);
	return b->data;
}

static inline void unmap_iblock_2(struct indexblock *b, void *buf)
{
	if (test_bit(B_InoIdx, &b->b.flags))
		unmap_dblock_2(LAFSI(b->b.inode)->dblock, buf);
}

static inline void decode_time(struct timespec *ts, u64 te)
{
	/* low 35 bits are seconds (800 years)
	 * high 29 bits are 2nanoseconds
	 */
	ts->tv_sec = te& (0x7FFFFFFFFULL);
	ts->tv_nsec = (te>>34) & ~(long)1;
}

static inline u64 encode_time(struct timespec *ts)
{
	u64 t, tn;
	t = ts->tv_sec;
	t &= (0x7FFFFFFFFULL);
	tn = ts->tv_nsec & ~(long)1;
	tn <<= 34;
	return t | tn;
}

/* s_fs_info points to an allocated sb_key structure */
struct sb_key {
	struct inode *root;
	struct fs *fs;
};

static inline struct fs *fs_from_sb(struct super_block *sb)
{
	struct sb_key *k = sb->s_fs_info;
	return k->fs;
}

static inline struct fs *fs_from_inode(struct inode *ino)
{
	return fs_from_sb(ino->i_sb);
}

static inline int set_phase(struct block *b, int ph)
{
	if (b->inode->i_ino == 0 && b->fileaddr == 0)
		dprintk("SETPHASE %s to  %d\n", strblk(b), ph);
	if (ph)
		set_bit(B_Phase1, &b->flags);
	else
		clear_bit(B_Phase1, &b->flags);

	/* FIXME do I need to lock access to ->parent */
	if (!test_and_set_bit(B_Pinned, &b->flags) &&
	    b->parent) {
		atomic_inc(&b->parent->pincnt[ph]);
		return 1;
	} else
		return 0;
}

/*
 * db->my_inode is protected by rcu. We can 'get' it and
 * remain rcu_protected, or 'iget' it and be protected by a
 * refcount
 */
static inline struct inode *rcu_my_inode(struct datablock *db)
{
	struct inode *ino;
	if (db == NULL)
		return NULL;
	if (LAFSI(db->b.inode)->type != TypeInodeFile)
		return NULL;
	rcu_read_lock();
	ino = rcu_dereference(db->my_inode);
	if (ino)
		return ino;
	rcu_read_unlock();
	return NULL;
}
static inline void rcu_iput(struct inode *ino)
{
	if (ino)
		rcu_read_unlock();
}

static inline struct inode *iget_my_inode(struct datablock *db)
{
	struct inode *ino = rcu_my_inode(db);
	struct inode *rv = NULL;
	if (ino)
		rv = igrab(ino);
	rcu_iput(ino);
	return rv;
}

/*
 * blocks (data and index) are reference counted.
 * 'getref' increments the reference count, and could remove the
 *     block from any 'lru' list.  However to save effort, we simply
 *     treat anything on an lru list which has a non-zero reference
 *     count as invisible.
 * 'putref' drops the count and calls lafs_refile to see if anything
 *     has changed.
 */
int lafs_is_leaf(struct block *b, int ph);
void lafs_refile(struct block *b, int dec);

extern struct indexblock *lafs_getiref_locked(struct indexblock *ib);

extern spinlock_t lafs_hash_lock;

static inline struct block *__getref(struct block *b)
{
	if (b)
		atomic_inc(&b->refcnt);
	return b;
}

static inline struct block *_getref(struct block *b)
{
	LAFS_BUG(b && atomic_read(&b->refcnt) == 0, b);
	return __getref(b);
}

static inline struct datablock *_getdref_locked(struct datablock *b)
{
	LAFS_BUG(!spin_is_locked(&b->b.inode->i_data.private_lock), &b->b);
	__getref(&b->b);
	return b;
}

static inline struct block *_getref_locked(struct block *b)
{
	LAFS_BUG(!spin_is_locked(&b->inode->i_data.private_lock), b);
	if (test_bit(B_InoIdx, &b->flags))
		return &lafs_getiref_locked(iblk(b))->b;
	__getref(b);
	return b;
}

static inline struct indexblock *_getiref_locked_needsync(struct indexblock *b)
{
	LAFS_BUG(!spin_is_locked(&lafs_hash_lock), &b->b);
	if (test_bit(B_InoIdx, &b->b.flags))
		return lafs_getiref_locked(b);
	__getref(&b->b);
	return b;
}

static inline struct block *_getref_locked_needsync(struct block *b)
{
	LAFS_BUG(!spin_is_locked(&fs_from_inode(b->inode)->lock), b);
	if (test_bit(B_InoIdx, &b->flags))
		return &lafs_getiref_locked(iblk(b))->b;
	__getref(b);
	return b;
}

static inline void _putref(struct block *b)
{
	if (!b)
		return;
	BUG_ON(atomic_read(&b->refcnt)==0);
	lafs_refile(b, 1);
}

#if DEBUG_REF == 0
#define _reflog(c,blk,ref,add) (c(blk))
#else
#define _reflog(c,blk,ref,add) ({ !blk ? 0 : \
			add? add_ref(blk,ref,__FILE__,__LINE__) : \
			del_ref(blk,ref, __FILE__,__LINE__); c(blk); })
#define _refxlog(c,blk,ref,add) ({ \
			add? add_ref(&(blk)->b,ref,__FILE__,__LINE__) :	\
			del_ref(&(blk)->b,ref, __FILE__,__LINE__); c(blk); })
#endif
#define getref(blk, r) ( ({BUG_ON((blk) && ! atomic_read(&(blk)->refcnt));}), _reflog(_getref, blk,r,1))
#define getref_locked(blk, r) _reflog(_getref_locked, blk,r,1)
#define getdref_locked(blk, r) _refxlog(_getdref_locked, blk,r,1)
#define getref_locked_needsync(blk, r) _reflog(_getref_locked_needsync, blk,r,1)
#define getiref_locked_needsync(blk, r) _refxlog(_getiref_locked_needsync, blk,r,1)
#define putref(blk, r) _reflog(_putref, blk,r,0)
#define getdref(blk, r) ( ({BUG_ON((blk) && ! atomic_read(&(blk)->b.refcnt));}), _reflog(_getref, (&(blk)->b),r,1),blk)
#define getiref(blk, r) ( ({BUG_ON((blk) && ! atomic_read(&(blk)->b.refcnt));}), _reflog(_getref, (&(blk)->b),r,1),blk)
#define putdref(blk, r) ({ if (blk) _reflog(_putref, (&(blk)->b),r,0);})
#define putiref(blk, r) ({ if (blk) _reflog(_putref, (&(blk)->b),r,0);})

/* When we get a ref on a block other than under b->inode->i_data.private_lock, we
 * need to call this to ensure that lafs_refile isn't still handling a refcnt->0 transition.
 */
static inline void sync_ref(struct block *b)
{
	spin_lock(&b->inode->i_data.private_lock);
	spin_unlock(&b->inode->i_data.private_lock);
}

/*
 * Notes on locking and block references.
 * There are several places that can hold uncounted references to blocks.
 * When we try to convert such a reference to a counted reference we need to be careful.
 * We must increment the refcount under a lock that protects the particular reference, and
 * then must call sync_ref above to ensure our new reference is safe.
 * When the last counted reference on a block is dropped (in lafs_refile) we hold private_lock
 * while cleaning up and sync_ref assures that cleanup is finished.
 * Before we can free a block we must synchronise with these other locks and
 * then ensure that the refcount is still zero.
 * For index blocks that means taking lafs_hash_lock and ensuring the count is still zero.
 * For most data blocks we need to remove from the lru list using fs->lock, and check the count is
 * still zero.
 * For inode data blocks we need to follow the inode and break the ->dblock lock under the inodes
 * own lock - but only if the refcount is still zero.
 *
 * The list of non-counted references and the locks which protect them is:
 *
 *         index hash table                      lafs_hash_lock
 *         index freelist                        lafs_hash_lock
 *         phase_leafs / clean_leafs             fs->lock
 *         inode->iblock                         lafs_hash_lock
 *         inode->dblock                         inode->i_data.private_lock
 *         inode->free_index                     lafs_hash_lock
 *         page->private                         page lock
 *
 * Lock ordering among these is:
 *   inode->i_data.private_lock
 *      LAFSI(inode)->dblock->inode->i_data.private_lock
 *         fs->lock
 *            lafs_hash_lock
 *
 */

int __must_check lafs_setparent(struct datablock *blk);

/*
 * extract little-endian values out of memory.
 * Each function is given a char*, and moves it forwards
 */

#define decode16(p) ({ unsigned int _a; _a= (unsigned char)*(p++); \
			_a + (((unsigned char)*p++)<<8); })
#define decode32(p) ({ u32 _b; _b = decode16(p); _b + ((u32)decode16(p)<<16); })
#define decode48(p) ({ u64 _c; _c = decode32(p); _c + ((u64)decode16(p)<<32); })

#define encode16(p,n) ({ *(p++) = (n)&255; *(p++) = ((n)>>8) & 255; })
#define encode32(p,n) ({ encode16(p,n); encode16(p, ((n)>>16)); })
#define encode48(p,n) ({ encode32(p,n); encode16(p, ((n)>>32)); })

#define DIV_ROUND_UP(n,d) (((n) + (d) - 1) / (d))

static inline int space_needed(int len, int chainoffset, int psz)
{
	int space;
	space = len + (chainoffset > 255 ? 4 : chainoffset > 1 ? 1 : 0);
	space += offsetof(struct dirpiece, name);
	space = DIV_ROUND_UP(space, 1<<psz);
	return space;
}

struct dir_ent {
	char *name;
	int nlen;
	u32 target;
	int type;
};

struct update_handle {
	struct fs *fs;
	int reserved;
};

void lafs_dir_print(char *buf, int psz);
int lafs_dir_blk_size(char *block, int psz);
void lafs_dir_clearparent(struct datablock *b);
struct block *lafs_dir_drop_parent(struct datablock *b);

int lafs_hash_name(u32 seed, int len, const char *name);
int lafs_dir_find(char *block, int psz, u32 seed, u32 hash,
		  u8 *pp);
int lafs_dir_findfirst(char *block, int psz);
void lafs_dir_init_block(char *block, int psz, const char *name, int len,
			 u32 target, int type, int chainoffset);
#define DT_TEST	128 /* for lafs_dir_add_ent, this flags to test for space,
		     * but not do any actual add
		     */
#define	DT_INTERNAL 129 /* Flags that this is an internal dir block */
int lafs_dir_add_ent(char *block, int psz, const char *name, int len,
		     u32 target, int type, u32 seed, u32 hash, int chainoffset);
int lafs_dir_del_ent(char *block, int psz, u32 seed, u32 hash);
int lafs_dir_next_ent(char *block, int psz, int *pp, char *name, u32 *target,
		      int *type);
void lafs_dir_repack(char *block, int psz, char *new, u32 seed, int merge);
void dir_get_prefix(char *b1, char *b2, int psz, char *prefix);
void lafs_dir_split(char *orig, int psz, char *new1, char *new2, const char *name,
		   u32 target, int type, u32 *newhash, u32 seed, u32 hash,
		   int chainoffset);
/*void dir_settarget(char *block, int psz, int piece, u32 target);*/
struct dir_ent *lafs_dir_extract(char *block, int psz, struct dir_ent *de,
				 int pnum, u32 *hash);
void lafs_dir_set_target(char *block, int psz, struct dir_ent *de, int pnum);
int lafs_dir_empty(char *block);
void lafs_dir_make_index(char *orig, char *new, int psz, u32 target);

int lafs_dir_handle_orphan(struct datablock *db);
int lafs_dir_roll_mini(struct inode *dir, int handle, int dirop,
		       u32 inum, char *name, int len);

int lafs_release_page(struct page *page, gfp_t gfp_flags);
void lafs_invalidate_page(struct page *page, unsigned long offset);

extern struct file_system_type lafs_fs_type;
extern struct file_system_type lafs_snap_fs_type;

int lafs_ihash_init(void);
void lafs_ihash_free(void);
void lafs_release_index(struct list_head *head);
struct indexblock *lafs_iblock_alloc(struct fs *fs, int gfp, int with_buffer,
				     REFARG);
void lafs_iblock_free(struct indexblock *ib);
void lafs_hash_iblock(struct indexblock *ib);
void lafs_unhash_iblock(struct indexblock *ib);

void lafs_summary_update(struct fs *fs, struct inode *ino,
			 u64 oldphys, u64 newphys, int is_index, int phase,
			 int moveref);
int lafs_summary_allocate(struct fs *fs, struct inode *ino, int diff);
void lafs_qcommit(struct fs *fs, struct inode *ino, int diff, int phase);
void lafs_incorporate(struct fs *fs, struct indexblock *ib);
void lafs_walk_leaf_index(struct indexblock *ib,
			  int (*handle)(void*, u32, u64, int),
			  void *data);
void lafs_clear_index(struct indexblock *ib);
void lafs_print_uninc(struct uninc *ui);

int lafs_allocated_block(struct fs *fs, struct block *blk, u64 phys);

int lafs_pin_dblock(struct datablock *b, int alloc_type);
int lafs_reserve_block(struct block *b, int alloc_type);
void lafs_dirty_dblock(struct datablock *b);
void lafs_erase_dblock(struct datablock *b);
int lafs_erase_dblock_async(struct datablock *b);
void lafs_dirty_iblock(struct indexblock *b, int want_realloc);
void block_drop_addr(struct fs *fs, struct inode *ino, u32 addr);
void lafs_flush(struct datablock *b);

bio_end_io_t *lafs_cluster_endio_choose(int which, int header);
int lafs_cluster_update_prepare(struct update_handle *uh, struct fs *fs,
				int len);
int lafs_cluster_update_pin(struct update_handle *uh);
unsigned long long
     lafs_cluster_update_commit(struct update_handle *uh,
				struct datablock *b,
				int offset, int len);
unsigned long long
     lafs_cluster_update_commit_buf(struct update_handle *uh, struct fs *fs,
				    struct inode *ino, u32 addr,
				    int offset, int len, const char *str1,
				    int len2, const char *str2);
void lafs_cluster_update_abort(struct update_handle *uh);
#define MAX_CHEAD_BLOCKS 4

static inline void lafs_cluster_wait(struct fs *fs, unsigned long long seq)
{
	wait_event(fs->wc[0].pending_wait,
		   fs->wc[0].cluster_seq > seq);
}

void lafs_cluster_wait_all(struct fs *fs);
int lafs_cluster_empty(struct fs *fs, int cnum);

/* super.c */
int lafs_write_state(struct fs *fs);
void lafs_destroy_inode(struct inode *inode);

/* checkpoint.c */
void lafs_checkpoint_lock(struct fs *fs);
void lafs_checkpoint_unlock(struct fs *fs);
void lafs_checkpoint_unlock_wait(struct fs *fs);
void lafs_checkpoint_wait(struct fs *fs);
unsigned long long lafs_checkpoint_start(struct fs *fs);
unsigned long lafs_do_checkpoint(struct fs *fs);
struct block *lafs_get_flushable(struct fs *fs, int phase);

int lafs_make_orphan(struct fs *fs, struct datablock *db);
int lafs_make_orphan_nb(struct fs *fs, struct datablock *db);
void lafs_orphan_release(struct fs *fs, struct datablock *db);
long lafs_run_orphans(struct fs *fs);
void lafs_add_orphan(struct fs *fs, struct datablock *db);
void lafs_orphan_forget(struct fs *fs, struct datablock *db);
struct datablock *lafs_find_orphan(struct inode *ino);
int lafs_count_orphans(struct inode *ino);
void lafs_add_orphans(struct fs *fs, struct inode *ino, int count);

/* Segment.c */
int lafs_prealloc(struct block *b, int type);
int lafs_seg_ref_block(struct block *b, int ssnum);
void lafs_seg_deref(struct fs *fs, u64 addr, int ssnum);
void lafs_add_active(struct fs *fs, u64 addr);
void lafs_seg_forget(struct fs *fs, int dev, u32 seg);
void lafs_seg_flush_all(struct fs *fs);
void lafs_seg_apply_all(struct fs *fs);
void lafs_seg_put_all(struct fs *fs);
void lafs_empty_segment_table(struct fs *fs);
int __must_check lafs_seg_dup(struct fs *fs, int which);
void lafs_seg_move(struct fs *fs, u64 oldaddr, u64 newaddr,
		   int ssnum, int phase, int moveref);
int lafs_segtrack_init(struct segtracker *st);
void lafs_segtrack_free(struct segtracker *st);
void lafs_update_youth(struct fs *fs, int dev, u32 seg);

extern int temp_credits;/* debugging */
void lafs_free_get(struct fs *fs, unsigned int *dev, u32 *seg,
		   int nonlogged);
int lafs_get_cleanable(struct fs *fs, u16 *dev, u32 *seg);

void lafs_space_return(struct fs *fs, int credits);
int lafs_alloc_cleaner_segs(struct fs *fs, int max);
int lafs_space_alloc(struct fs *fs, int credits, int why);
unsigned long lafs_scan_seg(struct fs *fs);
int lafs_clean_count(struct fs *fs, int *any_clean);
void lafs_clean_free(struct fs *fs);

/* Cleaner */
unsigned long lafs_do_clean(struct fs *fs);
void lafs_unclean(struct datablock *db);

/* Thread management */
int lafs_start_thread(struct fs *fs);
void lafs_stop_thread(struct fs *fs);
void lafs_wake_thread(struct fs *fs);
void lafs_trigger_flush(struct block *b);

/* cluster.c */
int lafs_cluster_allocate(struct block *b, int cnum);
void lafs_cluster_flush(struct fs *fs, int cnum);
int lafs_calc_cluster_csum(struct cluster_head *head);
int lafs_cluster_init(struct fs *fs, int cnum, u64 addr, u64 prev, u64 seq);
void lafs_clusters_done(struct fs *fs);
void lafs_done_work(struct work_struct *ws);
void lafs_close_all_segments(struct fs *fs);
u32 lafs_seg_setsize(struct fs *fs, struct segpos *seg, u32 size);
void lafs_seg_setpos(struct fs *fs, struct segpos *seg, u64 addr);
u64 lafs_seg_next(struct fs *fs, struct segpos *seg);

/* index.c */
void lafs_pin_block_ph(struct block *b, int ph);
static inline void lafs_pin_block(struct block *b)
{
	lafs_pin_block_ph(b, fs_from_inode(b->inode)->phase);
}

int lafs_add_block_address(struct fs *fs, struct block *blk);
void lafs_flip_dblock(struct datablock *db);
void lafs_phase_flip(struct fs *fs, struct indexblock *ib);
struct indexblock * __must_check
lafs_make_iblock(struct inode *ino, int adopt, int async, REFARG);
struct indexblock *
lafs_iblock_get(struct inode *ino, faddr_t addr, int depth, paddr_t phys, REFARG);

/* io.c */
void lafs_write_head(struct fs *fs, struct cluster_head *head, u64 virt,
		     struct wc *wc);
void lafs_write_block(struct fs *fs, struct block *b, struct wc *wc);
void lafs_write_flush(struct fs *fs, struct wc *wc);

/* quota.c */
int lafs_quota_allocate(struct fs *fs, struct inode *ino, int diff);

#define __wait_event_lock(wq, condition, lock)				\
do {									\
	wait_queue_t __wait;						\
	init_waitqueue_entry(&__wait, current);				\
									\
	add_wait_queue(&wq, &__wait);					\
	for (;;) {							\
		set_current_state(TASK_UNINTERRUPTIBLE);		\
		if (condition)						\
			break;						\
		spin_unlock(&lock);					\
		schedule();						\
		spin_lock(&lock);					\
	}								\
	current->state = TASK_RUNNING;					\
	remove_wait_queue(&wq, &__wait);				\
} while (0)

#define wait_event_lock(wq, condition, lock)				\
do {									\
	if (condition)							\
		break;							\
	__wait_event_lock(wq, condition, lock);				\
} while (0)

