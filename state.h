
/*
 * fs/lafs/state.h
 * Copyright (C) 2005-2009
 * Neil Brown <neilb@suse.de>
 * Released under the GPL, version 2
 *
 * The stateblock and superblocks are copied into the
 * internal 'struct fs' at startup time, and are written
 * out based on information there-in.
 * The first stateblock read has sufficient information
 * to size all the components. These are only changed
 * in unusual circumstances (Adding devices, taking snapshots).
 */

#define HASHBITS 10
#define	HASHSIZE (1<<HASHBITS)
#define HASHMASK (HASHSIZE-1)

/* for segment hash */
#define SHASHBITS	8
#define	SHASHSIZE	(1<<SHASHBITS)
#define	SHASHMASK	(SHASHSIZE-1)

/* for quota hash */
#define	QHASHBITS 9
#define	QHASHSIZE (1<<QHASHBITS)
#define	QHASHMASK (QHASHSIZE-1)

/* skip points are used to accelerate sequential-order insert
 * in the list of blocks pending write
 */
#define	SKIP_MAX_HEIGHT	8
struct skippoint {
	struct block *b;
	struct skippoint *next[SKIP_MAX_HEIGHT];
};

#define WC_NUM 3	/* 3 active write-clusters: new, clean, and defrag */
#define CLEANER_SEGS 4	/* Clean at most 4 segments at a time */

#define	ACCOUNT_RESERVED  3 /* Reserve 3 segments of space for accounting and
			     * cleaning
			     */
#define	RELEASE_RESERVED 1 /* Reserve 1 segment of space for overhead required
			    * to release space (e.g. delete file)
			    */
#define TOTAL_RESERVED (ACCOUNT_RESERVED + RELEASE_RESERVED)
struct fs {
	struct	lafs_state	*state;

	u32	seq;
	u32	levels;
	u32	devices;
	u32	statesize;

	int	blocksize, blocksize_bits;
	int	devs_loaded;

	atomic_t sb_writes_pending;
	wait_queue_head_t sb_writes_wait;
	wait_queue_head_t trunc_wait;
	wait_queue_head_t async_complete;

	struct super_block *prime_sb;

	u32	nonlog_segment;
	unsigned short	nonlog_dev;
	u32	nonlog_offset;

	u32	maxsnapshot;
	u64	checkpointcluster;
	struct snapshot {
		u64	root_addr;
		struct inode	*root;
	} *ss;
	u32	current_ss;

	/* general purpose lock. Protects:
	 * - pending_orphans list
	 * - phase_locked
	 */
	spinlock_t lock;
	struct inode *orphans; /* the orphans file */

	struct list_head pending_orphans;

	int phase_locked;
	int	phase;	/* 0 or 1 */
	wait_queue_head_t phase_wait; /* Also use to wait for first_free_pass */

	/* flags to set on next cluster. */
	int	checkpointing;
	int	rolled; /* set when rollforward has completed */
	unsigned long fsstate;
#define CheckpointNeeded 0
#define	ThreadRunning	1
#define	ThreadNeeded	2
#define FinalCheckpoint 3
#define CleanerDisabled 4
#define OrphansRunning 5
#define CleanerBlocks 6	/* One or more threads is blocked waiting for the
			 * cleaner to progress - cleaner.need blocks are
			 * needed.
			 */
#define EmergencyClean 7/* Cleaner is in emergency mode and will clean
			 * the segment with the most space no matter
			 * how young it is.   The fs is considered to
			 * be full and new allocation requests get
			 * -ENOSPC
			 */
#define FlushNeeded 8  /* Need to flush the current cluster because
			* someone is waiting on writeback
			*/
#define SecondFlushNeeded 9 /* Need a second cluster to commit the blocks
			     * in the previous one
			     */
#define EmergencyPending 10 /* Cleaner isn't quite in emergency mode, but
			     * should be after the next checkpoint unless that
			     * releases lots of space
			     */
#define CheckpointOpen 11 /* Some data has been written since the last checkpoint,
			   * so 'next_checkpoint' is a valid timestamp
			   */
#define DelayYouth 12  /* While set, don't update any youth blocks.  The update will
			* happen either during seg_apply_all or in roll-forward
			*/

	unsigned long next_checkpoint; /* Valid when CheckpointOpen is set, holds
					* jiffie time by when we need to do a checkpoint
					*/

	struct work_struct done_work;	/* used for handling
					 * refile after write completes */

	struct {
		int active;		/* number of actively cleaned segments */
		u32 cleaning;		/* amount of space that is being cleaned
					 * this checkpoint
					 */
		u32 need;		/* Amount of space that is needed by
					 * Some thread waiting on CleanerBlocks
					 * flag.
					 */
		struct mutex lock;	/* protects list mani and refcnt of core
					 * cleaner.
					 */
		struct toclean {
			u16 dev;
			u32 seg;
			u64 haddr;	/* Address of this write-cluster-header */
			u64 seq;	/* seq of most recent header loaded */
			struct cluster_head *ch;
			struct group_head *gh; /* current group head */
			struct descriptor *desc;
			struct async_complete {
				int state;	/* 0=ignore, 1=valid, 2=loading,
						   3=loaded, 4 = ioerror */
				struct fs *fs;
			} ac;
			int have_addr;	/* true if dev/seg are valid */
			struct list_head cleaning;
			struct page *chead;
		} seg[CLEANER_SEGS];
	} cleaner;
	struct task_struct *thread;

	unsigned long newsegments;	/* number of segments written since checkpoint */
	unsigned long max_newsegs;	/* max segments in a checkpoint (roughly) */

	/* counters for (pre)allocating space. */
	spinlock_t alloc_lock;
	s64	free_blocks; /* initialised from free segment info */
	u64	allocated_blocks; /* Blocks that have been (pre)allocated */
	u64	clean_reserved; /* Blocks reserved for cleaner segments */
	u64	max_segment; /* largest segment size */
	u64	total_free;  /* free space found in all segments this scan */
	u64	total_free_prev; /* "  "   " in previous scan */
	int cluster_updates; /* DEBUGGING */
#define NewSpace 0
#define ReleaseSpace 1
#define CleanSpace 2
#define AccountSpace 3

	struct list_head	inode_index; /* list of inode index_blocks,
					      * largely so they can look hashed
					      */

	struct list_head	phase_leafs[2]; /* list of pinned blocks that
						 * have no pinned children,
						 * and are in phase p
						 */

	struct list_head	clean_leafs;	/* list of pinned blocks that
						 * have no pinned children
						 * and are being cleaned
						 */
	struct list_head	account_leafs;	/* list of accounting block
						 * that we need to write after
						 * the checkpoint is done.
						 * They are now pinned to the
						 * next phase.
						 */

	/* Youth management */
	int	youth_next;	/* number to assign to next segment */
	int	checkpoint_youth;

/* 10 heights: 0 to 9 */
#define	SEG_NUM_HEIGHTS (10)
	struct segtracker {
		void *page[4];
		int size[4]; /* entry size in page as "number of skip pointers" */
		struct slist {
			unsigned short first, last;
			unsigned short cnt;
		} unused, free, cleanable, clean;
		unsigned short head[SEG_NUM_HEIGHTS]; /* head of skiplist */
		int total;
		long long max_score;
		int sorted_size;
	} segtrack[1];

	/*
	 * NOTE: there should probably be a free list for each 'level'
	 */

	/* For scan */
	struct {
		int	free_dev, free_block, free_stage;
		int	first_free_pass;	/* true the first time */
		int	done, do_decay; /* cleared on each checkpoint */
		struct	datablock *youth_db, *usage0_db;
		u32	*free_usages; /* This is an allocated page */
		int	trace;
	} scan;

	/* quotas. */
	int	qphase;	/* like phase, but switches later in qflush */
	struct list_head flush_list; /* list of qents that need flushing */
	struct list_head qhash[QHASHSIZE];

	struct backing_dev_info bdi;
	struct fs_dev {
		struct block_device *bdev;
		struct lafs_dev *devblk;

		u64	start, size;
		u64	devaddr[2];
		u64	stateaddr[4];

		u32	width, stride;
		u32	segment_size;
		u32	segment_offset;
		u32	segment_stride;
		u32	segment_count;
		u32	usage_inum;

		u32 	rows_per_table, tables_per_seg;

		int	recent_dev, recent_state;

		int	tablesize; /* in segusage file, not in segments */
		struct inode *segsum;
	} *devs;

	struct wc {
		/* A 'write-cluster' descriptor
		 * Any write-cluster that we are in the process of
		 * building has this structure to describe it.
		 * We gather dirty blocks and keep track of cluster-header usage
		 */
		struct skippoint slhead;	/* skiplist of pending blocks */
		struct list_head clhead;	/* list of pending blocks (linked
						 * on ->lru */

		struct mutex lock;

		struct page	*page[4];/* a page which the cluster-head is built
					  * in - never in high-memory */
		struct cluster_head *chead; /* the cluster head == page */
		int		chead_blocks; /* blocks allocated for cluster head */
		int		cluster_space; /* space remaining in cluster_head
						* after current commitments
						*/
		int		chead_size;	/* space used already in cluster_head
						 * new miniblock and allocation
						 * happen from here
						 */
		int		remaining;	/* available space in segment */

		unsigned long long cluster_seq; /* seq number of next cluster*/
		u64		prev_addr;	/* where previous cluster is */

		int		cnum; /* Which wc entry this is - needed for
				       * finding the 'fs' from a 'wc' */

		/* We keep track of how many blocks are still in-flight
		 * for each cluster.  This allows us to know when data is
		 * safe on disk.
		 * The count included all blocks in the cluster - including
		 * those in the header - plus all those in the header of
		 * the next cluster, and possibly the one after (in the case
		 * of VerifyNext2).
		 * This implies that we need to track as many as 4 separate
		 * write cluster.  The one that is currently being built,
		 * and the previous 3.  We use a ring buffer with a pointer
		 * to the entry what is currently being built.
		 */
		atomic_t	pending_cnt[4];
		int		pending_vfy_type[4];
		struct list_head pending_blocks[4];
		int		pending_next;
		wait_queue_head_t pending_wait;

		struct bio *bio; /* bio we are building */
		u64 bio_virt;   /* next sector we can add to bio */
		int bio_head;	/* True if current bio contains a header block */
		int bio_which;	/* pending[X] entry for this bio */
		struct request_queue *bio_queue; /* queue to unplug */

		struct segpos {
			int		dev;
			u32		num;

			u32 st_table, st_row;
			u32 nxt_table, nxt_row;
			u32 table, row, col;

		} seg;
	} wc[WC_NUM];

	struct hlist_head stable[SHASHSIZE];
	spinlock_t stable_lock;
	int stable_changed; /* Flag so we can see if the table changed while
			     * we dropped a lock */

	struct rename_roll {
		struct rename_roll *next;
		u32 key;
		struct inode *dir, *inode;
		int nlen;
		char name[1];
	} *pending_renames;

};

static inline int test_phase_locked(struct fs *fs)
{
	return fs->phase_locked > 0;
}

/* There are two sorts of blocks, data blocks and index blocks.
 * Data blocks store the contents of files, including inode files.
 * So each inode is in a datablock.
 * Index blocks store index/indirect/extent blocks. As inodes often
 * contain index information, Inode will usually have an index block aswell.
 *
 * Data blocks are stored in an address space and are indexed by inode and offset.
 * Index blocks are indexed by inode, depth, and offset.  The "depth" is
 * distance from data, so indirect and extent blocks have depth of 1.
 * This index doesn't often change.
 *
 * All blocks have a pointer to their parent, which is an index block (except
 * for inodes?).
 * All index blocks have a linked list of children which is used to find children
 * to move when the block is split.
 * There are also lists of children with pending changes to incorporate.
 *
 * A datablock for an inode points to the struct inode.
 * The struct inode points to the index block for the inode
 * The index block points to the datablock through the "parent" pointer.
 */

struct block {
	unsigned long		flags;
	unsigned long		fileaddr;
	struct inode		*inode;
	atomic_t		refcnt;
	u64			physaddr;

	struct indexblock	*parent;
	struct list_head	siblings; /* Next unreachable block in the same
				    * reachability-set as this block
				    */

	struct list_head	lru; /* phase_leafs, clean_leafs, account_leafs,
				      * clhead, pending_blocks */

	struct list_head	peers;	/* other blocks that use the same location
					 * in storage (i.e. in another snapshot)
					 */

	struct block		*chain; /* on list of unincorporated changes, or list of blocks
					 * being read in */

#if DEBUG_REF
	struct ref {int cnt; char *name; } holders[16];
#endif
#if DEBUG_IOLOCK
	/* More debugging */
	char *iolock_file;
	int iolock_line;
#endif
};
struct datablock {
	struct block b;
	struct page		*page;
	u32	orphan_slot;	/* slot in orphan file to record that this
				 * block is an orphan
				 */
	union {
		/* If a block is both an orphan and undergoing
		 * cleaning, it lives on the cleaning list until
		 * the cleaner has checked it.  It is then moved
		 * to the pending_orphans list.
		 */
		struct list_head orphans; /* linked list of blocks needing orphan
					   * processing.
					   */
		struct list_head cleaning; /* list of blocks being cleaned.
					    */
	};
	union {
		struct inode *my_inode;	/* only valid for block holding an inode */
	};
};
struct indexblock {
	struct block		b;
	char *			data;
	struct hlist_node	hash;
	int			depth;

	struct list_head	children;

	/*
	 * pincnt[p] is the number of pinned blocks in phase 'p' which
	 * have us as their ->parent.
	 */
	atomic_t		pincnt[2];

	/* Addresses that need to be incorporated in the current
	 * phase are stored in an extent table until incorporation
	 * which gets scheduled when the table is full.
	 * Index block are kept on an list.
	 * Blocks in the next phase are kept on a list until
	 * the phase moves past and incorporation can be considered.
	 */
	struct uninc {
		int credits;	/* Number of uninc credits still available */
		int pending_cnt; /* number of valid entries in pending_addr */
		/* The number of extents in pending_addr must be at most
		 * half the number of extents that can fit into an extent
		 * block.  At 512 bytes, 512/12 == 42.  So now more than
		 * 21.  See do_incorporate_leaf
		 */
		struct addr {
			u32 fileaddr;
			u32 cnt;
			u64 physaddr;
		} pending_addr[8];
	} uninc_table;
	/* If this is internal (depth>1) the actual block are kept on this list*/
	struct block		*uninc;

	/* these are all in the next phase.  On phase-change,
	 * we copy all the addresses into the uninc_table
	 * and free the blocks
	 */
	struct block		*uninc_next;
};

#define iblk(__bl) container_of(__bl, struct indexblock, b)
#define dblk(__bl) container_of(__bl, struct datablock, b)

enum {
	/* NOTE: 32 flags in used.  Need to overlap 'data' with 'index' if
	 * we need much more
	 */
	/* First flags that are meaningful for both Data and Index blocks
	 * Currently 22 */
	B_Phase1 = 0,	/* phase when pinned  - must be '1' - used for indexing */
	B_Dirty,	/* block has changes which haven't been committed */
	B_Index,	/* This is an index block, not a data block */
	B_Linked,	/* block is known to be linked with all peers */

	B_Realloc,	/* If set on a B_Dirty block, it was only dirtied for
			 * cleaning purposes and so should be written to the
			 * cleaner segment.
			 */
	B_Valid,	/* block contains valid data */
	B_Uninc,	/* on ->unincorporated list for next phase, so we
			 * cannot free it just yet.*/
	B_InoIdx,	/* is the index block for an inode */
	B_Pinned,	/* B_Phase1 is meaningful and block must be written
			 * (if dirty) in this phase
			 */
	B_Async,	  /* An async IO is pending on this block */
	B_Root,		/* data or index block for root. ->parent is NULL */

	B_SegRef,	/* holds a reference on segment of physaddr */
	B_Credit,	/* data block with space preallocated */
	B_ICredit,	/* index space preallocated for data block */
	B_NCredit,	/* Credit for next phase */

	B_NICredit,	/* ICredit for next phase */
	B_UnincCredit,	/* Uninc carries a credit */
	B_IOLock,	/* Block is undergoing IO */
	B_Writeback,	/* Block is in a cluster */
	B_IOLockLock,	/* lock while testing IOLock on a page */

	B_WriteError,	/* Got a write error while writing (when else?) */
	B_PhysValid,	/* ->physaddr is correct */

	/* Flags that are only relevant for Data Block
	 * currently 7 */

	B_PinPending,	 /* set on data blocks while checkpoint_locked if we might
			   * want to mark them dirty
			   */
	B_Prealloc,	/* This data block has physaddr==0 and has been counted
			 * in various ablock counters.
			 */
	B_Orphan,	/* ->orphan_slot is valid */

	B_HaveLock,	/* We own the page lock and when all blocks are unlocked,
			 * the page should be unlocked */
	B_HaveWriteback,/* We own the page writeback flag and when all blocks
			 * are unlocked we should clear it. */
	B_Claimed,	/* Used for exclusive allocation of inodes */
	B_Cleaning,	/* Used by cleaner to track which blocks it has a
			 * reference on already. */

	/* Flags that are only relevant for Index Blocks
	 * currently 3 */
	B_OnFree,	/* index block on the free list */
	B_PrimaryRef,	/* This indexblock has not been incorporated into the
			 * parent, and so can only be found by being a sibling
			 * of a 'primary' which is incorporated.  Consequently
			 * a counted reference is held on ->siblings.prev,
			 * which is either the primary or another block holding
			 * a PrimaryRef.
			 */
	B_EmptyIndex,	/* Index block is empty, has no (non-EmptyIndex) children,
			 * will be destroyed soon.  Indexing must avoid it
			 * unless if the first child of parent.
			 * Gets allocated to '0'.
			 * Flag only set under B_IOLock.
			 */

	B_NUM_FLAGS
};
/* indexing info stays in the block, not in the inode */
struct lafs_inode {
	struct inode	vfs_inode;
	struct inode	*filesys;	/* Inode of containing TypeInodeFile */
	struct indexblock	*iblock;
	struct datablock	*dblock;
	long	cblocks, /* data blocks which are commited to this file */
		pblocks, /* extra data blocks that are commited in next phase */
		ablocks, /* data blocks that are allocated */

		ciblocks, /* index blocks commited */
		piblocks; /* index blocks in next phase */
	unsigned long	iflags;
#define	I_Phase1 1	/* set to 'this' phase when iblocks correct */
#define	I_Dirty 2
#define	I_Deleting 3	/* preserve the inode while truncate-on-delete happens */
#define I_Destroyed 4	/* inode destroy has been delayed */
#define	I_Trunc	5	/* a truncation is in process */
#define	I_Pinned 6	/* InoIdx is pinned, i_nlink is non-zero, and consequently
			 * we own an extra ref to the inode and superblock.
			 */
#define I_AccessTime 7	/* Set if this inode holds a reference to the block
			 * in the accesstime file that holds our atime
			 */
/* next three indicate if we hold a reference on the relevant qent */
#define	I_QUid	8
#define	I_QGid	9
#define	I_QTid	10

	struct rw_semaphore ind_sem;
	unsigned long long update_cluster;

/*	int		generation; */
	int		trunc_gen;
	int		flags;
	int		type;
	int		metadata_size;
	int		depth;

	struct list_head	free_index;

	loff_t		trunc_next; /* next block to truncate from */
	union {
		struct fs_md {
			int	usagetable;   /* 0 and unused for subsets,
					       * 1 for main fs, >1 for snapshots
					       */
			u64	cblocks_used; /* blocks commited */
			u64	pblocks_used; /* extra blocks in next phase */
			u64	ablocks_used; /* extra blocks allocated */
			u64	blocks_allowed;
			u64	blocks_unalloc; /* FIXME what is this for ?? */

			u64	creation_age;
			u32	parent;		/* like 'parent' in directory */
			u32	inodes_used;
			u32	quota_inums[3];
			struct inode *quota_inodes[3];
			struct inode *accesstime;
			char	*name;
		} fs;
		struct inodemap_md {
			u32	size;
			/* These are state use to manage inum allocation */
			u32	thisblock;
			int	nextbit; /* bit in nextblock-1 to check next */
		} inodemap;
		struct su_md {
			u32	table_size;	/* (blocks) */
		} segmentusage;
		struct file_md {
			u16	atime_offset; /* value stored in atime file */
			u16	flags;
/*			u16	mode; */
/*			u32	uid; */
/*			u32	gid; */
			u32	treeid;
			u64	creationtime;
/*			u64	modifytime; */
/*			u64	ctime; */
/*			u64	accesstime; */
			struct timespec	i_accesstime; /* as stored in inode */
/*			u64	size; */
			u32	parent;
/*			u32	linkcount; */
			/* for directories */
			u32	seed;
		} file;
		struct orphan_md {
			u32	nextfree;
			u32	reserved;
		} orphan;
		struct quota_md {
			u32	gracetime; /* multiplier for graceunits */
			u32	graceunits; /* seconds per unit */
		} quota;

		/* We free inodes using rcu, by which time any
		 * metadata is irrelevant and the type is zero
		 */
		struct rcu_head rcu;
	} md;
};

static inline struct lafs_inode *LAFSI(struct inode *in)
{
	return container_of(in, struct lafs_inode, vfs_inode);
}

