
/*
 * segment tracking routines for LaFS
 * fs/lafs/segments.c
 * Copyright (C) 2006-2009
 * NeilBrown <neilb@suse.de>
 * Released under the GPL, version 2
 */

/* In here we manage:
 * - active blocks in segment usage files
 * - next-phase usage updates
 * - youth tracking and decay
 * - list of segments that could be cleaned
 * - list of free segments that could be reused
 * - tracking space allocation and available free space
 *
 * Segment usage blocks:
 *  Every Pinned, Dirty or Realloc block with physaddr != 0 owns a reference
 *  on the block in the segment usage file that counts that
 *  block.  When the physaddr is changed, the new block will always
 *  be available so the reference is simply moved.
 *
 *  Normally when ->physaddr changes, the counts in the relevant
 *  segusage blocks are updated directly.  However when the change is
 *  for a write in the next phase (during a checkpoint) that is not
 *  possible as the segusage blocks need to be written with the old value.
 *  So we track these updates in a simple data structure that
 *  records delta against each ss+dev+seg.  If such a data structure
 *  cannot be allocated (-ENOMEM) the associated write is blocked until
 *  the checkpoint completes. FIXME - implement that.
 *
 *  Every Pinned or Dirty block holds a number of space credits,
 *  following a pattern described elsewhere.  The space allocation
 *  unit identifies segments to be used in advance and holds a reference
 *  on every block.
 *
 * Youth
 *  Youth numbers are handed out sequentially to new write clusters.
 *  When the available space approaches being full all youth numbers are
 *  decayed (as differentiation between old youth values is uninteresting).
 *  This decay is done in a staged fashion, one block at a time so that
 *  a checkpoint can happen at any time and the current block in the
 *  decay sweep is recorded.
 *
 * Free Segments
 *  Holding a record of all free segments is impractical in general.  Instead
 *  We record a limited number of free segments (1 page worth - 512 entries,
 *  possibly with ranges) and when needed we scan the segment usage
 *  files for free segments and add them to the list.  This scan needs to
 *  be rate limited - not sure how best to do that.
 *
 * Cleanable Segments
 *  Similar to free segments we need to keep track of cleanable segments.
 *  Almost all non-free segments are cleanable, and we assign a weight to
 *  each based on youth and usage.  We want to keep the most cleanable
 *  segments based on this weight.
 *  We keep an unordered list of cleanable segments.  When it becomes
 *  full we sort it by weight and discard the worst half.
 *
 * Scanning
 *  As there could be hundreds of snapshots (hopefully not thousands)
 *  we cannot load the segusage block for each snapshot and then parse them
 *  in parallel.  Instead we allocate space to store a max usage and
 *  merge each block one at a time into that max.  We then combine the
 *  max with the youth to get a 64bit weight... I wonder if that is good.
 *
 */

/* Every block that could change its ->physaddr holds a reference
 * on the segment summary block through this structure
 */

#include	"lafs.h"
#include	<linux/hash.h>
#include	<linux/random.h>
#include	<linux/slab.h>

struct segsum {
	u32	segnum;
	int	devnum;
	int	ssnum;

	struct hlist_node hash;

	atomic_t	refcnt;
	atomic_t	delayed;

	struct datablock *ssblk;
	/* youthblk is only set when ssnum is 0 */
	struct datablock *youthblk;
};

static int shash(u32 segnum, int devnum, int ssnum)
{
	unsigned long h = hash_long(segnum, BITS_PER_LONG);
	return hash_long(h ^ (devnum | (ssnum << 16)), SHASHBITS);
}

static inline void ss_get(struct segsum *ss)
{
	BUG_ON(atomic_read(&ss->refcnt) == 0);
	atomic_inc(&ss->refcnt);
}

static void ss_put(struct segsum *ss, struct fs *fs)
{
	if (atomic_dec_and_lock(&ss->refcnt, &fs->stable_lock)) {
		if (atomic_read(&ss->delayed) != 0)
			spin_unlock(&fs->stable_lock);
		else {
			if (!hlist_unhashed(&ss->hash))
				hlist_del(&ss->hash);
			if (!fs->stable_changed)
				fs->stable_changed = 1;
			spin_unlock(&fs->stable_lock);
			putdref(ss->ssblk, MKREF(ss));
			putdref(ss->youthblk, MKREF(ssyouth));
			kfree(ss);
		}
	}
}

static struct segsum *segsum_find(struct fs *fs, u32 segnum,
				  int devnum, int ssnum)
{
	struct hlist_head *head = &fs->stable[shash(segnum, devnum, ssnum)];
	struct segsum *ss, *new = NULL;
	struct hlist_node *n;
	struct fs_dev *dv;
	u32 addr;
	int err;

retry:
	spin_lock(&fs->stable_lock);
	hlist_for_each_entry(ss, n, head, hash)
		if (ss->segnum == segnum &&
		    ss->ssnum == ssnum &&
		    ss->devnum == devnum) {
			atomic_inc(&ss->refcnt);
			spin_unlock(&fs->stable_lock);
			if (new)
				ss_put(new, fs);
			if (!fs->rolled && fs->qphase != fs->phase)
				return ss;
			if (ss->ssblk &&
			    !test_bit(B_Valid, &ss->ssblk->b.flags) ) {
				/* need to read this now that roll forward
				 * has progressed.
				 */
				err = lafs_read_block(ss->ssblk);
				if (err) {
					ss_put(ss, fs);
					return ERR_PTR(err);
				}
			}
			if (ss->youthblk &&
			    !test_bit(B_Valid, &ss->youthblk->b.flags)) {
				/* need to read this now that roll forward
				 * has progressed.
				 */
				err = lafs_read_block(ss->youthblk);
				if (err) {
					ss_put(ss, fs);
					return ERR_PTR(err);
				}
			}
			return ss;
		}
	if (new) {
		hlist_add_head(&new->hash, head);
		spin_unlock(&fs->stable_lock);
		return new;
	}
	spin_unlock(&fs->stable_lock);

	/* seems we must allocate something */

	new = kmalloc(sizeof(*new), GFP_KERNEL | __GFP_NOFAIL);
	new->segnum = segnum;
	new->devnum = devnum;
	new->ssnum = ssnum;
	atomic_set(&new->refcnt, 1);
	atomic_set(&new->delayed, 0);
	INIT_HLIST_NODE(&new->hash);
	dv = fs->devs + devnum;
	addr = LAFSI(fs->ss[ssnum].root)->md.fs.usagetable * dv->tablesize;
	addr += segnum >> (fs->blocksize_bits - USAGE_SHIFT);
	new->ssblk = lafs_get_block(dv->segsum, addr, NULL,
				    GFP_KERNEL,
				    MKREF(ss));
	if (ssnum == 0)
		new->youthblk = lafs_get_block(dv->segsum,
					       segnum >> (fs->blocksize_bits
							  - YOUTH_SHIFT),
					       NULL,
					       GFP_KERNEL,
					       MKREF(ssyouth));
	else
		new->youthblk = NULL;
	BUG_ON(!new->ssblk || IS_ERR(new->ssblk) || IS_ERR(new->youthblk));
	if (!fs->rolled && fs->qphase != fs->phase) {
		/* Too early to safely read segusage blocks,
		 * leave it until later
		 */
		goto retry;
	}
	err = lafs_read_block(new->ssblk);
	if (new->youthblk && err == 0)
		err = lafs_read_block(new->youthblk);
	if (err) {
		ss_put(new, fs);
		return ERR_PTR(err);
	}
	goto retry;
}

static struct segsum *segsum_byaddr(struct fs *fs, u64 addr, int ssnum)
{
	int dev;
	u32 offset;
	u32 seg;
	virttoseg(fs, addr, &dev, &seg, &offset);

	return segsum_find(fs, seg, dev, ssnum);
}

void lafs_seg_put_all(struct fs *fs)
{
	/* remove all the ssblk and youthblk references from
	 * all segsums.
	 * This is called when filesystem is being unmounted
	 */
	int i;
	for (i = 0; i < SHASHSIZE; i++)
		if (!hlist_empty(&fs->stable[i])) {
			struct segsum *ss;
			struct datablock *b, *y;
			struct hlist_node *n, *pos;

			spin_lock(&fs->stable_lock);
		retry:
			hlist_for_each_entry_safe(ss, pos, n, &fs->stable[i], hash) {
				b = ss->ssblk;
				y = ss->youthblk;
				if (!b && !y)
					continue;
				ss->ssblk = NULL;
				ss->youthblk = NULL;
				spin_unlock(&fs->stable_lock);
				if (b && test_and_clear_bit(B_Async, &b->b.flags))
					putdref(b, MKREF(async));
				if (y && test_and_clear_bit(B_Async, &y->b.flags))
					putdref(y, MKREF(async));
				putdref(b, MKREF(ss));
				putdref(y, MKREF(ssyouth));
				spin_lock(&fs->stable_lock);
				if (fs->stable_changed)
					goto retry;
			}
			spin_unlock(&fs->stable_lock);
		}
}

/* We need to set SegRef on this block, and all parents,
 * and the segsum block and all parents, and so on.
 * We find the first ancestor that does not have SegRef,
 * find the matching segsum block.  If it already
 * has SegRef, we add the reference and retry from the start.
 * If it doesn't have SegRef, we prealloc and then tail recurse.
 * Note: we never set SegRef on B_Root blocks as their usage
 * isn't recorded.
 */
int lafs_seg_ref_block(struct block *b, int ssnum)
{
	struct fs *fs = fs_from_inode(b->inode);

	LAFS_BUG(test_bit(B_InoIdx, &b->flags), b);
	getref(b, MKREF(segref));

	while (!test_bit(B_SegRef, &b->flags)) {
		struct block *p;
		struct block *b2, *to_put = NULL;
		struct segsum *ss;
		struct inode *ino;
		int err;

		BUG_ON(test_bit(B_Root, &b->flags));

		/* The extra reference of 'b2' and the use for 'to_put'
		 * deserves some explanation.
		 * We will normally hold an implicit reference to b2 due to
		 * our reference on 'b' and the fact that b2 is an ancestor
		 * of 'b'.  However at this point we have no locks on the file
		 * at all so it is possible that a btree manipulation could
		 * split b2 resulting in b2 not being the parent of b any more
		 * so our reference guarantee is lost.  Even in that case,
		 * the ancestor of b would probably have a B_PrimaryRef on
		 * b2.  However it is hard to prove that will stay around long
		 * enough so we take an extra ref just in case.
		 * We only need the ref when we drop private_lock so only take
		 * it then.  We cannot drop an old ref at that point, so
		 * store the old ref in 'to_put' and drop it when releasing
		 * private_lock.
		 */

		b2 = b;
		/* Need to check parents */
		ino = b->inode;
		spin_lock(&ino->i_data.private_lock);
		for (p = b;
		     p && !test_bit(B_SegRef, &p->flags);
		     p = &(p->parent)->b) {
			if (test_bit(B_InoIdx, &p->flags)) {
				struct datablock *db = LAFSI(p->inode)->dblock;
				p = &db->b;
				getref(b2, MKREF(segref2));
				spin_unlock(&ino->i_data.private_lock);
				ino = p->inode;
				if (to_put)
					putref(to_put, MKREF(segref2));
				spin_lock(&ino->i_data.private_lock);
				to_put = b2;
			}
			if (test_bit(B_SegRef, &p->flags))
				break;
			if (!test_bit(B_Root, &p->flags))
				b2 = p;
		}
		getref(b2, MKREF(segref2));
		spin_unlock(&ino->i_data.private_lock);
		if (to_put)
			putref(to_put, MKREF(segref2));
		/* b2 is the first ancestor (closest to root) without SegRef */

		if (b2->physaddr == 0) {
			/* There is no segsum to reference */
			set_bit(B_SegRef, &b2->flags);
			putref(b2, MKREF(segref2));
			continue;
		}

		ss = segsum_byaddr(fs, b2->physaddr, ssnum);
		if (IS_ERR(ss)) {
			putref(b, MKREF(segref));
			putref(b2, MKREF(segref2));
			return PTR_ERR(ss);
		}

		if (&ss->ssblk->b == b) {
			/* Don't need to check for Prealloc or
			 * SegRef, just take a reference now
			 */
			if (test_and_set_bit(B_SegRef, &b2->flags))
				ss_put(ss, fs);
			putref(b2, MKREF(segref2));
			continue;
		}

		/* Don't need iolock here as segusage is very special */
		set_bit(B_PinPending, &ss->ssblk->b.flags);
		err = lafs_pin_dblock(ss->ssblk, AccountSpace);
		if (err) {
			ss_put(ss, fs);
			putref(b, MKREF(segref));
			putref(b2, MKREF(segref2));
			return err;
		}
		if (!test_bit(B_SegRef, &ss->ssblk->b.flags)) {
			putref(b, MKREF(segref));
			b = getref(&ss->ssblk->b, MKREF(segref));
			ss_put(ss, fs);
		} else if (test_and_set_bit(B_SegRef, &b2->flags))
			ss_put(ss, fs);
		putref(b2, MKREF(segref2));
	}
	putref(b, MKREF(segref));
	return 0;
}

void lafs_seg_deref(struct fs *fs, u64 physaddr, int ssnum)
{
	if (physaddr) {
		struct segsum *ss = segsum_byaddr(fs, physaddr, ssnum);
		BUG_ON(IS_ERR(ss));
		BUG_ON(atomic_read(&ss->refcnt) < 2);
		ss_put(ss, fs);
		ss_put(ss, fs);
	}
}

static void seg_inc(struct fs *fs, struct segsum *ss, int diff, int in_phase)
{
	if (!in_phase)
		atomic_add(diff, &ss->delayed);
	else {
		u32 *b, *p;
		b = map_dblock(ss->ssblk);
		spin_lock(&fs->stable_lock);
		p = &b[ss->segnum & ((fs->blocksize-1)>>USAGE_SHIFT)];
		//BUG_ON(diff < 0 && le32_to_cpu(*p) < -diff);
		if (diff < 0 && le32_to_cpu(*p) < -diff) {
			printk("diff=%d p=%d segnum=%d\n", diff, le32_to_cpu(*p),
			       ss->segnum);
			BUG();
		}
		*p = cpu_to_le32(le32_to_cpu(*p) + diff);
		spin_unlock(&fs->stable_lock);
		unmap_dblock(ss->ssblk, b);
		lafs_dirty_dblock(ss->ssblk);
	}
}

void lafs_seg_move(struct fs *fs, u64 oldaddr, u64 newaddr,
		   int ssnum, int phase, int moveref)
{
	struct segsum *ss;

	dprintk("SEGMOVE %llu %llu\n",
		(unsigned long long) oldaddr,
		(unsigned long long) newaddr);

	if (newaddr) {
		ss = segsum_byaddr(fs, newaddr, ssnum);
		seg_inc(fs, ss, 1, fs->qphase == phase);

		if (!moveref)
			ss_put(ss, fs);
	}

	if (oldaddr) {
		ss = segsum_byaddr(fs, oldaddr, ssnum);
		seg_inc(fs, ss, -1, fs->qphase == phase);
		ss_put(ss, fs);
		if (moveref)
			ss_put(ss, fs);
	}
}

static void set_youth(struct fs *fs, struct segsum *ss)
{
	u16 y;
	u16 *ybuf, *youthp;
	int dirty = 0;

	/* HACK youth_next should always be at least 0x8000 so that
	 * cleanable score differentiates well for new segments.
	 * old code would sometimes set youth_next very low, so
	 * over-ride that
	 */
	if (fs->youth_next < 0x8000)
		fs->youth_next = 0x8000;
	y = fs->youth_next;
	if (fs->scan.do_decay &&
	    (fs->scan.free_dev < ss->devnum
	     || (fs->scan.free_dev == ss->devnum
		 && fs->scan.free_block < (ss->segnum
					   / (fs->blocksize / 2)))
		    ))
		/* Haven't decayed this block yet - revert decay */
		y = decay_undo(y);
	ybuf = map_dblock(ss->youthblk);
	youthp = ybuf + (ss->segnum & ((1 << (fs->blocksize_bits
					      - YOUTH_SHIFT)) - 1));
	if (le16_to_cpu(*youthp) < 8) {
		*youthp = cpu_to_le16(y);
		fs->youth_next++;
		dirty = 1;
	}
	unmap_dblock(ss->youthblk, youthp);
	if (dirty)
		lafs_dirty_dblock(ss->youthblk);
}

static void seg_apply(struct fs *fs, struct segsum *ss)
{
	int err;
	int diff = atomic_read(&ss->delayed);
	if (diff == 0)
		return;
	atomic_set(&ss->delayed, 0);
	dprintk("Seg apply %d %d\n", (int)ss->segnum, diff);
	err = lafs_read_block(ss->ssblk);
	BUG_ON(err); // FIXME do something useful here
	if (ss->youthblk)
		err = lafs_read_block(ss->youthblk);
	BUG_ON(err);
	seg_inc(fs, ss, diff, 1);

	if (diff > 0 && ss->youthblk)
		set_youth(fs, ss);
}

/* lafs_seg_apply_all
 * During a checkpoint, blocks written to the next phase cannot immediately
 * update the segment usage tables so those updates need to be delayed.
 * This function applies all the delayed updates to the segment usage
 * files at the end of a checkpoint.
 */
void lafs_seg_apply_all(struct fs *fs)
{
	int i;

	for (i = 0 ; i < SHASHSIZE ; i++) {
		struct hlist_head *head = &fs->stable[i];
		struct segsum *ss;
		struct hlist_node *n, *pos;
		spin_lock(&fs->stable_lock);
	retry:
		fs->stable_changed = 0;
		hlist_for_each_entry_safe(ss, pos, n, head, hash) {
			if (atomic_read(&ss->delayed) == 0)
				continue;
			atomic_inc(&ss->refcnt);
			spin_unlock(&fs->stable_lock);
			seg_apply(fs, ss);
			ss_put(ss, fs);
			spin_lock(&fs->stable_lock);
			if (fs->stable_changed)
				goto retry;
		}
		spin_unlock(&fs->stable_lock);
	}
	/* Now any clean segments found earlier are free. */
}

/*
 * When creating a snapshot, we need to duplicate table[1] in
 * each segment usage file into table[n].
 * It would be nice to just copy block pointers but this might
 * confuse cleaning etc. so duplicate the data for now.
 */
int lafs_seg_dup(struct fs *fs, int newtable)
{
	/* for each device,
	 * get blocks for each table and memcpy
	 */
	int d;
	int err = 0;
	for (d=0; d < fs->devices ; d++) {
		struct fs_dev *dv = &fs->devs[d];
		int b;
		for (b=0 ; b < dv->tablesize ; b++) {
			struct datablock *obl, *nbl;
			u32 addr;
			void *from, *to;

			addr = dv->tablesize + b;
			obl = lafs_get_block(dv->segsum, addr, 0,
					     GFP_KERNEL | __GFP_NOFAIL,
					     MKREF(ss));
			err = lafs_read_block(obl);
			if (err) {
				putdref(obl, MKREF(ss));
				goto out;
			}

			addr = dv->tablesize * newtable + b;
			nbl = lafs_get_block(dv->segsum, addr, 0,
					     GFP_KERNEL | __GFP_NOFAIL,
					     MKREF(ss));

			from = map_dblock(obl);
			to = map_dblock_2(nbl);
			memcpy(to, from, fs->blocksize);
			unmap_dblock_2(nbl, to);
			unmap_dblock(obl, from);
			lafs_dirty_dblock(nbl);
			putdref(obl, MKREF(ss));
			putdref(nbl, MKREF(ss));
		}
	}
out:
	return err;
}

/*************************************************************
 * Space management:  allocate, use, free
 */

static int count_credits(struct block *b)
{
	int credits = 0;
	struct indexblock *ib = iblk(b);
	struct inode *ino = NULL;

	if (test_bit(B_Credit, &b->flags))
		credits++;
	if (test_bit(B_ICredit, &b->flags))
		credits++;
	if (test_bit(B_NCredit, &b->flags))
		credits++;
	if (test_bit(B_NICredit, &b->flags))
		credits++;
	if (test_bit(B_UnincCredit, &b->flags))
		credits++;
	if (test_bit(B_Dirty, &b->flags))
		credits++;
	if (test_bit(B_Realloc, &b->flags))
		credits++;

	if (test_bit(B_Index, &b->flags)) {
		list_for_each_entry(b, &ib->children, siblings) {
			LAFS_BUG(b->parent != ib, b);
			credits += count_credits(b);
		}
		credits += ib->uninc_table.credits;
	} else if ((ino = rcu_my_inode(dblk(b))) != NULL &&
		   LAFSI(ino)->iblock &&
		   LAFSI(ino)->iblock->b.parent
		) {
		LAFS_BUG(LAFSI(ino)->iblock->b.parent != b->parent, b);
		credits += count_credits(&LAFSI(ino)->iblock->b);
	}
	rcu_iput(ino);
	return credits;
}

int temp_credits;
static void check_credits(struct fs *fs)
{
	/* DEBUGGING AID
	 * count credits in tree and make sure they match allocate_blocks
	 */
	int credits;
	if (fs->ss[0].root == NULL ||
	    LAFSI(fs->ss[0].root)->iblock == NULL ||
	    LAFSI(fs->ss[0].root)->dblock == NULL)
		return;
	credits = count_credits(&LAFSI(fs->ss[0].root)->iblock->b);
	credits += count_credits(&LAFSI(fs->ss[0].root)->dblock->b);
	if (credits + fs->cluster_updates + temp_credits != fs->allocated_blocks) {
		printk("credits=%d updates=%d temp=%d allocated=%d\n", credits,
		       fs->cluster_updates, temp_credits,
		       (int)fs->allocated_blocks);
		lafs_dump_tree();
		WARN_ON(1);
	}
}

void lafs_space_return(struct fs *fs, int credits)
{
	spin_lock(&fs->alloc_lock);
	BUG_ON(credits < 0);
	if (credits > fs->allocated_blocks)
		printk("cred=%d ab=%d\n", credits, (int)fs->allocated_blocks);
	BUG_ON(credits > fs->allocated_blocks);
	fs->allocated_blocks -= credits;
	spin_unlock(&fs->alloc_lock);
	check_credits(fs);
}

int lafs_alloc_cleaner_segs(struct fs *fs, int max)
{
	/* cleaner is about to start.
	 * See how many segments of space can safely
	 * be reserved for its use.
	 * We don't allocate more to the cleaner than we
	 * leave spare for new allocations.
	 */
	int watermark = fs->max_segment * 4;
	int rv = 0;
	spin_lock(&fs->alloc_lock);
	while (fs->clean_reserved < max * fs->max_segment &&
	       fs->free_blocks > 0 &&
	       (u64)fs->free_blocks > (fs->clean_reserved
				       + fs->allocated_blocks
				       + watermark)) {
		fs->clean_reserved += fs->max_segment;
		fs->free_blocks -= fs->max_segment;
		rv++;
	}
	spin_unlock(&fs->alloc_lock);
	return rv;
}

int lafs_space_alloc(struct fs *fs, int credits, int why)
{
	/* 'why's for space reservation.
	 * NewSpace means we want to write a block which didn't exist
	 *   before.  This is allowed to fail or block if the cleaner
	 *   appears able to make progress.
	 * ReleaseSpace means we want to write a block which does currently
	 *   exist, so doing this will eventually free up space.  This must
	 *   never fail, but can block.
	 * CleanSpace means we want to write a block to relocate it to
	 *   a 'cleaning' segment.   This may never fail.
	 * AccountSpace means we absolutely need this block now, and it is
	 *   a BUG is there is no space available.
	 */
	u64 watermark = 0;

	check_credits(fs);
	spin_lock(&fs->alloc_lock);
	switch(why) {
	case NewSpace:
		watermark += RELEASE_RESERVED * fs->max_segment;
		/* FALL THROUGH */
	case ReleaseSpace:
		watermark += ACCOUNT_RESERVED * fs->max_segment;
		/* FALL THROUGH */
	case CleanSpace:
	case AccountSpace:
		/* Definitely no water mark here. */
		break;
	}

	if (fs->rolled && watermark) {
		/* We cannot account properly before roll-forward has
		 * completed. FIXME once it has completed we need to
		 * check and invalidate the FS if there was a problem.
		 */
		if (fs->free_blocks < 0 ||
		    (u64)fs->free_blocks < (fs->allocated_blocks
					    + credits + watermark))
			credits = 0; /* Sorry, no room */
	}
	if (fs->rolled && watermark == 0) {
		/* When including the clean_reserved space, there should
		 * be room for these controlled allocations
		 */
		BUG_ON(fs->free_blocks < 0);
		if (fs->free_blocks + fs->clean_reserved <
		    fs->allocated_blocks + credits)
			BUG();
	}

	if (credits == 0) {
		if (why == NewSpace && !test_bit(EmergencyClean, &fs->fsstate))
			set_bit(EmergencyPending, &fs->fsstate);
		if (why >= ReleaseSpace || !test_bit(EmergencyClean, &fs->fsstate))
			if (!test_bit(CleanerBlocks, &fs->fsstate) ||
			    fs->cleaner.need > watermark + fs->max_segment) {
				fs->cleaner.need = watermark + fs->max_segment;
				set_bit(CleanerBlocks, &fs->fsstate);
				lafs_wake_thread(fs);
			}
	} else if (why == NewSpace)
		if (test_bit(EmergencyClean, &fs->fsstate) ||
		    test_bit(EmergencyPending, &fs->fsstate)) {
			clear_bit(EmergencyPending, &fs->fsstate);
			clear_bit(EmergencyClean, &fs->fsstate);
		}

	fs->allocated_blocks += credits;
	BUG_ON(fs->free_blocks + fs->clean_reserved < fs->allocated_blocks);
	spin_unlock(&fs->alloc_lock);
	return credits;
}

/********************************************************************
 *
 * Segment usage / youth tracking
 */

/* Information about free and cleanable segments are stored in a table
 * comprised of a few pages.  Indexes into this table are 16bit.  4bits
 * for page number, 12 bits for index in the page.
 * Different pages have different sized entries to allow for different
 * depths in the skiplist that sorts entries by dev/segment.
 * Entries are at least 16 bytes, so 12 bit can index up 64K.
 * Each entry is also on one of four lists:
 *  - unused:  this entry is not used
 *  - free: this entry is for a free segment
 *  - cleanable:  this entry is for a cleanable segment.
 *  - clean: segment has been cleaned but is not yet free (awaiting checkpoint).
 * These are singly linked lists (via 'next').  We record head and tail.
 * The end of this list has a pointer to 0xFFFF, not 0.
 * We usually remove entries from the head, but when the table is
 * full, we might walk part way down a list and discard all the rest.
 * We discard them by setting score to 0xFFFFFFFF.  We then walk the
 * skip list moving anything with a score of 0xFFFFFFFF to the unused list.
 *
 * We occasionally sort the cleanable list using a merge sort.
 *
 * There is a progression from 'cleanable' to 'clean' to 'free', though
 * segments can enter at any point.
 * When we choose a segment to clean, we remove the entry from the cleanable
 * list. We do this by setting the score to 0xFFFFFFFE and unlinking it.
 * After cleaning has completed a scan should find that it is clean and so
 * add it to the 'clean' list.
 * When we record a 'clean' segment as 'free' (after a checkpoint) we
 * move it from the clean list to the free list, from where it will be
 * removed when needed.
 * We don't add new free segments when the total of free+clean segments
 * is more than half the size of the table.
 * Similarly when the cleanable table reaches half the available size
 * we remove the least-interesting half.
 * When we choose a free segment to start filling, we remove it from the
 * free list but not from the table.  The score is set to 0xFFFFFFFD to
 * record that it is unlinked but busy.  If it is found to be cleanable,
 * it will be ignored.  When we finish filling a segment, we find the entry
 * again and remove it properly so it can become cleanable later.
 */

#define SCORE_MAX	0xFFFFFFFFFFFFFFFCULL	/* Maximum normal score */
#define SCORE_ACTIVE	0xFFFFFFFFFFFFFFFDULL	/* This segment is being written to */
#define SCORE_CLEANING	0xFFFFFFFFFFFFFFFEULL	/* This segment in being cleaned */
#define SCORE_DEAD	0xFFFFFFFFFFFFFFFFULL	/* This segment is to be removed */

struct segstat {
	u16	next;
	u16	dev;
	u32	segment;
	u64	score;
	u32	usage;
	u16	skip[0]; /* or larger... */
};

static inline struct segstat *segfollow(struct segtracker *st, u16 link)
{
	void *a;
	if (link == 0xFFFF)
		return NULL;
	a = st->page[link >> 12];
	a += (link & 0xFFF) *
		(st->size[link >> 12] * sizeof(u16) + sizeof(struct segstat));
	return (struct segstat *) a;
}

int lafs_segtrack_init(struct segtracker *st)
{
	int found;
	int h, i;
	int n[4];

	st->page[0] = kmalloc(PAGE_SIZE, GFP_KERNEL);
	st->page[1] = kmalloc(PAGE_SIZE, GFP_KERNEL);
	st->page[2] = kmalloc(PAGE_SIZE, GFP_KERNEL);
	st->page[3] = kmalloc(PAGE_SIZE, GFP_KERNEL);

	if (st->page[0] == NULL ||
	    st->page[1] == NULL ||
	    st->page[2] == NULL ||
	    st->page[3] == NULL) {
		lafs_segtrack_free(st);
		return -ENOMEM;
	}
	st->size[0] = 1;
	st->size[1] = 1;
	st->size[2] = 5;
	st->size[3] = 9;
	BUG_ON(SEG_NUM_HEIGHTS <= 9);

	st->unused.first = st->unused.last = 0xffff;
	st->cleanable.first = st->cleanable.last = 0xffff;
	st->free.first = st->free.last = 0xffff;
	st->clean.first = st->clean.last = 0xffff;
	st->unused.cnt = st->free.cnt = st->cleanable.cnt = st->clean.cnt = 0;

	for (h = 0; h < SEG_NUM_HEIGHTS; h++)
		st->head[h] = 0xFFFF;

	/* how many entries in each page */
	for (i = 0 ; i < 4; i++)
		n[i] = PAGE_SIZE /  (sizeof(struct segstat) +
				     st->size[i] * sizeof(u16));

	do {
		char rand;
		int p;
		found = 0;
		get_random_bytes(&rand, 1);
		p = rand & 3;

		while (p < 3 && n[p] == 0)
			p++;
		for ( ; p >= 0; p--)
			if (n[p]) {
				int sn;
				struct segstat *ss;
				n[p]--;
				sn = (p<<12) + n[p];

				ss = segfollow(st, sn);
				ss->next = st->unused.first;
				st->unused.first = sn;
				if (st->unused.cnt == 0)
					st->unused.last = sn;
				st->unused.cnt++;
				found = 1;
			}
	} while (found);
	st->total = st->unused.cnt;
	return 0;
}

void lafs_segtrack_free(struct segtracker *st)
{
	kfree(st->page[0]);
	kfree(st->page[1]);
	kfree(st->page[2]);
	kfree(st->page[3]);
}

int lafs_check_seg_cnt(struct segtracker *st)
{
	/* check at unused, free, cleanable, clean have correct count.
	 */
	int sn, cnt, prev;

	cnt = 0;
	for (prev = sn = st->unused.first ;
	     sn != 0xFFFF ;
	     sn = segfollow(st, (prev = sn))->next)
		cnt++;
	if (cnt != st->unused.cnt) {
		printk("%d != %d\n", cnt, st->unused.cnt); WARN_ON(1);
		return 1;
	}
	if (st->unused.last != prev) {
		printk("L%d != %d\n", prev, st->unused.last); WARN_ON(1);
		return 1;
	}

	cnt = 0;
	for (prev = sn = st->free.first ;
	     sn != 0xFFFF;
	     sn = segfollow(st, (prev = sn))->next)
		cnt++;
	if (cnt != st->free.cnt) {
		printk("%d != %d\n", cnt, st->free.cnt); WARN_ON(1);
		return 1;
	}
	if (st->free.last != prev) {
		printk("L%d != %d\n", prev, st->free.last); WARN_ON(1);
		return 1;
	}

	cnt = 0;
	for (prev = sn = st->clean.first ;
	     sn != 0xFFFF;
	     sn = segfollow(st, (prev = sn))->next)
		cnt++;
	if (cnt != st->clean.cnt) {
		printk("%d != %d\n", cnt, st->clean.cnt); WARN_ON(1);
		return 1;
	}
	if (st->clean.last != prev) {
		printk("L%d != %d\n", prev, st->clean.last); WARN_ON(1);
		return 1;
	}

	cnt = 0;
	for (prev = sn = st->cleanable.first ;
	     sn != 0xFFFF;
	     sn = segfollow(st, (prev = sn))->next)
		cnt++;
	if (cnt != st->cleanable.cnt) {
		printk("%d != %d\n", cnt, st->cleanable.cnt); WARN_ON(1);
		return 1;
	}
	if (st->cleanable.last != prev) {
		printk("L%d != %d\n", prev, st->cleanable.last); WARN_ON(1);
		return 1;
	}

	return 0;
}

static void segsort(struct segtracker *st, struct slist *l)
{
	/* sort the 'l' list of st by score - lowest first */
	/* use a merge sort */

	u16 last = 0xFFFF;
	u16 head[2];
	head[0] = l->first;
	head[1] = 0xFFFF;

	do {
		u16 *hp[2], h[2];
		int curr = 0;
		u32 prev = 0;
		int next = 0;

		hp[0] = &head[0];
		hp[1] = &head[1];

		h[0] = head[0];
		h[1] = head[1];

		/* Find the least of h[0] and h[1] that is not less than
		 * prev, and add it to hp[curr].  If both are larger than
		 * prev, flip 'curr' and add smallest.
		 */
		while (h[0] != 0xFFFF || h[1] != 0xFFFF) {
			if (h[next] == 0xFFFF ||
			    (h[1-next] != 0xFFFF &&
			     !((prev <= segfollow(st, h[1-next])->score)
			       ^ (segfollow(st, h[1-next])->score
				  <= segfollow(st, h[next])->score)
			       ^ (segfollow(st, h[next])->score <= prev))
				    ))
				next = 1 - next;

			if (segfollow(st, h[next])->score < prev)
				curr = 1 - curr;
			prev = segfollow(st, h[next])->score;
			*hp[curr] = h[next];
			hp[curr] = &segfollow(st, h[next])->next;
			last = h[next];
			h[next] = segfollow(st, h[next])->next;
		}
		*hp[0] = 0xFFFF;
		*hp[1] = 0xFFFF;
	} while (head[0] != 0xFFFF && head[1] != 0xFFFF);
	if (head[0] == 0xFFFF)
		l->first = head[1];
	else
		l->first = head[0];
	l->last = last;
}

static int statcmp(struct segstat *ss, u16 dev, u32 seg)
{
	if (ss->dev < dev)
		return -1;
	if (ss->dev > dev)
		return 1;
	if (ss->segment < seg)
		return -1;
	if (ss->segment > seg)
		return 1;
	return 0;
}

static struct segstat *segfind(struct segtracker *st, u16 dev,
			       u32 segnum, u16 *where[SEG_NUM_HEIGHTS])
{
	/* Find the segment entry for dev/segnum.
	 * Return link info in where to allow insertion/deletion.
	 */
	int level;
	u16 *head = st->head;
	for (level = SEG_NUM_HEIGHTS-1 ; level >= 0; level--) {
		while (head[level] != 0xffff &&
		       statcmp(segfollow(st, head[level]), dev, segnum) < 0) {
			head = segfollow(st, head[level])->skip;
		}
		where[level] = &head[level];
	}
	if (head[0] == 0xFFFF ||
	    statcmp(segfollow(st, head[0]), dev, segnum) != 0)
		return NULL;

	return segfollow(st, head[0]);
}

static int segchoose_height(struct segtracker *st, u16 ssn)
{
	unsigned long rand[DIV_ROUND_UP(SEG_NUM_HEIGHTS * 2 / 8 + 1,
					sizeof(unsigned long))];
	int max = st->size[ssn>>12];
	int h = 0;
	get_random_bytes(rand, sizeof(rand));

	if (max > 1)
		h = 1;

	while (h < max &&
	       test_bit(h*2, rand) &&
	       test_bit(h*2+1, rand))
		h++;
	return h;
}

static void seginsert(struct segtracker *st, u16 ssn,
		      u16 *where[SEG_NUM_HEIGHTS])
{
	/* We looked for 'ss' but didn't find it.  'where' is the
	 * result of looking.  Now insert 'ss'
	 */
	struct segstat *ss = segfollow(st, ssn);
	int h = segchoose_height(st, ssn);
	while (h >= 0) {
		ss->skip[h] = *where[h];
		*where[h] = ssn;
		h--;
	}
}

static void segdelete(struct segtracker *st, struct segstat *ss)
{
	/* This segstat has just been removed from a list (free or cleanable).
	 * Remove it from the skiplist as well
	 */
	u16 *where[SEG_NUM_HEIGHTS];
	struct segstat *ss2 = segfind(st, ss->dev, ss->segment, where);
	int h;
	int pos;

	BUG_ON(ss2 == NULL);

	pos = *where[0];
	for (h = 0; h < SEG_NUM_HEIGHTS && *where[h] == pos ; h++)
		*where[h] = ss2->skip[h];
	ss2->next = st->unused.first;
	st->unused.first = pos;
	if (st->unused.cnt == 0)
		st->unused.last = pos;
	st->unused.cnt++;
	lafs_check_seg_cnt(st);
}

static void segdelete_all(struct segtracker *st, struct fs *fs)
{
	/* walk entire skip list.  Any entry with score of SCORE_DEAD
	 * is deleted
	 */
	u16 *where[SEG_NUM_HEIGHTS];
	int h;

	for (h = SEG_NUM_HEIGHTS-1; h >= 0; h--)
		where[h] = &st->head[h];

	while (*where[0] != 0xFFFF) {
		u16 pos = *where[0];
		struct segstat *ss = segfollow(st, pos);
		if (ss->score == SCORE_DEAD) {
			/* delete this entry */

			/* FIXME Locking is really bad here */
			struct segsum *ssm;
			ssm = segsum_find(fs, ss->segment, ss->dev, 0);
			putdref(ssm->ssblk, MKREF(intable));
			putdref(ssm->youthblk, MKREF(intable));
			ss_put(ssm, fs);

			for (h = 0; h < SEG_NUM_HEIGHTS && *where[h] == pos; h++)
				*where[h] = ss->skip[h];
			ss->next = st->unused.first;
			st->unused.first = pos;
			if (st->unused.cnt == 0)
				st->unused.last = pos;
			st->unused.cnt++;
			lafs_check_seg_cnt(st);
		} else {
			/* advance 'where' to here */
			for (h = 0; h < SEG_NUM_HEIGHTS && *where[h] == pos; h++)
				where[h] = &ss->skip[h];
		}
	}
}

static u16 seg_pop(struct segtracker *st, struct slist *which)
{
	struct segstat *ss;
	u16 rv = which->first;
	if (rv == 0xFFFF)
		return rv;
	ss = segfollow(st, rv);
	which->first = ss->next;
	which->cnt--;
	if (ss->next == 0xFFFF)
		which->last = 0xFFFF;
	return rv;
}

static struct segstat *seg_add_new(struct segtracker *st, struct slist *which, int atend,
				   int dev, u32 seg, long long score, int usage,
				   u16 *where[SEG_NUM_HEIGHTS])
{
	int ssn;
	struct segstat *ss;

	ssn = seg_pop(st, &st->unused);
	ss = segfollow(st, ssn);
	if (!ss)
		return ss;

	if (which) {
		if (atend) {
			int l = which->last;
			ss->next = 0xFFFF;
			if (l == 0xFFFF) {
				which->first = ssn;
				which->last = ssn;
			} else {
				which->last = ssn;
				BUG_ON(segfollow(st, l)->next != 0xFFFF);
				segfollow(st, l)->next = ssn;
			}
		} else {
			ss->next = which->first;
			which->first = ssn;
			if (which->last == 0xFFFF)
				which->last = ssn;
		}
		which->cnt++;
	}
	ss->dev = dev;
	ss->segment = seg;
	ss->score = score;
	ss->usage = usage;
	seginsert(st, ssn, where);
	lafs_check_seg_cnt(st);
	return ss;
}

void lafs_free_get(struct fs *fs, unsigned int *dev, u32 *seg,
		   int nonlogged)
{
	/* Select and return a free segment.
	 * The youth value must have been zero, and we set it to the
	 * current youth number.
	 */
	struct segstat *ss;
	struct segsum *ssum = NULL;
	int ssnum;

	BUG_ON(nonlogged); // FIXME should handle this case, not ignore it

again:
	spin_lock(&fs->lock);

	wait_event_lock(fs->phase_wait,
			!fs->scan.first_free_pass ||
			fs->segtrack->free.first != 0xFFFF,
			fs->lock);

	ss = segfollow(fs->segtrack, fs->segtrack->free.first);
	BUG_ON(!ss);

	*dev = ss->dev;
	*seg = ss->segment;

	if (!test_bit(DelayYouth, &fs->fsstate) &&
	    !(ssum &&
	      ssum->devnum == ss->dev &&
	      ssum->segnum == ss->segment)) {
		spin_unlock(&fs->lock);
		if (ssum)
			ss_put(ssum, fs);

		ssum = segsum_find(fs, ss->segment, ss->dev, 0);
		/* As we hold a ref on youth block for anything in the
		 * table, and that block was loaded at the time, it must
		 * still be valid.
		 */
		BUG_ON(!ssum || !ssum->youthblk);
		BUG_ON(!test_bit(B_Valid, &ssum->youthblk->b.flags));
		set_bit(B_PinPending, &ssum->youthblk->b.flags);
		lafs_pin_dblock(ssum->youthblk, AccountSpace);
		goto again;
	}
	seg_pop(fs->segtrack, &fs->segtrack->free);

	ss->score = SCORE_ACTIVE;
	/* still in table, but unlinked */

	if (ssum && test_bit(DelayYouth, &fs->fsstate)) {
		ss_put(ssum, fs);
		ssum = NULL;
	}

	if (ssum)
		set_youth(fs, ssum);

	spin_unlock(&fs->lock);

	/* now need to reserve/dirty/reference the youth and
	 * segsum block for each snapshot that could possibly
	 * get written here.
	 * NOTE: this needs fixing to support snapshots. Later.
	 */
	for (ssnum = 0; ssnum < 1 ; ssnum++) {
		struct segsum *ssum2;
		ssum2 = segsum_find(fs, ss->segment, ss->dev, ssnum);
		if (IS_ERR(ssum2))
			/* ?? what do I need to release etc */
			/* Maybe this cannot fail because we own references
			 * to the two blocks !! */
			LAFS_BUG(1, NULL);
		lafs_checkpoint_lock(fs);
		set_bit(B_PinPending, &ssum2->ssblk->b.flags);
		(void)lafs_pin_dblock(ssum2->ssblk, AccountSpace);
		lafs_checkpoint_unlock(fs);
	}
	if (ssum)
		ss_put(ssum, fs);

	dprintk("NEXT segment found %d/%d youth %d\n",
		*dev, *seg, fs->youth_next - 1);
	/* Note that we return an implicit reference to the ssum */
}

void lafs_update_youth(struct fs *fs, int dev, u32 seg)
{
	struct segsum *ssum = NULL;
	ssum = segsum_find(fs, seg, dev, 0);
	set_bit(B_PinPending, &ssum->youthblk->b.flags);
	lafs_pin_dblock(ssum->youthblk, AccountSpace);
	spin_lock(&fs->lock);
	set_youth(fs, ssum);
	spin_unlock(&fs->lock);
	ss_put(ssum, fs);
}

void lafs_seg_forget(struct fs *fs, int dev, u32 seg)
{
	/* this segment was being filled and is now full.
	 * We need to drop it from the table, and drop
	 * references to the blocks
	 */
	struct segstat tmp;
	struct segsum *ss;

	spin_lock(&fs->lock);
	tmp.dev = dev;
	tmp.segment = seg;
	segdelete(fs->segtrack, &tmp);
	spin_unlock(&fs->lock);

	ss = segsum_find(fs, seg, dev, 0);
	BUG_ON(IS_ERR(ss));
	BUG_ON(atomic_read(&ss->refcnt) < 2);
	/* Removed from table so ... */
	putdref(ss->ssblk, MKREF(intable));
	putdref(ss->youthblk, MKREF(intable));
	ss_put(ss, fs);
	ss_put(ss, fs);
}

int lafs_clean_count(struct fs *fs, int *any_clean)
{
	int rv;
	spin_lock(&fs->lock);
	*any_clean = fs->segtrack->clean.cnt != 0;
	rv = fs->segtrack->free.cnt + fs->segtrack->clean.cnt;
	spin_unlock(&fs->lock);
	return rv;
}

static int add_free(struct fs *fs, unsigned int dev, u32 seg, u16 *youthp)
{
	/* This dev/seg is known to be free. add it to the list */
	struct segstat *ss;
	u16 *where[SEG_NUM_HEIGHTS];
	dprintk("add_free %d %d\n", (int)dev, (int)seg);
	spin_lock(&fs->lock);
	if (*youthp) {
		/* not really free any more */
		spin_unlock(&fs->lock);
		return 0;
	}

	ss = segfind(fs->segtrack, dev, seg, where);
	if (ss) {
		/* already in the table.  If it happens to be
		 * on the cleanable list, we have to wait for
		 * the cleaner to find it and add it to our list.
		 */
		spin_unlock(&fs->lock);
		return 0;
	}
	if (fs->segtrack->free.cnt + fs->segtrack->clean.cnt >=
	    fs->segtrack->total / 2) {
		/* Have enough free/clean entries already */
		spin_unlock(&fs->lock);
		return 0;
	}

	seg_add_new(fs->segtrack, &fs->segtrack->free, 0,
		    dev, seg, 0, 0, where);

	spin_unlock(&fs->lock);
	return 1;
}

static int add_clean(struct fs *fs, unsigned int dev, u32 seg, bool if_present)
{
	/* This dev/seg is now clean.  Make sure it is on the list.
	 * Chances are that this is already present in the table as
	 * a recently cleaned segment.  If 'if_present', then insist
	 * on this.
	 * Return TRUE if segment was added to the table (as this
	 * implies a reference count).
	 */
	struct segstat *ss;
	u16 *where[SEG_NUM_HEIGHTS];

	dprintk("ADD CLEAN %d/%d %d #####################################\n",
		dev, seg, fs->segtrack->clean.cnt);
	spin_lock(&fs->lock);
	ss = segfind(fs->segtrack, dev, seg, where);
	if (ss) {
		/* already in the table.  If loose, attach it to
		 * the clean list.  If cleanable, set score to 0 to make
		 * sure it is found soon
		 */
		if (ss->score == SCORE_CLEANING) {
			ss->next = fs->segtrack->clean.first;
			fs->segtrack->clean.first = where[0][0];
			if (fs->segtrack->clean.last == 0xFFFF)
				fs->segtrack->clean.last =
					fs->segtrack->clean.first;
			fs->segtrack->clean.cnt++;
		}
		if (ss->score != SCORE_ACTIVE) {
			/* Must be on the clean list now */
			ss->score = 0;
			ss->usage = 0;
		}
		spin_unlock(&fs->lock);
		return 0;
	}
	if (if_present) {
		spin_unlock(&fs->lock);
		return 0;
	}

	if (fs->segtrack->free.cnt + fs->segtrack->clean.cnt >=
	    fs->segtrack->total / 2) {
		/* Have enough free/clean entries already */
		spin_unlock(&fs->lock);
		return 0;
	}

	ss = seg_add_new(fs->segtrack, &fs->segtrack->clean, 0,
			 dev, seg, 0, 0, where);

	spin_unlock(&fs->lock);
	if (ss)
		return 1;
	else
		return 0;
}

void lafs_add_active(struct fs *fs, u64 addr)
{
	/* add this segment to the table as 'active'.
	 * This is only called once at mount time for the
	 * active segments.  Other segments become active
	 * via free_get
	 */
	struct segstat *ss;
	u16 *where[SEG_NUM_HEIGHTS];
	struct segsum *ssum = segsum_byaddr(fs, addr, 0);
	(void)getdref(ssum->ssblk, MKREF(intable));
	(void)getdref(ssum->youthblk, MKREF(intable));
	spin_lock(&fs->lock);
	ss = segfind(fs->segtrack, ssum->devnum, ssum->segnum, where);
	BUG_ON(ss);
	ss = seg_add_new(fs->segtrack, NULL, 0,
			 ssum->devnum, ssum->segnum,
			 SCORE_ACTIVE, 0, where);
	BUG_ON(!ss);

	spin_unlock(&fs->lock);
}

void lafs_clean_free(struct fs *fs)
{
	/* We are finishing off a checkpoint.  Move all from 'clean'
	 * list to 'free' list, and set the youth for each to 0.
	 * Note that we can walk the 'clean' list without locking as
	 * items are only ever removed by this thread.
	 */
	int ssn;
	struct segstat *ss;
	if (fs->segtrack->clean.cnt == 0)
		return;
	for (ssn = fs->segtrack->clean.first ; ssn != 0xffff; ssn = ss->next) {
		struct datablock *db;
		int err;
		ss = segfollow(fs->segtrack, ssn);
		spin_lock(&fs->lock);
		fs->free_blocks += fs->devs[ss->dev].segment_size;
		spin_unlock(&fs->lock);
		db = lafs_get_block(fs->devs[ss->dev].segsum,
				    ss->segment >> (fs->blocksize_bits
						    - YOUTH_SHIFT),
				    NULL, GFP_KERNEL | __GFP_NOFAIL,
				    MKREF(cleanfree));
		err = lafs_read_block(db);
		if (err == 0)
			err = lafs_reserve_block(&db->b, AccountSpace);
		if (err == 0) {
			u16 *b = map_dblock(db);
			spin_lock(&fs->stable_lock);
			b[ss->segment & ((fs->blocksize-1)>>YOUTH_SHIFT)] = 0;
			spin_unlock(&fs->stable_lock);
			unmap_dblock(db, b);
			lafs_dirty_dblock(db);
		} else
			/* FIXME make filesystem read-only */
			BUG();
		putdref(db, MKREF(cleanfree));
	}
	/* Now move them to the 'free' list */
	spin_lock(&fs->lock);
	fs->segtrack->free.cnt += fs->segtrack->clean.cnt;
	if (fs->segtrack->free.last == 0xFFFF)
		fs->segtrack->free.first = fs->segtrack->clean.first;
	else
		segfollow(fs->segtrack, fs->segtrack->free.last)->next
			= fs->segtrack->clean.first;
	fs->segtrack->free.last = fs->segtrack->clean.last;
	fs->segtrack->clean.first = 0xFFFF;
	fs->segtrack->clean.last = 0xFFFF;
	fs->segtrack->clean.cnt = 0;
	spin_unlock(&fs->lock);
}

void lafs_dump_cleanable(void)
{
	int i;
	struct segtracker *st;
	u16 ssn;
	struct segstat *ss;
	if (!dfs) {
		printk("No cleanable table\n");
		return;
	}
	st = dfs->segtrack;
	printk("============= Cleanable table (%d) =================\n",
	       st->cleanable.cnt);
	printk("pos: dev/seg  usage score\n");

	i = 0;
	for (ssn = st->cleanable.first; ssn != 0xffff; ssn = ss->next) {
		ss = segfollow(st, ssn);

		printk("%3d: %3d/%-4d %5d %lld\n",
		       i, ss->dev,
		       ss->segment,
		       ss->usage,
		       ss->score);
		i++;
	}
	printk("...sorted....\n");
	segsort(st, &st->cleanable);
	i = 0;
	for (ssn = st->cleanable.first; ssn != 0xffff; ssn = ss->next) {
		ss = segfollow(st, ssn);

		printk("%3d: %3d/%-4d %5d %lld\n",
		       i, ss->dev,
		       ss->segment,
		       ss->usage,
		       ss->score);
		i++;
	}
	printk("--------------- Free table (%d) ---------------\n",
	       st->free.cnt);
	i = 0;
	for (ssn = st->free.first; ssn != 0xffff; ssn = ss->next) {
		ss = segfollow(st, ssn);

		printk("%3d: %3d/%-4d %5d %lld\n",
		       ssn, ss->dev,
		       ss->segment,
		       ss->usage,
		       ss->score);
		i++;
	}
	printk("--------------- Clean table (%d) ---------------\n",
	       st->clean.cnt);
	i = 0;
	for (ssn = st->clean.first; ssn != 0xffff; ssn = ss->next) {
		ss = segfollow(st, ssn);

		printk("%3d: %3d/%-4d %5d %lld\n",
		       ssn, ss->dev,
		       ss->segment,
		       ss->usage,
		       ss->score);
		i++;
	}
	printk("--------\n");
	printk("free_blocks=%lld allocated=%llu max_seg=%llu clean_reserved=%llu\n",
	       dfs->free_blocks, dfs->allocated_blocks, dfs->max_segment,
	       dfs->clean_reserved);
}

int lafs_get_cleanable(struct fs *fs, u16 *dev, u32 *seg)
{
	/* Return the most cleanable segment on the list.
	 * The segstat is removed from the 'cleanable' list
	 * and left in the table.  This prevents it being re-added.
	 * When it is later found to be clean, it will be added
	 * to the 'clean' list.
	 */
	struct segtracker *st = fs->segtrack;
	struct segsum *ssm;
	struct segstat *ss;

	lafs_check_seg_cnt(st);

retry:
	spin_lock(&fs->lock);
	if (st->cleanable.first == 0xFFFF) {
		spin_unlock(&fs->lock);
		return 0;
	}

	if (st->sorted_size < st->cleanable.cnt / 2) {
		segsort(st, &st->cleanable);
		st->sorted_size = st->cleanable.cnt;
		st->max_score = segfollow(st, st->cleanable.last)->score;
	}

	lafs_check_seg_cnt(fs->segtrack);

	ss = segfollow(st, seg_pop(st, &st->cleanable));

	if (lafs_check_seg_cnt(fs->segtrack)) {
		int sj;
		printk("first=%x last=%x cnt=%d\n", st->cleanable.first,
		       st->cleanable.last, st->cleanable.cnt);
		for (sj = st->cleanable.first ;
		     sj != 0xFFFF;
		     sj = segfollow(st, sj)->next)
			printk("  %x\n", sj);

		WARN_ON(1);
	}

	st->sorted_size--;

	*dev = ss->dev;
	*seg = ss->segment;

	dprintk("SEG: cleanable %d/%d score=%llu usage=%d\n",
		ss->dev, ss->segment, (unsigned long long)ss->score, ss->usage);

	ss->score = SCORE_CLEANING;
	if (ss->usage == 0) {
		int rv;
		spin_unlock(&fs->lock);
		rv = add_clean(fs, *dev, *seg, true);
		BUG_ON(rv); /* passing 'true' makes this impossible */
		goto retry;
	}
	segdelete(fs->segtrack, ss);
	spin_unlock(&fs->lock);

	ssm = segsum_find(fs, ss->segment, ss->dev, 0);
	putdref(ssm->ssblk, MKREF(intable));
	putdref(ssm->youthblk, MKREF(intable));
	ss_put(ssm, fs);

	return 1;
}

static int add_cleanable(struct fs *fs, unsigned int dev, u32 seg,
		   u16 youth, u32 usage)
{
	u64 score;
	struct segstat *ss;
	u32 segsize;
	u16 *where[SEG_NUM_HEIGHTS];

	lafs_check_seg_cnt(fs->segtrack);
	if (fs->scan.trace || lafs_trace || 1)
		printk("CLEANABLE: %u/%lu y=%d u=%d\n",
		       dev, (unsigned long)seg, (int)youth, (int)usage);
	if (youth < 8)
		return 0;
	if (youth >= fs->checkpoint_youth)
		return 0;

	segsize = fs->devs[dev].segment_size;
	fs->total_free += segsize - usage /* - 1 */;

	if (usage == 0) {
		int rv = add_clean(fs, dev, seg, false);
		lafs_check_seg_cnt(fs->segtrack);
		return rv;
	}

	if (test_bit(EmergencyClean, &fs->fsstate))
		score = usage;
	else {
		/* 0x100000000 is to ensure this score is always
		 * more than the above score */
		score = (u64)youth * usage;
		do_div(score, segsize);
		score += 0x100000000;
	}

	spin_lock(&fs->lock);
	if (score > SCORE_MAX)
		score = SCORE_MAX;

	ss = segfind(fs->segtrack, dev, seg, where);
	if (ss) {
		/* already in the table.  Just update the usage.
		 * It must be on the right list.
		 */
		if (ss->score == SCORE_ACTIVE)
			; /* still in use */
		else if (ss->usage == 0 && ss->score > 0)
			; /* on free list, leave it alone */
		else {
			ss->usage = usage;
			ss->score = score;
		}
		lafs_check_seg_cnt(fs->segtrack);
		spin_unlock(&fs->lock);
		return 0;
	}

	if (fs->segtrack->cleanable.cnt >= fs->segtrack->total/2) {
		/* Table of cleanable segments is full.  Sort and discard
		 * half
		 */
		int ssnum, prev;
		struct segstat *ss;
		int keep, i;
		segsort(fs->segtrack, &fs->segtrack->cleanable);
		ssnum = fs->segtrack->cleanable.first;
		keep = (fs->segtrack->cleanable.cnt+1) / 2;
		for (i = 0; i < keep; i++) {
			prev = ssnum;
			ss = segfollow(fs->segtrack, ssnum);
			ssnum = ss->next;
		}
		fs->segtrack->max_score = ss->score;
		ss->next = 0xffff;
		fs->segtrack->cleanable.last = prev;
		fs->segtrack->cleanable.cnt = keep;
		ss = segfollow(fs->segtrack, ssnum);
		while (ss) {
			ss->score = SCORE_DEAD;
			ss = segfollow(fs->segtrack, ss->next);
		}
		segdelete_all(fs->segtrack, fs);
		fs->segtrack->sorted_size = fs->segtrack->total/2;
	}
	if (fs->segtrack->cleanable.cnt <= fs->segtrack->total/4)
		fs->segtrack->max_score = 0;
	if (fs->segtrack->max_score &&
	    fs->segtrack->max_score < score) {
		/* score too high to bother with */
		lafs_check_seg_cnt(fs->segtrack);
		spin_unlock(&fs->lock);
		return 0;
	}

	seg_add_new(fs->segtrack, &fs->segtrack->cleanable, 1,
		    dev, seg, score, usage, where);
	spin_unlock(&fs->lock);
	return 1;
}

static void merge_usage(struct fs *fs, u32 *d)
{
	u32 *u = fs->scan.free_usages;
	int segperblk = fs->blocksize >> USAGE_SHIFT;
	int i;

	for (i = 0; i < segperblk; i++)
		if (le32_to_cpu(d[i]) > le32_to_cpu(u[i]))
			u[i] = d[i];
}

unsigned long lafs_scan_seg(struct fs *fs)
{
	/* FIXME this comment is very out-dated */
	/* Process one block of youth or segment-usage data.  We
	 * collect free segments (youth==0) into a table that is kept
	 * sorted to ensure against duplicates.  It is treated like a
	 * ring buffer with a head and a tail to distinguish free
	 * space from used space.  head/tail point to the free space
	 * (which is never empty).  As we scan all segments
	 * sequentially any free segment we find is placed at the head
	 * of the free list and the entry just after the tail might
	 * get discarded if we run out of room, or if that entry
	 * matches the entry we just inserted.  New free segments are
	 * allocated from just after the tail.  i.e. we increment the
	 * tail and return the entry recorded there.  If tail+1 ==
	 * head, there are no segments in the buffer.
	 *
	 * We also collect potentially cleanable segments.  These are any
	 * segments which is not empty and not non-logged (i.e youth>=8).
	 * We record them in a table and whenever the table gets full, we
	 * sort the table by score, and discard the less-cleanable half.
	 * We only add segments to the table when it is less than half full,
	 * or when the new segment has a higher score than the segment at
	 * the half-way mark.
	 *
	 * To get the score for a segment, we need its youth and its usage.
	 * The usage is the maximum usage from all snapshots.
	 * We allocate a block to hold the current max usage.
	 * We:
	 *   load the youth table, find free segments, decay youth if needed.
	 *   load the first usage table and copy to temporary block
	 *   repeat: load other usage tables and merge
	 *   calculate score for each segment and add those with a good score
	 *
	 * On the first pass through, we count all the free segments into
	 * the fs->free_blocks.
	 *
	 * In the first pass, we continue make a full pass without deliberately
	 * waiting.  In subsequent passes ... only once per checkpoint???
	 */

	if (fs->scan.done)
		return MAX_SCHEDULE_TIMEOUT;

	dprintk("scan: dev=%d block=%d stage=%d\n",
		fs->scan.free_dev, fs->scan.free_block, fs->scan.free_stage);
	if (fs->scan.free_stage == 0) {
		/* Need to find the youth block for next dev/offset.
		 * Possibly we are finished with this dev and must go
		 * to next.  Possibly we are finished altogether.
		 */
		int dev = fs->scan.free_dev;
		int block = fs->scan.free_block + 1;
		int youthblock = block >> (USAGE_SHIFT - YOUTH_SHIFT);
		int err;

		while (dev < 0 ||
		       block > (fs->devs[dev].segment_count
				>> (fs->blocksize_bits - 1))) {
			dev++;
			block = 0;
			if (dev >= fs->devices) {
				fs->scan.free_dev = -1;
				dev = -1;
				fs->scan.do_decay = 0;
				fs->scan.trace = 0;
				fs->total_free_prev = fs->total_free;
				fs->total_free = 0;
				fs->scan.first_free_pass = 0;
				break;
			}
		}
		if (fs->scan.youth_db)
			if (fs->scan.youth_db->b.fileaddr != youthblock ||
			    dev < 0 ||
			    fs->scan.youth_db->b.inode != fs->devs[dev].segsum) {
				putdref(fs->scan.youth_db, MKREF(youth_scan));
				fs->scan.youth_db = NULL;
			}
		if (dev == -1) {
			fs->scan.done = 1;
			lafs_wake_thread(fs);
			return MAX_SCHEDULE_TIMEOUT;
		}

		if (fs->scan.youth_db == NULL)
			fs->scan.youth_db =
				lafs_get_block(fs->devs[dev].segsum,
					       youthblock,
					       NULL, GFP_KERNEL, MKREF(youth_scan));
		if (!fs->scan.youth_db) {
			printk("EEEEEKKKKK get_block failed\n");
			return MAX_SCHEDULE_TIMEOUT;
		} else
			switch (err = lafs_read_block_async(fs->scan.youth_db)) {
			default:
			case -EIO:
				printk("EEEEEKKKKK read of youth block failed\n");
				break;
			case -EAGAIN:
				return MAX_SCHEDULE_TIMEOUT;
			case 0:
				break;
			}

		if (fs->scan.do_decay) {
			/* youth_db must be writable */
			struct datablock *db = fs->scan.youth_db;
			lafs_checkpoint_lock(fs);
			set_bit(B_PinPending, &db->b.flags);
			lafs_pin_dblock(db, AccountSpace);
		}
		spin_lock(&fs->lock);
		fs->scan.free_block = block;
		fs->scan.free_dev = dev;
		if (!err && fs->scan.do_decay &&
		    youthblock << (USAGE_SHIFT - YOUTH_SHIFT) == block) {
			u16 *yp = map_dblock(fs->scan.youth_db);
			int i;
			int segperblk = fs->blocksize >> YOUTH_SHIFT;

			for (i = 0 ; i < segperblk ; i++) {
				int y = le16_to_cpu(yp[i]);
				if (y >= 8)
					y = decay_youth(y);
			}
			unmap_dblock(fs->scan.youth_db, yp);
			lafs_dirty_dblock(fs->scan.youth_db);
		}
		spin_unlock(&fs->lock);
		if (fs->scan.do_decay)
			lafs_checkpoint_unlock(fs);
		if (err)
			return 1;
		fs->scan.free_stage = 1;
	}
	lafs_check_seg_cnt(fs->segtrack);
	if (fs->scan.free_stage == 1) {
		/* Find the main usage block and copy the data into
		 * our temp block
		 */
		struct datablock *db;
		char *d;
		u16 *yp, *yp0;

		int i;
		int firstseg;
		int segperblk = fs->blocksize >> USAGE_SHIFT;
		int segments = segperblk;
		int segcount;
		int blks;

		db = lafs_get_block(fs->devs[fs->scan.free_dev].segsum,
				    fs->scan.free_block +
				    fs->devs[fs->scan.free_dev].tablesize,
				    NULL, GFP_KERNEL, MKREF(usage0));
		if (!db) {
			printk("EEEEKKK get_block for first usage failed\n");
		abort:
			fs->scan.free_stage = 0;
			return 1;
		}
		switch (lafs_read_block_async(db)) {
		default:
		case -EIO:
			printk("EEEEKKK read of usage block failed\n");
			putdref(db, MKREF(usage0));
			goto abort;
		case -EAGAIN:
			putdref(db, MKREF(usage0));
			return MAX_SCHEDULE_TIMEOUT;
		case 0:
			break;
		}
		d = map_dblock(db);
		memcpy(fs->scan.free_usages, d, fs->blocksize);
		unmap_dblock(db, d);

		/* Now add any free blocks */
		segcount = fs->devs[fs->scan.free_dev].segment_count;
		blks = segcount / segments;
		if (fs->scan.free_block == blks)
			segments = segcount % segperblk;
		firstseg = fs->scan.free_block * segperblk;

		yp0 = yp = map_dblock(fs->scan.youth_db);
		yp += (fs->scan.free_block -
		       (fs->scan.youth_db->b.fileaddr << (USAGE_SHIFT - YOUTH_SHIFT)))
			* segperblk;
		for (i = 0; i < segments ; i++)
			if (yp[i] == cpu_to_le16(0)) {
				if (fs->scan.first_free_pass) {
					spin_lock(&fs->lock);
					fs->free_blocks +=
						fs->devs[fs->scan.free_dev]
						.segment_size;
					spin_unlock(&fs->lock);
				}
				if (add_free(fs, fs->scan.free_dev, firstseg + i,
					     &yp[i])) {
					/* Everything in the table owns a reference
					 * to youth and segusage[0]
					 */
					(void)getdref(fs->scan.youth_db, MKREF(intable));
					(void)getdref(db, MKREF(intable));
				}
				fs->total_free +=
					fs->devs[fs->scan.free_dev]
					.segment_size /*- 1*/;
			}
		unmap_dblock(fs->scan.youth_db, yp0);

		fs->scan.usage0_db = db;
		fs->scan.free_stage = 2;
	}
	lafs_check_seg_cnt(fs->segtrack);
	while (fs->scan.free_stage > 1 &&
	       fs->scan.free_stage < fs->maxsnapshot + 1) {
		struct datablock *db;
		u32 *d;

		if (fs->ss[fs->scan.free_stage-1].root == NULL) {
			fs->scan.free_stage++;
			continue;
		}
		/* Load the usage block for this snapshot and merge */

		db = lafs_get_block(fs->devs[fs->scan.free_dev].segsum,
				    fs->scan.free_block +
				    fs->devs[fs->scan.free_dev].tablesize *
				    LAFSI(fs->ss[fs->scan.free_stage-1].root)
				    ->md.fs.usagetable,
				    NULL, GFP_KERNEL, MKREF(usage_ss));
		if (!db) {
			printk("EEEEKKK get_block for subsequent usage failed\n");
		abort2:
			fs->scan.free_stage = 0;
			putdref(fs->scan.usage0_db, MKREF(usage0));
			fs->scan.usage0_db = NULL;
			return 1;
		}
		switch (lafs_read_block_async(db)) {
		default:
		case -EIO:
			printk("EEEEKKK read of subsequent usage block failed\n");
			putdref(db, MKREF(usage_ss));
			goto abort2;
		case -EAGAIN:
			putdref(db, MKREF(usage_ss));
			return MAX_SCHEDULE_TIMEOUT;
		case 0:
			break;
		}
		d = map_dblock(db);
		merge_usage(fs, d);
		unmap_dblock(db, d);
		fs->scan.free_stage++;
		putdref(db, MKREF(usage_ss));

		lafs_check_seg_cnt(fs->segtrack);
	}
	if (fs->scan.free_stage == fs->maxsnapshot + 1) {
		/* All usage data has been merged, we can record all these
		 * cleanable segments now
		 */
		u16 *yp = map_dblock(fs->scan.youth_db);
		u16 *yp0 = yp;
		u32 *up = fs->scan.free_usages;
		int i;
		int segperblk = fs->blocksize >> USAGE_SHIFT;
		int segments = segperblk;
		int segcount = fs->devs[fs->scan.free_dev].segment_count;
		int blks = segcount / segments;
		if (fs->scan.free_block == blks)
			segments = segcount % segperblk;

		yp += (fs->scan.free_block -
		       (fs->scan.youth_db->b.fileaddr << (USAGE_SHIFT - YOUTH_SHIFT)))
			* segperblk;
		for (i = 0; i < segments; i++)
			if (add_cleanable(fs, fs->scan.free_dev,
					  i + fs->scan.free_block * segperblk,
					  le16_to_cpu(yp[i]),
					  le16_to_cpu(up[i]))) {
				/* Every segment in table owns these
				 * references
				 */
				(void)getdref(fs->scan.youth_db, MKREF(intable));
				(void)getdref(fs->scan.usage0_db, MKREF(intable));
			}

		unmap_dblock(fs->scan.youth_db, yp0);
		putdref(fs->scan.usage0_db, MKREF(usage0));
		fs->scan.usage0_db = NULL;
		fs->scan.free_stage = 0;
	}
	lafs_check_seg_cnt(fs->segtrack);
	if (fs->scan.trace)
		return HZ/10;
	else if (fs->scan.free_stage == 0)
		return 1;
	else
		return MAX_SCHEDULE_TIMEOUT;
}

void lafs_empty_segment_table(struct fs *fs)
{
	/* remove everything from the table. */
	int ssnum;
	struct segstat *ss;

	ssnum = fs->segtrack->cleanable.first;
	fs->segtrack->cleanable.first = 0xffff;
	fs->segtrack->cleanable.last = 0xffff;
	fs->segtrack->cleanable.cnt = 0;
	while (ssnum != 0xffff) {
		ss = segfollow(fs->segtrack, ssnum);
		ss->score = SCORE_DEAD;
		ssnum = ss->next;
	}

	ssnum = fs->segtrack->clean.first;
	fs->segtrack->clean.first = 0xffff;
	fs->segtrack->clean.last = 0xffff;
	fs->segtrack->clean.cnt = 0;
	while (ssnum != 0xffff) {
		ss = segfollow(fs->segtrack, ssnum);
		ss->score = SCORE_DEAD;
		ssnum = ss->next;
	}

	ssnum = fs->segtrack->free.first;
	fs->segtrack->free.first = 0xffff;
	fs->segtrack->free.last = 0xffff;
	fs->segtrack->free.cnt = 0;
	while (ssnum != 0xffff) {
		ss = segfollow(fs->segtrack, ssnum);
		ss->score = SCORE_DEAD;
		ssnum = ss->next;
	}

	segdelete_all(fs->segtrack, fs);
}

void lafs_dump_usage(void)
{
	if (!dfs)
		return;
	dfs->scan.trace = 1;
	dfs->scan.done = 0;
	lafs_wake_thread(dfs);
}
